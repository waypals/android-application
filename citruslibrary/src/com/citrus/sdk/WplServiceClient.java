package com.citrus.sdk;

import android.os.AsyncTask;

import com.citrus.analytics.EventsManager;
import com.citrus.sdk.response.ServiceResponse;
import com.google.gson.Gson;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by surya on 6/1/16.
 */
public class WplServiceClient extends AsyncTask<TransactionResponse, Void, TransactionResponse> {

    private AsyncResultHandler handler;

    public WplServiceClient(AsyncResultHandler handler)
    {
        this.handler = handler;
    }

    @Override
    protected TransactionResponse doInBackground(TransactionResponse... strings) {

        try {
            //      public static String callService(String apiPath, String input) throws Exception {
            String s = null;

            TransactionResponse response = strings[0];

            if(response != null) {
                HttpURLConnection httpsURLConnection;
                try {
                    String resUrl = Constants.PAYMENT_RESPONSE_URL;
                    if ("".equals(response.getToken())){
                        resUrl = Constants.UPDATE_ITEM_ORDER;
                    }

                    URL url = new URL(resUrl);
                    httpsURLConnection = (HttpURLConnection) url.openConnection();
                    httpsURLConnection.setDoInput(true);
                    httpsURLConnection.setDoOutput(true);
                    httpsURLConnection.setRequestMethod("POST");
                    httpsURLConnection.setRequestProperty("Content-Type", "application/json");
                    httpsURLConnection.setRequestProperty("Accept-encoding", "gzip");

                    if (!"".equals(response.getToken())) {
                        httpsURLConnection.setRequestProperty("security-token", response.getToken());
                    }

                    OutputStream outputStream = new BufferedOutputStream(httpsURLConnection.getOutputStream());
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    //   JSONObject jsonObject = new JSONObject();
                    //   jsonObject.put("imei", input);
                    if (!"".equals(response.getToken())) {
                        int status = 1;
                        if (TransactionResponse.TransactionStatus.SUCCESSFUL.equals(response.getTransactionStatus())) {
                            status = 0;
                        } else if (TransactionResponse.TransactionStatus.CANCELLED.equals(response.getTransactionStatus())) {
                            status = 3;
                        }
                        bufferedWriter.write("{\n" +
                                "\t\"tranRef\": \"" + response.getTransactionDetails().getTransactionId() + "\",\n" +
                                "\n" +
                                "\t\"paymentStatus\": \"" + status + "\",\n" +
                                "\n" +
                                "}");
                    }else{
                        int status = 0;

                        if (TransactionResponse.TransactionStatus.SUCCESSFUL.equals(response.getTransactionStatus())) {
                            status = 1;
                        } else if (TransactionResponse.TransactionStatus.CANCELLED.equals(response.getTransactionStatus())) {
                            status = 2;
                        }else if(TransactionResponse.TransactionStatus.FAILED.equals(response.getTransactionStatus())){
                            status = 3;
                        }

                        bufferedWriter.write("{\n" +
                                "\t\"orderId\": \"" + response.getTransactionDetails().getTransactionId() + "\",\n" +
                                "\n" +
                                "\t\"paymentStatus\": \"" + status + "\",\n" +
                                "\n" +
                                "}");
                    }
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();

                    httpsURLConnection.connect();

                    InputStream inputStream = httpsURLConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                    StringBuilder stringBuilder = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }

                    response.setResponseData(stringBuilder.toString());

                    return response;

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(TransactionResponse s) {
        handler.onResponseReceived(s);
    }
}
