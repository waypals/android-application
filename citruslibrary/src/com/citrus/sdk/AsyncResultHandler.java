package com.citrus.sdk;

/**
 * Created by surya on 6/1/16.
 */
public interface AsyncResultHandler {

    void onResponseReceived(TransactionResponse response);
}
