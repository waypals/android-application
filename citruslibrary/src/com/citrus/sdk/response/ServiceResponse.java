package com.citrus.sdk.response;

import java.io.Serializable;
import java.util.List;

public class ServiceResponse implements Serializable {
    String response;
    String errorCode;
    boolean isValid;
    String[] NotAUser = null;
    String friendRequestStatus = null;

    List<String> vehicleType;
    List<String> manufactuerurs;
    List<String> models;

    String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<String> getManufactuerurs() {
        return manufactuerurs;
    }

    public void setManufactuerurs(List<String> manufactuerurs) {
        this.manufactuerurs = manufactuerurs;
    }

    public List<String> getModels() {
        return models;
    }

    public void setModels(List<String> models) {
        this.models = models;
    }

    public List<String> getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(List<String> vehicleType) {
        this.vehicleType = vehicleType;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }



    public String[] getNotAUser() {
        return NotAUser;
    }

    public void setNotAUser(String[] notAUser) {
        NotAUser = notAUser;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }


}
