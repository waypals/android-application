package com.waypals;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.FriendsMyFriendsAdapter;
import com.waypals.response.FriendInfo;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.List;


public class FriendsMyFriendsFragment extends Fragment implements AsyncFinishInterface {

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            int size = adapter.filter(charSequence.toString());
            setCount(size);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    private EditText input;
    private Activity activity;
    private AsyncFinishInterface asyncFinishInterface;
    private ListView listView;
    private TextView Friend_list_count;
    private Typeface tf;
    private ApplicationPreferences prefs;
    private FriendsMyFriendsAdapter adapter;
    private String method;
    private ServiceResponse serviceResponse;
    private String tag = "MyFriends Fragment";
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (action.equals("DELETE_FRIEND")) {
                String input = intent.getStringExtra("input");

                GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.UNFRIEND, "DELETE_FRIEND", input, tag, asyncFinishInterface, true);
                service.execute();
                Log.d(tag, "delete friend service called");
            }

        }
    };
    private View loading;

    public FriendsMyFriendsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        asyncFinishInterface = this;
        tf = Typeface.createFromAsset(activity.getAssets(), CH_Constant.Font_Typface_Path);
        prefs = new ApplicationPreferences(getActivity());

        IntentFilter filter = new IntentFilter();
        filter.addAction("DELETE_FRIEND");
        LocalBroadcastManager.getInstance(activity).registerReceiver(receiver, filter);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends_my_friends, container, false);
        listView = (ListView) view.findViewById(R.id.listViewFriendsList);

        adapter = new FriendsMyFriendsAdapter(activity);
        loading = getLoadingView();
        listView.addHeaderView(loading);
        listView.setAdapter(adapter);

        input = (EditText) getActivity().findViewById(R.id.nav_title_txt);
        input.addTextChangedListener(watcher);


        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.GET_FRIEND_LIST, "GET_MY_FRIENDS", "", "friends my friend list fragment", asyncFinishInterface, false);
        service.execute();

        view.setTag("FriendsMyFriendsFragment");
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(receiver);
        input.removeTextChangedListener(watcher);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(receiver);
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        Gson gson = new Gson();
        if (method.equals("GET_MY_FRIENDS")) {
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
        if (method.equals("DELETE_FRIEND") && response != null) {
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (loading != null) {
            listView.removeHeaderView(loading);
        }

        if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(getActivity(),
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(getActivity(), Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(getActivity(), getString(R.string.server_error));
            }

            return;
        }

        if (method != null && method.equals("GET_MY_FRIENDS")) {
            if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                List<FriendInfo> friendInfos = serviceResponse.getAcceptedFriendsResult();
                adapter.clear();
                for (FriendInfo f : friendInfos) {
                    adapter.add(f);
                }
                adapter.notifyDataSetChanged();

                setCount(friendInfos.size());
            }
        }
        if (method != null && method.equals("DELETE_FRIEND")) {
            if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                Utility.makeNewToast(activity, "User has been deleted");
                GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.GET_FRIEND_LIST, "GET_MY_FRIENDS", "", "friends my friend list fragment", asyncFinishInterface, false);
                service.execute();
            } else {
                Utility.makeNewToast(activity, "Could not delete user");
            }
        }

    }

    public void setCount(int count) {
        Log.d(tag, "Count : " + count);
        TextView Friend_list_count = (TextView) getView().findViewById(R.id.mytotalfriendscount);
        Friend_list_count.setText(String.valueOf(count));
    }

    private View getLoadingView() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.loading, null);
        return view;
    }
}
