package com.waypals.trip;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.adapters.TripAdapter;
import com.waypals.exception.ExceptionHandler;
import com.waypals.objects.ObdEndOfTrip;
import com.waypals.request.VehicleTripRequest;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.TripComparator;
import com.waypals.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class TripdataActivity extends Activity implements AsyncFinishInterface {

    private Typeface tf;

    private TripAdapter adapter;
    private ListView listView;

    private ApplicationPreferences appPrefs;
    private ServiceResponse serviceResponse;
    private Date startDate, endDate;

    BroadcastReceiver fragmentHelper = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {

                case CH_Constant.TRIP_OVERVIEW:
                    try {

                        String vehicleId = intent.getStringExtra("vehicleId");
                        String startTime = intent.getStringExtra("startTime");
                        String endTime = intent.getStringExtra("endTime");
                        String dis = intent.getStringExtra("distance");
                        String maxS = intent.getStringExtra("maxSpeed");
                        String idleT = intent.getStringExtra("idleTime");
                        int tripScore = intent.getIntExtra("tripScore", 0);

                        Intent tripOverview = new Intent(TripdataActivity.this, TripOverviewActivity.class);
                        tripOverview.putExtra("vehicleId", vehicleId);
                        tripOverview.putExtra("startTime", startTime);
                        tripOverview.putExtra("endTime", endTime);
                        tripOverview.putExtra("distance", dis);
                        tripOverview.putExtra("maxSpeed", maxS);
                        tripOverview.putExtra("idleTime", idleT);
                        tripOverview.putExtra("tripScore", tripScore);
                        startActivity(tripOverview);

                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                    break;
            }
        }
    };
    private CustomDateTimePicker custom, endDateTime;
    private TextView dateFrom;
    private TextView dateTo;
    SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tripdata);

        custom = new CustomDateTimePicker(TripdataActivity.this, new CustomDateTimePicker.ICustomDateTimeListener() {
            @Override
            public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                              String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                              int hour24, int hour12, int min, int sec, String AM_PM) {
                dfDate.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                startDate = dateSelected;
                String sDateTime = dfDate.format(dateSelected);
                dateFrom.setText(sDateTime);
            }

            @Override
            public void onCancel() {

            }
        });
        custom.set24HourFormat(true);

        endDateTime = new CustomDateTimePicker(TripdataActivity.this, new CustomDateTimePicker.ICustomDateTimeListener() {
            @Override
            public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                              String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                              int hour24, int hour12, int min, int sec, String AM_PM) {
                dfDate.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                endDate = dateSelected;
                String sDateTime = dfDate.format(dateSelected);
                dateTo.setText(sDateTime);
            }

            @Override
            public void onCancel() {

            }
        });
        endDateTime.set24HourFormat(true);

        IntentFilter filter = new IntentFilter();
        filter.addAction(CH_Constant.TRIP_OVERVIEW);
        LocalBroadcastManager.getInstance(this).registerReceiver(fragmentHelper, filter);

        appPrefs = new ApplicationPreferences(this);

        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        TextView nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText("TRIPS");

        ImageView back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        adapter = new TripAdapter(this);

        listView = (ListView) findViewById(R.id.trip_list);

        listView.setAdapter(adapter);

        dateFrom = (TextView) findViewById(R.id.from_date);

        dateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(startDate != null){
                    custom.setDate(startDate);
                }else {
                    custom.setDate(Calendar.getInstance());
                }

                custom.showDialog();
            }
        });

//        dateFrom.setOnClickListener(new DateTimeClickListener(dateFrom, this, new DateTimeClickListener.DateSelectedCallback() {
//            @Override
//            public void onSet(Date date) {
//                startDate = date;
//            }
//        }));

        dateTo = (TextView) findViewById(R.id.to_date);

        dateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(endDate != null){
                    endDateTime.setDate(endDate);
                }else {
                    endDateTime.setDate(Calendar.getInstance());
                }

                endDateTime.showDialog();
            }
        });

//        dateTo.setOnClickListener(new DateTimeClickListener(dateTo, this, new DateTimeClickListener.DateSelectedCallback() {
//            @Override
//            public void onSet(Date date) {
//                endDate = date;
//            }
//        }));

        Button btnGetTrip = (Button) findViewById(R.id.btn_get_trip);
        btnGetTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(startDate == null && endDate == null){
                    Utility.makeNewToast(TripdataActivity.this, "Please enter from/to date");
                }else{
                    if(startDate.after(endDate)){
                        Utility.makeNewToast(TripdataActivity.this, "Start date should be prior to end date");
                    }else{
                       makeTripRequest(startDate, endDate);
                    }
                }
            }
        });

        try {
            startDate = Utility.getDate(-1);
            endDate = Utility.getDate(0);

            String set_DateTime = dfDate.format(startDate);
            dateFrom.setText(set_DateTime);

            String endTime = dfDate.format(endDate);
            dateTo.setText(endTime);

            makeTripRequest(startDate, endDate);
        }catch (Exception ex){

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(fragmentHelper != null){
                LocalBroadcastManager.getInstance(this).unregisterReceiver(fragmentHelper);
        }

    }

    private void makeTripRequest(Date startDate, Date endDate){

        try {
            String s = Utility.convertToString(startDate, TimeZone.getTimeZone("UTC"));
            String e = Utility.convertToString(endDate, TimeZone.getTimeZone("UTC"));

            Log.d("Start Time", s);
            Log.d("End Time", e);
            if (!Utility.isStringNullEmpty(s) && !Utility.isStringNullEmpty(e)) {
                getTrips(s, e);
            }
        }catch (Exception ex){

        }
    }

    private void getTrips(String startTime, String endTime)
    {
        Log.d("Start Date", startTime);
        Log.d("End Date", endTime);
        VehicleTripRequest tripRequest = new VehicleTripRequest();
        tripRequest.setVehicleId(""+appPrefs.getVehicle_Selected());

        tripRequest.setStartTime(startTime);
        tripRequest.setEndTime(endTime);

        Gson gson = new Gson();
        String s = gson.toJson(tripRequest, VehicleTripRequest.class);

        GenericAsyncService service = new GenericAsyncService(appPrefs,this, CH_Constant.GET_TRIP_DATA, "GET_TRIPS", s, "", this, true);
        service.execute();
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if("GET_TRIPS".equals(method))
        {
            Log.d("Response", response);
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response,ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if(serviceResponse!=null)
        {
            if("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse()))
            {
                listView.setAdapter(null);
                adapter.clear();
                adapter.notifyDataSetChanged();
                List<ObdEndOfTrip> alert = serviceResponse.getTrips();

                if(alert != null && !alert.isEmpty()) {
                    Collections.sort(alert, new TripComparator());

                    for(ObdEndOfTrip trip: alert){
                        adapter.add(trip);
                    }
                }

                listView.setAdapter(adapter);

                adapter.notifyDataSetChanged();

            }
            else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(TripdataActivity.this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }else if("NO_DATA".equalsIgnoreCase(errorMessage)){
                    adapter.clear();
                    adapter.notifyDataSetChanged();
                    Utility.makeNewToast(TripdataActivity.this, "No trips available for this period");
                }
                else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                }
            }
        }
    }
}
