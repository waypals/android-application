package com.waypals.trip;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.RouteCoordinates;
import com.waypals.objects.VehicleTripOverview;
import com.waypals.request.VehicleTripRequest;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.RouteComparator;
import com.waypals.utils.Utility;

import java.util.Collections;
import java.util.List;

public class TripOverviewActivity extends FragmentActivity implements OnMapReadyCallback, AsyncFinishInterface {

    private GoogleMap map;
    private ApplicationPreferences preferences;
    private ServiceResponse serviceResponse;
    private Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_overview);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TextView title = (TextView) findViewById(R.id.nav_title_txt);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        title.setTypeface(tf);
        title.setText("TRIP OVERVIEW");
        ImageView back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        preferences = new ApplicationPreferences(this);

        Intent thisIntent = getIntent();
        Bundle extras = thisIntent.getExtras();

        String vehicleId = "";
        String startTime = "";
        String endTime = "";
        String dis = "";
        String maxS = "";
        String idleT = "";

        if(extras != null) {
            vehicleId = extras.getString("vehicleId");
            startTime = extras.getString("startTime");
            endTime = extras.getString("endTime");
            dis = extras.getString("distance");
            maxS = extras.getString("maxSpeed");
            idleT = extras.getString("idleTime");
        }
        int score = extras.getInt("tripScore");

        TextView startTimeText = (TextView) findViewById(R.id.start_time);
        TextView endTimeText = (TextView) findViewById(R.id.end_time);

        TextView distance = (TextView) findViewById(R.id.distance_text);
        TextView maxSpeed = (TextView) findViewById(R.id.max_speed);
        TextView idleTime = (TextView) findViewById(R.id.idle_time);
        TextView tripScore = (TextView) findViewById(R.id.trip_score);

        try {

            tripScore.setText(""+score+"/100");

            if (!Utility.isStringNullEmpty(startTime)) {
                startTimeText.setText(Utility.parseDateForTrip(startTime));
            }

            if (!Utility.isStringNullEmpty(endTime)) {
                endTimeText.setText(Utility.parseDateForTrip(endTime));
            }

            if (!Utility.isStringNullEmpty(dis)) {
                distance.setText(dis);
            }

            if (!Utility.isStringNullEmpty(maxS)) {
                maxSpeed.setText(maxS);
            }

            if (!Utility.isStringNullEmpty(idleT)) {
                idleTime.setText(idleT);
            }

            getTripDetails(startTime, endTime, vehicleId);
        }catch (Exception ex){
            ExceptionHandler.handleException(ex);
        }
    }

    private void getTripDetails(String startTime, String endTime, String vehicleId) {

        if (!Utility.isStringNullEmpty(vehicleId) &&
                !Utility.isStringNullEmpty(startTime) && !Utility.isStringNullEmpty(endTime)) {
            VehicleTripRequest request = new VehicleTripRequest();
            request.setStartTime(startTime);
            request.setEndTime(endTime);
            request.setVehicleId(vehicleId);

            Gson gson = new Gson();
            String s = gson.toJson(request, VehicleTripRequest.class);

            GenericAsyncService service = new GenericAsyncService(preferences, this, CH_Constant.GET_TRIP_OVERVIEW, "GET_TRIP_OVERVIEW", s, "", this, true);
            service.execute();
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    private void updateCamera(RouteCoordinates[] routeCoordinates) {
        if (map != null && routeCoordinates != null && routeCoordinates.length > 1) {

            final List<LatLng> lstCoord = Utility.getLatLngFromCoordinates(routeCoordinates);
            Polyline line = map.addPolyline(new PolylineOptions()
                    .addAll(lstCoord)
                    .width(5)
                    .color(Color.BLUE));
            final LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng lt : lstCoord) {
                builder.include(lt);
            }

            addMarkers(map, lstCoord.get(0), lstCoord.get(lstCoord.size() - 1));

            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                }
            });
        }
    }

    private void addMarkers(GoogleMap googleMap, LatLng start, LatLng end) {
        MarkerOptions markerOptionsStart = new MarkerOptions();
        markerOptionsStart.position(start);
        markerOptionsStart.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_circle_pointer));

        MarkerOptions markerOptionsEnd = new MarkerOptions();
        markerOptionsEnd.position(end);
        markerOptionsEnd.icon(BitmapDescriptorFactory.fromResource(R.drawable.grey_circle_pointer));

        googleMap.addMarker(markerOptionsStart);
        googleMap.addMarker(markerOptionsEnd);
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if ("GET_TRIP_OVERVIEW".equals(method)) {
            Log.d("Response", response);
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if (serviceResponse != null) {
            if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                List<VehicleTripOverview> alert = serviceResponse.getTripOverviews();

                if (alert != null && !alert.isEmpty()) {
                    Collections.sort(alert, new RouteComparator());
                    RouteCoordinates[] coordinates = new RouteCoordinates[alert.size()];
                    int i = 0;
                    for (VehicleTripOverview l : alert) {
                        coordinates[i++] = l.getLocation();
                    }
                    updateCamera(coordinates);
                }
            } else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(TripOverviewActivity.this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                } else if ("NO_DATA".equalsIgnoreCase(errorMessage)) {
                    Utility.makeNewToast(TripOverviewActivity.this, "No trip data available for this period");
                } else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                }
            }
        }
    }
}
