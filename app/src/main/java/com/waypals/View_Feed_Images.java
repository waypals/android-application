package com.waypals;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.android.volley.toolbox.ImageLoader;
import com.waypals.feedImpl.AppController;
import com.waypals.feedImpl.FeedImageView;
import com.waypals.feedItems.Images;
import com.wareninja.opensource.common.ObjectSerializer;

import java.util.HashMap;


public class View_Feed_Images extends Activity {
    private final GestureDetector detector = new GestureDetector(new SwipeGestureDetector());
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private Context mContext;
    private ViewFlipper viewFlipper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        mContext = this;
        HashMap<String, LinearLayout> imgInfo = new HashMap<String, LinearLayout>();

        setContentView(R.layout.activity_view__feed__images);
         viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        String img = getIntent().getStringExtra("images");
        String selected = getIntent().getStringExtra("selected");

        Log.d("feed images", selected);
        Images[] images = (Images[]) ObjectSerializer.deserialize(img);

        // Set up the user interaction to manually show or hide the system UI.

        if (images != null) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            int count = R.id.blankLinearLayout;
            for (Images i : images) {
                LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.blank_linear_layout, null, false);
                layout.setId(count);
                FeedImageView f = new FeedImageView(this);
              //  f.setLayoutParams(params);
                f.setImageUrl(i.getImageLink(), imageLoader);
                layout.addView(f);
                viewFlipper.addView(layout);
                imgInfo.put(i.getImageLink().trim(), layout);
                count++;
                Log.d("feed images", "added " + i.getImageName() + "|ids: " + layout.getId());
            }
        }

        Log.d("feed images", "selected id :" + String.valueOf(imgInfo.get(selected.trim()).getId()));

        viewFlipper.setDisplayedChild(imgInfo.get(selected).getId());
        viewFlipper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewFlipper.showNext();
            }
        });

        viewFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
               boolean status = detector.onTouchEvent(event);
//                if(status == false){
//
//                        viewFlipper.showNext();
//
//
//                }
                return true;
            }
        });
    }

        class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                try {
                    // right to left swipe
                    if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                        viewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_in));
                        viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_out));
                        viewFlipper.showNext();
                        return true;
                    } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                        viewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_in));
                        viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext,R.anim.right_out));
                        viewFlipper.showPrevious();
                        return true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return false;
            }
        }

    }



