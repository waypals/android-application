package com.waypals;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.services.FetchAddressIntentService;
import com.waypals.services.LocationService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.Collection;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class Map_Screen extends FragmentActivity implements
        OnMarkerClickListener, AsyncFinishInterface
/*
 * IEnableParkingFence, IDisableParkingFence
 */ {
    public static GoogleMap map = null;
    public static Handler refresh_handler = new Handler();
    static String tag = "Map Screen";
    static DrawerLayout myDrawer;
    static Marker myPosition;
    static LatLng MY_Position;
    static double x;
    static double y;
    static Context context;
    static Long carSelected;
    static Marker selCar_mrker;
    static int totalCars = 0;
    static Collection<VehicleInformationVO> vehicleInfoVO;
    static TextView map_app_title;
    static AsyncFinishInterface asynFinishIterface;
    static VehicleServices vs = new VehicleServices();
    static ImageView maplayer_img;
    static ImageView maploc_img;
    static String pinLoc = "none";

    LocationService appLocationService;

    int counter = 0;

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            // timer.cancel();
            x = location.getLatitude();
            y = location.getLongitude();

            if (myPosition != null) {
                myPosition.remove();
            }

            appLocationService = new LocationService(
                    Map_Screen.this);

            pinLoc = "gps";
            MY_Position = new LatLng(x, y);
            myPosition = map.addMarker(new MarkerOptions()
                    .position(MY_Position)
                    .title("Me")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.blue_person)));
            updateMapZoom();
            // System.out.println("GPS ::" + x + y);

        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            // timer.cancel();
            x = location.getLatitude();
            y = location.getLongitude();
            // locationManager.removeUpdates(this);
            // locationManager.removeUpdates(locationListenerGps);

            if (myPosition != null) {
                myPosition.remove();
            }

            pinLoc = "net";
            MY_Position = new LatLng(x, y);
            myPosition = map.addMarker(new MarkerOptions()
                    .position(MY_Position)
                    .title("Me")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.blue_person)));

            // System.out.println("Network ::" + x + y);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationManager locationManager;
    // static CheckBox checkButton;
    // static TextView parkfencetext;
    boolean gps_enabled;
    // ImageView mapbtnSlide_right;
    boolean network_enabled;
    int maplayer = 1;
    boolean me_position = false;
    Timer timer;
    LatLng selectedCar_latlng;
    String[] vehicaleList;
    boolean[] isParkFenceOn;
    ImageView backbtn;
    ListView leftDrawerList;
    ListView rightDrawerList;
    ApplicationPreferences appPref;
    String location = "";
    Typeface tf;
    Handler handler = new Handler();
    NetworkInfo currentNetworkInfo;
    VehicleServiceResponse vehicleServiceResponse;

    private boolean isSetCentered = false;

    private AddressResultReceiver mResultReceiver;
    private String mAddressOutput;

    // ...

    protected void startIntentService(int id, double lat, double lon, String state, String time) {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra("receiver", mResultReceiver);
        intent.putExtra("id", id);
        intent.putExtra("lat", lat);
        intent.putExtra("lon", lon);
        intent.putExtra("state", state);
        intent.putExtra("time", time);
        startService(intent);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            String address = resultData.getString("address");
            int id = resultData.getInt("id");
            String state = resultData.getString("state");
            String time = resultData.getString("time");
            //displayAddressOutput();

            String newSnippet = "";
            if (selCar_mrker != null ) {
                //VehicleInformationVO[] lstVehicles = new VehicleInformationVO[carMarker.size()];
                //vehicleInfoVO.toArray(lstVehicles);
                //String title = lstVehicles[id].getRegistrationNumber();
                newSnippet = newSnippet + "\n" +
                        state + "\n" +
                        time;
                newSnippet = newSnippet + "\n" + address;

                //selCar_mrker.setTitle(title);
                selCar_mrker.setSnippet(newSnippet);
            }
            // carMarker.get(id).showInfoWindow();
            // carMarker.get(id).setVisible(true);
        }
    }

    private BroadcastReceiver Network_Receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent
                    .getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_IS_FAILOVER, false);

            currentNetworkInfo = (NetworkInfo) intent
                    .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent
                    .getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);

            if (currentNetworkInfo.isConnected()) {

                if (maploc_img != null)
                    maploc_img.setImageResource(R.drawable.man_icon);
                me_position = false;

                if (CH_Constant.isNetworkAvailable(Map_Screen.this)) {

					/* setup contents */
                    try {
                        Log.i(tag, "Load vehicle service data");
                        // /vs.getVehiclesData(appPref, asynFinishIterface);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            } else {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No Network Connected", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 80);
                toast.show();
            }
        }
    };
    private ImageView rightBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        setContentView(R.layout.location_screen);
        // map_prkfence_layout
        // findViewById(R.id.map_prkfence_layout).setVisibility(View.GONE);
        mResultReceiver = new AddressResultReceiver(new Handler());
        context = Map_Screen.this;
        appPref = new ApplicationPreferences(context);
        asynFinishIterface = Map_Screen.this;

        backbtn = (ImageView) findViewById(R.id.back);
        backbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });

        rightBtn = (ImageView) findViewById(R.id.map_centered_on_off);


        rightBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isSetCentered = !isSetCentered;

                if (isSetCentered) {
                    rightBtn.setImageResource(R.drawable.map_center_active_icon);
                } else {
                    rightBtn.setImageResource(R.drawable.map_center_deactive_icon);
                }
            }
        });

        FragmentManager fmanager = getSupportFragmentManager();
        Fragment fragment = fmanager.findFragmentById(R.id.mapFrag);
        SupportMapFragment supportmapfragment = (SupportMapFragment) fragment;
        supportmapfragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Map_Screen.this.map = googleMap;
                if (map != null) {
                    map.setMapType(maplayer);
                    map.getUiSettings().setCompassEnabled(true);
                    map.setOnMarkerClickListener(Map_Screen.this);

                    /* setup contents */

                    Log.i(tag, "Load vehicle service data");
                    try {
                        vs.getVehiclesData(appPref, Map_Screen.this, true);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {

                            LinearLayout info = new LinearLayout(Map_Screen.this);
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(Map_Screen.this);
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());

                            TextView snippet = new TextView(Map_Screen.this);
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());

                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });

                    if (ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    map.setMyLocationEnabled(false);
                }
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void finish(String response, String method) {

        Log.i(tag, "method RES: " + method);
        Gson gson = new Gson();
        if (method != null && method.contains("vehiclesdata")) {
            Log.i(tag, "getVehiclesData RES: " + response);
            vehicleServiceResponse = gson.fromJson(
                    response, VehicleServiceResponse.class);

            if (vehicleServiceResponse != null && "SUCCESS".equalsIgnoreCase(vehicleServiceResponse.getResponse())) {
                vehicleInfoVO = vehicleServiceResponse.getVehicles();

                int totalVehicle = vehicleInfoVO.size();
                vehicaleList = new String[totalVehicle];
                isParkFenceOn = new boolean[totalVehicle];
                totalCars = totalVehicle;
                Log.i(tag, "Data RES: total vehicale " + totalVehicle);

                Iterator<VehicleInformationVO> v1 = vehicleInfoVO.iterator();
                int count = 0;
                while (v1.hasNext()) {
                    VehicleInformationVO v2 = v1.next();
                    Log.d(tag, "Data RES: REG " + v2.getRegistrationNumber());
                    Log.d(tag, "Data RES: REG " + v2.getState());
                    Log.d(tag, "Data RES: REG " + v2.getVehicleId());
                    Log.d(tag, "Data RES: REG " + v2.isSystemFenceEnabled());
                    vehicaleList[count] = v2.getRegistrationNumber();
                    isParkFenceOn[count] = v2.isSystemFenceEnabled();
                    count++;
                }
            }
        }

    }

    @Override
    public void onPostExecute(Void Result) {

        if (vehicaleList != null) {

            map_app_title = (TextView) findViewById(R.id.map_main_app_title_txt);
            map_app_title.setText(appPref.getSelectedRegistrationNumber());
            map_app_title.setTypeface(tf);

        }
        setUpMapIfNeeded();
    }

    public void onPostExecute() {

        if (vehicaleList != null) {

            map_app_title = (TextView) findViewById(R.id.map_main_app_title_txt);
            map_app_title.setText(appPref.getSelectedRegistrationNumber());
            map_app_title.setTypeface(tf);

        }
        setUpMapIfNeededOnRefresh();
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void setUpMapIfNeededOnRefresh() {
        if (map == null) {
            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mapFrag)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    Map_Screen.this.map = googleMap;

                    updateMapAfterRefresh();
                }
            });

        }else{
            updateMapAfterRefresh();
        }
    }

    private void updateMapAfterRefresh(){
        if (map != null) {
            map.setMapType(maplayer);
            map.getUiSettings().setCompassEnabled(true);
            map.setOnMarkerClickListener(Map_Screen.this);
            if (isGoogleMapsInstalled()) {
                if (map != null) {
                    // Getting LocationManager object from System Service
                    // LOCATION_SERVICE
                    locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                    gps_enabled = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);
                    network_enabled = locationManager
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (gps_enabled)
                        if (ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            return;
                        }
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 0, 0,
                            locationListenerGps);
                    if (network_enabled)
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER, 0, 0,
                                locationListenerNetwork);

                    if (timer == null) {
                        timer = new Timer();
                        timer.schedule(new GetLastLocation(), 20000, 20000);
                    }

                    Iterator<VehicleInformationVO> vehicleInfoItr = vehicleInfoVO
                            .iterator();
                    if (vehicleInfoItr != null) {
//                        if (selCar_mrker != null) {
//                            selCar_mrker.remove();
//                        }
                        int i = 0;
                        while (vehicleInfoItr.hasNext()) {
                            VehicleInformationVO vehicleInfo = vehicleInfoItr
                                    .next();
                            if (vehicleInfo != null && vehicleInfo.getLocation() != null) {
                                String location = vehicleInfo.getLocation().toString();
                                Log.i(tag, location);
                                LatLng latlng = new LatLng(Double.parseDouble(location
                                        .split("/")[0]), Double.parseDouble(location
                                        .split("/")[1]));

                                if (appPref.getVehicle_Selected() == vehicleInfo
                                        .getVehicleId()) {
                                    int res_icon = CH_Constant.getCarCurrentIcon(
                                            vehicleInfo.getState(),
                                            vehicleInfo.isSystemFenceEnabled, vehicleInfo.getVehicleType());

                                    if (selCar_mrker != null) {
                                        selCar_mrker.remove();
                                    }

                                    selCar_mrker = map.addMarker(new MarkerOptions()
                                            .position(latlng).icon(
                                                    BitmapDescriptorFactory
                                                            .fromResource(res_icon)));

                                    updateMarker(latlng.latitude, latlng.longitude, vehicleInfo);
                                    selectedCar_latlng = latlng;
                                    appPref.saveSelectedCarLatLng(location);
                                }
                            }
                            i++;
                        }

                        float zoom = 11;

                        maplayer_img = (ImageView) Map_Screen.this
                                .findViewById(R.id.maplayer_img);
                        maploc_img = (ImageView) Map_Screen.this
                                .findViewById(R.id.maploc_img);
                    }

                }

            }

            if (ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            map.setMyLocationEnabled(false);
        }
    }

    private void updateMapZoom() {
        if ((isSetCentered || counter++ < 1)) {

            if (selectedCar_latlng != null) {
                //  zoom = map.getCameraPosition().zoom;
                //  CameraPosition cameraPosition = new CameraPosition.Builder()
                //          .target(selectedCar_latlng)
                //         .zoom(zoom).build();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(selectedCar_latlng);

                if (MY_Position != null) {
                    //   builder.include(MY_Position);
                }
                if (map != null) {
//                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                    //map.moveCamera(CameraUpdateFactory.zoomBy(11));
                    if(MY_Position != null) {
                       // map.moveCamera(CameraUpdateFactory.newLatLngZoom(MY_Position, 5));
                    }
                }
                //     map.moveCamera(CameraUpdateFactory
                //           .newCameraPosition(cameraPosition));
                //  map.animateCamera(
                //        CameraUpdateFactory.zoomTo(zoom),
                //      1000, null);
            } else if (MY_Position != null) {
                //   zoom = map.getCameraPosition().zoom;
                // CameraPosition cameraPosition = new CameraPosition.Builder()
                //        .target(MY_Position)
                //      .zoom(zoom).build();

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(MY_Position);
                if (map != null && MY_Position != null) {

                   // map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                   // map.moveCamera(CameraUpdateFactory.newLatLngZoom(MY_Position, 5));
                    //map.moveCamera(CameraUpdateFactory.zoomBy(11));
                }
                //   map.moveCamera(CameraUpdateFactory
                //          .newCameraPosition(cameraPosition));
                //  map.animateCamera(
                //         CameraUpdateFactory.zoomTo(zoom),
                //          1000, null);
            }
        }
    }

    private void updateMarker(double latitude, double longitude, VehicleInformationVO vehicleInfo) {

        selCar_mrker.setTitle(vehicleInfo.getRegistrationNumber());

       // String snippet = vehicleInfo.getState() + "\n" + Utility.parseServerDate(vehicleInfo.getDateTime());
        //selCar_mrker.setSnippet(snippet);
        //selCar_mrker.setVisible(true);

        startIntentService(0, latitude, longitude,
                vehicleInfo.getState(), Utility.parseServerDate(vehicleInfo.getDateTime()));

       // LocationAddress locationAddress = new LocationAddress();
       // locationAddress.getAddressFromLocation(0, latitude, longitude,
         //       getApplicationContext(), new GeocoderHandler());

        if (isSetCentered || counter++ < 1) {
            map.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(new LatLng(latitude, longitude), 14));
            // map.animateCamera(CameraUpdateFactory.zoomTo(15));
            map.animateCamera(
                    CameraUpdateFactory.zoomTo(14),
                    1000, null);
        }


    }

    private void setUpMapIfNeeded() {
        if (map == null) {
            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mapFrag)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    Map_Screen.this.map = googleMap;
                    updateMapAfterSetUp();
              }
          });

        }else{
            updateMapAfterSetUp();
        }
    }

    private void updateMapAfterSetUp(){
        if (map != null) {
            map.setMapType(maplayer);
            map.getUiSettings().setCompassEnabled(true);
            map.setOnMarkerClickListener(Map_Screen.this);
            if (isGoogleMapsInstalled()) {
                if (map != null) {
                    // Getting LocationManager object from System Service
                    // LOCATION_SERVICE
                    locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                    gps_enabled = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);
                    network_enabled = locationManager
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (gps_enabled)
                        if (ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 0, 0,
                            locationListenerGps);
                    if (network_enabled)
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER, 0, 0,
                                locationListenerNetwork);

                    if (timer == null) {
                        timer = new Timer();
                        timer.schedule(new GetLastLocation(), 20000, 20000);
                    }

                    if (vehicleInfoVO != null) {
                        Iterator<VehicleInformationVO> vehicleInfoItr = vehicleInfoVO
                                .iterator();
                        if (vehicleInfoItr != null) {
//                            if (selCar_mrker != null) {
//                                selCar_mrker.remove();
//                            }
                            int i = 0;
                            while (vehicleInfoItr.hasNext()) {
                                VehicleInformationVO vehicleInfo = vehicleInfoItr
                                        .next();
                                if (vehicleInfo.getLocation() != null) {
                                    String location = vehicleInfo.getLocation()
                                            .toString();
                                    Log.i(tag, location);
                                    LatLng latlng = new LatLng(
                                            Double.parseDouble(location.split("/")[0]),
                                            Double.parseDouble(location.split("/")[1]));

                                    if (appPref.getVehicle_Selected() == vehicleInfo
                                            .getVehicleId()) {
                                        int res_icon = CH_Constant.getCarCurrentIcon(
                                                vehicleInfo.getState(),
                                                vehicleInfo.isSystemFenceEnabled, vehicleInfo.getVehicleType());
                                        if (selCar_mrker != null) {
                                            selCar_mrker.remove();
                                        }
                                        selCar_mrker = map
                                                .addMarker(new MarkerOptions()
                                                        .position(latlng)
                                                        .icon(BitmapDescriptorFactory
                                                                .fromResource(res_icon)));
                                        selCar_mrker.setVisible(true);
                                        updateMarker(latlng.latitude, latlng.longitude, vehicleInfo);
                                        selectedCar_latlng = latlng;
                                        appPref.saveSelectedCarLatLng(location);
                                    }
                                }
                                i++;
                            }
                            float zoom = 12;

                            updateMapZoom();

                            maplayer_img = (ImageView) Map_Screen.this
                                    .findViewById(R.id.maplayer_img);
                            maploc_img = (ImageView) Map_Screen.this
                                    .findViewById(R.id.maploc_img);
                            maplayer_img.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    maplayer++;
                                    map.setMapType(maplayer);

                                    if (maplayer == 4) {
                                        maplayer = 0;
                                    }

                                }
                            });

                            maploc_img.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    int status = GooglePlayServicesUtil
                                            .isGooglePlayServicesAvailable(getBaseContext());
                                    if (status != ConnectionResult.SUCCESS) {
                                        // Google Play Services are not available
                                        int requestCode = 10;
                                        Dialog dialog = GooglePlayServicesUtil
                                                .getErrorDialog(status,
                                                        Map_Screen.this, requestCode);
                                        dialog.show();
                                    } else {

                                        if (!me_position) {
                                            maploc_img
                                                    .setImageResource(R.drawable.car_icon);
                                            me_position = true;

                                            LatLng latLng = new LatLng(x, y);

                                            LatLng MY_Position = latLng;

                                            if (myPosition != null) {
                                                myPosition.remove();
                                            }

                                            myPosition = map
                                                    .addMarker(new MarkerOptions()
                                                            .position(MY_Position)
                                                            .title("Me")
                                                            .icon(BitmapDescriptorFactory
                                                                    .fromResource(R.drawable.blue_person)));

                                            map.moveCamera(CameraUpdateFactory
                                                    .newLatLngZoom(latLng, 5));
                                            // map.animateCamera(CameraUpdateFactory.zoomTo(15));
                                            map.animateCamera(
                                                    CameraUpdateFactory.zoomTo(5),
                                                    1000, null);

                                            // CameraPosition cameraPosition = new
                                            // CameraPosition.Builder()
                                            // .target(MY_Position) // Sets the center
                                            // of the map to Mountain View
                                            // .zoom(17) // Sets the zoom
                                            // .bearing(90) // Sets the orientation of
                                            // the camera to east
                                            // .tilt(90) // Sets the tilt of the camera
                                            // to 30 degrees
                                            // .build(); // Creates a CameraPosition
                                            // from the builder
                                            // map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                                        } else {
                                            try {
                                                if (selectedCar_latlng != null) {
                                                    maploc_img
                                                            .setImageResource(R.drawable.man_icon);
                                                    me_position = false;
                                                    map.moveCamera(CameraUpdateFactory
                                                            .newLatLngZoom(
                                                                    selectedCar_latlng,
                                                                    5));
                                                    map.animateCamera(
                                                            CameraUpdateFactory
                                                                    .zoomTo(5), 1000,
                                                            null);
                                                    if (myPosition != null) {
                                                        myPosition.remove();
                                                    }
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    }

                                }
                            });

                        }
                    }

                }

                if (ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                map.setMyLocationEnabled(false);
            }
        }
    }

    public boolean isGoogleMapsInstalled() {
        try {
            Log.i(tag, "check Google Map installed");
            ApplicationInfo info;
            info = getPackageManager().getApplicationInfo(
                    "com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;

        }
    }

    @Override
    protected void onDestroy() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {

        if (timer == null) {
            timer = new Timer();
            timer.schedule(new GetLastLocation(), 20000, 20000);
        }
        super.onResume();

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }

            if (locationAddress != null) {

                String newSnippet = "";
                if (selCar_mrker.getSnippet() != null) {
                    newSnippet = selCar_mrker.getSnippet() + "\n" + locationAddress;
                }else{
                    newSnippet = locationAddress;
                }
                selCar_mrker.setSnippet(newSnippet);


            }
            //tvAddress.setText(locationAddress);
        }
    }

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
            // locationManager.removeUpdates(locationListenerGps);
            // locationManager.removeUpdates(locationListenerNetwork);

            Location net_loc = null, gps_loc = null;
            if (gps_enabled)
                if (ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Map_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }

            if(locationManager == null){
                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            }
            gps_loc = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (network_enabled)
                net_loc = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            // if there are both values use the latest one
            if (gps_loc != null && net_loc != null) {
                if (gps_loc.getTime() > net_loc.getTime()) {
                    x = gps_loc.getLatitude();
                    y = gps_loc.getLongitude();

                    MY_Position = new LatLng(x, y);
                } else {
                    x = net_loc.getLatitude();
                    y = net_loc.getLongitude();
                    MY_Position = new LatLng(x, y);
                }

            }

            if (gps_loc != null) {
                {
                    x = gps_loc.getLatitude();
                    y = gps_loc.getLongitude();
                    MY_Position = new LatLng(x, y);
                }

            }
            if (net_loc != null) {
                {
                    x = net_loc.getLatitude();
                    y = net_loc.getLongitude();
                    MY_Position = new LatLng(x, y);
                }
            }
            try {
                VehicleServices.getVehicleData(appPref, context, asynFinishIterface, "vehiclesdata");
            } catch (Exception e) {
                ExceptionHandler.handleException(e);
            }

        }

    }
}
