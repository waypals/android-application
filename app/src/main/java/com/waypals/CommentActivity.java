package com.waypals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Comments;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.wareninja.opensource.common.ObjectSerializer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CommentActivity extends Activity implements AsyncFinishInterface {

    AsyncFinishInterface asyncFinishInterface;
    ServiceResponse serviceResponse;
    Activity activity;
    String method;
    private TextView CommentActivity_title_txt;
    private Typeface tf;
    private ArrayList<Comments> commentDetailsList;
    private ListView commentList;
    private Long feedID;
    private EditText et_MyComment;
    private ImageView sendBtn;
    private ApplicationPreferences appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_comment);
        asyncFinishInterface = this;
        activity = this;
        et_MyComment = (EditText) findViewById(R.id.et_comment);
        sendBtn = (ImageView) findViewById(R.id.btn_send);

        appPrefs = new ApplicationPreferences(this);

        String comments = getIntent().getStringExtra("comments");
        Log.d("commment activyt", "comment found : " + comments);

        CommentActivity_title_txt = (TextView) findViewById(R.id.comment_title_text);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);

        String from = getIntent().getStringExtra("from");
        boolean fromMsg = (from != null && from.equals("mymessage")) ? true : false;
        CommentActivity_title_txt.setTypeface(tf);

        if (fromMsg) {
            CommentActivity_title_txt.setVisibility(View.INVISIBLE);
            CommentActivity_title_txt.getLayoutParams().height = 0;
        }


        if (comments != null && comments.trim().length() > 0 && !comments.endsWith("null")) {
            commentDetailsList = (ArrayList<Comments>) ObjectSerializer.deserialize(comments);
            Log.d("Comment activity", "comment list size " + commentDetailsList);

            if (fromMsg) {
                CommentActivity_title_txt.setText("Reply(" + commentDetailsList.size() + ")");
            } else {
                CommentActivity_title_txt.setText("Comments(" + commentDetailsList.size() + ")");
            }
        } else {
            commentDetailsList = new ArrayList<Comments>();

            if (fromMsg) {
                CommentActivity_title_txt.setText("Reply");
            } else {
                CommentActivity_title_txt.setText("Comments");
            }

        }

        feedID = getIntent().getLongExtra("feedId", -1);


        commentList = (ListView) findViewById(R.id.comment_list);



        ContentAdapter adapter = new ContentAdapter(CommentActivity.this, R.layout.comment_details, commentDetailsList);

        int UNBOUNDED = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        if(commentDetailsList.size() > 3){
            View item = adapter.getView(0, null, commentList);
            item.measure(UNBOUNDED, UNBOUNDED);
            ViewGroup.LayoutParams params = commentList.getLayoutParams();
         //   params.width = ViewGroup.LayoutParams.MATCH_PARENT;
         //   params.height = 3 * item.getMeasuredHeight() + (commentList.getDividerHeight() * (adapter.getCount() - 1));
            params.height = 240;
            commentList.setLayoutParams(params);
            commentList.requestLayout();
        }

        commentList.setAdapter(adapter);

        Log.d("After2:", "" + commentList.getLayoutParams().height);


        sendBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String myComment = et_MyComment.getText().toString();
                myComment = myComment != null ? myComment.trim() : myComment;
                if (myComment != null && !myComment.isEmpty()) {
                    try {
                        JSONObject myCommentJson = new JSONObject();
                        myCommentJson.put("feedId", feedID);
                        myCommentJson.put("comment", myComment);

                        GenericAsyncService service = new GenericAsyncService(appPrefs, activity, CH_Constant.COMMENT_ON_FEED, "COMMENT", myCommentJson.toString(), "Comment Activity", asyncFinishInterface, true);
                        service.execute();

                    } catch (Exception e) {
                        ExceptionHandler.handleException(e, CommentActivity.this);
                    }
                } else {
                    try {
                        Utility.makeNewToast(activity, getString(R.string.inp_alert_comment));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                }

            }
        });


    }

    @Override
    public void finish(String response, String method) throws Exception {

        try {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
            this.method = method;
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }

    }

    @Override
    public void onPostExecute(Void Result) {

        try {

            if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                }

                return;
            }

            if (serviceResponse != null && "SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                Utility.makeNewToast(this, getString(R.string.op_alert_comment_post));
                finish();
            } else {
                Utility.makeNewToast(this, getString(R.string.err_try_again));
                finish();
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, activity);
        }
    }


    class ContentAdapter extends ArrayAdapter<Comments> {

        Context context;
        int layoutResourceId;
        ArrayList<Comments> commentDetailsList;
        ContentsHolder holder;


        public ContentAdapter(Context context, int resource, List<Comments> commentDetails) {
            super(context, resource, commentDetails);
            this.layoutResourceId = resource;
            this.commentDetailsList = (ArrayList<Comments>) commentDetails;
            this.context = context;
        }


        @Override
        public boolean isEnabled(int position) {
            return false;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                holder = new ContentsHolder();

                LayoutInflater inflater = ((CommentActivity) context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder.name = (TextView) row.findViewById(R.id.tv_name);
                holder.date = (TextView) row.findViewById(R.id.tv_time_stamp);
                holder.comment = (TextView) row.findViewById(R.id.tv_comment);

                holder.name.setTypeface(tf);
                holder.date.setTypeface(tf);
                holder.comment.setTypeface(tf);

                row.setTag(holder);
            }
            holder = (ContentsHolder) row.getTag();

            Long time = Utility.getDateToLong(commentDetailsList.get(position).getDateTime());
            Log.d("comment ac", "time in long" + time);
            if (time != null) {
                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
                holder.date.setText(timeAgo);
                Log.d("comment ac", "time " + timeAgo);
            } else {
                holder.date.setText(commentDetailsList.get(position).getDateTime());
            }

            holder.name.setText(commentDetailsList.get(position).getCommentedBy().getName().getFullName());
            holder.comment.setText(commentDetailsList.get(position).getComment());

            return row;
        }


    }

    class ContentsHolder {
        TextView name;
        TextView date;
        TextView comment;
    }


}
