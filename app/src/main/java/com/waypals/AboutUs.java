package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.utils.CH_Constant;

public class AboutUs extends Activity {

    private ImageView back_btn;
    private TextView nav_title_txt;
    private Typeface tf;

    public static final String ABOUT_US_TITLE = "title";
    public static final String ABOUT_US_TEXT = "terms_conditions";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        Intent intent = getIntent();

        String titleText = "";
        String contentText = "";

        if(intent != null)
        {
            titleText = intent.getStringExtra(ABOUT_US_TITLE);
            contentText = intent.getStringExtra(ABOUT_US_TEXT);
        }

        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText(titleText);


        WebView browser = (WebView) findViewById(R.id.webview);
        browser.loadUrl(contentText);
       // TextView content = (TextView) findViewById(R.id.emergency_list);
       // content.setText(contentText);
    }
}
