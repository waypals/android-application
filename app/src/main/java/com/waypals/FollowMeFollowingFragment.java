package com.waypals;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.FollowMeViewAdpater;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Name;
import com.waypals.response.FollowmeUserWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONObject;

import java.util.List;

public class FollowMeFollowingFragment extends Fragment implements AsyncFinishInterface {

    private OnFragmentInteractionListener mListener;
    private FollowMeViewAdpater adapter;
    private ListView listView;
    private ApplicationPreferences prefs;
    private ServiceResponse serviceResponse;
    private String tag = "Follow me follower fragment";
    private List<FollowmeUserWrapper> wrapper;
    private FollowmeUserWrapper[] arry;
    private View loading;
    private String method;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            switch (action) {
                case "stop_user":
                    try {
                        Log.d(tag, "broad cast received to stop user from following");
                        Long userid = intent.getLongExtra("userId", -1);
                        String token = intent.getStringExtra("token");

                        JSONObject input = new JSONObject();
                        input.put("token", token);

                        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_STOP, "STOP_USR_TO_FOLLOWME", input.toString(), "", FollowMeFollowingFragment.this, false);
                        service.execute();

                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                    break;

            }
        }
    };
    public FollowMeFollowingFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_follow_me_followers, container, false);
        try {

            IntentFilter filter = new IntentFilter();
            filter.addAction("stop_user");
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);

            adapter = new FollowMeViewAdpater(getActivity());
//            prefs = new ApplicationPreferences(getActivity());
//            JSONObject input = new JSONObject();
//            input.put("vehicleId", prefs.getVehicle_Selected());
//            GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_REQUESTS, "FOLLOWING", input.toString(), tag, this, false);
//            service.execute();

            loading = getLoadingView();

            listView = (ListView) view.findViewById(R.id.listView);

            listView.addHeaderView(loading);
            listView.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            prefs = new ApplicationPreferences(getActivity());
            JSONObject input = new JSONObject();
            input.put("vehicleId", prefs.getVehicle_Selected());
            GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_REQUESTS, "FOLLOWING", input.toString(), tag, this, false);
            service.execute();
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, getActivity());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        Log.d(tag, "Following Response: " + response + "|method:" + method);
        this.method = method;
        if ("FOLLOWING".equals(method)) {
            Gson gson = new Gson();
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
        if ("STOP_USR_TO_FOLLOWME".equals(method)) {
            Gson gson = new Gson();
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }

    }

    @Override
    public void onPostExecute(Void Result) {

        if (loading != null) {
            listView.removeHeaderView(loading);
        }

        if ("FOLLOWING".equals(method) && serviceResponse != null && serviceResponse.getResponse().equals("SUCCESS")) {
            adapter.clear();
            if (serviceResponse.getListFollowMeUserWrapper() != null && serviceResponse.getListFollowMeUserWrapper().size() > 0) {
                FollowmeUserWrapper u = new FollowmeUserWrapper();
                Name name = new Name();
                name.setFirstName("All");
                name.setLastName("");
                u.setFollowerUserName(name);
                adapter.add(u);

                if (serviceResponse.getListFollowMeUserWrapper() != null && serviceResponse.getListFollowMeUserWrapper().size() > 0) {
                    for (FollowmeUserWrapper u1 : serviceResponse.getListFollowMeUserWrapper()) {

                        if (u1.getToken() != null && !u1.getToken().trim().isEmpty() && u1.getToken().trim().length() >= 0 && !u1.getFollowerUserName().getFirstName().trim().isEmpty()) {
                            adapter.add(u1);
                        }
                    }
                }
            } else {
                FollowmeUserWrapper u = new FollowmeUserWrapper();
                Name name = new Name();
                name.setFirstName("You do not have any request");
                name.setLastName("");
                u.setFollowerUserName(name);
                adapter.add(u);
            }

            wrapper = serviceResponse.getListFollowMeUserWrapper();

            adapter.notifyDataSetChanged();
            Log.d(tag, "added data in adapter");
        }   else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(getActivity(),
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(getActivity(), Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(getActivity(), "Server not responding, please try after some time!!");
            }
        }

        if ("STOP_USR_TO_FOLLOWME".equals(method)) {
            if (serviceResponse != null && serviceResponse.getResponse().equals("SUCCESS")) {
                try {
                    Utility.makeNewToast(getActivity(), "Invite has been removed");
                        prefs = new ApplicationPreferences(getActivity());
                        JSONObject input = new JSONObject();
                        input.put("vehicleId", prefs.getVehicle_Selected());
                        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_REQUESTS, "FOLLOWING", input.toString(), tag, this, false);
                        service.execute();

                } catch (Exception e) {
                    ExceptionHandler.handleException(e);
                }
            }
        }
    }

    private View getLoadingView() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.loading, null);
        return view;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }
}
