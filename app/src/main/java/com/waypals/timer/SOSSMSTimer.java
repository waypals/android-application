package com.waypals.timer;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.waypals.EmergencyNoActivity;
import com.waypals.Home_Screen;
import com.waypals.Settings_Screen;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.gson.vo.RideLocation;
import com.waypals.gson.vo.SOSContacts;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.util.GetAddress;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.waypals.utils.WplHttpClient;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimerTask;

public class SOSSMSTimer extends TimerTask implements AsyncFinishInterface {

    Home_Screen activity;
    LocationManager locationManager;
    boolean gps_enabled;
    boolean network_enabled;
    LatLng myPosition;
    ApplicationPreferences appPref;
    Context context;
    private String address;
    private String method;
    private AsyncFinishInterface asyn;

    public SOSSMSTimer(Home_Screen homescreen, ApplicationPreferences appPref, Context context) {
        this.activity = homescreen;
        this.appPref = appPref;
        this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @SuppressWarnings("deprecation")
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    @Override
    public void run() {
        try {
            Log.d("SOS Timere", "running sms thread");
            String sosSend = appPref.getSOSSend();
            if (!appPref.getLogout() && (sosSend != null && "true".equals(sosSend))) {
                getMyLocation();
                asyn = this;

                    List<SOSContacts> sosNumbers = appPref.getSOSServiceResponse(false);
                    if (sosNumbers == null) {
                        getSOSNumbers();
                    } else {
                        sendSMSNow(null, sosNumbers);
                    }
            } else {
                cancel();
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }

    }

    public void getMyLocation() {
        Location net_loc = null, gps_loc = null;
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        gps_enabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps_enabled) {
//
            LocationListener mlocListener = new MyLocationListener();
            if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    mlocListener, Looper.getMainLooper());

            gps_loc = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }


        if (network_enabled)
            net_loc = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        // if there are both values use the latest one
        if (gps_loc != null && net_loc != null) {
            if (gps_loc.getTime() > net_loc.getTime()) {
                double x = gps_loc.getLatitude();
                double y = gps_loc.getLongitude();

                myPosition = new LatLng(x, y);
            } else {
                double x = net_loc.getLatitude();
                double y = net_loc.getLongitude();
                myPosition = new LatLng(x, y);
            }

        } else if (gps_loc != null) {
            {
                double x = gps_loc.getLatitude();
                double y = gps_loc.getLongitude();
                myPosition = new LatLng(x, y);
            }
        } else if (net_loc != null) {
            {
                double x = net_loc.getLatitude();
                double y = net_loc.getLongitude();
                myPosition = new LatLng(x, y);
            }
        }
    }

    private void sendSMSNow(String address, List<SOSContacts> sosNumbers) {
        try {
            if (myPosition != null) {
                address = "http://maps.google.com/?q=" + myPosition.latitude + "," + myPosition.longitude;
            }else{
                address = null;
            }
            if (sosNumbers != null && !sosNumbers.isEmpty()) {
                Log.d("SOS Timer", "sending sms now");
                String messageText = "SOS: I need help. I can't talk right now.";
                if (address != null) {
                    messageText += " My location is " + address;
                }
                System.out.println("Message Text is == " + messageText);
                SmsManager smsManager = SmsManager.getDefault();
                String SENT = "SMS_SENT";
                String DELIVERED = "SMS_DELIVERED";
                PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
                PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

                context.registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        switch (getResultCode()) {
                            case Activity.RESULT_OK:
                                Toast.makeText(activity, "SMS Sent.", Toast.LENGTH_LONG)
                                        .show();
                                break;
                            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                Toast.makeText(activity, "Generic failure",
                                        Toast.LENGTH_SHORT).show();
                                break;
                            case SmsManager.RESULT_ERROR_NO_SERVICE:
                                Toast.makeText(activity, "No service",
                                        Toast.LENGTH_SHORT).show();
                                break;
                            case SmsManager.RESULT_ERROR_NULL_PDU:
                                Toast.makeText(activity, "Null PDU",
                                        Toast.LENGTH_SHORT).show();
                                break;
                            case SmsManager.RESULT_ERROR_RADIO_OFF:
                                Toast.makeText(activity, "Radio off",
                                        Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }, new IntentFilter(SENT));

                ArrayList<PendingIntent> lstSent = new ArrayList<PendingIntent>();
                lstSent.add(sentPI);

                for (SOSContacts number : sosNumbers) {
                    String phoneNumber = number.getEmergencyPhoneNumber().getCountryCode() + number.getEmergencyPhoneNumber().getPhoneNo();
                    ArrayList<String> partedMsg = smsManager.divideMessage(messageText);
                    smsManager.sendMultipartTextMessage(phoneNumber, null, partedMsg, lstSent, null);
                    Log.d("SOS Timer", "SMS sent: " + messageText + "|Number: " + number.getEmergencyPhoneNumber().getPhoneNo());
                }

                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        TelephonyManager telMgr = (TelephonyManager) Home_Screen.context.getSystemService(Context.TELEPHONY_SERVICE);
                        int simState = telMgr.getSimState();
                        int subId = telMgr.getCallState();
                        if (simState == TelephonyManager.SIM_STATE_ABSENT) {
                            Toast.makeText(activity, "Not registered on network.", Toast.LENGTH_LONG)
                                    .show();
                            return;
                        }
                        if (isAirplaneModeOn(activity)) {
                            return;
                        }

                    }
                });
            } else {
              //  Utility.makeToastBottom(this.activity, "No emergency contact found");
                onEmptyEmergencyContactList();
            }
        } catch (Throwable e) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(activity, "SMS Failed.", Toast.LENGTH_LONG)
                            .show();
                }
            });
            ExceptionHandler.handleException(e);
        }

    }


    private void getSOSNumbers() {
        try {
            WplHttpClient.callVolleyService(context, CH_Constant.GET_SOS_NUMBERS,
                    VehicleServices.getVehicleIdRequest(appPref), appPref, this, "GET_SOS", false);
        } catch (NetworkException ex) {
            ExceptionHandler.handlerException(ex, this.activity);
        } catch (Exception ex) {
            ExceptionHandler.handlerException(ex, this.activity);
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        Log.d("SOS timer", "address found: " + response);
        if ("GET_SOS".equalsIgnoreCase(method)) {
            appPref.saveSOSNumbers(response);
        } else if (response != null && "GET_ADDRESS".equals(method)) {
            address = response;
            onPostExecute(null);
        } else {
            address = null;
            onPostExecute(null);
        }

    }

    private void onEmptyEmergencyContactList()
    {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(SOSSMSTimer.this.context, android.R.style.Theme_Holo_Light_Dialog));
        dialog.setTitle("No emergency contact available");
        dialog.setMessage("Add emergency contact now?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(SOSSMSTimer.this.activity, EmergencyNoActivity.class);
                SOSSMSTimer.this.activity.startActivity(intent);
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dialog.setCancelable(true);
        dialog.create();
        dialog.show();
        cancel();
    }

    @Override
    public void onPostExecute(Void Result) {


        if ("GET_SOS".equalsIgnoreCase(method)) {
            List<SOSContacts> sosNumbers = appPref.getSOSServiceResponse(false);
            if (sosNumbers == null || sosNumbers.isEmpty()) {
                //Utility.makeToastBottom(this.activity, "No emergency contact found");
                //cancel();
                onEmptyEmergencyContactList();
            } else {
                sendSMSNow(address, sosNumbers);
            }
        } else if ("GET_ADDRESS".equals(method)) {
            List<SOSContacts> sosNumbers = appPref.getSOSServiceResponse(false);
            if (sosNumbers != null && !sosNumbers.isEmpty()) {
                sendSMSNow(address, sosNumbers);
            } else {
                getSOSNumbers();
            }
        }
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            loc.getLatitude();
            loc.getLongitude();

            Geocoder gcd = new Geocoder(context,
                    Locale.getDefault());

        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(context, "Gps Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(context, "Gps Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

}