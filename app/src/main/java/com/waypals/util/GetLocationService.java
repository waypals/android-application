package com.waypals.util;

import android.content.Context;

import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.RideLocation;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.utils.WplHttpClient;

public class GetLocationService {

    public static void getAddress(RideLocation location, Context context, AsyncFinishInterface asyncFinishInterface) {

        String address1 = "";
        String address2 = "";
        String city = "";
        String state = "";
        String country = "";
        String county = "";
        String PIN = "";
        String full_address = "";
        if (location != null) {
            try {
                full_address = location.getLatitude() + "/" + location.getLongitude();
                String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.getLatitude() + ","
                        + location.getLongitude();
                WplHttpClient.callVolleyGetService(context,url, asyncFinishInterface, false);

            } catch (Exception e) {
                ExceptionHandler.handleException(e);
            }
        }
    }


}


