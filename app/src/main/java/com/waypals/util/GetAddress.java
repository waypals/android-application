package com.waypals.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.waypals.exception.ExceptionHandler;
import com.waypals.rest.service.AsyncFinishInterface;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by shantanu on 29/1/15.
 */
public class GetAddress extends AsyncTask<Location, Void, String> {

    private Context context;
    private AsyncFinishInterface asyncFinishInterface;
    private Double latitude;
    private Double longitude;
    private String tag = "Get address";

    // Constructor called by the system to instantiate the task
    public GetAddress(Context context, AsyncFinishInterface asyncFinishInterface, Double lat, Double lon) {
        super();
        this.latitude = lat;
        this.longitude = lon;
        this.context = context;
        this.asyncFinishInterface = asyncFinishInterface;
    }

    @Override
    protected synchronized String doInBackground(Location... params) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        Log.d(tag, "request added for getting address");
        // Try to get an address for the current location. Catch IO or network problems.
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException exception1) {
            return null;
        } catch (IllegalArgumentException exception2) {
            return null;
        }
        // If the reverse geocode returned an address
        if (addresses != null && addresses.size() > 0) {

            // Get the first address
            Address address = addresses.get(0);
            StringBuilder addressText = new StringBuilder();
            int lines = 0;
            if ((lines = address.getMaxAddressLineIndex()) > 0) {
                for (int i = 0; i < lines && i < 1; i++) {
                    addressText.append(address.getAddressLine(i) + " ");
                }
            }
            addressText.append(address.getLocality() + ", ");
            addressText.append(address.getCountryName());
            return addressText.toString();
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String address) {
        try {
            asyncFinishInterface.finish(address, "GET_ADDRESS");
        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }
}