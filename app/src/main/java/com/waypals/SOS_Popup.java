package com.waypals;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.RideLocation;
import com.waypals.gson.vo.SOSContacts;
import com.waypals.gson.vo.SOSServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.util.GetLocationService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import android.graphics.drawable.ShapeDrawable;
//import android.graphics.drawable.shapes.RoundRectShape;

public class SOS_Popup extends Activity implements AsyncFinishInterface {
    static String tag = "SOS Popup";
    static MyCustomAdapter dataAdapter = null;
    List<SOSContacts> lstOfSOSContacts;
    ArrayList<Callerlist> callerItemList = new ArrayList<Callerlist>();
    ApplicationPreferences appPref;
    TextView callBtn;
    Typeface tf;
    TextView msgBtn;
    TextView contactName;
    TextView sosTitle;
    String location;
    HashMap<String, String> map;
    LocationManager locationManager;
    boolean gps_enabled;
    boolean network_enabled;
    LatLng myPosition;

    RideLocation myLocation;

    public static void SendAlert(Context c, String title, String msg) {
        AlertDialog.Builder ad = new AlertDialog.Builder(c);
        ad.setTitle(title);
        ad.setMessage(msg);
        ad.setCancelable(false);
        ad.setNeutralButton("Ok", null);
        ad.create();
        ad.show();
    }

    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            gps_enabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            /*RoundRectShape rs = new RoundRectShape(new float[]{20, 20, 20,
                    20, 20, 20, 20, 20}, null, null);
            ShapeDrawable sd = new ShapeDrawable(rs);*/

            setContentView(R.layout.sospopup);

            tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);

            appPref = new ApplicationPreferences(this);

            if(CH_Constant.isNetworkAvailable(this)) {
                VehicleServices vehicleServices = new VehicleServices();
                vehicleServices.getSOSNumber(appPref, this, "GET_EMERGENCY_CONTACT");
            }else {
                showSOSNumbers();
            }

            /*TextView title = (TextView) findViewById(R.id.sos_callertitle_text);
            title.setTypeface(tf);*/

        } catch (Exception e) {
            ExceptionHandler.handleException(e, this);
        }

    }

    public void setCallerList() {
        Log.i(tag, "setting up caller list");

        dataAdapter = new MyCustomAdapter(this, R.layout.sos_caller_list,
                callerItemList);
        ListView listView = (ListView) findViewById(R.id.sos_callerlist);
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                TextView name = (TextView) view.findViewById(R.id.caller_name);
                String number = map.get(name.getText());
                Log.i(tag, "object : " + name.getText() + ":" + number);
                createCall(number);
                finish();
            }

        });
    }

    public void createCall(String number) {
        try {
            Log.i(tag, "inititate call to " + number);
            String url = "tel:" + number;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));

            PackageManager pm = this.getBaseContext().getPackageManager();
            int i = pm.checkPermission(Manifest.permission.CALL_PHONE, this.getBaseContext().getPackageName());

            if (i == pm.PERMISSION_GRANTED) {
                startActivity(intent);
            }

        } catch (Exception ex) {
            Log.e("Error", ex.getMessage());
            SendAlert(SOS_Popup.this, "Error", "Could not create call !");
            return;
        }
    }

    public void SendSMS(final String number) {
        Log.i(tag, "send sms to " + number);
        final SmsManager smsManager = SmsManager.getDefault();
        if (location == null) {
            new Thread() {
                @Override
                public void run() {
                    getMyLocation();
                    if (myLocation != null) {
                        GetLocationService.getAddress(myLocation, getApplicationContext(), new AsyncFinishInterface() {
                            @Override
                            public void finish(String response, String method) throws Exception {
                                location = response;
                                String messageText = " I am in trouble and would need your help urgently. I won't be able to take your call at this time. " +
                                        "I'm currently at " + location + ". Time :" + new Date(System.currentTimeMillis()).toGMTString();
                                Log.d(tag, "Message Text is " + messageText);
                                smsManager.sendTextMessage(number, null, messageText,
                                        null, null);
                            }

                            @Override
                            public void onPostExecute(Void Result) {

                            }
                        });

                    }
                }
            }.start();
        } else {
            String messageText = " I am in trouble and would need your help urgently. I won't be able to take your call at this time. "
                    + "I'm currently at "
                    + location
                    + ". Time :"
                    + new Date(System.currentTimeMillis()).toGMTString();
            Log.d(tag, "Message Text is " + messageText);
            smsManager.sendTextMessage(number, null, messageText, null, null);
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish(String response, String method) {
        Gson gson = new Gson();

        if (response != null) {
            SOSServiceResponse sosServiceResponse = gson.fromJson(response,
                    SOSServiceResponse.class);
            // TODO:Save SOS number in preferences. If SOS number service fails use
            // contacts in preferences.
            // If service returns data then update preferences with new numbers
            if (sosServiceResponse != null
                    && sosServiceResponse.getResponse() != null
                    && sosServiceResponse.getResponse().equals(CH_Constant.SUCCESS)) {
                lstOfSOSContacts = sosServiceResponse.getSosNumber();
                appPref.saveSOSNumbers(response);
            }
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        Log.i(tag, "SOS Contact : " + lstOfSOSContacts);
        try {
            showSOSNumbers();
        } catch (Exception e) {
            ExceptionHandler.handleException(e, SOS_Popup.this);
        }

    }

    private void showSOSNumbers()
    {
        lstOfSOSContacts = appPref.getSOSServiceResponse(true);
        if (lstOfSOSContacts != null && !lstOfSOSContacts.isEmpty()) {

            map = new HashMap<String, String>();
            for (int i = 0; i < lstOfSOSContacts.size(); i++) {
                Callerlist _item = new Callerlist(lstOfSOSContacts.get(i)
                        .getEmergencyPhoneNumber().getPhoneNo(),
                        lstOfSOSContacts.get(i).getName());
                callerItemList.add(_item);
                map.put(lstOfSOSContacts.get(i).getName(), lstOfSOSContacts
                        .get(i).getEmergencyPhoneNumber().getCountryCode()
                        + lstOfSOSContacts.get(i).getEmergencyPhoneNumber()
                        .getPhoneNo());
            }
            Log.i(tag, "Caller List: " + callerItemList);

            if (callerItemList.size() > 0) {
                setCallerList();
            }

        } else {
         //   Utility.makeToastBottom(this, "Please enter SOS contacts in your profile");
            Intent intent = new Intent();
            intent.setAction(CH_Constant.NO_EMERGENCY_CONTACT);
            LocalBroadcastManager.getInstance(SOS_Popup.this).sendBroadcast(intent);
            this.finish();

        }
    }

    public void getMyLocation() {
        Location net_loc = null, gps_loc = null;
        if (gps_enabled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    gps_loc = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
            } else {
                gps_loc = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        if (network_enabled)
            net_loc = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        // if there are both values use the latest one
        if (gps_loc != null && net_loc != null) {
            if (gps_loc.getTime() > net_loc.getTime()) {
                double x = gps_loc.getLatitude();
                double y = gps_loc.getLongitude();

                myPosition = new LatLng(x, y);
                myLocation = new RideLocation(
                        new BigDecimal(x), new BigDecimal(y));
            } else {
                double x = net_loc.getLatitude();
                double y = net_loc.getLongitude();
                myPosition = new LatLng(x, y);
                myLocation = new RideLocation(
                        new BigDecimal(x), new BigDecimal(y));
            }

        }

        if (gps_loc != null) {
            {
                double x = gps_loc.getLatitude();
                double y = gps_loc.getLongitude();
                myPosition = new LatLng(x, y);
                myLocation = new RideLocation(
                        new BigDecimal(x), new BigDecimal(y));
            }

        }
        if (net_loc != null) {
            {
                double x = net_loc.getLatitude();
                double y = net_loc.getLongitude();
                myPosition = new LatLng(x, y);
                myLocation = new RideLocation(
                        new BigDecimal(x), new BigDecimal(y));
            }
        }
    }

	/*
     * @Override public void getVehicleState(String location) {
	 * this.location=location;
	 * 
	 * }
	 */

    public class Callerlist {

        long number;
        String name = null;

        public Callerlist(long l, String name) {
            super();
            this.number = l;
            this.name = name;

        }

        public long getNumber() {
            return number;
        }

        public void setNumber(long number) {
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    private class MyCustomAdapter extends ArrayAdapter<Callerlist> {

        private ArrayList<Callerlist> itemList;

        public MyCustomAdapter(Context context, int textViewResourceId,

                               ArrayList<Callerlist> data) {
            super(context, textViewResourceId, data);
            this.itemList = new ArrayList<Callerlist>();
            this.itemList.addAll(data);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = new ViewHolder();
            Log.v("ConvertView", String.valueOf(position));
            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.sos_caller_list_row, null);

            holder.callername = (TextView) convertView
                    .findViewById(R.id.caller_name);

            convertView.setTag(holder);
            holder = (ViewHolder) convertView.getTag();
            Callerlist item = itemList.get(position);
            holder.callername.setText(item.getName());
            holder.callername.setTypeface(tf);
            holder.callername.setTag(item);

            return convertView;
        }

        private class ViewHolder {
            TextView callername;
            TextView number;
        }

    }


}
