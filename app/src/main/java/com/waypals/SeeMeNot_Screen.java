package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.waypals.request.SeeMeNotRequest;
import com.waypals.request.TimeRange;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by shantanu on 13/1/15.
 */
public class SeeMeNot_Screen extends Activity implements AsyncFinishInterface {
    static Typeface tf;
    private static String startime = null, endtime = null;
    private static Date startDate, endDate;
    ImageView back_btn, save_btn;
    TextView title;
    private ApplicationPreferences appPref;
    private EditText startTime, endtTime;
    private ImageView setStartTime, setEndTime;
    private CustomDateTimePicker custom;
    private Activity context;
    private String tag = "See me not";
    private AsyncFinishInterface asyncFinishInterface;
    private HashMap<String, String> vehicleInfo = new HashMap<>();
    private ServiceResponse serviceResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seemenot_screen);
        asyncFinishInterface = this;
        context = this;

        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        appPref = new ApplicationPreferences(this);
        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setTypeface(tf);
        title.setText("SEE ME NOT");

        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save_btn = (ImageView) findViewById(R.id.right_btn);
        save_btn.setBackgroundResource(R.drawable.done_btn);
        save_btn.setVisibility(View.VISIBLE);


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Spinner spinner = (Spinner) findViewById(R.id.vehicleSpineer);
                final String selected = spinner.getSelectedItem().toString();

                if (selected.equals("Select Vehicle")) {
                    try {
                        Utility.makeNewToast(context, "Please select a vehicle");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                if (selected.equals("No Vehicle Found")) {
                    try {
                        Utility.makeNewToast(context, "You do not have any vehicle");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }

                if (startime == null) {
                    try {
                        Utility.makeNewToast(context, "Please select start time");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                if (endtime == null) {
                    try {
                        Utility.makeNewToast(context, "Please select end time");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                } else {
                    Date d = new Date();

                    if (endDate.after(d)) {
                        try {
                            Utility.makeNewToast(context, "Future date is not allowed");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                }


                if (startDate.after(endDate)) {
                    try {
                        Utility.makeNewToast(context, "Start time must be lesser than end time");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }


                AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                dialog.setTitle("See me not");
                dialog.setMessage("Do you want to save this?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(tag, "save see me not");
                        try {
                            SeeMeNotRequest request = new SeeMeNotRequest();
                            request.setVehicleId(vehicleInfo.get(selected));
                            TimeRange range = new TimeRange();
                            range.setStartTime(startime);
                            range.setEndTime(endtime);
                            request.setRange(range);
                            Gson gson = new Gson();
                            String input = gson.toJson(request);
                            Log.d(tag, "See me not request: " + input);
                            GenericAsyncService service = new GenericAsyncService(appPref, context, CH_Constant.SAVE_SEE_ME_NOT, "SAVE_SEE_ME_NOT", input, tag, asyncFinishInterface, true);
                            service.execute();

                        } catch (Exception ex) {
                            ExceptionHandler.handleException(ex, context);
                        }
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });
                dialog.setCancelable(false);
                dialog.create();
                dialog.show();

            }
        });

        startTime = (EditText) findViewById(R.id.starttime);
        endtTime = (EditText) findViewById(R.id.endtime);

        setStartTime = (ImageView) findViewById(R.id.setStartTime);
        setEndTime = (ImageView) findViewById(R.id.setEndTime);

        setStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                custom = new CustomDateTimePicker(context, new CustomDateTimePicker.ICustomDateTimeListener() {
                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec, String AM_PM) {

                        String set_DateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz").format(dateSelected);

                        startTime = (EditText) findViewById(R.id.starttime);

                        System.out.println("Formated Date :: " + set_DateTime);
                        startDate = dateSelected;
                        startime = set_DateTime;

                        startTime.setText(new SimpleDateFormat("dd.MM.yyyy, HH:mm ").format(dateSelected));
                    }

                    @Override
                    public void onCancel() {

                    }


                });

                /**
                 * Pass Directly current time format it will return AM and PM if you set
                 * false
                 */
                custom.set24HourFormat(false);
                /**
                 * Pass Directly current data and time to show when it pop up
                 */
                custom.setDate(Calendar.getInstance());

                custom.showDialog();
            }
        });

        setEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                custom = new CustomDateTimePicker(context, new CustomDateTimePicker.ICustomDateTimeListener() {
                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec, String AM_PM) {

                        String set_DateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz").format(dateSelected);

                        endtTime = (EditText) findViewById(R.id.endtime);

                        System.out.println("Formatted Date :: " + set_DateTime);

                        endtime = set_DateTime;
                        endDate = dateSelected;
                        endtTime.setText(new SimpleDateFormat("dd.MM.yyyy, HH:mm ").format(dateSelected));
                    }

                    @Override
                    public void onCancel() {

                    }


                });

                /**
                 * Pass Directly current time format it will return AM and PM if you set
                 * false
                 */
                custom.set24HourFormat(false);
                /**
                 * Pass Directly current data and time to show when it pop up
                 */
                custom.setDate(Calendar.getInstance());

                custom.showDialog();
            }
        });

        ArrayList<String> spinnerArray = new ArrayList<>();

        VehicleServiceResponse response = appPref.getVehicleResponse();

        if (response != null) {
            Collection<VehicleInformationVO> vehicles = response.getVehicles();

            if (vehicles != null && vehicles.size() > 0) {
                spinnerArray.add("Select Vehicle");
                for (VehicleInformationVO v : vehicles) {
                    spinnerArray.add(v.getRegistrationNumber());
                    vehicleInfo.put(v.getRegistrationNumber(), String.valueOf(v.getVehicleId()));
                }
            } else {
                spinnerArray.add("No Vehicle Found");
            }
        }else{
            spinnerArray.add("No Vehicle Found");
        }

        Spinner spinner = (Spinner) findViewById(R.id.vehicleSpineer);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);


    }

    @Override
    public void finish(String response, String method) throws Exception {
        Log.d(tag, "Response: " + response);
        if (method.equals("SAVE_SEE_ME_NOT")) {
            if (response != null && !response.isEmpty()) {
                Gson gson = new Gson();
                serviceResponse = gson.fromJson(response, ServiceResponse.class);

            }
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())))  {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, getString(R.string.server_error));
            }

            return;
        }

        if (serviceResponse != null) {
            if (serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                try {
                    Utility.makeNewToast(context, "Your \"See Me Not\" request has been submitted successfully");
                    startTime = (EditText) findViewById(R.id.starttime);
                    endtTime = (EditText) findViewById(R.id.endtime);
                    Spinner spinner = (Spinner) findViewById(R.id.vehicleSpineer);
                    startTime.setText("");
                    endtTime.setText("");
                    spinner.setSelection(0);
                } catch (Exception e) {
                    ExceptionHandler.handleException(e);
                }
            } else {
                try {
                    Utility.makeNewToast(context, "Could not delete the data");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e);
                }
            }
        } else {
            try {
                Utility.makeNewToast(context, "Please try again later!");
            } catch (Exception e) {
                ExceptionHandler.handleException(e);
            }
        }
    }

}
