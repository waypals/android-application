package com.waypals.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.objects.ObdEndOfTrip;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by surya on 10/5/16.
 */
public class TripAdapter extends ArrayAdapter<ObdEndOfTrip> {

    private final Context activity;
    private List<ObdEndOfTrip> items;

    public TripAdapter(Context context) {
        super(context, android.R.layout.two_line_list_item);

        this.activity = context;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {

        if (row == null) {
            row = LayoutInflater.from(activity).inflate(R.layout.trip_data_layout, null);
        }

            final ObdEndOfTrip data = getItem(position);
            TextView startTime = (TextView) row.findViewById(R.id.start_time);
            TextView endTime = (TextView) row.findViewById(R.id.end_time);
            TextView distance = (TextView) row.findViewById(R.id.distance);
            TextView tripOverview = (TextView) row.findViewById(R.id.trip_overview);
            //TextView tripScore = (TextView) row.findViewById(R.id.trip_score);

            tripOverview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent registrationComplete = new Intent(CH_Constant.TRIP_OVERVIEW);
                    registrationComplete.putExtra("vehicleId", ""+data.getVehicleId());
                    registrationComplete.putExtra("startTime", data.getStartTime());
                    registrationComplete.putExtra("endTime", data.getLogTime());
                    if (data.getScore() != null) {
                        registrationComplete.putExtra("tripScore", data.getScore().getScore());
                    }

                    String dist = "";
                    if (data.getDistance() != null && data.getDistance().getDistance() != null && data.getDistance().getUnit() != null) {
                        if (("METER".equalsIgnoreCase(data.getDistance().getUnit()) || "METRE".equalsIgnoreCase(data.getDistance().getUnit()))) {
                            float dis = data.getDistance().getDistance().floatValue() / 1000;
                            dist = "" + new DecimalFormat("##.##").format(dis) + " (KM)";
                        } else {
                            dist = "" + new DecimalFormat("##.##").format(data.getDistance().getDistance().floatValue()) + " (" + data.getDistance().getUnit() + ")";
                        }
                    }

                    registrationComplete.putExtra("distance", dist);

                    String maxS = "";
                    if(data.getMaxSpeed() != null && data.getMaxSpeed().getSpeed() != null && data.getMaxSpeed().getUnit() != null){
                        maxS = ""+data.getMaxSpeed().getSpeed()+" ("+data.getMaxSpeed().getUnit().toString()+")";
                    }

                    registrationComplete.putExtra("maxSpeed", maxS);
                    registrationComplete.putExtra("idleTime", Utility.getSecMinHourDayFormat(data.getIdleTime()));
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(registrationComplete);
                }
            });

            startTime.setText(Utility.parseDateForTrip(data.getStartTime()));
            endTime.setText(Utility.parseDateForTrip(data.getLogTime()));

            if (data.getDistance() != null && data.getDistance().getDistance() != null && data.getDistance().getUnit() != null) {
                if (("METER".equalsIgnoreCase(data.getDistance().getUnit()) || "METRE".equalsIgnoreCase(data.getDistance().getUnit()))) {
                    float dis = data.getDistance().getDistance().floatValue() / 1000;
                    distance.setText("" + new DecimalFormat("##.##").format(dis) + " (KM)");
                } else {
                    distance.setText("" + new DecimalFormat("##.##").format(data.getDistance().getDistance().floatValue()) + " (" + data.getDistance().getUnit() + ")");
                }
            }

        return row;
    }

}
