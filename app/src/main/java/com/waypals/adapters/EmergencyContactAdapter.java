package com.waypals.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.objects.EmergencyContact;
import com.waypals.utils.CH_Constant;

/**
 * Created by surya on 23/10/15.
 */
public class EmergencyContactAdapter extends ArrayAdapter<EmergencyContact> {

    private Activity activity;
    public EmergencyContactAdapter(Context context) {
        super(context, android.R.layout.two_line_list_item);

        this.activity = (Activity) context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.emergency_row, null);

            TextView number = (TextView) convertView.findViewById(R.id.emergency_number_text);
            TextView name = (TextView) convertView.findViewById(R.id.emergency_name_text);
            TextView relation = (TextView) convertView.findViewById(R.id.emergency_relation_text);

            EmergencyContact item = getItem(position);

            number.setText(item.getCountryCode() + item.getPhoneNumber());
            name.setText(item.getName());
            relation.setText(item.getRelation());

            final int id = item.getId();

            ImageView editNum = (ImageView) convertView.findViewById(R.id.emergency_num_edit_icon);

            editNum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent j = new Intent();
                    j.setAction(CH_Constant.EDIT_EMERGENCY_NO);
                    j.putExtra("id", id);
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(j);
                }
            });

            ImageView deleteNum = (ImageView) convertView.findViewById(R.id.emergency_num_delete_icon);

            deleteNum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Light_Dialog));
                    dialog.setTitle("Delete Emergency Contact");
                    dialog.setMessage("Do you want to delete this contact ?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent j = new Intent();

                            j.setAction(CH_Constant.DELETE_EMERGENCY_NO);
                            j.putExtra("id", id);

                            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(j);
                        }
                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            return;
                        }
                    });
                    dialog.setCancelable(false);
                    dialog.create();
                    dialog.show();
                }
            });

        }

        return convertView;
    }

}
