package com.waypals.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.objects.IncidentAlerts;
import com.waypals.utils.Utility;

import java.util.HashMap;

/**
 * Created by shantanu on 4/2/15.
 */
public class IncidentAlertsAdapter extends ArrayAdapter<IncidentAlerts> {

    private Activity activity;
    private HashMap<String, Integer> alertImagesSet = new HashMap<String, Integer>();

    public IncidentAlertsAdapter(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
        alertImagesSet.put("ACCIDENT", R.drawable.accident);
        alertImagesSet.put("TRAFFIC_VIOLATION", R.drawable.traffic_violation);
        alertImagesSet.put("DEVICE_BREAKDOWN", R.drawable.break_down);
        alertImagesSet.put("VEHICLE_BREAKDOWN", R.drawable.break_down);
        alertImagesSet.put("THEFT", R.drawable.theft);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {

        try {

            if (row == null) {
                row = LayoutInflater.from(activity).inflate(R.layout.alert_list_row, null);
            }

                final Holder holder = new Holder();
                holder.alertImage = (ImageView) row.findViewById(R.id.inci_image);
                holder.carNumberPlate = (TextView) row.findViewById(R.id.car_no_plate_alrt);
                holder.time = (TextView) row.findViewById(R.id.date_time_alert);
                holder.alert_type = (TextView) row.findViewById(R.id.alert_type);

                final IncidentAlerts alert = getItem(position);
                holder.alert = alert;

                if (holder.alertImage != null) {
                    if (alertImagesSet != null) {
                        if (alert.getEvent().getType().getEventType() != null) {
                            //Log.d("Alert Type", alert.getEvent().getType().getEventType());
                            if (alertImagesSet.get(alert.getEvent().getType().getEventType()) != null) {
                                holder.alertImage.setImageResource(alertImagesSet.get(alert.getEvent().getType().getEventType()));
                            }
                        }
                    }
                }
                holder.carNumberPlate.setText(alert.getEvent().getVehicleMetadata().getRegistrationNo());
                holder.time.setText(Utility.getUTCtoIST(alert.getEvent().getReportingTime()));
                holder.alert_type.setText(alert.getEvent().getDescription());

                row.setTag(holder);
            //}
        }catch (Exception e)
        {
            ExceptionHandler.handleException(e, (Activity) getContext());
        }

        return row;
    }

    public class Holder {
        TextView carNumberPlate, time, alert_type;
        ImageView alertImage;
        IncidentAlerts alert;

        public TextView getCarNumberPlate() {
            return carNumberPlate;
        }

        public void setCarNumberPlate(TextView carNumberPlate) {
            this.carNumberPlate = carNumberPlate;
        }

        public TextView getTime() {
            return time;
        }

        public void setTime(TextView time) {
            this.time = time;
        }

        public TextView getAlert_type() {
            return alert_type;
        }

        public void setAlert_type(TextView alert_type) {
            this.alert_type = alert_type;
        }

        public ImageView getAlertImage() {
            return alertImage;
        }

        public void setAlertImage(ImageView alertImage) {
            this.alertImage = alertImage;
        }

        public IncidentAlerts getAlert() {
            return alert;
        }

        public void setAlert(IncidentAlerts alert) {
            this.alert = alert;
        }
    }
}
