package com.waypals.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.request.TimeRange;
import com.waypals.response.FollowmeUserWrapper;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

/**
 * Created by shantanu on 17/1/15.
 */
public class FollowMeViewAdpater extends ArrayAdapter<FollowmeUserWrapper> {

    public FollowMeViewAdpater(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {

        try {
            Holder holder = new Holder();

            row = LayoutInflater.from(getContext()).inflate(R.layout.followme_viewlist_row, parent, false);

            holder.name = (TextView) row.findViewById(R.id.name);
            holder.viewOnMap = (ImageView) row.findViewById(R.id.view_on_map);
            holder.timeFrom = (TextView) row.findViewById(R.id.time_from);
            holder.to = (TextView) row.findViewById(R.id.time_to);
            holder.vehicleNum = (TextView) row.findViewById(R.id.vehicle_num);
            holder.timeTill = (TextView) row.findViewById(R.id.time_till);
            holder.deleteInvite = (ImageView) row.findViewById(R.id.delete_request);

            final FollowmeUserWrapper data = getItem(position);

            holder.deleteInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*Log delete request*/

                    try {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Light_Dialog));
                        dialog.setTitle("Delete Invite");
                        dialog.setMessage("Do you want to delete this invite ?");
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent j = new Intent();
                                j.setAction("stop_user");
                                j.putExtra("userId", data.getUserId());
                                j.putExtra("token", data.getToken());
                                j.putExtra("userName", data.getFollowerUserName());
                                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(j);
                            }
                        });
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        dialog.setCancelable(false);
                        dialog.create();
                        dialog.show();
                    } catch (Throwable e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            });

            if (data != null && data.getFollowerUserName() != null && "You do not have any request".

                    equals(data.getFollowerUserName()

                                    .

                                            getFirstName()

                    ))

            {
                holder.name.setText(data.getFollowerUserName().getFirstName());
                RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) holder.name.getLayoutParams();
                rl.addRule(RelativeLayout.CENTER_VERTICAL);
                holder.name.setLayoutParams(rl);
            } else if (data != null && data.getFollowerUserName() != null && "All".

                    equals(data.getFollowerUserName()

                                    .

                                            getFirstName()

                    ))

            {
                holder.name.setText(data.getFollowerUserName().getFirstName());
                RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) holder.name.getLayoutParams();
                rl.addRule(RelativeLayout.CENTER_VERTICAL);
                holder.name.setLayoutParams(rl);
            } else

            {
                if (data != null) {
                    if (data.getFollowerUserName() != null) {
                        holder.name.setText(data.getFollowerUserName().getFullName());
                    }
                    if (data.getRange() != null) {
                        holder.timeTill.setVisibility(View.VISIBLE);
                        holder.timeFrom.setVisibility(View.VISIBLE);
                        holder.to.setVisibility(View.VISIBLE);
                        holder.vehicleNum.setText(data.getFollowingVehicle());
                        holder.vehicleNum.setVisibility(View.VISIBLE);
                        holder.deleteInvite.setVisibility(View.VISIBLE);

                        holder.timeFrom.setText(Utility.getUTCtoIST(data.getRange().getStartTime()));
                        holder.timeTill.setText(Utility.getUTCtoIST(data.getRange().getEndTime()));
                    }
                }
            }

            if (data != null && data.getFollowerUserName() != null && "You do not have any request".

                    equals(data.getFollowerUserName()

                                    .

                                            getFirstName()

                    ))

            {
                holder.name.setPadding(50, 0, 0, 0);
                holder.name.requestLayout();
                holder.viewOnMap.setVisibility(View.GONE);
                row.findViewById(R.id.imageViewFrndImage).setVisibility(View.GONE);
                row.requestLayout();
            } else
            {
                holder.viewOnMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent();
                        i.setAction(CH_Constant.broadcast_fragment_view_map);

                        if (data.getFollowerUserName().getFirstName().equals("All")) {
                            i.putExtra("for", "ALL");
                            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                        } else {
                            i.putExtra("for", "one");
                            i.putExtra("token", data.getToken());
                            i.putExtra("userId", data.getUserId().toString());
                            i.putExtra("userName", data.getFollowerUserName().getFirstName());

                            if (isInBound(data.getRange())) {
                                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                            }else {
                                Utility.makeNewToast((Activity)getContext(), "Time has not started/expired");
                            }

                        }

                    }
                });
            }

            holder.wrapper = data;
            row.setTag(holder);

        } catch (Exception ex) {
            Log.e("Follow Me", ex.getMessage());
            ExceptionHandler.handleException(ex);
        }
        return row;
    }

    private boolean isInBound(TimeRange range)
    {
      if(!Utility.isBeforeCurrentDate(range.getStartTime()) || Utility.isBeforeCurrentDate(range.getEndTime()))
      {
          return false;
      }

      return true;
    }

    class Holder {
        TextView name;
        ImageView viewOnMap;
        ImageView deleteInvite;
        TextView timeFrom;
        TextView to;
        TextView vehicleNum;
        TextView timeTill;
        FollowmeUserWrapper wrapper;
    }

}
