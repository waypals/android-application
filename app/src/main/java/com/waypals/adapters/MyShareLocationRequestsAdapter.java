package com.waypals.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.ShareLocationViewOnMap;
import com.waypals.exception.ExceptionHandler;
import com.waypals.objects.MyShareLocationResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.wareninja.opensource.common.ObjectSerializer;

/**
 * Created by shantanu on 17/1/15.
 */
public class MyShareLocationRequestsAdapter extends ArrayAdapter<MyShareLocationResponse> implements AsyncFinishInterface {

    private Activity activity;
    private ApplicationPreferences preferences;
    private AsyncFinishInterface asyncFinishInterface;
    private String tag = "My Share location request adapter";
    private ServiceResponse serviceResponse;

    public MyShareLocationRequestsAdapter(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
        this.activity = activity;
        preferences = new ApplicationPreferences(activity);
        this.asyncFinishInterface = this;
    }

    @Override
    public View getView(int position, View row, final ViewGroup parent) {

        Holder holder = new Holder();

        row = LayoutInflater.from(getContext()).inflate(R.layout.myshare_location_requests_content_row, parent, false);

        holder.name = (TextView) row.findViewById(R.id.name);
        holder.viewOnMap = (ImageView) row.findViewById(R.id.view_on_map);
        holder.delete = (ImageView) row.findViewById(R.id.delete);
        holder.imageViewFrndImage = (ImageView) row.findViewById(R.id.imageViewFrndImage);
        holder.time = (TextView) row.findViewById(R.id.time);
        final MyShareLocationResponse data = getItem(position);

        if (data.getName().equals("All")) {
            holder.name.setText(data.getName());
            holder.time.setVisibility(View.GONE);
            holder.viewOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ShareLocationViewOnMap.class);
                    i.putExtra("data", "ALL");
                    activity.startActivity(i);
                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Light_Dialog));
                        dialog.setTitle("Delete Request");
                        dialog.setMessage("Do you want to delete this request ?");
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                GenericAsyncService service = new GenericAsyncService(preferences, activity, CH_Constant.DELETE_SHARELOCATION_REQUEST + data.getId(), "DELETE", "{}", tag, asyncFinishInterface, true);
                                service.execute();
                            }
                        });
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        dialog.setCancelable(false);
                        dialog.create();
                        dialog.show();
                    } catch (Throwable e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            });
        } else if (data.getName().equals("You do not have any requests")) {
            holder.name.setText(data.getName());
            holder.name.setPadding(50, 0, 0, 0);
            holder.name.requestLayout();
            holder.viewOnMap.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
            holder.imageViewFrndImage.setVisibility(View.GONE);
            holder.time.setVisibility(View.GONE);
        } else {
            holder.name.setText(data.getName());
            String date = Utility.getUTCtoIST(data.getTimeStamp());
            holder.time.setText(date);
            holder.wrapper = data;

            holder.viewOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity, ShareLocationViewOnMap.class);
                    i.putExtra("data", ObjectSerializer.serialize(data));
                    activity.startActivity(i);
                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Light_Dialog));
                        dialog.setTitle("Delete Request");
                        dialog.setMessage("Do you want to delete this request ?");
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                GenericAsyncService service = new GenericAsyncService(preferences, activity, CH_Constant.DELETE_SHARELOCATION_REQUEST + data.getId(), "DELETE", "{}", tag, asyncFinishInterface, true);
                                service.execute();
                            }
                        });
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        dialog.setCancelable(false);
                        dialog.create();
                        dialog.show();
                    } catch (Throwable e) {
                        ExceptionHandler.handleException(e);
                    }


                }
            });

        }


        row.setTag(holder);
        return row;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (method.equals("DELETE")) {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())))  {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this.activity,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this.activity, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getContext().startActivity(i);
            }
            else {
                Utility.makeNewToast(this.activity, getContext().getString(R.string.server_error));
            }

            return;
        }

        if (serviceResponse != null) {
            if (serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                Utility.makeNewToast(activity, "Request has been deleted");
            } else {
                Utility.makeNewToast(activity, "Could not delete request at this time");
            }
            Intent i = new Intent();
            i.setAction("RELOAD");
            LocalBroadcastManager.getInstance(activity).sendBroadcast(i);
        }
    }

    class Holder {
        TextView name, time;
        ImageView viewOnMap, delete, imageViewFrndImage;
        MyShareLocationResponse wrapper;
    }
}
