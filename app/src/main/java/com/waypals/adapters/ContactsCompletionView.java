package com.waypals.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.feedItems.Friend;
import com.tokenautocomplete.TokenCompleteTextView;


public class ContactsCompletionView extends TokenCompleteTextView {

    public ContactsCompletionView(Context context) {
        super(context);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(final Object object) {

        Friend p = (Friend) object;

        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        final RelativeLayout view = (RelativeLayout) l.inflate(R.layout.contact_token_ori, (ViewGroup) ContactsCompletionView.this.getParent(), false);

        String name = p.getFirstName();
        if(p.getLastName() != null)
        {
            name = p.getFirstName() + " " + p.getLastName();
        }
        ((TextView) view.findViewById(R.id.name)).setText(name);

        return view;
    }

    @Override
    protected Object defaultObject(String completionText) {
        return null;
    }

}
