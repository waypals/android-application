package com.waypals.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.request.Friend;
import com.waypals.request.UnFriendRequest;
import com.waypals.response.FriendInfo;
import com.waypals.utils.CH_Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shantanu on 29/1/15.
 */
public class FriendsMyFriendsAdapter extends ArrayAdapter<FriendInfo> {

    static Typeface tf;
    private Activity activity;
    private String tag = "Friends my friend adapter";
    private List<FriendInfo> info = null;

    public FriendsMyFriendsAdapter(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
        this.activity = activity;
    }

    public View getView(int position, View row, ViewGroup parent) {

        tf = Typeface.createFromAsset(activity.getAssets(),
                CH_Constant.Font_Typface_Path);

        row = LayoutInflater.from(getContext()).inflate(R.layout.friends_list_content_row, parent, false);

        ContentsHolder holder = new ContentsHolder();

        holder.delete = (ImageView) row.findViewById(R.id.imageViewDeleteFriend);
        holder.name = (TextView) row
                .findViewById(R.id.textViewFrndName);
        holder.email = (TextView) row
                .findViewById(R.id.textViewFrndEmail);

        final FriendInfo content = getItem(position);

        holder.name.setText(content.getFirstName() + " " + content.getLastName());
        holder.email.setText(content.getEmail());
        holder.name.setTypeface(tf);
        holder.email.setTypeface(tf);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ShowMessage("Are you sure you want to delete this friend?", content.getEmail(), content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return row;
    }

    public void ShowMessage(String msg, final String email, final FriendInfo info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Holo_Light_Dialog));

        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Log.d(tag, "Delete friend request of email :" + email);
                        Friend f = new Friend();
                        f.setFriendId(info.getUserId().toString());
                        f.setFriendCollectionId(info.getFriendConsumerId().toString());
                        List<Friend> l = new ArrayList<Friend>();
                        l.add(f);
                        UnFriendRequest request = new UnFriendRequest();
                        request.setFriends(l);

                        Gson gson = new Gson();
                        String input = gson.toJson(request).toString();

                        Intent i = new Intent();
                        i.setAction("DELETE_FRIEND");
                        i.putExtra("input", input);
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public int filter(String text) {
        if (info == null) {
            info = new ArrayList<>();
            for (int i = 0; i < getCount(); i++) {
                if (!info.contains(getItem(i))) {
                    info.add(getItem(i));
                }
            }
        } else {
            for (int i = 0; i < getCount(); i++) {
                if (!info.contains(getItem(i))) {
                    info.add(getItem(i));
                }
            }
        }
        text = text.trim();
        if (text.length() == 0 || text.equals("")) {
            this.clear();
            for (FriendInfo i : info) {
                this.add(i);
            }
            this.notifyDataSetChanged();
        } else {
            text = text.toLowerCase();
            this.clear();
            for (FriendInfo i : info) {
                if (i.getEmail().toLowerCase().contains(text) || i.getFirstName().toLowerCase().contains(text) || i.getLastName().toLowerCase().contains(text)) {
                    this.add(i);
                }
            }
            this.notifyDataSetChanged();
        }
        return getCount();
    }

    class ContentsHolder {
        TextView name;
        TextView email;
        ImageView delete;
    }
}
