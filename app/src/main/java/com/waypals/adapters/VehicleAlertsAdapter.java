package com.waypals.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.objects.VehicleAlerts;
import com.waypals.utils.Utility;

import java.util.HashMap;

/**
 * Created by shantanu on 4/2/15.
 */
public class VehicleAlertsAdapter extends ArrayAdapter<VehicleAlerts> {

    private Activity activity;
    private HashMap<String, Integer> alertImagesSet = new HashMap<String, Integer>();

    public VehicleAlertsAdapter(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
        alertImagesSet.put("SYSTEM_FENCE", R.drawable.parkingfence_icon);
        alertImagesSet.put("OUT_BOUND_GEO_FENCE", R.drawable.enter_geofence);
        alertImagesSet.put("IN_BOUND_GEO_FENCE", R.drawable.leave_geofence);
        alertImagesSet.put("SPEED_LIMIT", R.drawable.speed_limit_violation);
        alertImagesSet.put("FUEL_LIMIT", R.drawable.fuel_alert);
        alertImagesSet.put(Utility.PushMessageType.IN_NETWORK.toString(), R.drawable.vehicle_network_alert);
        alertImagesSet.put(Utility.PushMessageType.TOWED.toString(), R.drawable.vehicle_tow_alert);
        alertImagesSet.put(Utility.PushMessageType.BATTERY_CHARGING_FAULT.toString(), R.drawable.battery_img);
        alertImagesSet.put(Utility.PushMessageType.BATTERY_DISCHARGED.toString(), R.drawable.battery_img);
        alertImagesSet.put(Utility.PushMessageType.VEHICLE_POLICY_ALERT.toString(), R.drawable.vehicle_policy_alert);
        alertImagesSet.put(Utility.PushMessageType.VEHICLE_SERVICE_ALERT.toString(), R.drawable.vehicle_service_alert);
        alertImagesSet.put(Utility.PushMessageType.VEHICLE_POLLUTION_ALERT.toString(), R.drawable.vehicle_pollution_alert);
        alertImagesSet.put(Utility.PushMessageType.DEVICE_UNPLUGED.toString(), R.drawable.battery_unplugged);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {

        if (row == null) {
            row = LayoutInflater.from(activity).inflate(R.layout.vehicle_alerts_content_row, null);

            final Holder holder = new Holder();
            holder.content = (TextView) row.findViewById(R.id.content);
            holder.image = (ImageView) row.findViewById(R.id.alertImage);

            final VehicleAlerts alert = getItem(position);

            if(Utility.PushMessageType.VEHICLE_POLICY_ALERT.toString().equalsIgnoreCase(alert.getType())
                    || Utility.PushMessageType.VEHICLE_POLLUTION_ALERT.toString().equalsIgnoreCase(alert.getType())
                    || Utility.PushMessageType.VEHICLE_SERVICE_ALERT.toString().equalsIgnoreCase(alert.getType()))
            {
                holder.content.setText(alert.getMessage() + "" + Utility.getLongToDateOnly(alert.getDate()));
            }
            else {
                holder.content.setText(alert.getMessage() + " " + Utility.getLongToDate(alert.getDate()));
            }
            if(alertImagesSet.get(alert.getType()) != null) {
                holder.image.setImageResource(alertImagesSet.get(alert.getType()));
            }

            row.setTag(holder);
        }

        return row;
    }

    class Holder {
        TextView content;
        ImageView image;
        VehicleAlerts alerts;

        public TextView getContent() {
            return content;
        }

        public void setContent(TextView content) {
            this.content = content;
        }

        public ImageView getImage() {
            return image;
        }

        public void setImage(ImageView image) {
            this.image = image;
        }

        public VehicleAlerts getAlerts() {
            return alerts;
        }

        public void setAlerts(VehicleAlerts alerts) {
            this.alerts = alerts;
        }
    }
}
