package com.waypals.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.waypals.R;
import com.waypals.documentcase.DocImageData;
import com.waypals.documentcase.DocumentCaseFragment;
import com.waypals.documentcase.VolleySingleton;
import com.waypals.utils.WplSingleton;

import java.io.File;

/**
 * Created by surya on 18/1/16.
 */
public class DocumentImageAdapter extends ArrayAdapter<DocImageData> {

    private Context context;
    private ImageLoader mImageLoader;

    public DocumentImageAdapter(Context context, int resource)
    {
        super(context, resource);
        this.context = context;
    }

    public DocumentImageAdapter(Context context, int resource, DocImageData[] data) {
        super(context, resource, data);
        this.context = context;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

       // if(view == null)
        {
            view = LayoutInflater.from(context).inflate(R.layout.document_image_layout, null);

            final DocImageData v = getItem(i);
            TextView title = (TextView) view.findViewById(R.id.doc_title);
            title.setText(v.getTitle());

            if(v.getImageUrl() != null)
            {
                final NetworkImageView imageView = (NetworkImageView) view.findViewById(R.id.doc_image);

                ImageView imageViewNormal = (ImageView) view.findViewById(R.id.doc_image_normal);

                boolean isLocal = false;
                if(v.getImageUrl().contains("ftp|http"))
                {
                    isLocal = false;
                }

                if(!v.isLink()) {
                    imageViewNormal.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.GONE);
                    imageViewNormal.setImageURI(Uri.fromFile(new File(v.getImageUrl())));

                }else {
                    RequestQueue queue = VolleySingleton.getInstance(getContext().getApplicationContext()).getRequestQueue();
                    mImageLoader = VolleySingleton.getInstance(getContext().getApplicationContext()).getImageLoader();
                    imageView.setImageUrl(v.getImageUrl(), mImageLoader);
                    imageViewNormal.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        }

        return view;
    }
}
