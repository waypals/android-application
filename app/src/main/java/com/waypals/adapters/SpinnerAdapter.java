package com.waypals.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.waypals.R;

import java.lang.reflect.Method;

public class SpinnerAdapter<T> extends ArrayAdapter<T> {
    private OnItemSelected onSelection;
    private Activity activity;
    private TextView textView;
    private Spinner spinner;

    public SpinnerAdapter(Context context, int resource, T[] data, TextView view, Spinner spinner) {
        super(context, resource, data);

        this.activity = (Activity) context;
        this.textView = view;
        this.spinner = spinner;
    }

    public SpinnerAdapter(Context context, int resource, T[] data, TextView view, Spinner spinner, OnItemSelected selected) {
        super(context, resource, data);

        this.activity = (Activity) context;
        this.textView = view;
        this.spinner = spinner;
        this.onSelection = selected;
    }


    @Override
    public View getDropDownView(final int position, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.spinner_row_layout, null);
        }

        T item = getItem(position);

        final TextView tView = (TextView) convertView.findViewById(R.id.text_content);
        tView.setText(item.toString());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSpinnerDropDown(SpinnerAdapter.this.spinner);
                SpinnerAdapter.this.textView.setText(tView.getText().toString());
                if (onSelection != null){
                    onSelection.selected(tView.getText().toString());
                }
            }
        });

        return convertView;
    }

    private void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnItemSelected{
        void selected(String value);
    }
}