package com.waypals.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.waypals.R;
import com.waypals.objects.GreenDriveData;
import com.waypals.objects.VehicleAlerts;
import com.waypals.utils.Utility;

import java.text.DecimalFormat;

/**
 * Created by surya on 15/02/2017.
 */
public class ScoreAdapter extends ArrayAdapter<GreenDriveData> {

    private Activity activity;

    public ScoreAdapter(Activity context) {
        super(context, android.R.layout.two_line_list_item);
        this.activity = context;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {

        if (row == null) {
            row = LayoutInflater.from(activity).inflate(R.layout.score_row, null);

            final Holder holder = new Holder();
            holder.title = (TextView) row.findViewById(R.id.scoreTitle);
            holder.value = (TextView) row.findViewById(R.id.scoreValue);
            holder.bar = (RoundCornerProgressBar) row.findViewById(R.id.bar);
            holder.barText = (TextView) row.findViewById(R.id.barText);

            final GreenDriveData alert = getItem(position);
            DecimalFormat df = new DecimalFormat("#");
            if (alert != null) {
                if (alert.getTitle() != null) {
                    holder.title.setText(alert.getTitle());
                }
                float points = Float.parseFloat(alert.getPoints());
                points = (int) Math.ceil(points);

                float allowed = Float.parseFloat(alert.getAllowedInTrip());
                allowed = (int) Math.ceil(allowed);

                holder.value.setText("Allowed: "+(int) allowed);

                float minValue = 0;
                float maxValue =  points+allowed;
                float progressValue = (points - minValue)/(maxValue - minValue);
                float maxLimit = 1;

                holder.bar.setProgress(progressValue);
                holder.barText.setText(""+(int) points);
                holder.bar.setMax(1);
                holder.bar.setSecondaryProgress(1);
                if((int) points <= (int) allowed){
                    holder.bar.setProgressColor(Color.parseColor(getColor(null)));
                }else{
                    holder.bar.setProgressColor(Color.parseColor(getColor(GreenDriveData.TYPE.SPEED)));
                }
                holder.bar.setSecondaryProgressColor(Color.parseColor(getColor(GreenDriveData.TYPE.IDLING)));
                holder.bar.setProgressBackgroundColor(Color.parseColor(getColor(GreenDriveData.TYPE.IDLING)));
            }else if(alert == null || alert.getTitle() == null){
                row.setVisibility(View.GONE);
            }

            if(row != null) {
                row.setTag(holder);
            }
        }

        return row;
    }

    private String getColor(GreenDriveData.TYPE type){
        String color = "#ADCF6E";
        if (type != null) {
            switch (type) {
                case ACCEL:
                    color = activity.getResources().getString(R.string.harsh_acc_full);
                    break;
                case SPEED:
                    color = activity.getResources().getString(R.string.harsh_speed_full);
                    break;
                case BREAKING:
                    color = activity.getResources().getString(R.string.harsh_break_full);
                    break;
                case IDLING:
                    color = activity.getResources().getString(R.string.harsh_idle_full);
                    break;
                case CORNERING:
                    color = activity.getResources().getString(R.string.harsh_cornering_full);
                    break;
            }
        }
        return color;
    }

    class Holder {
        TextView title;
        TextView value;
        RoundCornerProgressBar bar;
        TextView barText;

        public TextView getBarText() {
            return barText;
        }

        public void setBarText(TextView barText) {
            this.barText = barText;
        }

        public RoundCornerProgressBar getBar() {
            return bar;
        }

        public void setBar(RoundCornerProgressBar bar) {
            this.bar = bar;
        }

        public TextView getTitle() {
            return title;
        }

        public void setTitle(TextView title) {
            this.title = title;
        }

        public TextView getValue() {
            return value;
        }

        public void setValue(TextView value) {
            this.value = value;
        }
    }
}
