package com.waypals.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.waypals.documentcase.DocImageData;
import com.waypals.documentcase.DocumentViewerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surya on 20/1/16.
 */
public class ImageViewerAdapter extends FragmentStatePagerAdapter {

    List<DocImageData> lstData = new ArrayList<>();

    private Context context;

    public ImageViewerAdapter(FragmentManager fm, List<DocImageData> data, Context context) {
        super(fm);
        this.lstData = data;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        if(lstData != null)
        {
            DocImageData d = lstData.get(position);
            DocumentViewerFragment documentViewerFragment = DocumentViewerFragment.newInstance(position, d.getTitle(), d.getImageUrl(), d.isLink(), d.getDocumentId(), this.context);

            return documentViewerFragment;
        }

        return null;
    }

    @Override
    public int getCount() {

        if(lstData != null)
        {
            return lstData.size();
        }
        return 0;
    }
}
