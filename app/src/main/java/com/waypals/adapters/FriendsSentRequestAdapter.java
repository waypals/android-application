package com.waypals.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.response.FriendInfo;
import com.waypals.utils.CH_Constant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shantanu on 29/1/15.
 */
public class FriendsSentRequestAdapter extends ArrayAdapter<FriendInfo> {

    static Typeface tf;
    private List<FriendInfo> info = null;
    private Activity activity;
    private String tag = "Friends my friend adapter";

    public FriendsSentRequestAdapter(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
        this.activity = activity;
    }

    public View getView(int position, View row, ViewGroup parent) {

        tf = Typeface.createFromAsset(activity.getAssets(),
                CH_Constant.Font_Typface_Path);

        row = LayoutInflater.from(getContext()).inflate(R.layout.request_sent_detail, parent, false);

        ContentsHolder holder = new ContentsHolder();

        holder.name = (TextView) row
                .findViewById(R.id.textViewFrndName);


        holder.email = (TextView) row
                .findViewById(R.id.textViewFrndEmail);


        holder.reject = (ImageView) row.findViewById(R.id.imageViewRejectRequest);

        holder.name.setTypeface(tf);
        holder.email.setTypeface(tf);
        final FriendInfo content = getItem(position);


        holder.name.setText(content.getFirstName() + " " + content.getLastName());
        holder.email.setText(content.getEmail());

        row.setTag(holder);

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    /*Call friend request reject service*/
                Log.i(tag, "Reject request of email :" + content.getEmail());
                try {
                    Log.i(tag, "Accept request of email :" + content.getEmail());
                    JSONObject friend = new JSONObject();
                    friend.put("friendId", content.getUserId());
                    friend.put("friendCollectionId", content.getFriendConsumerId());
                    JSONObject input = new JSONObject();
                    input.put("friend", friend);

                    ShowMessage(input.toString());
                } catch (Exception e) {
                    ExceptionHandler.handleException(e);
                }
            }
        });


        return row;
    }

    public void ShowMessage(final String input) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Holo_Light_Dialog));

        builder.setMessage("Do you want to cancel this request?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent i = new Intent();
                        i.setAction("CANCEL_REQUEST");
                        i.putExtra("input", input);
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public int filter(String text) {
        if (info == null) {
            info = new ArrayList<>();
            for (int i = 0; i < getCount(); i++) {
                if (!info.contains(getItem(i))) {
                    info.add(getItem(i));
                }
            }
        } else {
            for (int i = 0; i < getCount(); i++) {
                if (!info.contains(getItem(i))) {
                    info.add(getItem(i));
                }
            }
        }
        text = text.trim();
        if (text.length() == 0 || text.equals("")) {
            this.clear();
            for (FriendInfo i : info) {
                this.add(i);
            }
            this.notifyDataSetChanged();
        } else {
            text = text.toLowerCase();
            this.clear();
            for (FriendInfo i : info) {
                if (i.getEmail().toLowerCase().contains(text) || i.getFirstName().toLowerCase().contains(text) || i.getLastName().toLowerCase().contains(text)) {
                    this.add(i);
                }
            }
            this.notifyDataSetChanged();
        }
        return getCount();
    }

    class ContentsHolder {
        TextView name;
        TextView email;
        ImageView reject;

    }


}
