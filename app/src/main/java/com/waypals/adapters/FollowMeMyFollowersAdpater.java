package com.waypals.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.request.FollowMeUpdateRequest;
import com.waypals.request.TimeRange;
import com.waypals.response.FollowmeUserWrapper;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by shantanu on 17/1/15.
 */
public class FollowMeMyFollowersAdpater extends ArrayAdapter<FollowmeUserWrapper> {
    private static Date startDate, endDate;
    private static String startime = null, endtime = null;
    private static int clickCount = 0;
    ApplicationPreferences appPref;
    Activity activity;
    private CustomDateTimePicker custom;

    public FollowMeMyFollowersAdpater(Activity activity) {
        super(activity, android.R.layout.two_line_list_item);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View row, final ViewGroup parent) {

        final Holder holder = new Holder();
        appPref = new ApplicationPreferences(getContext());
        row = LayoutInflater.from(getContext()).inflate(R.layout.followme_friendlist_row, parent, false);

        holder.name = (TextView) row.findViewById(R.id.name);
        holder.startime = (TextView) row.findViewById(R.id.fromtime);
        holder.endtime = (TextView) row.findViewById(R.id.totime);
        holder.edit = (ImageView) row.findViewById(R.id.followme_accept);
        holder.delete = (ImageView) row.findViewById(R.id.followme_cancel);
        holder.userimage = (ImageView) row.findViewById(R.id.imageViewFrndImage); 
        holder.inEditing = false;
        final FollowmeUserWrapper data = getItem(position);

        if ("You have not invited any one".equals(data.getFollowerUserName().getFirstName())) {
            holder.name.setText(data.getFollowerUserName().getFirstName());
            RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) holder.name.getLayoutParams();
            rl.addRule(RelativeLayout.CENTER_VERTICAL);
            holder.name.setLayoutParams(rl);
            holder.startime.setVisibility(View.GONE);
            holder.endtime.setVisibility(View.GONE);
            holder.edit.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
            holder.userimage.setVisibility(View.GONE);
            row.requestLayout();
            row.findViewById(R.id.to).setVisibility(View.GONE);
        } else {
            holder.name.setText(data.getFollowerUserName().getFullName());

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                startime = Utility.parseDate(data.getRange().getStartTime());
                endtime = Utility.parseDate(data.getRange().getEndTime());

                if(startime != null) {
                    setDateText(holder.startime, sdf.parse(startime));
                    Log.d("StartTime", startime);
                }

                if(endtime != null) {
                    setDateText(holder.endtime, sdf.parse(endtime));
                    Log.d("EndTime", endtime);
                }

            } catch (Exception e) {
                ExceptionHandler.handleException(e);
            }

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                /*Log delete request*/

                    if (!holder.inEditing) {
                        try {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Light_Dialog));
                            dialog.setTitle("Delete Invite");
                            dialog.setMessage("Do you want to delete this invite ?");
                            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent j = new Intent();
                                    j.setAction("stop_user");
                                    j.putExtra("userId", data.getUserId());
                                    j.putExtra("token", data.getToken());
                                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(j);
                                }
                            });
                            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    return;
                                }
                            });
                            dialog.setCancelable(false);
                            dialog.create();
                            dialog.show();
                        } catch (Throwable e) {
                            ExceptionHandler.handleException(e);
                        }

                    } else {
                        String startime = data.getRange().getStartTime();
                        String endtime = data.getRange().getEndTime();

                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                            Date s = sdf.parse(startime);
                            Date e = sdf.parse(endtime);

                            setDateText(holder.startime, s);
                            setDateText(holder.endtime, e);
                            holder.edit.setImageResource(R.drawable.editicon);
                            holder.startime.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
                            holder.endtime.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            holder.inEditing = false;
                            holder.endtime.setOnClickListener(null);
                            holder.startime.setOnClickListener(null);
                            clickCount = 0;
                        }

                    }

                }
            });

            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickCount++;
                    holder.inEditing = true;
                    holder.edit.setImageResource(R.drawable.accept_request_icon);
                    holder.startime.setTextColor(getContext().getResources().getColor(R.color.dark_gray));
                    holder.endtime.setTextColor(getContext().getResources().getColor(R.color.dark_gray));
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                    try {
                        Date s = sdf.parse(data.getRange().getStartTime());
                        startDate = s;
                        Date b = sdf.parse(data.getRange().getEndTime());
                        endDate = b;

                    } catch (Exception ex) {
                        ExceptionHandler.handleException(ex);
                    }

                    try {
                        holder.startime.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CustomDateTimePicker custom = new CustomDateTimePicker(activity, new CustomDateTimePicker.ICustomDateTimeListener() {
                                    @Override
                                    public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                                      String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                                      int hour24, int hour12, int min, int sec, String AM_PM) {

                                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                                        String set_DateTime = df.format(dateSelected);

                                        System.out.println("Formated Date :: " + set_DateTime);
                                        startDate = dateSelected;
                                        startime = set_DateTime;

                                        setDateText(holder.startime, dateSelected);
                                    }

                                    @Override
                                    public void onCancel() {

                                    }


                                });
                                custom.set24HourFormat(false);
                                /**
                                 * Pass Directly current data and time to show when it pop up
                                 */
                                //custom.setDate(Calendar.getInstance());
                                custom.setDate(startDate);

                                custom.showDialog();


                            }
                        });


                        holder.endtime.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CustomDateTimePicker custom = new CustomDateTimePicker(activity, new CustomDateTimePicker.ICustomDateTimeListener() {
                                    @Override
                                    public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                                      String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                                      int hour24, int hour12, int min, int sec, String AM_PM) {

                                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                                        String set_DateTime = df.format(dateSelected);

                                        endtime = set_DateTime;
                                        endDate = dateSelected;
                                        setDateText(holder.endtime, dateSelected);
                                    }

                                    @Override
                                    public void onCancel() {

                                    }


                                });

                                /**
                                 * Pass Directly current time format it will return AM and PM if you set
                                 * false
                                 */
                                custom.set24HourFormat(false);
                                /**
                                 * Pass Directly current data and time to show when it pop up
                                 */
                                custom.setDate(endDate);

                                custom.showDialog();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (clickCount > 1) {
                        Date d = new Date();


                        Log.d("Adapter", "start date: " + startime + "| end date: " + endtime);
                        sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                        try {
                            Date s = sdf.parse(startime);
                            startDate = s;
                            Date b = sdf.parse(endtime);
                            endDate = b;
                            Log.d("Adapter", "start date: " + startDate.toString() + "| end date: " + endDate.toString());
                        } catch (Exception ex) {
                            ExceptionHandler.handleException(ex);
                        }

                        if (startDate.after(endDate)) {
                            Utility.makeNewToast(activity, "Start time must be lesser than end time");
                            return;
                        }

                        if (endDate.before(startDate)) {
                            Utility.makeNewToast(activity, "End time must be greater than start time");
                            return;
                        }


                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Holo_Light_Dialog));
                        builder
                                .setTitle("Update")
                                .setMessage("Do you want to update time?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        if(startime != null) {
                                            Log.d("StartTime", startime);
                                        }

                                        if(endtime != null) {
                                            Log.d("EndTime", endtime);
                                        }

                                        TimeRange range = new TimeRange();
                                        range.setStartTime(startime);
                                        range.setEndTime(endtime);

                                        FollowMeUpdateRequest request = new FollowMeUpdateRequest();
                                        request.setTimeRange(range);
                                        request.setToken(data.getToken());
                                        request.setUserId(data.getUserId());

                                        Gson gson = new Gson();
                                        String input = gson.toJson(request);
                                        Log.d("Follow Me Request", input);
                                        Intent j = new Intent();
                                        j.setAction("update_followme_request");
                                        j.putExtra("input", input);
                                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(j);

                                        clickCount = 0;
                                        holder.inEditing = false;
                                        holder.edit.setImageResource(R.drawable.editicon);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }

                }
            });

            holder.wrapper = data;
        }
        holder.wrapper = data;


        row.setTag(holder);


        return row;
    }

    private void setDateText(TextView view, Date date)
    {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dfTime = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        String set_DateTime = df.format(date);
        String sDateTime = (dfDate.format(date)+"\n"+dfTime.format(date)).toUpperCase();
        view.setText(sDateTime);
    }

    class Holder {
        TextView name, startime, endtime;
        ImageView edit, delete, userimage;
        FollowmeUserWrapper wrapper;
        Boolean inEditing;
    }

}
