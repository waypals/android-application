package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.VehicleAlertsAdapter;
import com.waypals.objects.VehicleAlerts;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.List;

/**
 * Created by shantanu on 4/2/15.
 */
public class Alerts_Screen_New extends Activity implements AsyncFinishInterface{

    private VehicleAlertsAdapter adapter;
    private ListView listView;
    private ApplicationPreferences appPrefs;
    private View loading;
    private String tag = "Alerts Screen New";
    private ServiceResponse serviceResponse;
    private ImageView back_btn;
    private TextView nav_title_txt;
    private Spinner spinner;
    private Typeface tf;
    private SwipeRefreshLayout swipeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alerts_list);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText("ALERT(S)");

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe_alert);
        spinner = (Spinner) findViewById(R.id.right_drop_down);
        spinner.setVisibility(View.INVISIBLE);
        ArrayAdapter<CharSequence> spinnerAdapter = new ArrayAdapter<CharSequence>(getApplication(), R.layout.spinner_text_layout);
        spinnerAdapter.add("All");
        spinnerAdapter.add("Geofence");
        spinnerAdapter.add("Fuel");
        spinnerAdapter.add("Battery");
        spinner.setAdapter(spinnerAdapter);
        appPrefs = new ApplicationPreferences(this);
        listView = (ListView) findViewById(R.id.alert_list);
        loading = getLoadingView();
        listView.addHeaderView(loading);
        adapter = new VehicleAlertsAdapter(this);
        listView.setAdapter(adapter);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if (listView != null && listView.getChildCount() > 0) {
                    boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    boolean enable = firstItemVisible && topOfFirstItemVisible;
                    swipeView.setEnabled(enable);
                }
            }
        });

        swipeView.setColorSchemeColors(Color.parseColor("#406A8C"), Color.parseColor("#F213A1"), Color.parseColor("#13F264"), Color.parseColor("#1317F2"), Color.parseColor("#F2EE13"));
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAlerts();
                        swipeView.setRefreshing(false);

                    }
                }, 10000);
            }
        });

        getAlerts();

    }

    private void getAlerts()
    {
        GenericAsyncService service = new GenericAsyncService(appPrefs,this, CH_Constant.GET_ALERTS, "GET_ALERTS", "", tag, this, true);
        service.execute();
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if("GET_ALERTS".equals(method))
        {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response,ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if(loading != null)
        {
            listView.removeHeaderView(loading);
        }
        if(serviceResponse!=null)
        {
            if("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse()))
            {
                List<VehicleAlerts> alert = serviceResponse.getAlertsResponseList();
                for(VehicleAlerts l : alert)
                {
                    adapter.add(l);
                }
                adapter.notifyDataSetChanged();
            }
            else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(Alerts_Screen_New.this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                }
            }
        }
    }

    private View getLoadingView() {
        View view = LayoutInflater.from(this).inflate(R.layout.loading, null);
        return view;
    }
}
