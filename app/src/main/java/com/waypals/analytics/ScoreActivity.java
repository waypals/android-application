package com.waypals.analytics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.adapters.ScoreAdapter;
import com.waypals.adapters.SpinnerAdapter;
import com.waypals.request.ScoreRequest;
import com.waypals.request.TimeRange;
import com.waypals.response.ScoreResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.trip.TripOverviewActivity;
import com.waypals.trip.TripdataActivity;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.ArrayList;
import java.util.TimeZone;

public class ScoreActivity extends Activity implements AsyncFinishInterface{

    PieChart overAllTripScoreChart;
    PieChart latestTripScoreChart;
    ListView latestGreenList, overAllGreenList;
    ScoreAdapter latestAdapter, overAllAdapter;
//    TextView overAllTripText;
//    TextView latestTripText;

    ApplicationPreferences appPref;
    private String method;
    private RelativeLayout lytMonthSpinner;
    private Spinner spinnerMonth;

    PieData pieData;
    private TextView textGender;


    enum TYPE{
        LATEST_TRIP, OVERALL_TRIP
    }


//    private float[] yData=new float[2];//{65f,35f/*,22.6f,22.6f*/};
//    private String[] xData={"Test1","Test2"/*,"Test3","Test4"*/};
    private ServiceResponse serviceResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        TextView nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setText("DRIVING SCORE");

        ImageView back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        appPref = new ApplicationPreferences(this);
        LinearLayout latestTripPiechartHolder=(LinearLayout) findViewById(R.id.LatestTripPieChart);
        latestTripScoreChart = (PieChart) latestTripPiechartHolder.findViewById(R.id.Piechart);
//        latestTripText=(TextView)latestTripPiechartHolder.findViewById(R.id.TripTypetextView);
        LinearLayout OverAlTripPieLayout = (LinearLayout) findViewById(R.id.OverAllTripPieChart);
        overAllTripScoreChart = (PieChart) OverAlTripPieLayout.findViewById(R.id.Piechart);
//         overAllTripText=(TextView)  OverAlTripPieLayout.findViewById(R.id.TripTypetextView);
        //overAllTripScoreChart

        addDataSet(overAllTripScoreChart);
        addDataSet(latestTripScoreChart);

        latestGreenList = (ListView) findViewById(R.id.latest_list);
        overAllGreenList = (ListView) findViewById(R.id.overall_list);

        latestAdapter = new ScoreAdapter(this);
        overAllAdapter = new ScoreAdapter(this);

        latestGreenList.setAdapter(latestAdapter);
        overAllGreenList.setAdapter(overAllAdapter);

        lytMonthSpinner = (RelativeLayout) findViewById(R.id.layout_month_spinner);

        spinnerMonth = (Spinner) lytMonthSpinner.findViewById(R.id.spinner_drop_down);
        spinnerMonth.setPrompt("Select Month");

        String[] gender = Utility.getLast12Months();
        textGender = (TextView) lytMonthSpinner.findViewById(R.id.text_spinner);
        textGender.setHint("Select Month");
        textGender.setText(gender[0]);
        final SpinnerAdapter<String> spinnerGenderAdapter = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, gender, textGender, spinnerMonth, new SpinnerAdapter.OnItemSelected(){

            @Override
            public void selected(String value) {
                Log.d("Value", value);
                TimeRange time = Utility.getTimeRange(value);
                if(time != null){
                    ScoreActivity.this.getTripScoreData(time.getStartTime(), time.getEndTime(), TYPE.OVERALL_TRIP);
                }
            }
        });

        spinnerMonth.setAdapter(spinnerGenderAdapter);

        textGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerMonth.performClick();
            }
        });

        latestTripScoreChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScoreActivity.this, TripdataActivity.class);
                startActivity(intent);
            }
        });

//        latestTripText.setText("Latest Trip Score");
//        overAllTripText.setText("OverAll Score");
        this.getTripScoreData(null, null, TYPE.LATEST_TRIP);

    }

    private void addDataSet(PieChart pieChart) {

        pieChart.setTouchEnabled(false);
        pieChart.setRotationEnabled(false);
        pieChart.setHoleRadius(85f);

        pieChart.setCenterTextSize(25);
        pieChart.setCenterTextColor(getResources().getColor(R.color.follow_me_deactive));



//        for( int i=0;i<xData.length;i++)
//        {
//            xEntry.add(xData[i]);
//        }

        //create eData set
        //add colors

        //add legends to chart

        Legend legend= pieChart.getLegend();
        legend.setEnabled(false);

        //Set Description

        Description description=new Description();
        description.setEnabled(false);
        pieChart.setDescription(description);

        //create pieData Object

        setTripScore(0, TYPE.LATEST_TRIP);
        setTripScore(0, TYPE.OVERALL_TRIP);
        pieChart.setData(pieData);
        pieChart.invalidate();

    }

    private void getTripScoreData(String startDate, String endDate, TYPE type){
        ScoreRequest request = new ScoreRequest();
        request.setStartTime(startDate);
        request.setEndTime(endDate);
        request.setVehicleId(""+appPref.getVehicle_Selected());
        Gson gson = new Gson();
        String s = gson.toJson(request, ScoreRequest.class);

        String url = CH_Constant.GET_LATEST_TRIP_SCORE;
        if(TYPE.OVERALL_TRIP == type){
            url = CH_Constant.GET_OVERALL_TRIP_SCORE;
        }
        GenericAsyncService service = new GenericAsyncService(appPref,this, url , type.toString(), s, "", this, true);
        service.execute();
    }

    @Override
    public void finish(String response, String method) throws Exception {

        Log.d("Response", response);
            Gson gson = new Gson();
            this.method = method;
            serviceResponse = gson.fromJson(response,ServiceResponse.class);
    }

    /*
       This method sets the latest trip score and updates the pie chart
     */
    private void setTripScore(int value, TYPE type){

        if (TYPE.LATEST_TRIP == type) {
            latestTripScoreChart.setCenterText(value + "/100");
        }else if(TYPE.OVERALL_TRIP == type){
            overAllTripScoreChart.setCenterText(value + "/100");
        }

         float[] yData={value,(100-value)/*,22.6f,22.6f*/};

        ArrayList<PieEntry> yEntry = new ArrayList();
        ArrayList<String> xEntry = new ArrayList();

        for( int i=0;i<yData.length;i++)
        {
            yEntry.add(new PieEntry(yData[i],i));
        }

        PieDataSet pieDataSet=new PieDataSet(yEntry,"");
        pieDataSet.setSliceSpace(0);
        pieDataSet.setValueTextSize(0);

        ArrayList<Integer> colors=new ArrayList<>();
        colors.add(getResources().getColor(R.color.follow_me_dactive_text_color));
        colors.add(getResources().getColor(R.color.sharefeed_border));

        pieDataSet.setColors(colors);

        PieData pie = new PieData(pieDataSet);

        if (TYPE.LATEST_TRIP == type) {
            latestTripScoreChart.setData(pie);
            latestTripScoreChart.invalidate();
        }else if(TYPE.OVERALL_TRIP == type){
            overAllTripScoreChart.setData(pie);
            overAllTripScoreChart.invalidate();
        }

    }

    private void filterData(ScoreAdapter adapter, ScoreResponse response){
        adapter.clear();
        if (response != null) {
            if (response.getGreenDriveDetail() != null && response.getGreenDriveDetail().getHARSH_ACCELERATION() != null && response.getGreenDriveDetail().getHARSH_ACCELERATION().getTitle() != null) {
                adapter.add(response.getGreenDriveDetail().getHARSH_ACCELERATION());
            }
            if (response.getGreenDriveDetail() != null && response.getGreenDriveDetail().getHARSH_BRAKING() != null && response.getGreenDriveDetail().getHARSH_BRAKING().getTitle() != null) {
                adapter.add(response.getGreenDriveDetail().getHARSH_BRAKING());
            }
            if (response.getGreenDriveDetail() != null && response.getGreenDriveDetail().getHARSH_CORNERING() != null && response.getGreenDriveDetail().getHARSH_CORNERING().getTitle() != null) {
                adapter.add(response.getGreenDriveDetail().getHARSH_CORNERING());
            }
            if (response.getGreenDriveDetail() != null && response.getGreenDriveDetail().getIDLING() != null && response.getGreenDriveDetail().getIDLING().getTitle() != null) {
                adapter.add(response.getGreenDriveDetail().getIDLING());
            }
            if (response.getGreenDriveDetail() != null && response.getGreenDriveDetail().getSPEEDING() != null && response.getGreenDriveDetail().getSPEEDING().getTitle() != null) {
                adapter.add(response.getGreenDriveDetail().getSPEEDING());
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPostExecute(Void Result) {

        if(serviceResponse!=null)
        {
            if("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse()))
            {
                ScoreResponse alert = serviceResponse.getScore();
                if(TYPE.LATEST_TRIP.toString().equals(method)) {

                    if (alert != null) {
                        filterData(latestAdapter, alert);
                        setTripScore(alert.getScore(), TYPE.LATEST_TRIP);

                        if (textGender.getText() != null) {
                            TimeRange time = Utility.getTimeRange(textGender.getText().toString());
                            if (time != null) {
                                ScoreActivity.this.getTripScoreData(time.getStartTime(), time.getEndTime(), TYPE.OVERALL_TRIP);
                            }
                        }
                    }else{
                       // Utility.makeNewToast(ScoreActivity.this, "No score data available for this period");
                    }
                }else{
                    filterData(overAllAdapter, alert);
                    if(alert != null && alert.getGreenDriveDetail() != null) {
                        setTripScore(alert.getScore(), TYPE.OVERALL_TRIP);
                    }else if(alert != null){
                        setTripScore(alert.getScore(), TYPE.OVERALL_TRIP);
                        Utility.makeNewToast(ScoreActivity.this, "No score data available for this period");
                    }else{
                        Utility.makeNewToast(ScoreActivity.this, "No score data available for this period");
                    }

                }
            } else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(ScoreActivity.this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }else if("NO_DATA".equalsIgnoreCase(errorMessage)){

                    Utility.makeNewToast(ScoreActivity.this, "No score data available for this period");
                }
                else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                }
            }
        }

    }
}
