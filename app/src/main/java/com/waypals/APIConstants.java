package com.waypals;

import com.waypals.utils.CH_Constant;

public interface APIConstants {

//	public static String SERVER ="http://devicedata.ride.in:8080/jarvis-ui-restfulWS/jarvisWS";

    public static String SERVER = CH_Constant.SERVER;
    String NEW_FRIEND = SERVER + "/friend/newfriend/";
    String INVITE_FRIEND = SERVER + "/friend/invitefriend/";
    String LIST_ACCEPTED_FRIEND = SERVER + "/friend/listacceptedfriends";
    String LIST_REJECTED_FRIEND = SERVER + "friend/listrejectedfriends";
    String LIST_PENDING_FRIEND = SERVER + "/friend/listpendingfriends";
    String LIST_FREIND_REQUEST_SENT_BY_ME = SERVER + "/friend/listfriendsrequestsentbyme";
    String LIST_PENDING_REQUEST_COUNT = SERVER + "/friend/pendingfriendcount";
    String SHARE_FEED = SERVER + "/feed/sharefeed";
    String NEW_FEED = SERVER + "/feed/newfeed";
    String UNFRIEND = SERVER + "/friend/unfriend";


}
