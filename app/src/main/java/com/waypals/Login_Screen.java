package com.waypals;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.Credential;
import com.waypals.gson.vo.UserCredential;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.MobileDeRegistration;
import com.waypals.rest.service.MobileRegistration;
import com.waypals.services.GCMRegisterationService;
import com.waypals.services.GenericAsyncService;
import com.waypals.signup.SignUp;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import io.fabric.sdk.android.Fabric;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicInteger;

public class Login_Screen extends Activity implements AsyncFinishInterface {
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static int connectionTimeOut;
    static String tag = "Login Screen";
    //TextView login_title_txt;
    Button login_btn;
    TextView buy_now_btn;
    EditText user_name_edit, password_edit;
    String user_id_text, passwd_text;
    ApplicationPreferences appPrefs;
    Activity context;
    AsyncFinishInterface asyncFinishInterface;
    String regid;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    String SENDER_ID;
    String response, errorCode;
    String token;
    JSONObject resObject = null;
    String method;
    // private TextView tvUserName;
    //private TextView tvPassword;
    private TextView tvNewUser;
    private TextView tvForgetPassword;
    private ServiceResponse serviceResponse;

    private static ProgressDialog dialog;

    private boolean isAlreadyTriedLogging = false;

    private BroadcastReceiver fragmentHelper = null;
    private final int REQUEST_READ_CONTACTS = 0, REQUEST_READ_CAMERA = 1, REQUEST_READ_LOCATION = 2, REQUEST_READ_PHONE = 3,
        REQUEST_READ_SMS = 4, REQUEST_READ_PHONE_STATE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Answers(), new Crashlytics());
        setContentView(R.layout.login);

      //  Button buyNow = (Button) findViewById(R.id.buy_now_btn);
      //  buyNow.setVisibility(View.INVISIBLE);

        fragmentHelper = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                switch (action) {

                    case CH_Constant.REGISTRATION_FAILED:
                        Log.d(Home_Screen.tag, "Calling De-registration");
                        try {
                            proceedToHome();
                        } catch (Exception e) {
                            ExceptionHandler.handleException(e);
                        }
                        break;


                    case CH_Constant.REGISTRATION_COMPLETE:
                        String token = intent.getStringExtra("token");

                        try {
                            regid = token;
                            appPrefs.saveDeviceRegistrationId(regid);

                            if (!regid.isEmpty() && regid != null) {
                                appPrefs.setDeviceReq(true);
                            }

                           // closeProgressBar();
                            MobileRegistration mr = new MobileRegistration(appPrefs, Login_Screen.this.context, appPrefs.getDeviceRegistrationId());
                            mr.execute();

                        } catch (Exception e) {
                            ExceptionHandler.handleException(e);
                        }
                        break;
                }
            }
        };

        TextView termService = (TextView) findViewById(R.id.term_service);

        if(termService != null)
        {
            termService.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(Login_Screen.this, AboutUs.class);
                    intent.putExtra(AboutUs.ABOUT_US_TITLE, "Terms & Conditions");
                    intent.putExtra(AboutUs.ABOUT_US_TEXT, getString(R.string.terms_conditions));
                    startActivity(intent);
                }
            });
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(CH_Constant.REGISTRATION_FAILED);
        filter.addAction(CH_Constant.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).registerReceiver(fragmentHelper, filter);

        context = this;
        SENDER_ID = getString(R.string.sender_id);

        asyncFinishInterface = this;
        appPrefs = new ApplicationPreferences(this);

        login_btn = (Button) findViewById(R.id.login_btn);

        buy_now_btn = (TextView) findViewById(R.id.buy_now_btn);

        buy_now_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login_Screen.this, OnlineStoreActivity.class);
                startActivity(intent);
            }
        });

        user_name_edit = (EditText) findViewById(R.id.username_et);
        password_edit = (EditText) findViewById(R.id.password_et);
        //login_title_txt = (TextView) findViewById(R.id.login_title_txt);

        //  tvUserName = (TextView) findViewById(R.id.username_et);
        //tvPassword = (TextView) findViewById(R.id.password_et);
        tvNewUser = (TextView) findViewById(R.id.title_newUSer);
        tvForgetPassword = (TextView) findViewById(R.id.title_forgot_passwrd);

        tvForgetPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ForgetPassword.class);
                startActivity(i);
                finish();
            }
        });


        //appPrefs.saveChleonLog(false);
        //appPrefs.clear();

        tvNewUser.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Utility.openUrl(context, getString(R.string.register_user_url));
                Intent signUpIntent = new Intent(Login_Screen.this, SignUp.class);
                startActivity(signUpIntent);
            }
        });

        Typeface tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);
        //login_title_txt.setTypeface(tf);
        login_btn.setTypeface(tf);
        //tvUserName.setTypeface(tf);
        //tvPassword.setTypeface(tf);
        tvNewUser.setTypeface(tf);
        tvForgetPassword.setTypeface(tf);

        login_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                user_id_text = user_name_edit.getText().toString();
                passwd_text = password_edit.getText().toString();
                if (user_id_text.equals("") || passwd_text.equals("")) {
                    try {
                        Utility.makeNewToast(context, getString(R.string.error_login));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                } else {
                    if (CH_Constant.isNetworkAvailable(Login_Screen.this)) {
//                        String login_json = "{ userCredential: {login:"
//                                + user_id_text + ", password:"
//                                + passwd_text + " } }";
                        UserCredential userCredential = new UserCredential();

                        Credential credential = new Credential();
                        credential.setLogin(user_id_text);
                        credential.setPassword(passwd_text);
                        userCredential.setUserCredential(credential);

                        String login_json = new Gson().toJson(userCredential, UserCredential.class);

                        try {
                            appPrefs.setDeviceReq(false);
                            appPrefs.saveDeviceRegistrationId(null);
                            GenericAsyncService service =
                                    new GenericAsyncService(appPrefs, context,
                                            CH_Constant.Login_Api, "login",
                                            login_json.toString(), tag, asyncFinishInterface, true);
                            service.execute();
                        } catch (Exception e) {
                            ExceptionHandler.handleException(e, context);
                        }

                    } else {
                        try {
                            Utility.makeNewToast(context, CH_Constant.NO_NETWORK);
                        } catch (Exception e) {
                            ExceptionHandler.handlerException(e, context);
                        }
                    }
                }
            }
        });

        ImageView imgView = (ImageView) findViewById(R.id.aeris_logo);

        if("aeris".equals(BuildConfig.FLAVOR.toString())){
            imgView.setVisibility(View.VISIBLE);
        }else{
            imgView.setVisibility(View.GONE);
        }

        if(checkPermission()) {
            validateOldLogin(appPrefs);
        }
    }

    private boolean checkPermission(){
        // Here, thisActivity is the current activity
        if ((ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA, Manifest.permission.SEND_SMS, Manifest.permission.READ_CONTACTS,
                                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_READ_PHONE_STATE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            return false;
        }


        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    validateOldLogin(appPrefs);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
    }


    @Override
    public void finish(String response, String method) throws Exception {
        try {
            this.method = method;

            if ("login".equals(method) || "relogin".equals(method)) {
                if (response != null) {
                    resObject = new JSONObject(response);
                } else {
                    try {
                        Utility.makeNewToast(context, getString(R.string.err_no_response));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                }
            }
            if ("ValidateLogin".equals(method)) {
                Gson gson = new Gson();
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }

            if("MOBILE_REG".equalsIgnoreCase(method)) {
                Log.d(tag, "Mobile App reg. Response: " + response);
                resObject = new JSONObject(response);
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, context);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (appPrefs == null) {
                appPrefs = new ApplicationPreferences(this);
            }
//            if (!appPrefs.getLogout() && Utility.isNetworkAvailable(this)) {
//                Intent i = new Intent(this, Home_Screen.class);
//                startActivity(i);
//                finish();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void relogin(){
        isAlreadyTriedLogging = true;

        if (!Utility.isStringNullEmpty(appPrefs.getUserName()) && !Utility.isStringNullEmpty(appPrefs.getUserPassword())) {

            if (CH_Constant.isNetworkAvailable(Login_Screen.this)) {
                String login_json = "{ userCredential: {login:"
                        + appPrefs.getUserName() + ", password:"
                        + appPrefs.getUserPassword() + " } }";
                try {
                    appPrefs.setDeviceReq(false);
                    appPrefs.saveDeviceRegistrationId(null);
                    GenericAsyncService service = new GenericAsyncService(appPrefs, context, CH_Constant.Login_Api, "relogin", login_json.toString(), tag, asyncFinishInterface, true);
                    service.execute();
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, context);
                }

            } else {
                try {
                    Utility.makeNewToast(context, CH_Constant.NO_NETWORK);
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, context);
                }
            }
        }
    }

    @Override
    public synchronized void onPostExecute(Void Result) {
        try {
            if ("login".equals(method) || "relogin".equals(method)) {

                if (resObject != null) {
                    response = resObject.getString("response");
                    if ("SUCCESS".equalsIgnoreCase(response)) {
                        appPrefs.saveLogout(false);
                        appPrefs.saveFirstTimeLoggedIn(true);
                        JSONObject resultObj = resObject.getJSONObject("result");
                        token = resultObj.getString("token");
                        /*Now check for device registration for push*/
                        if ( !appPrefs.getDeviceReg() && appPrefs.getDeviceRegistrationId() == null) {
                        //if(false){
                            Log.d(tag, "Application not registered");
                            setup();
                        } else {
                            Log.d(tag, "Application already registered with " + appPrefs.getUserName());
                            /*Now check for change user action*/
                            if (!user_id_text.equals(appPrefs.getUserName())) {
                                Log.d(tag, "Account has been changed. Re registering device");
                                Utility.onUserChanged(appPrefs);
                                setup();
                            } else {
                                Intent i = new Intent(Login_Screen.this,
                                        Home_Screen.class);
                                startActivity(i);
                                finish();
                            }
                        }

                        if("login".equals(method)) {
                            appPrefs.saveUserName(user_id_text);
                            appPrefs.saveUserPassword(passwd_text);
                        }
                        appPrefs.saveUserToken(resultObj.getString("token"));
                    } else {
                        errorCode = resObject.getString("errorCode");
                        try {
                            Utility.makeNewToast(context, CH_Constant.ERROR_Msg(errorCode));
                        } catch (Exception e) {
                            ExceptionHandler.handlerException(e, context);
                        }
                    }
                } else {
                    try {
                        Utility.makeNewToast(context, getString(R.string.err_no_response));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                }
            }
            if (method.equals("ValidateLogin")) {
                if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    loggedIn();
                } else
                {
                    if (!isAlreadyTriedLogging && !appPrefs.getLogout()) {
                        relogin();
                    }else{
                        appPrefs.setDeviceReq(false);
                        appPrefs.saveDeviceRegistrationId(null);
                    }
                }
            }

            if("MOBILE_REG".equalsIgnoreCase(method))
            {
                if (resObject != null && "SUCCESS".equalsIgnoreCase(resObject.optString("response"))) {
                    Log.d(tag, "mobile app reg successful");
                    proceedToHome();

                }   else {
                    Log.e("Error", "Mobile registration failed");
                    Utility.makeNewToast(this.context, "Mobile registration failed");
                }
            }

        } catch (Exception e) {
            ExceptionHandler.handlerException(e, context);
        }
    }

    private void proceedToHome(){
        Intent i = new Intent(Login_Screen.this,
                Home_Screen.class);
        startActivity(i);
        finish();
    }

    private void loggedIn()
    {
        appPrefs.saveLogout(false);
        appPrefs.saveFirstTimeLoggedIn(false);
        Intent i = new Intent(Login_Screen.this,
                Home_Screen.class);
        startActivity(i);
        finish();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(tag, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void registerInBackground() {

        Intent intent = new Intent(context, GCMRegisterationService.class);
        intent.putExtra("type",CH_Constant.REGISTRATION_COMPLETE);
        startService(intent);

    }

    public void showProgressBar(Context context)
    {
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void closeProgressBar()
    {
        if( dialog != null) {
            dialog.dismiss();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fragmentHelper);
    }


    public synchronized void setup() {
        if (checkPlayServices()) {
          //  gcm = GoogleCloudMessaging.getInstance(this);
            registerInBackground();
        } else {
            Log.i(tag, "No valid Google Play Services APK found.");
        }
    }

    private String getRegistrationId() {
        String registrationId = appPrefs.getDeviceRegistrationId();
        return registrationId;
    }


    private void validateOldLogin(ApplicationPreferences appPref) {
        try {
            if(appPrefs.getLogout())
            {
                appPrefs.setDeviceReq(false);
                appPrefs.saveDeviceRegistrationId(null);
            }
            else
            {
                if(CH_Constant.isNetworkAvailable(this)) {
                    GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_SETTINGS + "/" + appPref.getVehicle_Selected(), "ValidateLogin", "", "Login Screen", this, true);
                    service.execute();
                }else {
                        loggedIn();
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, context);
        }
    }

}
