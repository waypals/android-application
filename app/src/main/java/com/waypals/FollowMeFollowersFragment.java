package com.waypals;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.FollowMeMyFollowersAdpater;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Name;
import com.waypals.response.FollowmeUserWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONObject;

public class FollowMeFollowersFragment extends Fragment implements AsyncFinishInterface {

    private AsyncFinishInterface asyncFinishInterface;
    private ApplicationPreferences preferences;

    private OnFragmentInteractionListener mListener;
    private FollowMeMyFollowersAdpater adapter;
    private ListView listView;
    private ApplicationPreferences prefs;
    private ServiceResponse serviceResponse;
    private String tag = "Follow me follower fragment";
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            switch (action) {
                case "stop_user":
                    try {
                        Log.d(tag, "broad cast received to stop user from following");
                        Long userid = intent.getLongExtra("userId", -1);
                        String token = intent.getStringExtra("token");

                        JSONObject input = new JSONObject();
                        input.put("userId", userid);
                        input.put("token", token);

                        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_STOP_FOLLOWME, "STOP_USR_TO_FOLLOWME", input.toString(), "", asyncFinishInterface, false);
                        service.execute();

                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                    break;

                case "update_followme_request":
                    Log.d(tag, "broad cast received to update follow me request");
                    try {
                        String input = intent.getStringExtra("input");
                        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_UPDATE_REQUEST, "UPDATE_FOLLOWME", input, tag, asyncFinishInterface, true);
                        service.execute();
                    } catch (Exception ex) {
                        ExceptionHandler.handleException(ex);
                    }
                    break;
            }
        }
    };
    private String method;
    private Activity activity;
    private View loading;

    public FollowMeFollowersFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        IntentFilter filter = new IntentFilter();
        filter.addAction("stop_user");
        filter.addAction("update_followme_request");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
        asyncFinishInterface = this;
        View view = inflater.inflate(R.layout.fragment_follow_me_followers, container, false);
        try {
            adapter = new FollowMeMyFollowersAdpater(getActivity());
            prefs = new ApplicationPreferences(getActivity());
            JSONObject input = new JSONObject();
            input.put("vehicleId", prefs.getVehicle_Selected());

            listView = (ListView) view.findViewById(R.id.listView);
            loading = getLoadingView();
            listView.addHeaderView(loading);
            listView.setAdapter(adapter);

            GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_FOLLOWERS, "FOLLOWERS", input.toString(), tag, this, false);
            service.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        Log.d(tag, "response: " + response + "|method:" + method);
        if ("FOLLOWERS".equals(method)) {
            Gson gson = new Gson();
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
        if ("STOP_USR_TO_FOLLOWME".equals(method) || "UPDATE_FOLLOWME".equals(method)) {
            Gson gson = new Gson();
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        try {
            if (loading != null) {
                listView.removeHeaderView(loading);
            }

            if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(getActivity(),
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(getActivity(), Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                } else {
                    Utility.makeNewToast(getActivity(), "Server not responding, please try after some time!!");
                }

                return;
            }

            if (method.equals("FOLLOWERS")) {
                adapter.clear();
                if (serviceResponse != null && serviceResponse.getResponse().equals("SUCCESS")) {
                    if (serviceResponse.getListFollowMeUserWrapper() != null && serviceResponse.getListFollowMeUserWrapper().size() > 0) {
                        for (FollowmeUserWrapper u : serviceResponse.getListFollowMeUserWrapper()) {

                            adapter.add(u);
                            Log.d(tag, "added data in adapter " + u);
                        }
                    } else {
                        FollowmeUserWrapper blank = new FollowmeUserWrapper();
                        Name name = new Name();
                        name.setFirstName("You have not invited any one");
                        blank.setFollowerUserName(name);
                        adapter.add(blank);
                    }
                    adapter.notifyDataSetChanged();
                    Log.d(tag, "added data in adapter");
                }
            }
            if (method.equals("STOP_USR_TO_FOLLOWME")) {
                if (serviceResponse != null && serviceResponse.getResponse().equals("SUCCESS")) {
                    try {
                        Utility.makeNewToast(getActivity(), "Invite has been removed");
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            }
            if (method.equals("UPDATE_FOLLOWME")) {
                if (serviceResponse != null && serviceResponse.getResponse().equals("SUCCESS")) {
                    Utility.makeNewToast(getActivity(), "Request has been updated");

                    JSONObject input = new JSONObject();
                    input.put("vehicleId", prefs.getVehicle_Selected());
                    GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_FOLLOWERS, "FOLLOWERS", input.toString(), tag, asyncFinishInterface, true);
                    service.execute();
                } else {
                    Utility.makeNewToast(getActivity(), "Could not update request");
                }
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, getActivity());
        }
    }

    private View getLoadingView() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.loading, null);
        return view;
    }


    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }
}
