package com.waypals;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedAdapter.FeedAsyncInterface;
import com.waypals.feedAdapter.FeedHelper;
import com.waypals.feedAdapter.FeedMethodEnum;
import com.waypals.feedImpl.AppController;
import com.waypals.feedImpl.FeedImageView;
import com.waypals.feedItems.Comments;
import com.waypals.feedItems.Feed;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.feedItems.ImageContent;
import com.waypals.feedItems.Images;
import com.waypals.feedItems.JourneyFeedContent;
import com.waypals.feedItems.RouteCoordinates;
import com.waypals.feedItems.WayPoints;
import com.waypals.gson.vo.FeedContentDeserializer;
import com.waypals.map.TrailActivity;
import com.waypals.request.FeedRequest;
import com.waypals.request.Pagination;
import com.waypals.response.FeedContentResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.services.GetMyFeedsServices;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.wareninja.opensource.common.ObjectSerializer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static com.wareninja.opensource.common.ObjectSerializer.serialize;

public class MyFeed extends FeedHelper implements FeedAsyncInterface, AsyncFinishInterface {

    final static String fromBottom = "BOTTOM";
    final static String fromTop = "TOP";
    private static final int IMAGE_SELECT_ACTION = 100;
    AsyncFinishInterface asyncFinishInterface;
    FeedAsyncInterface feedAsyncInterface;
    FeedContentResponse feedContentResponse;
    List<FeedWrapper> feedWrappers;
    ImageView back_btn, right_btn;
    FeedAdapter feedAdapter;
    TextView title;
    Context context;
    Activity activity;
    EditText feedText;
    String fileNamePath = null;
    Boolean isImageAvailable = false;
    Button postFeed;
    ImageView cameraButton;
    AppController appController;
    TextView imageSelected;
    ServiceResponse serviceResponse;
    View footerView;
    String method;
    Typeface tf;
    FeedMethodEnum feedMethodEnum;
    String requestFrom;
    FeedWrapper selectedFordelete;
    TextView loadMore;
    SwipeRefreshLayout swipeView;
    /*
    @Params <fileName,imageHexString>
    */
    HashMap<String, String> images = new HashMap<String, String>();
    /*
   @Params <fileName,file path>
   */
    HashMap<String, String> filedetails = new HashMap<String, String>();
    private ApplicationPreferences appPref;
    private String tag = "My Feeds";
    private ListView listView;
    private int pageSize = 10;
    private int currentPage = 1;
    private boolean checkingFeeds = false;
    private int lostFeed = 0;
    private boolean showNotification = true;
    private Boolean onResumeRefresh = true;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_feed);


        asyncFinishInterface = this;
        feedAsyncInterface = this;
        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe);


        context = this;
        activity = this;
        listView = (ListView) findViewById(R.id.list_my_feed);
        appPref = new ApplicationPreferences(this);
        currentPage = appPref.getCurrentFeedPage();

        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setTypeface(tf);

        title.setText("FEED(S)");
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        LayoutInflater layoutInflater = getLayoutInflater();
        footerView = layoutInflater.inflate(R.layout.footer_layout, null, false);

        loadMore = (TextView) footerView.findViewById(R.id.loadmore);
        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNewFeeds(true, "BOTTOM");
            }
        });


        feedAdapter = new FeedAdapter(appPref, activity);
        listView.setAdapter(feedAdapter);

        right_btn = (ImageView) findViewById(R.id.right_btn);
        right_btn.setBackgroundResource(R.drawable.default_user_icon);
        right_btn.setVisibility(View.VISIBLE);

        imageSelected = (TextView) findViewById(R.id.selectedImageName);
        postFeed = (Button) findViewById(R.id.postButton);
        postFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                feedText = (EditText) findViewById(R.id.feedEditText);
                String text = feedText.getText().toString();
                text = text != null ? text.trim() : text;

                if (images == null) {
                    images = new HashMap<String, String>();
                }

                if (filedetails != null) {
                    Set<String> keys = filedetails.keySet();
                    Log.d(tag, "File Details: " + filedetails.values());
                    for (String filename : keys) {
                        Log.d(tag, "File:" + filename + "|Image hex string : " + Utility.getImageToHex(activity, filedetails.get(filename).toString()));
                        images.put(filename, Utility.getImageToHex(activity, filedetails.get(filename).toString()));
                    }
                } else {
                    images = null;
                }


                Log.d(tag, "List of images " + (images != null ? images : "not found"));

                if (!text.isEmpty()) {
                    FeedRequest feedRequest = new FeedRequest();
                    String input = feedRequest.createNewFeed(text, images);
                    Log.d(tag, "create feed input : " + input);
                    GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.CREATE_NEW_FEED, FeedMethodEnum.NEW_FEEDS.name(), input.toString(), tag, asyncFinishInterface, true);
                    service.execute();

                    feedText.setText("");
                    fileNamePath = null;
                } else if (images != null && images.size() != 0) {
                    FeedRequest feedRequest = new FeedRequest();
                    String img = Utility.getImageToHex(activity, fileNamePath);
                    String input = feedRequest.createNewFeed("", images);
                    Log.d(tag, "create feed input : " + input);

                    img = img != null ? img.trim() : img;

                    if (img != null || !img.isEmpty()) {
                        GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.CREATE_NEW_FEED, FeedMethodEnum.NEW_FEEDS.name(), input.toString(), tag, asyncFinishInterface, true);
                        service.execute();
                    } else {
                        return;
                    }
                    feedText.setText("");
                    fileNamePath = null;
                } else {
                    try {
                        Utility.makeNewToast(activity, "Please enter your message");
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e, activity);
                    }
                    return;
                }

                imageSelected = (TextView) findViewById(R.id.selectedImageName);
                imageSelected.setText("");
                Utility.clearTempImgDir();

                filedetails = null;
                images = null;
            }
        });

        cameraButton = (ImageView) findViewById(R.id.camera_button);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, Camera_Image_Container.class);
                i.putExtra("files", ObjectSerializer.serialize(images));
                setOnResumeRefresh(false);
                i.putExtra("filesDetails", ObjectSerializer.serialize(filedetails));
                startActivityForResult(i, IMAGE_SELECT_ACTION);
            }
        });


        // We first check for cached request
        appController = new AppController(context).getInstance();
        if (appController != null) {
            loadCachedFeeds();
        } else {
            Log.d(tag, "getting null controller");
        }

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (listView != null && listView.getChildCount() > 0) {
                    boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    boolean enable = firstItemVisible && topOfFirstItemVisible;
                    swipeView.setEnabled(enable);
                }

                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    if (listView.getFooterViewsCount() == 0) {
                        Log.d(tag, "adding new footer view");
                        listView.addFooterView(footerView);
                    } else {
                        Log.d(tag, "footer view already exists");
                    }
                    Log.d(tag, "I am on the bottom...");
                }

            }
        });

        swipeView.setColorSchemeColors(Color.parseColor("#406A8C"), Color.parseColor("#F213A1"), Color.parseColor("#13F264"), Color.parseColor("#1317F2"), Color.parseColor("#F2EE13"));
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cleanLoad(true);
                        swipeView.setRefreshing(false);

                    }
                }, 10000);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        onResumeRefresh = getOnResumeRefresh();
    }

    public Boolean getOnResumeRefresh() {
        return onResumeRefresh;
    }

    public void setOnResumeRefresh(Boolean onResumeRefresh) {
        this.onResumeRefresh = onResumeRefresh;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (onResumeRefresh) {
                Log.d(tag, "Resuming...");
                cleanLoad(false);
            }

        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(tag, "distroying...");
    }

    @Override
    public void cleanLoad(boolean b) {
        appPref.clearFeeds();
        appPref.saveLoadedFeeds(0);
        checkNewFeeds(b, fromTop);
    }

    @Override
    public void finish(String response, FeedMethodEnum method, String requestFrom, Boolean showNotification) throws Exception {
        try {
            this.feedMethodEnum = method;
            this.showNotification = showNotification;
            this.requestFrom = requestFrom;
            Log.d("FEED Response", response);
            Gson gson = new GsonBuilder().registerTypeAdapter(Feed.class, new FeedContentDeserializer()).create();
            if (this.feedMethodEnum == FeedMethodEnum.MY_FEEDS) {
                feedContentResponse = gson.fromJson(response, FeedContentResponse.class);
                if (feedContentResponse != null &&
                        feedContentResponse.getFeedResponseList() != null &&
                        feedContentResponse.getFeedResponseList()[0] != null) {
                    Log.d("Feed Content:", feedContentResponse.getFeedResponseList()[0].toString());
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        try {
            this.method = method;
            Gson gson = new GsonBuilder().registerTypeAdapter(Feed.class, new FeedContentDeserializer()).create();
            Log.d("FEED Response", response);
            if (method.equals(FeedMethodEnum.NEW_FEEDS.name())) {
                this.feedMethodEnum = FeedMethodEnum.NEW_FEEDS;
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
            if (method.equalsIgnoreCase(FeedMethodEnum.DELETE_FEED.name())) {
                this.feedMethodEnum = FeedMethodEnum.DELETE_FEED;
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        try {

            if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) || (feedContentResponse != null && "ERROR".equalsIgnoreCase(
                    feedContentResponse.getResponse())))  {
                String errorMessage = null;
                if(serviceResponse != null) {
                    errorMessage = serviceResponse.getErrorCode();
                }else if(feedContentResponse != null)
                {
                    errorMessage = feedContentResponse.getErrorCode();
                }
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }

                return;
            }


            if (this.feedMethodEnum == FeedMethodEnum.DELETE_FEED) {
                if (serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    Log.d(tag, "selected for delete: " + selectedFordelete);
                    appPref.removeFeedFromCache(selectedFordelete);
                    Utility.makeNewToast(activity, "Feed deleted !");
                    feedAdapter.remove(selectedFordelete);
                    feedAdapter.notifyDataSetChanged();
                } else {
                    Utility.makeNewToast(activity, "Please try again later!");
                }
            }

            if (this.feedMethodEnum == FeedMethodEnum.MY_FEEDS) {


                if (feedContentResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    appPref.saveAvailableFeedsCount(feedContentResponse.getTotalNumberOfRecords());
                    if (checkingFeeds) {
                        if (appPref.getAvailableFeedsCount() == 0) {
                            try {
                                Utility.makeNewToast(activity, "You do not have any feeds. Start Posting now");
                                feedAdapter.clear();
                                feedAdapter.notifyDataSetChanged();
                                appPref.clearFeeds();
                                appPref.saveLoadedFeeds(0);
                                return;
                            } catch (Exception e) {
                                ExceptionHandler.handleException(e, activity);
                            }
                        }
                        Log.d(tag, "Checking feeds response");
                        if (appPref.getLoadedFeeds() < appPref.getAvailableFeedsCount()) {
                            getFeeds();
                            return;
                        } else {
                            Log.d(tag, "Checking feeds response| No more data");
                            if (requestFrom.equals(fromBottom)) {
                                loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                                loadMore.setText(getString(R.string.inp_loading));
                                listView.removeFooterView(footerView);
                            }

                            if (this.showNotification) {
                                Utility.makeNewToast(activity, "Feeds are up to date");
                                loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                                loadMore.setText(getString(R.string.inp_loadmore));
                                checkingFeeds = false;
                            }
                            requestExists = false;
                            return;
                        }
                    } else {

                        requestExists = false;
                        if (requestFrom.equals(fromBottom)) {
                            listView.removeFooterView(footerView);
                        }
                        if (appPref.getLoadingFeedForFirstTime()) {
                            Log.d(tag, "feeds loaded for first time");
                            appPref.saveLoadingFeedForFirstTime(false);
                        }
                        /*depending upon request from call methods*/
                        if (this.requestFrom.equalsIgnoreCase(fromTop)) {
                            Log.d(tag, "loading New feeds");
                            loadNewFeeds();
                            if (listView.getFooterViewsCount() == 0) {
                                listView.addFooterView(footerView);
                            }
                        }
                        if (this.requestFrom.equalsIgnoreCase(fromBottom)) {
                            Log.d(tag, "loading Old feeds");
                            loadOldFeeds();
                            if (this.requestFrom.equals(fromBottom)) {
                                loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                                loadMore.setText(getString(R.string.inp_loadmore));
                            }
                        }
                    }

                } else {
                    try {
                        Utility.makeNewToast(activity, CH_Constant.ERROR_Msg(feedContentResponse.getErrorCode()));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                }

                /*Now allow to check new feeds*/
                requestExists = false;
            }
            if (this.feedMethodEnum == FeedMethodEnum.NEW_FEEDS) {
                if (serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    cleanLoad(false);
                    requestExists = false;
                    Utility.makeNewToast(this, "Feed posted !");
                } else {
                    Utility.makeNewToast(this, "Server not responding. Please try again later");
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public Pagination getPaginator() {
        Pagination pg = new Pagination();
        if (appPref.getLoadingFeedForFirstTime()) {
            pg.setPageNumber(String.valueOf(currentPage));
            pg.setPageSize(String.valueOf(pageSize));
        } else {

            int available = appPref.getAvailableFeedsCount();
            int loaded = appPref.getLoadedFeeds();
            int remaining = available - loaded;
            lostFeed = pageSize - (loaded % pageSize);
            int pageNumber = loaded / pageSize;
            pageNumber++;

            if (lostFeed == 0) {
                ++pageNumber;
            }

            if (remaining < pageSize) {
                lostFeed = remaining;
                appPref.saveIsLostFeedAvailable(true);
            } else {
                appPref.saveIsLostFeedAvailable(false);
            }
            pg.setPageNumber(String.valueOf(pageNumber));
            pg.setPageSize(String.valueOf(pageSize));
            Log.d(tag, "available:" + available + "|loaded:" + loaded + "|remaining:" + remaining + "|pageNumber:" + pageNumber + "|lost:" + lostFeed);
        }
        return pg;
    }

    @Override
    public synchronized void checkNewFeeds(boolean overrideShowNotification, String requestFrom) {
        try {
            if (!requestExists) {
                if (requestFrom.equals(fromBottom)) {
                    loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                    loadMore.setText(getString(R.string.inp_loading));
                }

                Log.d(tag, "checkNewFeeds|checking for available feeds");
                checkingFeeds = true;
                showNotification = overrideShowNotification;
                Pagination pg = new Pagination();
                pg.setPageNumber("1");
                pg.setPageSize("1");
                requestExists = true;
                GetMyFeedsServices myFeedsServices = new GetMyFeedsServices(appPref, activity, pg, feedAsyncInterface, requestFrom, overrideShowNotification);
                myFeedsServices.execute();
            } else {
                Log.d(tag, "Request already exists");
            }
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }
    }

    @Override
    public synchronized void loadNewFeeds() {
        Log.d(tag, "**********************************NEW FEED LOADING************************************************************");
        Log.d(tag, " before loaded feeds:" + appPref.getLoadedFeeds() + "|available: " + appPref.getAvailableFeedsCount());
        if (appPref.getLoadedFeeds() < appPref.getAvailableFeedsCount()) {
            feedAdapter.clear();
            feedAdapter.notifyDataSetChanged();

            int count = 0;
            for (FeedWrapper feedWrapper : feedContentResponse.getFeedResponseList()) {
                count = count + 1;
                feedAdapter.add(feedWrapper);
                appPref.addFeed(feedWrapper);
                Log.d("Feed", feedWrapper.toString() + " | count : " + count);
            }

            feedAdapter.notifyDataSetChanged();
            appPref.saveLoadedFeeds(count + appPref.getLoadedFeeds());

            if (this.showNotification) {
                try {
                    Utility.makeNewToast(activity, "Feed updated");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, activity);
                }
            }
        } else {
            if (this.showNotification) {
                try {
                    Utility.makeNewToast(activity, "Feeds are up to date");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, activity);
                }
            }
        }
        requestExists = false;
        Log.d(tag, "after loaded feeds:" + appPref.getLoadedFeeds() + "|availble: " + appPref.getAvailableFeedsCount());
    }

    @Override
    public synchronized void loadOldFeeds() {
        Log.d(tag, "**********************************OLD FEED LOADING************************************************************");
        Log.d(tag, " before loaded feeds:" + appPref.getLoadedFeeds() + "|available: " + appPref.getAvailableFeedsCount());
        if (appPref.getIsLostFeedAvailable()) {
            Log.d(tag, "Newly added feed available of size: " + lostFeed);
            int count = 0;
            for (FeedWrapper feedWrapper : feedContentResponse.getFeedResponseList()) {
                feedAdapter.add(feedWrapper);
                appPref.addFeed(feedWrapper);
                Log.d("Feed", feedWrapper.toString() + " | count : " + count);
                if (count == lostFeed) {
                    break;
                }
                ++count;
            }
            feedAdapter.notifyDataSetChanged();
            appPref.saveLoadedFeeds(count + appPref.getLoadedFeeds());
            lostFeed = 0;
            appPref.saveIsLostFeedAvailable(false);
        } else if (appPref.getLoadedFeeds() < appPref.getAvailableFeedsCount()) {
            int count = 0;
            for (FeedWrapper feedWrapper : feedContentResponse.getFeedResponseList()) {
                count = count + 1;
                feedAdapter.add(feedWrapper);
                appPref.addFeed(feedWrapper);
                Log.d("Feed", feedWrapper.toString() + " | count : " + count);

            }
            feedAdapter.notifyDataSetChanged();
            appPref.saveLoadedFeeds(count + appPref.getLoadedFeeds());
            if (showNotification) {
                try {
                    Utility.makeNewToast(activity, "Feed updated");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, activity);
                }
            }
        } else {
            try {
                Utility.makeNewToast(activity, "No more data available !");
            } catch (Exception e) {
                ExceptionHandler.handleException(e, activity);
            }
        }
        listView.removeFooterView(footerView);
        requestExists = false;
        Log.d(tag, "after loaded feeds:" + appPref.getLoadedFeeds() + "|available: " + appPref.getAvailableFeedsCount());
    }

    @Override
    public synchronized void getFeeds() {
        try {
            Log.d(tag, "Checking feeds response| Now gettings feeds");
            checkingFeeds = false;
            requestExists = true;
            GetMyFeedsServices myFeedsServices = new GetMyFeedsServices(appPref, activity, getPaginator(), this, this.requestFrom, this.showNotification);
            myFeedsServices.execute();

            appPref.saveLastFeedUpdateTime(System.currentTimeMillis());
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void loadCachedFeeds() {
        try {
            feedWrappers = appPref.getFeeds();
            if (feedWrappers != null) {
                feedAdapter.clear();
                feedAdapter.notifyDataSetChanged();
                for (FeedWrapper f : feedWrappers) {
                    feedAdapter.add(f);
                    Log.d(tag, "loading.. " + f);
                }
                feedAdapter.notifyDataSetChanged();
            } else {
                checkNewFeeds(false, fromTop);
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == IMAGE_SELECT_ACTION) {
            if (resultCode == RESULT_OK) {
                Log.d(tag, "User added images");
                if (data != null) {
                    filedetails = (HashMap<String, String>) ObjectSerializer.deserialize(data.getStringExtra("filesDetails"));


                    Set<String> keys = filedetails.keySet();

                    for (String f : keys) {
                        Log.d(tag, "file: " + f + "|path: " + filedetails.get(f));
                    }


                    if (filedetails.size() > 0) {
                        imageSelected = (TextView) findViewById(R.id.selectedImageName);
                        imageSelected.setText("added images");
                    }
                    Log.d(tag, "User added images size: " + filedetails.size());
                }
            }
            if (resultCode == RESULT_CANCELED) {
                imageSelected = (TextView) findViewById(R.id.selectedImageName);
                imageSelected.setText("");
                images = null;
                filedetails = null;
            }
            setOnResumeRefresh(true);
        }
    }

    class FeedAdapter extends ArrayAdapter<FeedWrapper> {
        ApplicationPreferences appPref;
        Activity activity;
        FeedAdapter fd;
        ImageLoader imageLoader = null;
        String tag = "Feed adapter";
        private boolean mHasMoreItems;

        public FeedAdapter(ApplicationPreferences appPref, Activity activity) {
            super(activity, android.R.layout.two_line_list_item);
            mHasMoreItems = false;
            this.activity = activity;
            this.appPref = appPref;
            this.fd = this;
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            try {
                final Holder holder = new Holder();
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.feed_content_row, parent, false);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.extraText = (TextView) convertView.findViewById(R.id.extraText);
                holder.feedText = (TextView) convertView.findViewById(R.id.feedText);
                holder.timeStamp = (TextView) convertView.findViewById(R.id.timestamp);
                holder.deleteTextView = (TextView) convertView.findViewById(R.id.deleteFeedtxt);
                holder.deleteIcon = (ImageView) convertView.findViewById(R.id.delete_icon);
                holder.imageView1 = (FeedImageView) convertView.findViewById(R.id.feedImage1);
                holder.imageView2 = (FeedImageView) convertView.findViewById(R.id.feedImage2);
                holder.imageView3 = (FeedImageView) convertView.findViewById(R.id.feedImage3);
                holder.commentText = (TextView) convertView.findViewById(R.id.commentOnFeedtxt);
                holder.commentIcon = (ImageView) convertView.findViewById(R.id.comment_icon);
                holder.firstImageRow = (TableRow) convertView.findViewById(R.id.firstRow);
                holder.secondImageRow = (TableRow) convertView.findViewById(R.id.secondRow);
                holder.container = (RelativeLayout) convertView.findViewById(R.id.imageContainer);
                holder.trailContainer =  (LinearLayout) convertView.findViewById(R.id.trail_container);
                holder.trailImage = (FeedImageView) convertView.findViewById(R.id.trail_image);
                holder.shareFeedIcon = (ImageView) convertView.findViewById(R.id.share_feed_icon);
                holder.shareFeedText = (TextView) convertView.findViewById(R.id.shareFeedtxt);
                holder.defaultRow = (TableRow) convertView.findViewById(R.id.defaultRow);

                final FeedWrapper feedWrapper = getItem(position);

                String name = feedWrapper.getFeedUsers().getSharedBy().getName().getFullName();
                Long Id = feedWrapper.getFeed().getOwner().getName().getId();
                String feedText = feedWrapper.getFeed().getContent().getDescription();
                String trailDescription = feedWrapper.getDescription();
                String type = feedWrapper.getFeed().getType();
                String timestamp = feedWrapper.getFeedUpdatedTime();
                Log.d(tag, "Time stamp: " + timestamp);
                final Comments comment[] = feedWrapper.getFeed().getComments();
                if (comment != null) {
                    holder.commentText.setText("Comments(" + comment.length + ")");
                }

                holder.name.setText(name);
                if (feedText != null && !feedText.trim().isEmpty()) {
                    holder.feedText.setText(feedText);
                } else {
                    holder.feedText.setHeight(10);
                    holder.feedText.setVisibility(View.INVISIBLE);
                }

                if (type.equals("TEXT")) {
                    holder.extraText.setText("shared a text");
                    holder.firstImageRow.setVisibility(View.GONE);
                    holder.secondImageRow.setVisibility(View.GONE);
                    holder.container.setVisibility(View.GONE);
                }
                if (type.equals("IMAGE")) {

                    holder.extraText.setText("added image");
                    ImageContent imageContent = (ImageContent) feedWrapper.getFeed().getContent();
                    final Images[] images = imageContent.getImages();
                    if (images != null && images.length == 0) {
                        holder.extraText.setText("shared a text");
                        holder.container.setVisibility(View.GONE);
                    } else {
                        if (images.length == 1) {
                            holder.extraText.setText("added a image");
                            holder.defaultRow.setVisibility(View.VISIBLE);
                            holder.secondImageRow.setVisibility(View.GONE);
                        }
                        if (images.length > 1) {
                            holder.extraText.setText("added images");
                        }
                     /*Depending upone number of images things to do*/

                        holder.container.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (view instanceof FeedImageView) {
                                    Intent i = new Intent(activity, View_Feed_Images.class);
                                    i.putExtra("images", ObjectSerializer.serialize(images));
                                    activity.startActivity(i);
                                }

                            }
                        });


                        try {
                            final ProgressBar p = (ProgressBar) convertView.findViewById(R.id.progressBar);
                            final TextView imgErr = (TextView) convertView.findViewById(R.id.text);
                            switch (images.length) {
                                case 1:

                                    holder.secondImageRow.setVisibility(View.GONE);
                                    holder.imageView1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[0].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    if (images[0].getImageLink() != null) {
                                        holder.imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        holder.imageView1.setVisibility(View.VISIBLE);
                                        holder.imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        //holder.container.setVisibility(View.GONE);
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView1.setVisibility(View.GONE);
                                    }
                                    break;


                                case 2:
                                    holder.firstImageRow.setVisibility(View.GONE);
                                    holder.secondImageRow.setVisibility(View.VISIBLE);
                                    holder.imageView2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[0].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    holder.imageView3.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[1].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    if (images[0].getImageLink() != null) {
                                        holder.imageView2.setImageUrl(images[0].getImageLink(), imageLoader);
                                        holder.imageView2.setVisibility(View.VISIBLE);
                                        holder.imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        //holder.container.setVisibility(View.GONE);
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[1].getImageLink() != null) {
                                        holder.imageView3.setImageUrl(images[1].getImageLink(), imageLoader);
                                        holder.imageView3.setVisibility(View.VISIBLE);
                                        holder.imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        //holder.container.setVisibility(View.GONE);
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView3.setVisibility(View.GONE);
                                    }

                                    break;

                                default:
                                    holder.imageView1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[0].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    holder.imageView2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[1].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    holder.imageView3.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[2].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    if (images[0].getImageLink() != null) {
                                        holder.imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        holder.imageView1.setVisibility(View.VISIBLE);
                                        holder.imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView1.setVisibility(View.GONE);
                                    }
                                    if (images[1].getImageLink() != null) {
                                        holder.imageView2.setImageUrl(images[1].getImageLink(), imageLoader);
                                        holder.imageView2.setVisibility(View.VISIBLE);
                                        holder.imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[2].getImageLink() != null) {
                                        holder.imageView3.setImageUrl(images[2].getImageLink(), imageLoader);
                                        holder.imageView3.setVisibility(View.VISIBLE);
                                        holder.imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView3.setVisibility(View.GONE);
                                    }


                                    break;

                            }

                        } catch (Exception error) {
                            error.printStackTrace();
                            Log.d(tag, "Found Error while loading images|Hiding Image container");
                            holder.container.setVisibility(View.GONE);
                        }
                    }

                }

                if (type.equals("JOURNEY")) {
                    holder.feedText.setText(trailDescription);
                    holder.extraText.setText("shared a trail");
                    JourneyFeedContent journeyFeedContent = (JourneyFeedContent) feedWrapper.getFeed().getContent();
                    Images imgs = journeyFeedContent.getTrailImage();
                    final RouteCoordinates[] rootCoord = journeyFeedContent.getRouteCoordinates();//feedWrapper.getFeed().getContent().getRouteCoordinates();
                    final WayPoints[] wayPointses = journeyFeedContent.getWaypoints();
                    if (holder.container != null && holder.trailContainer != null && imgs != null && imgs.getImageLink() != null) {
                        holder.trailContainer.setVisibility(View.VISIBLE);
                        holder.container.setVisibility(View.GONE);
                        holder.trailImage.setImageUrl(imgs.getImageLink().replace(" ","%20"), imageLoader);

                        holder.trailImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (rootCoord != null) {
                                    Intent mapIntent = new Intent(getContext(), TrailActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("coordinates", rootCoord);
                                    if (wayPointses != null) {
                                        bundle.putSerializable("waypoints", wayPointses);
                                    }

                                    mapIntent.putExtras(bundle);
                                    startActivity(mapIntent);
                                }
                            }
                        });

                        holder.trailImage.setResponseObserver(new FeedImageView.ResponseObserver() {
                            @Override
                            public void onError() {
                                holder.trailContainer.setVisibility(View.GONE);
                            }

                            @Override
                            public void onSuccess() {
                                holder.trailContainer.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    else if(imgs == null || imgs.getImageLink() == null)
                    {
                        holder.trailContainer.setVisibility(View.VISIBLE);
                        holder.container.setVisibility(View.GONE);
                        holder.trailImage.setDefaultImageResId(R.drawable.default_map);
                        holder.trailImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (rootCoord != null) {
                                    Intent mapIntent = new Intent(getContext(), TrailActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("coordinates", rootCoord);
                                    if (wayPointses != null) {
                                        bundle.putSerializable("waypoints", wayPointses);
                                    }

                                    mapIntent.putExtras(bundle);
                                    startActivity(mapIntent);
                                }
                            }
                        });

                    }
                }

                Long time = Utility.getDateToLong(timestamp);
                if (time != null) {
                    CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
                    holder.timeStamp.setText(timeAgo);
                } else {
                    holder.timeStamp.setText(timestamp);
                }

                if (holder.commentIcon != null) {
                    holder.commentIcon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            {
                                setOnResumeRefresh(true);
                                ArrayList<Comments> list = null;
                                if (comment != null) {
                                    list = new ArrayList<Comments>(Arrays.asList(comment));
                                }
                                Intent i = new Intent(activity, CommentActivity.class);
                                String comment = serialize(list);
                                i.putExtra("comments", comment);
                                i.putExtra("feedId", feedWrapper.getFeed().getId());
                                activity.startActivity(i);
                            }

                        }
                    });
                }

                if (holder.commentText != null) {
                    holder.commentText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            {
                                setOnResumeRefresh(true);
                                ArrayList<Comments> list = null;
                                if (comment != null) {
                                    list = new ArrayList<Comments>(Arrays.asList(comment));
                                }
                                Intent i = new Intent(activity, CommentActivity.class);
                                String comment = serialize(list);
                                i.putExtra("comments", comment);
                                i.putExtra("feedId", feedWrapper.getFeed().getId());
                                activity.startActivity(i);
                            }

                        }
                    });
                }

                if (holder.shareFeedIcon != null) {
                    holder.shareFeedIcon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
shareFeed(feedWrapper);
                        }
                    });
                }

                if (holder.shareFeedText != null) {
                    holder.shareFeedText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            shareFeed(feedWrapper);
                        }
                    });
                }



                if (holder.deleteIcon != null) {
                    holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            deleteSelectedFeed(feedWrapper);
                        }
                    });
                }


                if (holder.deleteTextView != null) {
                    holder.deleteTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            deleteSelectedFeed(feedWrapper);
                        }
                    });
                }
                convertView.setTag(holder);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        private void shareFeed(FeedWrapper feedWrapper)
        {
            if(feedWrapper != null) {
                try {
                    Intent i = new Intent(activity, Share_Message_Screen.class);
                    i.putExtra("screen", "feeds");
                    i.putExtra("feedId", feedWrapper.getFeed().getId());
                    i.putExtra("content", ObjectSerializer.serialize(feedWrapper));
                    activity.startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                    ExceptionHandler.handleException(e);
                }
            }
        }

        private void deleteSelectedFeed(final FeedWrapper feedWrapper)
        {
            try {
                AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                dialog.setTitle("Delete Feed");
                dialog.setMessage("Do you want to delete this feed ?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(tag, "deleting feed");
                        try {
                            JSONObject input = new JSONObject();
                            input.put("feedId", feedWrapper.getFeedUsers().getFeedUsersId());
                            selectedFordelete = feedWrapper;
                            Log.d(tag, "input: " + input);
//                                            DeleteFeedService deleteFeedService = new DeleteFeedService(activity, appPref, input, asyncFinishInterface);
//                                            deleteFeedService.execute();

                            GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.DELETE_FEED, FeedMethodEnum.DELETE_FEED.name(), input.toString(), tag, asyncFinishInterface, true);
                            service.execute();
                        } catch (Exception ex) {
                            ExceptionHandler.handleException(ex, activity);
                        }
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });
                dialog.setCancelable(false);
                dialog.create();
                dialog.show();
            } catch (Throwable e) {
                ExceptionHandler.handleException(e);
            }
        }


        private void addMarkers(GoogleMap googleMap, WayPoints[] wayPointses, LatLng start, LatLng end) {
            int count = 1;
            for (WayPoints wayPoints : wayPointses) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title(wayPoints.getName());
                markerOptions.snippet(wayPoints.getDescription());
                markerOptions.position(Utility.getLatLngFromCoordinate(wayPoints.getCordinate()));

                if (wayPoints.getWayPointCategory() != null) {
                    markerOptions.icon(Utility.getMarkerIcon(wayPoints.getWayPointCategory().getName()));
                }
                googleMap.addMarker(markerOptions);
                count++;
            }

            MarkerOptions markerOptionsStart = new MarkerOptions();
            markerOptionsStart.position(start);
            markerOptionsStart.icon(BitmapDescriptorFactory.fromResource(R.drawable.a));

            MarkerOptions markerOptionsEnd = new MarkerOptions();
            markerOptionsEnd.position(start);
            markerOptionsEnd.icon(BitmapDescriptorFactory.fromResource(R.drawable.b));

            googleMap.addMarker(markerOptionsStart);
            googleMap.addMarker(markerOptionsEnd);
        }

        class Holder {
            TextView name;
            TextView extraText;
            TextView feedText;
            TextView timeStamp;
            TextView commentText;
            TextView deleteTextView;
            FeedImageView imageView1;
            FeedImageView imageView2;
            FeedImageView imageView3;
            ImageView deleteIcon;
            ImageView commentIcon;
            ImageView shareFeedIcon;
            TextView shareFeedText;
            TableRow firstImageRow;
            TableRow secondImageRow;
            TableRow defaultRow;
            RelativeLayout container;
            LinearLayout trailContainer;
            FeedImageView trailImage;
        }
    }

}
