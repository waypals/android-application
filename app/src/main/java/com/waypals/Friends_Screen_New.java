package com.waypals;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by shantanu on 29/1/15.
 */
public class Friends_Screen_New extends FragmentActivity {

    private ImageView friendList, addFriend, friendRequests, sentRequest, back;
    private int container = R.id.friends_fragment_container;
    private TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_screen_new);


        back = (ImageView) findViewById(R.id.left_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final FragmentManager fm = getSupportFragmentManager();


        friendList = (ImageView) findViewById(R.id.friendList);
        friendList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(container, new FriendsMyFriendsFragment());
                ft.commit();

                friendList.setImageResource(R.drawable.friend_list_active_icon);
                addFriend.setImageResource(R.drawable.add_friend_icon);
                friendRequests.setImageResource(R.drawable.pending_list_icon);
                sentRequest.setImageResource(R.drawable.request_sent_icon);

                RelativeLayout lytBar = (RelativeLayout) findViewById(R.id.navbar);
                EditText searchBar = (EditText)lytBar.findViewById(R.id.nav_title_txt);
                searchBar.setVisibility(View.VISIBLE);

                TextView title = (TextView)lytBar.findViewById(R.id.nav_title_txt_only);
                title.setVisibility(View.GONE);

            }
        });

        addFriend = (ImageView) findViewById(R.id.addFriend);
        addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(container, new FriendsAddFriendFragment());
                ft.commit();

                friendList.setImageResource(R.drawable.friend_list_icon);
                addFriend.setImageResource(R.drawable.add_friend_active_icon);
                friendRequests.setImageResource(R.drawable.pending_list_icon);
                sentRequest.setImageResource(R.drawable.request_sent_icon);

                RelativeLayout lytBar = (RelativeLayout) findViewById(R.id.navbar);
                EditText searchBar = (EditText)lytBar.findViewById(R.id.nav_title_txt);
                searchBar.setVisibility(View.INVISIBLE);

                TextView title = (TextView)lytBar.findViewById(R.id.nav_title_txt_only);
                title.setVisibility(View.VISIBLE);
            }
        });

        friendRequests = (ImageView) findViewById(R.id.friendRequests);
        friendRequests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(container, new FriendsPendingRequestFragment());
                ft.commit();

                friendList.setImageResource(R.drawable.friend_list_icon);
                addFriend.setImageResource(R.drawable.add_friend_icon);
                friendRequests.setImageResource(R.drawable.pending_list_active_icon);
                sentRequest.setImageResource(R.drawable.request_sent_icon);

                RelativeLayout lytBar = (RelativeLayout) findViewById(R.id.navbar);
                EditText searchBar = (EditText)lytBar.findViewById(R.id.nav_title_txt);
                searchBar.setVisibility(View.VISIBLE);

                TextView title = (TextView)lytBar.findViewById(R.id.nav_title_txt_only);
                title.setVisibility(View.GONE);
            }
        });


        sentRequest = (ImageView) findViewById(R.id.sentRequest);
        sentRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(container, new FriendsSentRequestFragment());
                ft.commit();


                friendList.setImageResource(R.drawable.friend_list_icon);
                addFriend.setImageResource(R.drawable.add_friend_icon);
                friendRequests.setImageResource(R.drawable.pending_list_icon);
                sentRequest.setImageResource(R.drawable.request_sent_active_icon);

                RelativeLayout lytBar = (RelativeLayout) findViewById(R.id.navbar);
                EditText searchBar = (EditText)lytBar.findViewById(R.id.nav_title_txt);
                searchBar.setVisibility(View.VISIBLE);

                TextView title = (TextView)lytBar.findViewById(R.id.nav_title_txt_only);
                title.setVisibility(View.GONE);
            }
        });



        Intent i = getIntent();

        if(i != null && i.getBooleanExtra("friend_request_notification",false))
        {
            friendRequests.performClick();
        }
        else
        {
            friendList.performClick();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
