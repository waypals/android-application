package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.alarm.SetAlarmTimeActivity;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.waypals.request.ChangePassword;
import com.waypals.request.Notification;
import com.waypals.request.SettingsUpdateRequest;
import com.waypals.request.SpeedLimit;
import com.waypals.request.Unit;
import com.waypals.request.VehicleSpeedLimit;
import com.waypals.response.ServiceResponse;
import com.waypals.response.SettingsResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

public class Settings_Screen extends NavigationDrawer implements AsyncFinishInterface {
    static Context context;
    static String tag = "Settings Screen";
    static VehicleServices vs = new VehicleServices();
    static int totalCars = 0;
    static String[] vehicaleList;
    Button back_btn;
    TextView title;
    AsyncFinishInterface asyncFinishInterface;
    ApplicationPreferences appPref;
    Typeface tf;
    Activity activity;
    ImageView back_button, right_button;
    SettingsResponse settingsResponse = null;
    String response;
    String method;
    String maxSpeedLimit = "";
    Boolean userClicked = false;
    ServiceResponse serviceResponse;
    CheckBox checkBox_parkingfence, checkBox_speedLimit, checkBox_email, checkBox_sms, checkBox_push;
    TextView textView_parkingfence, textView_speedLimit, textView_email, textView_sms, textView_push, policyExpiry, pollutionExpiry, serviceLastExpiry, licenceExpiry;
    ImageView policyDatePicker, pollutionDatePicker, licenceDatePicker, serviceLastPicker;
    private Collection<VehicleInformationVO> vehicleInfo;
    private Long currentVehicle;
    VehicleServiceResponse vehicleServiceResponse;
    private AlertDialog.Builder alert;
    private boolean alertReady;
    private CheckBox checkBox_alarm;
    private TextView text_alarm_status;
    private TextView text_alarm_status_title;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        setContentView(R.layout.settings_screen);

        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setText("SETTINGS");
        asyncFinishInterface = this;
        title.setTypeface(tf);
        activity = this;
        context = Settings_Screen.this;
        appPref = new ApplicationPreferences(context);

        currentVehicle = appPref.getVehicle_Selected();

        back_button = (ImageView) findViewById(R.id.left_btn);
        right_button = (ImageView) findViewById(R.id.right_btn);
        right_button.setBackgroundResource(R.drawable.save_btn);
        right_button.setVisibility(View.VISIBLE);

        right_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsUpdateRequest req = createUpdateRequest();
                updateSetting(req);
            }
        });

        back_button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
            }
        });

        /* setup contents */

        Log.i(tag, "Load vehicle service data");
        try {
            vs.getVehiclesData(appPref, this, true);

        } catch (Exception e) {
            ExceptionHandler.handleException(e, this);
        }

        checkBox_alarm = (CheckBox) findViewById(R.id.set_alarm_alert);

        checkBox_email = (CheckBox) findViewById(R.id.setting_email);
        checkBox_parkingfence = (CheckBox) findViewById(R.id.setting_parkingFence);
        checkBox_push = (CheckBox) findViewById(R.id.setting_push);
        checkBox_sms = (CheckBox) findViewById(R.id.setting_sms);
        checkBox_speedLimit = (CheckBox) findViewById(R.id.setting_speedlimit);

        text_alarm_status_title = (TextView) findViewById(R.id.alarm_alert);

        text_alarm_status = (TextView) findViewById(R.id.alarm_alert_status);

        textView_email = (TextView) findViewById(R.id.text_email);
        textView_parkingfence = (TextView) findViewById(R.id.text_parkingfence);
        textView_push = (TextView) findViewById(R.id.text_push);
        textView_sms = (TextView) findViewById(R.id.text_sms);
        textView_speedLimit = (TextView) findViewById(R.id.text_speedlimit);
        textView_email.setTypeface(tf);
        textView_parkingfence.setTypeface(tf);
        textView_push.setTypeface(tf);
        textView_sms.setTypeface(tf);
        textView_speedLimit.setTypeface(tf);


        policyExpiry = (TextView) findViewById(R.id.policy_expiry);
        policyDatePicker = (ImageView) findViewById(R.id.policy_expiry_date);

        serviceLastExpiry = (TextView) findViewById(R.id.service_last);
        serviceLastPicker = (ImageView) findViewById(R.id.service_last_date);

        licenceExpiry = (TextView) findViewById(R.id.licence_expiry);
        licenceDatePicker = (ImageView) findViewById(R.id.licence_expiry_date);

        pollutionExpiry = (TextView) findViewById(R.id.pollution_expiry);
        pollutionDatePicker = (ImageView) findViewById(R.id.pollution_expiry_date);

        Button btn = (Button) findViewById(R.id.change_password_btn);

        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent changePass = new Intent(Settings_Screen.this, ChangePasswordActivity.class);
                changePass.putExtra("firstTime", false);
                startActivity(changePass);

            }
        });

        Button btnDeleteAccount = (Button) findViewById(R.id.delete_account_btn);

        btnDeleteAccount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                takePermission();

            }
        });


        policyDatePicker.setOnClickListener(new DatePickerListener(policyExpiry));
        serviceLastPicker.setOnClickListener(new DatePickerListener(serviceLastExpiry));
        licenceDatePicker.setOnClickListener(new DatePickerListener(licenceExpiry));
        pollutionDatePicker.setOnClickListener(new DatePickerListener(pollutionExpiry));

        checkBox_parkingfence.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkBox_parkingfence.setButtonDrawable(R.drawable.toggle_on);
                    textView_parkingfence.setText("ON");
                } else {
                    checkBox_parkingfence.setButtonDrawable(R.drawable.toggle_off);
                    textView_parkingfence.setText("OFF");
                        }
                    }
                });

        checkBox_email.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    textView_email.setText("ON");
                    checkBox_email.setButtonDrawable(R.drawable.toggle_on);
                } else {
                    textView_email.setText("OFF");
                    checkBox_email.setButtonDrawable(R.drawable.toggle_off);
                }
            }
        });

        checkBox_push.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    textView_push.setText("ON");
                    checkBox_push.setButtonDrawable(R.drawable.toggle_on);
                } else {
                    textView_push.setText("OFF");
                    checkBox_push.setButtonDrawable(R.drawable.toggle_off);
                }
            }
        });

        checkBox_sms.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    textView_sms.setText("ON");
                    checkBox_sms.setButtonDrawable(R.drawable.toggle_on);
                } else {
                    textView_sms.setText("OFF");
                    checkBox_sms.setButtonDrawable(R.drawable.toggle_off);
                }
            }
        });

        checkBox_speedLimit.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (userClicked) {
                        Intent i = new Intent(activity, SetSpeedLimitScreen.class);
                        i.putExtra("speed", settingsResponse.getSpeedLimit() != null ? settingsResponse.getSpeedLimit().getMax().getSpeed() : "");
                        startActivityForResult(i, 100);
                    }

                    textView_speedLimit.setText("ON");
                    checkBox_speedLimit.setButtonDrawable(R.drawable.toggle_on);
                } else {
                    textView_speedLimit.setText("OFF");
                    checkBox_speedLimit.setButtonDrawable(R.drawable.toggle_off);
                }
            }
        });

        checkBox_alarm.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (userClicked) {
                        Intent i = new Intent(activity, SetAlarmTimeActivity.class);
                        //i.putExtra("speed", settingsResponse.getSpeedLimit() != null ? settingsResponse.getSpeedLimit().getMax().getSpeed() : "");
                        startActivityForResult(i, 200);
                    }

                    text_alarm_status.setText("ON");
                    checkBox_alarm.setButtonDrawable(R.drawable.toggle_on);
                } else {
                    text_alarm_status.setText("OFF");
                    checkBox_alarm.setButtonDrawable(R.drawable.toggle_off);
                    appPref.setAlarm(false);
                }
            }
        });

        setAlarmStatus();
    }

    private void updateSetting(SettingsUpdateRequest req){
        if (req != null) {
            Gson gson = new Gson();
            String input = gson.toJson(req);
            GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.UPDATE_SETTINGS, "UpdateSettings", input, "Settings Screen", asyncFinishInterface, true);
            service.execute();
        }
    }

    private void deleteAccountAlert(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog));
        dialog.setTitle("Delete Account");
        dialog.setMessage("Do you want to delete this account?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteAccount();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });
        dialog.setCancelable(false);
        dialog.create();
        dialog.show();
    }

    public void deleteAccount(){
        try {
            GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.DELETE_ACCOUNT, "DELETE_ACCOUNT", "", "", this, true);
            service.execute();

        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }

    public void takePermission(){
        alert = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(this);
        edittext.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        alert.setMessage("Please enter your password");
        alert.setTitle("Authentication Required");

        alert.setView(edittext);
        alert.setCancelable(false);

        alert.setPositiveButton("Authenticate", null);


        alert.setNegativeButton("Cancel", null);

        alertReady = false;
        final AlertDialog alertDialog = alert.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if(!alertReady) {
                    Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            // TODO Do something

                            Editable textValue = edittext.getText();
                            if (textValue != null) {
                                if (textValue.toString().equals(appPref.getUserPassword())) {
                                    alertDialog.dismiss();
                                    deleteAccountAlert();
                                } else {
                                    Utility.makeNewToast(Settings_Screen.this, "Empty/Invalid password");
                                }
                            }
                        }



                    });
                    Button c = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    c.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                }else{
                    alertReady = true;
                }
            }
        });
        alertDialog.show();
    }

    public SettingsUpdateRequest createUpdateRequest() {
        SettingsUpdateRequest request = new SettingsUpdateRequest();

        request.setVehicleId(appPref.getVehicle_Selected());

        Notification notification = new Notification();
        notification.setMailNotification(checkBox_email.isChecked());
        notification.setPushNotification(checkBox_push.isChecked());
        notification.setSmsNotification(checkBox_sms.isChecked());
        request.setNotificationType(notification);
        if(policyExpiry.getText() != null) {
            if(Utility.formatDate(policyExpiry.getText().toString()) != null) {
                request.setPolicyDueDate(Utility.formatDate(policyExpiry.getText().toString()));
            }
        }

        if(serviceLastExpiry.getText() != null) {
            if(Utility.formatDate(serviceLastExpiry.getText().toString()) != null) {
                request.setLastServiceDate(Utility.formatDate(serviceLastExpiry.getText().toString()));
            }
        }

        if(licenceExpiry.getText() != null) {
            if(Utility.formatDate(licenceExpiry.getText().toString()) != null) {
                request.setLicenceExpDate(Utility.formatDate(licenceExpiry.getText().toString()));
            }
        }

        if(pollutionExpiry.getText() != null) {

            if(Utility.formatDate(pollutionExpiry.getText().toString()) != null) {
                request.setPollutionDueDate(Utility.formatDate(pollutionExpiry.getText().toString()));
            }
        }
        SpeedLimit limit = new SpeedLimit();
        Unit u = new Unit();
        u.setSpeed(Float.parseFloat("0.0"));
        u.setUnit("KMPH");
        limit.setMin(u);

        Unit deviation = new Unit();
        deviation.setSpeed(Float.parseFloat("0.0"));
        deviation.setUnit("KMPH");
        limit.setAllowedDeviation(deviation);

        Unit violatedSpeed = new Unit();
        violatedSpeed.setSpeed(Float.parseFloat("0.0"));
        violatedSpeed.setUnit("KMPH");
        limit.setViolatedSpeed(violatedSpeed);

        Unit u1 = new Unit();
        u1.setUnit("KMPH");
        u1.setSpeed(Float.parseFloat("0.0"));
        if (checkBox_speedLimit.isChecked()) {
            if (maxSpeedLimit != null && !maxSpeedLimit.isEmpty()) {
                u1.setSpeed(Float.parseFloat(maxSpeedLimit));
            } else {
                try {
                    Utility.makeNewToast(activity, "Please set speed limit", new Integer(Gravity.BOTTOM));
                    checkBox_speedLimit.setChecked(false);
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, activity);
                }
                return null;
            }
            limit.setActivated(true);
            limit.setMax(u1);
        } else {
            limit.setActivated(false);
            limit.setMax(u1);
        }

        VehicleSpeedLimit vehicleSpeedLimit = new VehicleSpeedLimit();
        vehicleSpeedLimit.setSpeedLimit(limit);
        vehicleSpeedLimit.setVehicleId(appPref.getVehicle_Selected());
        request.setVehicleSpeedLimit(vehicleSpeedLimit);

        if (checkBox_parkingfence.isChecked()) {
            request.setEnableParkingFenceVehicleId(appPref.getVehicle_Selected());
        } else {
            request.setDisableParkingFenceVehicleId(appPref.getVehicle_Selected());
        }

        return request;
    }


    public void setupListContent() {
        try {
            if (settingsResponse != null) {

                if (settingsResponse.getParkingFenceEnabled()) {
                    checkBox_parkingfence.setChecked(true);
                    textView_parkingfence.setText("ON");
                } else {
                    checkBox_parkingfence.setChecked(false);
                    textView_parkingfence.setText("OFF");
                }

                if (settingsResponse.getNotificationType() !=null && settingsResponse.getNotificationType().getMailNotification()) {
                    checkBox_email.setChecked(true);
                    textView_email.setText("ON");
                } else {
                    checkBox_email.setChecked(false);
                    textView_email.setText("OFF");
                }
                if (settingsResponse.getNotificationType() !=null && settingsResponse.getNotificationType().getPushNotification()) {
                    checkBox_push.setChecked(true);
                    textView_push.setText("ON");
                } else {
                    checkBox_push.setChecked(false);
                    textView_push.setText("OFF");
                }
                if (settingsResponse.getNotificationType() !=null && settingsResponse.getNotificationType().getSmsNotification()) {
                    checkBox_sms.setChecked(true);
                    textView_sms.setText("ON");
                } else {
                    checkBox_sms.setChecked(false);
                    textView_sms.setText("OFF");
                }
                if (settingsResponse.getNotificationType() !=null && settingsResponse.getSpeedLimit() != null && settingsResponse.getSpeedLimit().isActivated()) {
                    checkBox_speedLimit.setChecked(true);
                    textView_speedLimit.setText("ON");
                    maxSpeedLimit = "" + settingsResponse.getSpeedLimit().getMax().getSpeed();
                } else {
                    checkBox_speedLimit.setChecked(false);
                    textView_speedLimit.setText("OFF");
                }
                userClicked = true;
            }
        } catch (Exception ex) {
            try {
                Utility.makeNewToast(activity, getString(R.string.err_invalid_data), new Integer(Gravity.BOTTOM));
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, activity);
            }
        }
    }

    public void finish(String response, String method) {
        Log.i(tag, "method RES: " + method);
        this.method = method;
        this.response = response;
        Gson gson = new Gson();
        if (method != null && method.contains("vehiclesdata") && response != null) {
            Log.i(tag, "getVehiclesData RES: " + response);
            vehicleServiceResponse = gson.fromJson(
                    response, VehicleServiceResponse.class);

            if (vehicleServiceResponse != null && vehicleServiceResponse.getResponse()
                    .equalsIgnoreCase("SUCCESS")) {
                appPref.saveVehicleServiceResponse(response);
                createLeftPane(vehicleServiceResponse);
            }
        } else if (method != null && method.equalsIgnoreCase("UpdateSettings")) {
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
        else if (method != null && method.contains("SETTINGS")) {
            if (response != null) {
                settingsResponse = gson.fromJson(response, SettingsResponse.class);
            } else {
                settingsResponse = null;
            }
        } else if (method != null && method.contains("DELETE_ACCOUNT")) {
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
    }

    public void onPostExecute(Void Result) {

        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())) || (settingsResponse != null && "ERROR".equalsIgnoreCase(
                settingsResponse.getResponse())))  {
            String errorMessage = null;
            if(serviceResponse != null) {
                errorMessage = serviceResponse.getErrorCode();
            }else if(settingsResponse != null)
            {
                errorMessage = settingsResponse.getErrorCode();
            }
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, getString(R.string.server_error));
            }

            return;
        }else if(response == null){
            try {
                Utility.makeNewToast(this, getString(R.string.err_no_response));
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, this);
            }
        }

        if (method != null && method.contains("vehiclesdata")) {
            GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_SETTINGS + "/" + currentVehicle, "SETTINGS", "", "Settings Screen", this, true);
            service.execute();
        }
        else if (method != null && method.equalsIgnoreCase("UpdateSettings")) {
            if (serviceResponse != null && settingsResponse != null) {
                if ("SUCCESS".equalsIgnoreCase(settingsResponse.getResponse())) {
                    try {

                        if(serviceLastExpiry.getText() != null && !getString(R.string.no_info).equals(serviceLastExpiry.getText().toString()))
                        {
                            appPref.saveCurrentVehicleLastServiceDate(serviceLastExpiry.getText().toString());
                        }

                        if(licenceExpiry.getText() != null && !getString(R.string.no_info).equals(licenceExpiry.getText().toString()))
                        {
                            appPref.saveCurrentVehicleLicenceDate(licenceExpiry.getText().toString());
                        }

                        if(policyExpiry.getText() != null && !getString(R.string.no_info).equals(policyExpiry.getText().toString()))
                        {
                            appPref.saveCurrentVehiclePolicyDueDate(policyExpiry.getText().toString());
                        }

                        if(pollutionExpiry.getText() != null && !getString(R.string.no_info).equals(pollutionExpiry.getText().toString()))
                        {
                            appPref.saveCurrentVehiclePollutionDate(pollutionExpiry.getText().toString());
                        }

                        Utility.makeNewToast(activity, getString(R.string.op_alert_settingsUpdated), new Integer(Gravity.BOTTOM));
//                        GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_SETTINGS + "/" + currentVehicle, "SETTINGS", "", "Settings Screen", this, true);
//                        service.execute();
                        finish();
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                } else {
                    try {
                        Utility.makeNewToast(activity, getString(R.string.err_failed_settingsUpdated), new Integer(Gravity.BOTTOM));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                }
            }
        }else if (method != null && method.contains("SETTINGS")) {
            if (settingsResponse != null) {
                setupListContent();
            } else {
                try {
                    Utility.makeNewToast(activity, getString(R.string.err_no_response), new Integer(Gravity.BOTTOM));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, activity);
                }
            }
        } else if (method != null && method.equalsIgnoreCase("DELETE_ACCOUNT")) {
            if (serviceResponse != null) {
                if (serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    Toast toast = Toast.makeText(this,
                            "Account has been delete successfully", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    private void createLeftPane(VehicleServiceResponse vehicleServiceResponse) {

        vehicleInfo = vehicleServiceResponse.getVehicles();

        int totalVehicle = vehicleInfo.size();
        vehicaleList = new String[totalVehicle];
        //isParkFenceOn = new boolean[totalVehicle];
        totalCars = totalVehicle;
        appPref.saveTotalCar(totalVehicle);
        Utility.setCurrentCarSettings(appPref.getSelectedRegistrationNumber(), appPref, vehicleInfo);
//        Log.i(tag, "Data RES: total vehicale " + totalVehicle);
//
//        Iterator<VehicleInformationVO> v1 = vehicleInfo.iterator();
//        int count = 0;
//        while (v1.hasNext()) {
//            VehicleInformationVO vehicleInformation = v1.next();
//            Log.i(tag,
//                    "Data RES: REG "
//                            + vehicleInformation
//                            .getRegistrationNumber());
//            Log.i(tag, "Data RES: REG " + vehicleInformation.getState());
//            Log.i(tag,
//                    "Data RES: REG "
//                            + vehicleInformation.getVehicleId());
//            Log.i(tag,
//                    "Data RES: REG "
//                            + vehicleInformation.isSystemFenceEnabled());
//            vehicaleList[count] = vehicleInformation
//                    .getRegistrationNumber();
//            //isParkFenceOn[count] = vehicleInformation
//            //	.isSystemFenceEnabled();
//            if (count == 0) {
//                appPref.saveVehicle_Selected(vehicleInformation
//                        .getVehicleId());
//                appPref.saveCar1_Parkfence(vehicleInformation
//                        .isSystemFenceEnabled());
//                appPref.saveSelectedRegistrationNumber(vehicleInformation
//                        .getRegistrationNumber());
//
//                Utility.saveExpiryDates(appPref, vehicleInformation);
//                setExpiryDates();
//            }
//            count++;
//        }
        setExpiryDates();

    }

    private void setExpiryDates()
    {
        String noInfo = "No Info";
        if(appPref.getCurrentVehiclePolicyDueDate() != null) {
            policyExpiry.setText(appPref.getCurrentVehiclePolicyDueDate());
        }else
        {
            policyExpiry.setText(noInfo);
        }

        if(appPref.getCurrentVehiclePollutionDate() != null) {
            pollutionExpiry.setText(appPref.getCurrentVehiclePollutionDate());
        }else
        {
            pollutionExpiry.setText(noInfo);
        }

        if(appPref.getCurrentVehicleLastServiceDate() != null) {
            serviceLastExpiry.setText(appPref.getCurrentVehicleLastServiceDate());
        }else
        {
            serviceLastExpiry.setText(noInfo);
        }

        if(appPref.getCurrentVehicleLicenceDate() != null) {
            licenceExpiry.setText(appPref.getCurrentVehicleLicenceDate());
        }else
        {
            licenceExpiry.setText(noInfo);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String value = data.getStringExtra("value");
                    maxSpeedLimit = value;
                }
            } else {
                checkBox_speedLimit.setChecked(false);
            }
        }

        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    String start = data.getStringExtra("start");
                    String end = data.getStringExtra("end");
                    appPref.saveAlarmStartTime(start);
                    appPref.saveAlarmEndTime(end);
                    appPref.setAlarm(true);
                    SettingsUpdateRequest req = createUpdateRequest();
                    req.getNotificationType().setPushNotification(true);
                    req.setDisableParkingFenceVehicleId(null);
                    req.setEnableParkingFenceVehicleId(appPref.getVehicle_Selected());

                    updateSetting(req);
                    setAlarmStatus();
                }
            } else {
                checkBox_alarm.setChecked(false);
                appPref.setAlarm(false);
            }
        }
    }

    private void setAlarmStatus(){
        String start = appPref.getAlarmStartTime();
        String end = appPref.getAlarmEndTime();
        if (start != null && end != null) {
           // text_alarm_status_title.setText(start + " till " + end);
        }

        if (appPref.isAlarm()){
            checkBox_alarm.setChecked(true);
            text_alarm_status.setText("ON");
            checkBox_alarm.setButtonDrawable(R.drawable.toggle_on);
        }
    }

    @Override
    protected void setImmobilizer() {

    }

    private class DatePickerListener implements OnClickListener
    {

        TextView textView;

        public DatePickerListener(TextView textView)
        {
            this.textView = textView;

        }


        @Override
        public void onClick(View view) {
            CustomDateTimePicker custom = new CustomDateTimePicker(activity, true, new CustomDateTimePicker.ICustomDateTimeListener() {
                @Override
                public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                  String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                  int hour24, int hour12, int min, int sec, String AM_PM) {

                    SimpleDateFormat df = new SimpleDateFormat("MMM d, yyyy");
                    String set_DateTime = df.format(dateSelected);

                    DatePickerListener.this.textView.setText(set_DateTime);
                }

                @Override
                public void onCancel() {

                }


            });
            custom.set24HourFormat(false);
            /**
             * Pass Directly current data and time to show when it pop up
             */
            //custom.setDate(Calendar.getInstance());
            // custom.setDate(startDate);

            custom.showDialog();
        }
    }

    class Content {
        public String title;
        public int icon;
        boolean status;
        boolean isHeading;

        public Content(int icon, String title, boolean status, boolean isHeading) {
            super();
            this.icon = icon;
            this.title = title;
            this.status = status;
            this.isHeading = isHeading;
        }

    }

    class ContentAdapter extends ArrayAdapter<Content> {
        Context context;
        int layoutResourceId;
        Content data[] = null;

        public ContentAdapter(Context context, int resource, Content[] objects) {
            super(context, resource, objects);
            this.layoutResourceId = resource;
            this.context = context;
            this.data = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            final ContentsHolder holder;
            LayoutInflater inflater = ((Settings_Screen) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ContentsHolder();


            holder.checkBox = (CheckBox) row.findViewById(R.id.setting_status_icon);
            holder.txtTitle = (TextView) row.findViewById(R.id.setting_txt_content);
            holder.txtTitle.setTypeface(tf);

            holder.status = (TextView) row.findViewById(R.id.setting_status_txt);
            holder.status.setTypeface(tf);


            row.setTag(holder);
            Content drawerContent = data[position];
            holder.isHeading = drawerContent.isHeading;
            if (holder.isHeading) {
                /*display header*/
                //row.setButtonDrawable(R.drawable.navbar);
                row.setBackgroundColor(Color.parseColor("#406A8C"));
                if ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_LARGE) {
                    if (position == data.length - 1) {
                        row.getLayoutParams().height = 10;
                    } else {
                        row.getLayoutParams().height = 40;
                    }

                } else {
                    if (position == data.length - 1) {
                        row.getLayoutParams().height = 10;
                    } else {
                        row.getLayoutParams().height = 40;
                    }
                }
                row.setPadding(0, 1, 0, 1);
                row.requestLayout();

                holder.txtTitle.setTextColor(Color.WHITE);
                holder.txtTitle.setTextAppearance(Settings_Screen.this, android.R.style.TextAppearance_Medium);
                holder.txtTitle.setTypeface(tf);


                holder.checkBox.setVisibility(View.INVISIBLE);
                holder.status.setVisibility(View.INVISIBLE);
                row.requestLayout();
            }

            holder.txtTitle.setText(drawerContent.title);
            holder.status.setTypeface(tf);


            if (!holder.isHeading) {
                if (position == 4) {
                    holder.checkBox.setButtonDrawable(R.drawable.toggle_on_s);
                    holder.checkBox.setEnabled(false);
                    holder.status.setText("On");
                }
                holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton view, boolean isChecked) {

                        if (isChecked) {
                            holder.checkBox.setButtonDrawable(R.drawable.toggle_on_s);
                            holder.status.setText("On");
                        } else {
                            holder.checkBox.setButtonDrawable(R.drawable.toggle_off);
                            holder.status.setText("Off");
                        }

                    }
                });
            }


            return row;

        }

        class ContentsHolder {

            TextView txtTitle;
            TextView status;
            CheckBox checkBox;
            boolean isHeading;
        }
    }
}
