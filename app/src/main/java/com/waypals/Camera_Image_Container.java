package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.waypals.exception.ExceptionHandler;
import com.waypals.imageHelper.ImageResizer;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.wareninja.opensource.common.ObjectSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class Camera_Image_Container extends Activity {

    public static final int CAMERA_PICTURE = 2;
    public static final int GALLERY_PICTURE = 1;
    String fileNamePath = null;
    private ListView list;
    private Button AddImage, Cancel;
    //contains filename and image hexstring
    //private HashMap<String,String> files = new HashMap<String, String>();
    //contains filename and file path
    private HashMap<String, String> filesDetails = new HashMap<String, String>();
    private String tag = "Camera Image container";
    private Activity activity;
    private ArrayList<String> filepathList = new ArrayList<String>();
    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.camera_image_container);
//        Window window = this.getWindow();
//        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        this.setFinishOnTouchOutside(false);

        activity = this;

        list = (ListView) findViewById(R.id.listView);
        imageAdapter = new ImageAdapter(activity);
        list.setAdapter(imageAdapter);
        Cancel = (Button) findViewById(R.id.cancel);

        String oldfiles = getIntent().getStringExtra("files");
        //files = (HashMap<String, String>) ObjectSerializer.deserialize(oldfiles);
        oldfiles = getIntent().getStringExtra("filesDetails");
        filesDetails = (HashMap<String, String>) ObjectSerializer.deserialize(oldfiles);

//        if(files==null)
//        {
//            files = new HashMap<String, String>();
//        }
        if (filesDetails == null) {
            filesDetails = new HashMap<String, String>();
        }

        ArrayList<imageFiles> x = getImagesFromMap(filesDetails);
        if (x != null) {
            for (imageFiles i : x) {
                imageAdapter.add(i);
            }
            imageAdapter.notifyDataSetChanged();
        }


        if (filesDetails.size() > 0) {
            Cancel.setText("Finish");
        } else {
            Cancel.setText("Cancel");
        }


        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (filesDetails.size() == 0) {
                    Intent i = new Intent();
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    Intent i = new Intent();
                    //i.putExtra("files", ObjectSerializer.serialize(files));
                    i.putExtra("filesDetails", ObjectSerializer.serialize(filesDetails));
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            }
        });
        AddImage = (Button) findViewById(R.id.addimage);
        AddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }

    public void selectImage() {

        if (filesDetails.size() >= 4) {
            try {
                Utility.makeNewToast(activity, "You can not add more than 4 images");
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, activity);
            }
            return;
        }
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Camera_Image_Container.this);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                fileNamePath = null;
                Log.d(tag, "image selection canceled");
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if ((requestCode == CAMERA_PICTURE || requestCode == GALLERY_PICTURE) && resultCode == RESULT_CANCELED) {
        }

        Log.i("resultCode", String.valueOf(resultCode));
        if (requestCode == CAMERA_PICTURE && resultCode == RESULT_OK)// capture image form camera
        {
            onPhotoTaken();
        } else if (requestCode == GALLERY_PICTURE && resultCode == RESULT_OK) // pick picture from gallary
        {
            onPhotoTakenFromGallery(data);
        }

    }

    public void onPhotoTaken() {
        fileNamePath = (checkImageSize(fileNamePath));
        {
            try {
                Log.d("camera imagePath", fileNamePath);
                filepathList.add(fileNamePath);
                finalizeImageList();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("camera imagePath", "onactivity result");
            }
        }

    }

    public String checkImageSize(String fileNamePath) {
        try {
            File f = new File(fileNamePath);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            is.close();

            int kb = size / 1024;

            if (kb > 256) {
                File file = Utility.createFile();
                Bitmap b = ImageResizer.resize(new File(fileNamePath), 256, 256);
                FileOutputStream fout = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                fout.flush();
                b.recycle();
                System.gc();
                b = null;
                fout.close();
                return file.getAbsolutePath();
            } else {
                return fileNamePath;
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, activity);
            return null;
        }
    }

    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            Uri _uri = data.getData();
            if (_uri != null) {
                fileNamePath = getPath(_uri);

                fileNamePath = (checkImageSize(fileNamePath));
                filepathList.add(fileNamePath);
                Log.d(tag, "gallary image path: " + fileNamePath);
                finalizeImageList();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("imagePath", "onactivity result");
        }

    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/_cachedImages" + "/" + timeStamp + "_temp.jpg");
        fileNamePath = image.getAbsolutePath();
        return image;
    }

    private void finalizeImageList() {
        if (filepathList.size() > 0) {
            Cancel = (Button) findViewById(R.id.cancel);
            Cancel.setText("Finish");

            for (String path : filepathList) {
                String filename = path.substring(path.lastIndexOf("/") + 1);
                if (!filesDetails.containsKey(filename)) {
                    imageAdapter.add(new imageFiles(filename, path));
                    imageAdapter.notifyDataSetChanged();
                    //files.put(filename, Utility.getImageToHex(activity, path));
                    filesDetails.put(filename, path);
                }
            }

        }

    }

    private ArrayList<imageFiles> getImagesFromMap(HashMap<String, String> fileDetails) {
        final ArrayList<imageFiles> images = new ArrayList<imageFiles>();
        if (fileDetails != null && fileDetails.size() > 0) {
            Set<String> keys = fileDetails.keySet();
            for (String filename : keys) {
                images.add(new imageFiles(filename, fileDetails.get(filename)));
            }
        } else {
            Log.d(tag, "ERROR: IMAGE AND FILE DETAILS MAP ARE DIFFER");
        }
        if (images.size() > 0)
            return images;
        else
            return null;
    }

    class imageFiles {
        private String fileName;
        private String filePath;

        public imageFiles(String fileName, String filePath) {
            super();
            this.fileName = fileName;
            this.filePath = filePath;
        }
    }

    class ImageAdapter extends ArrayAdapter<imageFiles> {
        Context context;
        int layoutResourceId;
        imageFiles data[] = null;
        Typeface tf;
        ImageAdapter adapter;

        public ImageAdapter(Context context) {
            super(context, R.layout.camera_image_row);
            this.layoutResourceId = R.layout.camera_image_row;
            this.context = context;

            this.adapter = this;
        }

        @Override
        public View getView(int position, View row, ViewGroup parent) {
            final Holder holder = new Holder();
            LayoutInflater inflater = ((Camera_Image_Container) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder.fileName = (TextView) row.findViewById(R.id.filename);
            holder.delete = (ImageView) row.findViewById(R.id.deletefile);

            tf = Typeface.createFromAsset(getAssets(),
                    CH_Constant.Font_Typface_Path);


            final imageFiles c = getItem(position);
            holder.fileName.setText(c.fileName);
            holder.fileName.setTypeface(tf);
            holder.filepath = c.filePath;

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                        dialog.setTitle("Delete Image");
                        dialog.setMessage("Do you want to delete this image ?");
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.d(tag, "deleting image");
                                try {
                                    adapter.remove(c);
                                    adapter.notifyDataSetChanged();
                                    filepathList.remove(holder.filepath);
                                    //files.remove(c.fileName);
                                    filesDetails.remove(c.fileName);
                                } catch (Exception ex) {
                                    ExceptionHandler.handleException(ex, activity);
                                }
                            }
                        });
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        dialog.setCancelable(false);
                        dialog.create();
                        dialog.show();
                    } catch (Throwable e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            });


            return row;
        }


        class Holder {
            ImageView delete;
            TextView fileName;
            String filepath;
        }

    }

}
