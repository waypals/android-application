package com.waypals.rest.service;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import org.json.JSONObject;


public class MobileDeRegistration implements AsyncFinishInterface{

    static String tag = "Mobile De- Registration Service";
    String methodName;
    ApplicationPreferences appPref;
    String exceptionText;
    Activity context;
    JSONObject response, input;
    String token;
    private AsyncFinishInterface asynFinishIterface;

    public MobileDeRegistration(ApplicationPreferences appPref, Activity context, String token, AsyncFinishInterface asyncFinishInterface) {
        this.token = token;
        this.context = context;
        this.appPref = appPref;
    }

    //@Override
    public void execute() {

        try {
            methodName = CH_Constant.REMOVE_DEVICE_TOKEN;
            WplHttpClient.callVolleyService(context,
                    CH_Constant.REMOVE_DEVICE_TOKEN, createRequest().toString(), appPref, this, methodName, true);
        } catch (NetworkException ex) {
            ExceptionHandler.handleException(ex, context);
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);

        }
    }


    @Override
    public void finish(String response, String method) throws Exception {

        Log.d("Deregister", response);
        if(response != null) {
            this.response = new JSONObject(response);
        }
    }

    @Override
    public void onPostExecute(Void result) {

        if (response != null && "SUCCESS".equalsIgnoreCase(response.optString("response"))) {
            Log.d(tag, "mobile app de-registration successful");
            appPref.setDeviceReq(false);
            appPref.saveDeviceRegistrationId(null);
        }  else {
            Log.e("Error", "Mobile de-registration failed");
           // Utility.makeNewToast(this.context, "Mobile de-registration failed");
        }

        LogoutService ls = new LogoutService(context);
        ls.execute();
    }


    private JSONObject createRequest() {
        try {
            String deviceVersion = Build.VERSION.RELEASE;
            String android_id = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String imei = tm.getDeviceId();


            JSONObject req = new JSONObject();
            req.put("type", "ANDRIOD");
            req.put("deviceId", android_id);
            req.put("token", token);
            req.put("osVersion", deviceVersion);
            req.put("imei", imei);
            req.put("appName", context.getPackageName());
            Log.d(tag, "MOBILE DE-REG REQ|" + req.toString());

            return req;
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex);
            Log.d(tag, "Error while creating device reg. request" + ex.getMessage());
            return null;
        }
    }
}
