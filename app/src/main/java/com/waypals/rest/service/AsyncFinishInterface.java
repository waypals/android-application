package com.waypals.rest.service;


public interface AsyncFinishInterface {

    public void finish(String response, String method) throws Exception;

    public void onPostExecute(Void Result);
}
