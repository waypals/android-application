package com.waypals.rest.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import org.json.JSONObject;


public class WaypointCategoriesService {

    static String tag = "Waypoint Categories Service";
    String methodName;
    ApplicationPreferences appPref;
    String exceptionText;
    JSONObject response, input;
    private AsyncFinishInterface asynFinishIterface;
    private Activity context;

    public void finish() throws Exception {
        asynFinishIterface.finish(response.toString(), methodName);
    }

    public void getCategories(ApplicationPreferences appPref,
                              AsyncFinishInterface asynFinishIterface) throws Exception {
        this.asynFinishIterface = asynFinishIterface;
        Log.d(tag, "getCategories called");
        AsyncNetworkTask asynTask = new AsyncNetworkTask(
                CH_Constant.GET_WAYPOINT_CATEGORIES, appPref,
                (Activity) asynFinishIterface);
        asynTask.execute();
    }

    private class AsyncNetworkTask implements AsyncFinishInterface {

        String serviceName;
        ApplicationPreferences appPref;
        Activity context;
        /**
         * progress dialog to show user that the backup is processing.
         */
        private ProgressDialog dialog;

        public AsyncNetworkTask(String serviceName, ApplicationPreferences appPref,
                                Activity context) {
            this.serviceName = serviceName;
            this.appPref = appPref;
            dialog = new ProgressDialog(context);
            this.context = context;
        }

        public void execute()
        {
            try {
                methodName = serviceName;
                WplHttpClient.callVolleyService(context,
                        CH_Constant.GET_WAYPOINT_CATEGORIES, "", appPref, this, methodName, true);
            }catch (NetworkException ex)
            {
                ExceptionHandler.handleException(ex);
            }catch (Exception ex)
            {
                ExceptionHandler.handleException(ex);
            }
        }

        @Override
        public void finish(String result, String serviceName) throws Exception {
            response = new JSONObject(result);
            asynFinishIterface.finish(response.toString(), methodName);
        }

        @Override
        public void onPostExecute(Void result) {
            try {
                asynFinishIterface.onPostExecute(result);
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, (Activity) context);
            }
        }

    }

}
