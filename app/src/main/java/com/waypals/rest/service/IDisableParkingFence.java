package com.waypals.rest.service;

public interface IDisableParkingFence {

    public void disableParkingFence(boolean success, String exceptionText);
}
