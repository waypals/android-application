package com.waypals.rest.service;

import android.content.Context;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.gson.vo.RideLocation;
import com.waypals.gson.vo.VehicleStateResponse;
import com.waypals.util.GetLocationService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import org.json.JSONObject;

public class MyVehicleState implements AsyncFinishInterface {

    IVehicleState activity;
    String response;
    ApplicationPreferences appPref;
    RideLocation location;
    String address;
    private Context context;

    public MyVehicleState(IVehicleState activity,
                          ApplicationPreferences appPref) {
        this.activity = activity;
        this.appPref = appPref;
        this.context = (Context) activity;
    }

    public static String getVehicleIdRequest(ApplicationPreferences appPref) {
        String vehicleRequest = "{vehicleId:" + appPref.getVehicle_Selected()
                + "}";
        return vehicleRequest;
    }


    public void execute() {
        try {
            WplHttpClient.callVolleyService(this.context,
                    CH_Constant.GET_VEHICLE_STATE_ON_MAP_LOAD,
                    getVehicleIdRequest(appPref), appPref, this, null, true);
        } catch (NetworkException e) {
            ExceptionHandler.handleException(e);
        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if(response != null && !response.isEmpty()) {
            JSONObject resultJSON = new JSONObject(response);
            if (resultJSON != null
                    && "SUCCESS".equals(resultJSON.optString("response"))) {
                Gson gson = new Gson();
                VehicleStateResponse vehicleStateResponse = gson.fromJson(response, VehicleStateResponse.class);
                if (vehicleStateResponse != null) {
                    location = vehicleStateResponse.getLocation();
                    GetLocationService.getAddress(location, context, new AsyncFinishInterface() {
                        @Override
                        public void finish(String response, String method) throws Exception {
                            address = response;
                        }

                        @Override
                        public void onPostExecute(Void Result) {
                            activity.getVehicleState(address);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onPostExecute(Void result) {

    }
}
