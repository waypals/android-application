package com.waypals.rest.service;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.response.ServiceResponse;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;


public class LogoutService implements AsyncFinishInterface{
    public static Handler refresh_handler = new Handler();
    Activity con;
    String response, errorCode;
    int re_login_response = 0;
    int login_out_count = 1;
    ApplicationPreferences appPref;
    private ProgressDialog progressDialog;
    private ServiceResponse serviceResponse;

    public LogoutService(Activity context) {
        this.con = context;
        appPref = new ApplicationPreferences(con);
    }

    public void execute()
    {
        try {
                WplHttpClient.callVolleyService(con, CH_Constant.Logout_Api, null, appPref, this, "Logout", true);
         //       String login_json = "{\"userCredential\":{\"login\":\"" + appPref.getUserName() + "\",\"password\":\"" + appPref.getUserPassword() + "\"}}";
           //     WplHttpClient.callVolleyService(this.con, CH_Constant.Login_Api, login_json, appPref, this, "Login", true);
        }catch (NetworkException ex)
        {
            ExceptionHandler.handleException(ex, this.con);
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if ("Logout".equalsIgnoreCase(method) && response != null) {
            this.response = response;
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    private void logout(ApplicationPreferences appPref)
    {
        if(appPref != null) {
            appPref.clearAll();
            appPref.saveLogout(true);
            appPref.saveSOSSend("false");
            appPref.setDeviceReq(false);
            appPref.saveDeviceRegistrationId(null);
        }

        Intent intent = new Intent(con, Login_Screen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        con.startActivity(intent);
        con.finish();
    }


    @Override
    public void onPostExecute(Void res) {

        Log.d("LogoutResponse", response);
        if (serviceResponse != null && "SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
            try {
                    logout(appPref);
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, con);
            }
        }else
        {
            try {
                logout(appPref);
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, con);
            }
        }

    }

}