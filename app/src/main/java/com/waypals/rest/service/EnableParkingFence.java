package com.waypals.rest.service;

import android.content.Context;
import android.util.Log;

import com.waypals.exception.NetworkException;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import org.json.JSONObject;

public class EnableParkingFence implements  AsyncFinishInterface{

    IEnableParkingFence activity;
    String response;
    ApplicationPreferences appPref;
    boolean enable;
    String exceptionText;
    private Context context;

    public EnableParkingFence(IEnableParkingFence activity, ApplicationPreferences appPref) {
        this.activity = activity;
        this.appPref = appPref;
        this.context = (Context) activity;
    }

    public static String getVehicleIdRequest(ApplicationPreferences appPref) {
        String vehicleRequest = "{vehicleId:" + appPref.getVehicle_Selected()
                + "}";
        return vehicleRequest;
    }

    public void execute()
    {
        try {
            exceptionText = "";
            WplHttpClient.callVolleyService(context,
                    CH_Constant.EnablingParkfence_Api,
                    getVehicleIdRequest(appPref), appPref, this, null, true);
        } catch (NetworkException e) {
            Log.e("Error", e.getMessage());
            exceptionText = CH_Constant.NETWORK_ERROR;
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            exceptionText = e.getLocalizedMessage();
        }

    }

    @Override
    public void finish(String response, String method) throws Exception {
        JSONObject resultJSON = new JSONObject(response);
        if (response != null && resultJSON.optString("response").equals("SUCCESS")) {
            enable = true;
        } else {
            enable = false;
        }
    }

    @Override
    public void onPostExecute(Void result) {
        activity.enableParkingFence(enable, exceptionText);
    }

}
