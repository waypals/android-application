package com.waypals.rest.service;

import android.app.Activity;
import android.content.Context;

import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

public class VehicleServices {

    AsyncFinishInterface asynFinishIterface;
    String response;
    String methodName;

    public static void getVehicleData(ApplicationPreferences appPref, Context context, AsyncFinishInterface asynInterface, String method) throws Exception {
        WplHttpClient.callVolleyService(context, CH_Constant.GET_VEHICLE_DATA_ON_LOGIN_SUCCESS, null, appPref, asynInterface, method, true);
    }

    public static String getVehicleIdRequest(ApplicationPreferences appPref) {
        String vehicleRequest = "{vehicleId:" + appPref.getVehicle_Selected()
                + "}";
        return vehicleRequest;
    }

    public void getVehiclesData(ApplicationPreferences appPref,
                                AsyncFinishInterface asynFinishIterface, boolean showDialog) throws Exception {
        this.asynFinishIterface = asynFinishIterface;

        AsyncNetworkTask asynTask = new AsyncNetworkTask(
                CH_Constant.GET_VEHICLE_DATA_ON_LOGIN_SUCCESS, appPref,
                (Activity) asynFinishIterface, asynFinishIterface, showDialog);
        asynTask.execute();
    }

    public void getVehicleState(ApplicationPreferences appPref,
                                AsyncFinishInterface asynFinishIterface) throws Exception {
        this.asynFinishIterface = asynFinishIterface;

        AsyncNetworkTask asynTask = new AsyncNetworkTask(
                CH_Constant.GET_VEHICLE_STATE_ON_MAP_LOAD, appPref,
                (Activity) asynFinishIterface, asynFinishIterface);
        asynTask.execute();
    }

    public void getListOfGeoFence(ApplicationPreferences appPref, Context context, AsyncFinishInterface asyncFinishInterface)
            throws Exception {
        WplHttpClient.callVolleyService(context, CH_Constant.List_Geofence_Api,
                getVehicleIdRequest(appPref), appPref, asyncFinishInterface, null, true);
    }

    public void getCarInfo(ApplicationPreferences appPref,
                           AsyncFinishInterface asynFinishIterface) throws Exception {
        this.asynFinishIterface = asynFinishIterface;

        AsyncNetworkTask asynTask = new AsyncNetworkTask(
                CH_Constant.GET_CAR_INFO, appPref, (Activity) asynFinishIterface, asynFinishIterface);
        asynTask.execute();
    }

    public void getTelematicsData(ApplicationPreferences appPref,
                                  AsyncFinishInterface asynFinishIterface) throws Exception {
        this.asynFinishIterface = asynFinishIterface;

        AsyncNetworkTask asynTask = new AsyncNetworkTask(
                CH_Constant.GET_TELEMATICS_DATA, appPref,
                (Activity) asynFinishIterface, asynFinishIterface);
        asynTask.execute();
    }

    public void getSOSNumber(ApplicationPreferences appPref,
                             AsyncFinishInterface asynFinishIterface, String methodName) throws Exception {
        this.asynFinishIterface = asynFinishIterface;

        AsyncNetworkTask asynTask = new AsyncNetworkTask(
                CH_Constant.GET_SOS_NUMBERS, appPref,
                (Activity) asynFinishIterface, asynFinishIterface);

        asynTask.setMethodName(methodName);
        asynTask.execute();
    }

    public void finish() throws Exception {
        asynFinishIterface.finish(response, methodName);
    }

    private class AsyncNetworkTask{

        private String methodName; //} extends AsyncTask<Void, Void, Void> {

        String serviceName;
        ApplicationPreferences appPref;
        Activity context;
        AsyncFinishInterface asyncFinishInterface;
        boolean showDialog = true;

        public AsyncNetworkTask(String serviceName, ApplicationPreferences appPref,
                                Activity context, AsyncFinishInterface asyncFinishInterface) {
            this.serviceName = serviceName;
            this.appPref = appPref;
            this.context = context;
            this.methodName = serviceName;
            this.asyncFinishInterface = asyncFinishInterface;
        }

        public AsyncNetworkTask(String serviceName, ApplicationPreferences appPref,
                                Activity context, AsyncFinishInterface asyncFinishInterface, boolean showDialog) {
            this.serviceName = serviceName;
            this.appPref = appPref;
            this.context = context;
            this.asyncFinishInterface = asyncFinishInterface;
            this.showDialog = showDialog;
            this.methodName = serviceName;
        }

        public void setMethodName(String methodName)
        {
            this.methodName = methodName;
        }

        public void execute()
        {
            try {
                WplHttpClient.callVolleyService(context, serviceName,
                        getVehicleIdRequest(appPref), appPref, asyncFinishInterface, methodName, this.showDialog);
            } catch (NetworkException ex) {
                ExceptionHandler.handleException(ex, context);
            } catch (Throwable e) {
                ExceptionHandler.handleException(e);
            }
        }


    }

}
