package com.waypals.rest.service;

import android.content.Context;

import com.waypals.exception.NetworkException;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import org.json.JSONObject;

public class DisableParkingFence implements AsyncFinishInterface{

    IDisableParkingFence activity;
    String response;
    ApplicationPreferences appPref;
    boolean enable;
    String exceptionText;
    private Context context;

    public DisableParkingFence(IDisableParkingFence activity, ApplicationPreferences appPref) {
        this.activity = activity;
        this.appPref = appPref;
        this.context = (Context) activity;
    }

    public static String getVehicleIdRequest(ApplicationPreferences appPref) {
        String vehicleRequest = "{vehicleId:" + appPref.getVehicle_Selected()
                + "}";
        return vehicleRequest;
    }

    public void execute()
    {
        try {
            this.exceptionText = "";
            WplHttpClient.callVolleyService(context,
                    CH_Constant.DisablingParkfence_Api,
                    getVehicleIdRequest(appPref), appPref, this, null, true);

        } catch (NetworkException e) {
            e.printStackTrace();
            exceptionText = CH_Constant.NETWORK_ERROR;
        } catch (Exception e) {
            e.printStackTrace();
            exceptionText = e.getLocalizedMessage();
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        JSONObject resultJSON = new JSONObject(response);
        if (response != null && resultJSON.optString("response").equals("SUCCESS")) {
            enable = true;
        } else {
            enable = false;
        }
    }

    @Override
    public void onPostExecute(Void result) {
        activity.disableParkingFence(enable, exceptionText);
    }

}
