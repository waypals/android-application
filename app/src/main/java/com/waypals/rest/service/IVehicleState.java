package com.waypals.rest.service;


public interface IVehicleState {

    public void getVehicleState(String location);
}
