package com.waypals.rest.service;

public interface IEnableParkingFence {

    public void enableParkingFence(boolean success, String exceptionMessage);
}
