package com.waypals.rest.service;

import android.content.Context;

import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.RideLocation;
import com.waypals.util.GetLocationService;
import com.waypals.utils.ApplicationPreferences;

public class AddressService { //} extends AsyncTask<Void, Void, Void> {

    IAddress activity;
    RideLocation location;
    String address;
    private Context context;

    public AddressService(IAddress activity, RideLocation location) {
        this.location = location;
        this.activity = activity;
        this.context = (Context) activity;
    }

    public static String getVehicleIdRequest(ApplicationPreferences appPref) {
        String vehicleRequest = "{vehicleId:" + appPref.getVehicle_Selected()
                + "}";
        return vehicleRequest;
    }


    public void execute()
    {
        try {
             GetLocationService.getAddress(location, context, new AsyncFinishInterface() {
                 @Override
                 public void finish(String response, String method) throws Exception {
                     address = response;
                 }

                 @Override
                 public void onPostExecute(Void Result) {
                     activity.setAddress(address);
                 }
             });
        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }

}
