package com.waypals.rest.service;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.waypals.exception.ExceptionHandler;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import org.json.JSONObject;


public class MobileRegistration {

    static String tag = "Mobile Registration Service";
    String methodName;
    ApplicationPreferences appPref;
    String exceptionText;
    Activity context;
    JSONObject response, input;
    String token;

    public MobileRegistration(ApplicationPreferences appPref, Activity context, String token) {
        this.token = token;
        this.context = context;
        this.appPref = appPref;
    }

    public void execute() {
        try {
            WplHttpClient.callVolleyService(context, CH_Constant.SAVE_DEVICE_TOKEN, createRequest().toString(), appPref, (AsyncFinishInterface) context, "MOBILE_REG", true);
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    private JSONObject createRequest() {
        try {
            String deviceVersion = Build.VERSION.RELEASE;
            String android_id = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String imei = tm.getDeviceId();

            JSONObject req = new JSONObject();
            req.put("type", "ANDRIOD");
            req.put("deviceId", android_id);
            req.put("token", token);
            req.put("osVersion", deviceVersion);
            req.put("imei", imei);
            req.put("appName", context.getPackageName());
            Log.d(tag, "MOBILE REG REQ|" + req.toString());

            return req;
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex);
            Log.d(tag, "Error while creating device reg. request" + ex.getMessage());
            return null;
        }
    }
}
