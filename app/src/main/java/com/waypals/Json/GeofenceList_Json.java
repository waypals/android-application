package com.waypals.Json;

import android.content.Context;
import android.util.Log;

import com.waypals.Beans.GeofenceList_Beans;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GeofenceList_Json {
    public static int no_of_records;
    public static String errorCode, response;
    public static int connectionTimeOut;
    public static List<GeofenceList_Beans> Geofence_list;
    private static GeofenceList_Json mGeofenceList_Json;
    Context c;
    ApplicationPreferences appPrefs;

    public static GeofenceList_Json getGeofenceList_Json() {
        return mGeofenceList_Json;
    }

    public List<GeofenceList_Beans> parseGeofenceList_Json(Context contxt, long vehicleId) {
//		mGeofenceList_Json = this;
        connectionTimeOut = 0;
        appPrefs = new ApplicationPreferences(contxt);

        String bodyRequest = "{\"vehicleId\":\"" + vehicleId + "\"}";
        System.out.println("GeofenceList BodyRequest :: " + bodyRequest);

        try {
            String s = null;
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost postMethod = new HttpPost(CH_Constant.List_Geofence_Api);

//							byte[] data = bodyRequest.getBytes("UTF-8");
//							String base64 = Base64.encodeToString(data, Base64.DEFAULT);
//							postMethod.setEntity(new StringEntity(base64));

                postMethod.setEntity(new StringEntity(bodyRequest));
                postMethod.setHeader("Content-Type", "application/json");
                postMethod.setHeader("Accept-encoding", "gzip");
                postMethod.setHeader("security-token", appPrefs.getUserToken());

                HttpResponse httpresponse = httpclient.execute(postMethod);
                HttpEntity responseEntity = httpresponse.getEntity();
                s = EntityUtils.toString(responseEntity);
                Log.e(" GeofenceList response", s);
            } catch (ConnectTimeoutException e) {
                e.printStackTrace();
                connectionTimeOut = 1;
                return null;
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                connectionTimeOut = 2;
                return null;
            }


            JSONObject resObject = new JSONObject(s);

            response = resObject.getString("response");

            if (response.equalsIgnoreCase("SUCCESS")) {
                Geofence_list = new ArrayList<GeofenceList_Beans>();

                JSONArray geofenceArray = (JSONArray) resObject.get("geoFences");

                if (geofenceArray.length() > 0) {
                    for (int i = 0; i < geofenceArray.length(); i++) {
                        GeofenceList_Beans geo_Data = new GeofenceList_Beans();
                        JSONObject geoObj = geofenceArray.getJSONObject(i);

                        geo_Data.setGeofence_Name(geoObj.getString("id"));
                        geo_Data.setGeofence_Name(geoObj.getString("name"));

                        JSONObject geographArea_obj = geoObj.getJSONObject("geographicalArea");
                        geo_Data.setGraphArea_Id(geographArea_obj.getString("id"));
                        geo_Data.setGraphArea_LeftTop(geographArea_obj.getString("leftTop"));
                        geo_Data.setGraphArea_Radius(geographArea_obj.getString("radius"));
                        geo_Data.setGraphArea_RightBottom(geographArea_obj.getString("rightBottom"));
                        try {
                            JSONObject geographArea_center_obj = geographArea_obj.getJSONObject("center");
                            geo_Data.setGraphArea_Center_Lat(geographArea_center_obj.getString("latitude"));
                            geo_Data.setGraphArea_Center_Long(geographArea_center_obj.getString("longitude"));
                        } catch (Exception e) {
                            geo_Data.setGraphArea_Center_Lat("0.0");
                            geo_Data.setGraphArea_Center_Long("0.0");
                        }

                        try {
                            JSONObject allowedDeviation_obj = geoObj.getJSONObject("allowedDeviation");
                            geo_Data.setAllowedDeviation(allowedDeviation_obj.getString("distance") + allowedDeviation_obj.getString("unit"));
                        } catch (Exception e) {
                            e.printStackTrace();
                            geo_Data.setAllowedDeviation("");
                        }

                    }
                }

            } else
                errorCode = resObject.getString("errorCode");


        } catch (Exception e) {
            e.printStackTrace();
            connectionTimeOut = -1;
            return null;
        }


        return Geofence_list;
    }


}
