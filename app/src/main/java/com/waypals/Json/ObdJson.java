package com.waypals.Json;

import java.lang.reflect.Field;

public class ObdJson {

    OBDEndOfTrip obdEndOfTrip;
    private String response;
    private String speed;
    private String speedUnit;
    private String fuelLevel;
    private String engineState;
    private String rpm;
    private String throttlePosition;
    private String coolandTemp;
    private String intakeAirTemp;
    private String airFlowRate;
    private String vinNumber;
    private String engineLoad;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSpeed() {
        if (speed == null || speed == "null") {
            return "NA";
        }
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getSpeedUnit() {
        return speedUnit;
    }

    public void setSpeedUnit(String speedUnit) {
        this.speedUnit = speedUnit;
    }

    public String getFuelLevel() {
        if (fuelLevel == null || fuelLevel == "null") {
            return "NA";
        }

        return fuelLevel;
    }

    public void setFuelLevel(String fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public String getEngineState() {
        if (engineState == null || engineState == "null") {
            return "NA";
        }
        return engineState;
    }

    public void setEngineState(String engineState) {
        this.engineState = engineState;
    }

    public String getRpm() {
        if (rpm == null || rpm == "null") {
            return "NA";
        }
        return rpm;
    }

    public void setRpm(String rpm) {
        this.rpm = rpm;
    }

    public String getThrottlePosition() {

        if (throttlePosition == null || throttlePosition == "null") {
            return "NA";
        }
        return throttlePosition;
    }

    public void setThrottlePosition(String throttlePosition) {
        this.throttlePosition = throttlePosition;
    }

    public String getCoolandTemp() {
        if (coolandTemp == null || coolandTemp == "null") {
            return "NA";
        }
        return coolandTemp;
    }

    public void setCoolandTemp(String coolandTemp) {
        this.coolandTemp = coolandTemp;
    }

    public String getIntakeAirTemp() {
        if (intakeAirTemp == null || intakeAirTemp == "null") {
            return "NA";
        }
        return intakeAirTemp;
    }

    public void setIntakeAirTemp(String intakeAirTemp) {
        this.intakeAirTemp = intakeAirTemp;
    }

    public String getAirFlowRate() {
        if (airFlowRate == null || airFlowRate == "null") {
            return "NA";
        }
        return airFlowRate;
    }

    public void setAirFlowRate(String airFlowRate) {
        this.airFlowRate = airFlowRate;
    }

    public String getVinNumber() {
        if (vinNumber == null || vinNumber == "null") {
            return "NA";
        }
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getEngineLoad() {
        if (engineLoad == null || engineLoad == "null") {
            return "NA";
        }
        return engineLoad;
    }

    public void setEngineLoad(String engineLoad) {
        this.engineLoad = engineLoad;
    }

    public OBDEndOfTrip getObdEndOfTrip() {
        return obdEndOfTrip;
    }

    public void setObdEndOfTrip(OBDEndOfTrip obdEndOfTrip) {
        this.obdEndOfTrip = obdEndOfTrip;
    }

    @Override
    public String toString() {
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder result = new StringBuilder();

        for (Field field : fields) {
            result.append(" ");
            result.append(field.getName());
            result.append(": ");
            //requires access to private field:
            try {
                result.append(field.get(this));
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (obdEndOfTrip != null) {
            obdEndOfTrip.toString();
        }
        return result.toString();
    }

}
