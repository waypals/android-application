package com.waypals.Json;

import android.content.Context;
import android.util.Log;

import com.waypals.utils.ApplicationPreferences;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Parkingfence_Json {
    public static int no_of_records;
    public static String errorCode, response;
    public static int connectionTimeOut;
    private static Parkingfence_Json mParkingfence_Json;
    Context c;
    ApplicationPreferences appPrefs;

    public Parkingfence_Json(Context c) {
        this.c = c;
    }

    public static Parkingfence_Json getParkingfence_Json() {
        return mParkingfence_Json;
    }

    public Boolean parseParkingfence_Json(Context contxt, String apiPath, String bodyRequest) {
        mParkingfence_Json = this;
        connectionTimeOut = 0;
        appPrefs = new ApplicationPreferences(contxt);

        try {
            String s = null;
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost postMethod = new HttpPost(apiPath);

                //			byte[] data = login_json.getBytes("UTF-8");
                //			String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                //			postMethod.setEntity(new StringEntity(base64));

                postMethod.setEntity(new StringEntity(bodyRequest));
                postMethod.setHeader("Content-Type", "application/json");
                postMethod.setHeader("Accept-encoding", "gzip");
                postMethod.setHeader("security-token", appPrefs.getUserToken());

//				HttpParams httpParams = postMethod.getParams();
//				int timeoutdConnection = 60000;
//				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutdConnection);
//				postMethod.setParams(httpParams);

                HttpResponse httpresponse = httpclient.execute(postMethod);
                HttpEntity responseEntity = httpresponse.getEntity();
                s = EntityUtils.toString(responseEntity);
                Log.e(" Parkfence response", s);
            } catch (ConnectTimeoutException e) {
                e.printStackTrace();
                connectionTimeOut = 1;
                return false;
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                connectionTimeOut = 2;
                return false;
            }


            //	        {"response":"SUCCESS","result":{"token":"69f60in35wv7zm7ihfrand6x7dco5igf"}}
            //	        {"response":"ERROR","errorCode":"AUTHENTICATION_FAILED"}
            //	        {"response":"ERROR","errorCode":"SYSTEM_ERROR "}


            JSONObject resObject = new JSONObject(s);

            response = resObject.getString("response");

            if (response.equalsIgnoreCase("SUCCESS")) {
                return true;
            } else
                errorCode = resObject.getString("errorCode");


        } catch (Exception e) {
            e.printStackTrace();
            connectionTimeOut = -1;
            return false;
        }


        return false;
    }


}
