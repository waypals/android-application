package com.waypals.Json;

import java.io.Serializable;


//{"obdEot":{"distance":6036 m ,"maxSpeed":64 km/hr,"totalFuelCons":0,"idleTime":171 s,"totalTime":1010 s}}}.
public class OBDEndOfTrip implements Serializable {

    private double distance;
    private int totalFuelCons;
    private int idleTime; // in sec
    private int totalTime; // in sec
    private long logTime;
    private long vehicleId;
    private double maxSpeed;


    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getTotalFuelCons() {
        return totalFuelCons;
    }

    public void setTotalFuelCons(int totalFuelCons) {
        this.totalFuelCons = totalFuelCons;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(int idleTime) {
        this.idleTime = idleTime;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public long getLogTime() {
        return logTime;
    }

    public void setLogTime(long logTime) {
        this.logTime = logTime;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }


}
