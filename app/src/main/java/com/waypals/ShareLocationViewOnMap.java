package com.waypals;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.waypals.objects.MyShareLocationResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.wareninja.opensource.common.ObjectSerializer;

import java.util.List;

/**
 * Created by shantanu on 18/10/14.
 */
public class ShareLocationViewOnMap extends FragmentActivity implements
        AsyncFinishInterface {

    static String tag = "Share location View On MAP";

    TextView title;
    Typeface tf;
    Context context;
    ImageView back_btn, right_btn;
    ApplicationPreferences appPref;
    GoogleMap map = null;
    private AsyncFinishInterface asyncFinishInterface;
    private MyShareLocationResponse singleShareLocationRequest = null;
    private ServiceResponse serviceResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sharelocation_screen);
        asyncFinishInterface = this;

        context = this;
        appPref = new ApplicationPreferences(context);
        title = (TextView) findViewById(R.id.nav_title_txt);
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Typeface tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);
        title.setTypeface(tf);
        title.setText("SHARE LOCATION");

        setUpMap(true);


    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMap(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    public void setUpMap(final boolean isInitial) {
        if (map == null) {
            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;
                    map.setMapType(1);
                    map.getUiSettings().setCompassEnabled(true);

                    if(isInitial){
                        Intent intent = getIntent();
                        String data = intent.getStringExtra("data");
                        if (data != null && data.equals("ALL")) {
                            GenericAsyncService service = new GenericAsyncService(appPref, ShareLocationViewOnMap.this, CH_Constant.MY_SHARELOCATION_REQUEST, "MY_REQUESTS", "{}", tag, asyncFinishInterface, true);
                            service.execute();
                        } else {
                            singleShareLocationRequest = (MyShareLocationResponse) ObjectSerializer.deserialize(data);
                            LatLng l = new LatLng(singleShareLocationRequest.getLatitude(), singleShareLocationRequest.getLongitude());
                            MarkerOptions op = new MarkerOptions();
                            op.title(singleShareLocationRequest.getName() + " is here");
                            op.position(l);
                            map.addMarker(op);
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(l, 10));
                        }
                    }
                }
            });

        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (method.equals("MY_REQUESTS")) {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        setUpMapIfNeeded();

        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())))  {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, getString(R.string.server_error));
            }

            return;
        }

        if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
            List<MyShareLocationResponse> data = serviceResponse.getMyrequests();
            if (data != null) {
                for (MyShareLocationResponse r : data) {
                    LatLng l = new LatLng(r.getLatitude(), r.getLongitude());
                    MarkerOptions op = new MarkerOptions();
                    op.title(r.getName() + " is here");
                    op.position(l);
                    map.addMarker(op);

                    if (singleShareLocationRequest == null) {
                        singleShareLocationRequest = r;
                    }
                }
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(singleShareLocationRequest.getLatitude(), singleShareLocationRequest.getLongitude()), 10));
            }
        }
    }

    public boolean isGoogleMapsInstalled() {
        try {
            Log.i(tag, "check Google Map installed");
            ApplicationInfo info;
            info = getPackageManager().getApplicationInfo(
                    "com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;

        }
    }

    private void setUpMapIfNeeded() {
        setUpMap(false);
    }

}
