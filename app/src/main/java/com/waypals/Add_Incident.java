package com.waypals;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;

public class Add_Incident extends NavigationDrawer {

    public static Add_Incident mAdd_Incident;
    static Context context;
    static String tag = "Add Incident";
    ImageView back_btn;
    TextView navTitle;
    LinearLayout accidnt_btn, breakdwn_btn, theft_btn;
    ApplicationPreferences appPref;
    String location = "";
    Typeface tf;
    private TextView tv_accident;
    private TextView tv_Break_down;
    //  private TextView tv_Traffic_violation;
    private TextView tv_Theft;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_inci_list);
        context = Add_Incident.this;
        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        mAdd_Incident = this;

        back_btn = (ImageView) findViewById(R.id.left_btn);
        navTitle = (TextView) findViewById(R.id.nav_title_txt);
        accidnt_btn = (LinearLayout) findViewById(R.id.inci_accidnt_btn);
        //traffic_btn = (LinearLayout) findViewById(R.id.inci_traffic_btn);
        breakdwn_btn = (LinearLayout) findViewById(R.id.inci_brkdwn_btn);
        theft_btn = (LinearLayout) findViewById(R.id.inci_theft_btn);

        tv_accident = (TextView) findViewById(R.id.tv_accident);
        tv_Break_down = (TextView) findViewById(R.id.tv_Break_down);
        tv_Theft = (TextView) findViewById(R.id.tv_Theft);
        // tv_Traffic_violation = (TextView) findViewById(R.id.tv_Traffic_violation);


        Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        navTitle.setTypeface(tf);
        navTitle.setText("ADD INCIDENT");

        tv_accident.setTypeface(tf);
        tv_Break_down.setTypeface(tf);
//        tv_Traffic_violation.setTypeface(tf);
        tv_Theft.setTypeface(tf);


        appPref = new ApplicationPreferences(this);


//        try {
//            setupLeftDrawerListener(appPref, tf, false);
//            setupRightDrawerListener(tf);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

        accidnt_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Add_Incident.this,
                        Incident_Detail_Screen.class);
                i.putExtra("type", "get");
                i.putExtra("add_incident", "ACCIDENT");
                startActivity(i);
            }
        });

//        traffic_btn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Add_Incident.this,
//                        Incident_Detail_Screen.class);
//                i.putExtra("type", "get");
//                i.putExtra("add_incident", "TRAFFIC VIOLATION");
//                startActivity(i);
//            }
//        });

        breakdwn_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Add_Incident.this,
                        Incident_Detail_Screen.class);
                i.putExtra("type", "get");
                i.putExtra("add_incident", "BREAK DOWN");
                startActivity(i);
            }
        });

        theft_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Add_Incident.this,
                        Incident_Detail_Screen.class);
                i.putExtra("type", "get");
                i.putExtra("add_incident", "THEFT");
                startActivity(i);
            }
        });

        back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }


    @Override
    protected void setImmobilizer() {

    }
}
