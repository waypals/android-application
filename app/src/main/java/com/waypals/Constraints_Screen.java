package com.waypals;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.gson.vo.ConstraintAlertVO;
import com.waypals.request.ConstraintAlertRequest;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;
import com.waypals.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Constraints_Screen extends Activity {
    static Typeface typeface;
    String exceptionText;
    ApplicationPreferences appPref;
    ConstraintAlertVO[] constraintAlertVOArray;
    private ImageView backButton;
    private ListView constraint_list;
    private TextView nav_title_txt;

    public static String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPref = new ApplicationPreferences(this);
        setContentView(R.layout.constraints_list);
        typeface = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);

        constraint_list = (ListView) findViewById(R.id.constraint_list);
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(typeface);
        nav_title_txt.setText("ALERTS");

        backButton = (ImageView) findViewById(R.id.left_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        new ConstraintAsyncTask(this).execute();


    }

    public void populateList(JSONObject response) {
        JSONArray jsonArray;
        JSONObject row;

        try {
            jsonArray = response.getJSONArray("alertsResponseList");
            int size = jsonArray.length();

            if (size > 0) {
                constraintAlertVOArray = new ConstraintAlertVO[size];
                for (int i = 0; i < jsonArray.length(); i++) {
                    row = jsonArray.getJSONObject(i);
                    ConstraintAlertVO constraintAlertVO = new ConstraintAlertVO();

                    String date = getDate(Long.valueOf(row.getString("date")), "dd/MM/yyyy hh:mm a");
                    String[] dateSplited = date.split("\\s+");

                    constraintAlertVO.setDateInMilliSec(dateSplited[0] + " , " + dateSplited[1] + " " + dateSplited[2]);
                    constraintAlertVO.setMessage(row.getString("message"));
                    constraintAlertVO.setResponse(row.getString("response"));
                    constraintAlertVO.setType(row.getString("type"));


                    constraintAlertVOArray[i] = constraintAlertVO;

                }

                ContentAdapter contentAdapter = new ContentAdapter(this, R.layout.constraint_alert_list_row, constraintAlertVOArray);
                constraint_list.setAdapter(contentAdapter);
            } else {
                try {
                    Utility.makeNewToast(this, getString(R.string.op_alert_no_constraint_alerts));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, this);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class ConstraintAsyncTask implements AsyncFinishInterface {

        JSONObject response;
        Context constraints_Screen;

        public ConstraintAsyncTask(Context constraints_Screen) {

            this.constraints_Screen = constraints_Screen;

        }

        public void execute() {
            try {
                exceptionText = "";
                WplHttpClient.callVolleyService(constraints_Screen,
                        CH_Constant.GET_ALERTS,
                        "", appPref, this, null, true);

            } catch (NetworkException e) {
                ExceptionHandler.handleException(e);
                //				dialog.setMessage("Network Exception " + e.getMessage());
                exceptionText = CH_Constant.NETWORK_ERROR;
            } catch (Exception e) {
                ExceptionHandler.handleException(e);
                //				dialog.setMessage("Network Exception " + e.getMessage());
                exceptionText = e.getLocalizedMessage();
            }
        }

        @Override
        public void finish(String response, String method) throws Exception {
            this.response = new JSONObject(response);
            Log.d("ConstraintAlert", "" + response);
            Gson gson = new Gson();
            ConstraintAlertRequest constraintAlertRequest = gson.fromJson(response.toString(), ConstraintAlertRequest.class);
            if (constraintAlertRequest != null) {
                constraintAlertVOArray = constraintAlertRequest.getConstraintAlertVO();
            }
        }

        @Override
        public void onPostExecute(Void result) {

           populateList(response);

        }
    }

    class ContentAdapter extends ArrayAdapter<ConstraintAlertVO> {
        Context context;
        int layoutResourceId;
        ConstraintAlertVO data[] = null;
        private ContentsHolder holder;

        public ContentAdapter(Constraints_Screen context, int alertListRow, ConstraintAlertVO[] constraintAlertVOArray) {
            super(context, alertListRow, constraintAlertVOArray);

            this.context = context;
            layoutResourceId = alertListRow;
            data = constraintAlertVOArray;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;
            typeface = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            if (convertView == null) {
                LayoutInflater inflater = ((Constraints_Screen) context).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new ContentsHolder();
//			holder.alertType = (TextView) row.findViewById(R.id.car_no_plate_alrt);

                holder.message = (TextView) row.findViewById(R.id.date_time_alert);
                holder.message.setTypeface(typeface);
                holder.time = (TextView) row.findViewById(R.id.alert_type);
                holder.time.setTypeface(typeface);
                holder.inci_image = (ImageView) row.findViewById(R.id.inci_image);
                row.setTag(holder);
            }
            holder = (ContentsHolder) row.getTag();

            ConstraintAlertVO constraintAlertVO = data[position];
            String alertType = constraintAlertVO.getType();

            if (alertType.equalsIgnoreCase("OUT_BOUND_GEO_FENCE")) {
                holder.inci_image.setImageResource(R.drawable.leave_geofence);
            } else if (alertType.equalsIgnoreCase("SYSTEM_FENCE")) {
                holder.inci_image.setImageResource(R.drawable.parkingfence);
            } else if (alertType.equalsIgnoreCase("IN_BOUND_GEO_FENCE")) {
                holder.inci_image.setImageResource(R.drawable.enter_geofence);
            } else if (alertType.equalsIgnoreCase("SPEED_LIMIT")) {
                holder.inci_image.setImageResource(R.drawable.speed_limit_violation);
            }

//            holder.inci_image.getLayoutParams().width=140;
//            holder.inci_image.getLayoutParams().height=140;
//            holder.inci_image.requestLayout();
//		holder.alertType.setText(alertType);
            holder.message.setText(constraintAlertVO.getMessage());
            holder.time.setText("at " + constraintAlertVO.getDateInMilliSec());


            return row;
        }


        class ContentsHolder {
            TextView time;
            //		TextView alertType;
            TextView message;
            ImageView inci_image;

        }


    }
}




