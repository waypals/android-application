package com.waypals;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.EmergencyContactAdapter;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.SOSContacts;
import com.waypals.gson.vo.SosNumber;
import com.waypals.gson.vo.SosNumberRequest;
import com.waypals.objects.EmergencyContact;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EmergencyNoActivity extends Activity implements AsyncFinishInterface{

    private ImageView back_btn;
    private TextView nav_title_txt;
    private Typeface tf;
    private ImageView right_btn;
    private ServiceResponse serviceResponse;

    private EmergencyContactAdapter adapter;
    private ListView listView;
    private ApplicationPreferences appPref;
    private List<SOSContacts> lstOfSOSContacts;


    private BroadcastReceiver editDeleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            int id = -1;
            if(action != null)
            {
                switch (action)
                {
                    case CH_Constant.DELETE_EMERGENCY_NO:

                         id = intent.getIntExtra("id", -1);
                         if(id != -1)
                         {
                             int count = adapter.getCount();
                             List<EmergencyContact> lstItems = new ArrayList<>();
                             for(int i=0;i<count; i++)
                             {
                                 if(adapter.getItem(i).getId() != id) {
                                   //  adapter.getItem(i);
                                     lstItems.add(adapter.getItem(i));
                                 }
                             }

                             addContacts(lstItems);
                         }

                         break;
                    case CH_Constant.EDIT_EMERGENCY_NO:

                        id = intent.getIntExtra("id",-1);
                        if(id != -1)
                        {
                            addEditContact(id);
                        }

                         break;
                }
            }
        }
    };
    private String method;
    private String GET_EMERGENCY_CONTACTS = "GET_EMERGENCY_CONTACTS";

    private  void addContacts(List<EmergencyContact> lstContacts)
    {
        try {
            if (lstContacts != null && !lstContacts.isEmpty()) {
                List<SosNumber> lstSosContacts = Utility.convertToSosContacts(lstContacts);
                SosNumberRequest sosContactsList = new SosNumberRequest();
                sosContactsList.setSosNumbers(lstSosContacts);
                Gson gson = new Gson();
                String input = gson.toJson(sosContactsList);
                Log.d("SosRequest", input);
                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.SAVE_UPDATE_SOS, CH_Constant.DELETE_EMERGENCY_NO, input, "UpdateSoS", this, true);
                service.execute();
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(editDeleteReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_no);

        IntentFilter filter = new IntentFilter();
        filter.addAction(CH_Constant.EDIT_EMERGENCY_NO);
        filter.addAction(CH_Constant.DELETE_EMERGENCY_NO);
        LocalBroadcastManager.getInstance(this).registerReceiver(editDeleteReceiver, filter);

        appPref = new ApplicationPreferences(this);
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText(getString(R.string.emergency));

        right_btn = (ImageView) findViewById(R.id.right_btn);
        right_btn.setVisibility(View.VISIBLE);
        right_btn.setImageResource(R.drawable.addicon);
        right_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int len =  adapter.getCount();

                if(len < 5) {
                        addEditContact(-1);
                }else {
                    Utility.makeNewToast(EmergencyNoActivity.this, "Maximum limit reached");
                    return;
                }
            }
        });

        listView = (ListView) findViewById(R.id.emergency_list);
        adapter = new EmergencyContactAdapter(this);
        listView.setAdapter(adapter);

    //    getEmergencyContacts();

    }

    private void addEditContact(int id)
    {
        Intent addNumber = new Intent(EmergencyNoActivity.this, EmergencyNoAddActivity.class);
        List<EmergencyContact> contacts = new ArrayList<EmergencyContact>();
        int len =  adapter.getCount();
        for (int i = 0; i < len; i++) {
            contacts.add(adapter.getItem(i));
        }

        addNumber.putExtra("editId", id);
        addNumber.putExtra("contacts", (Serializable) contacts);
        startActivity(addNumber);
    }


    @Override
    protected void onResume() {
        super.onResume();
        getEmergencyContacts();
    }

    @Override
    public void finish(String response, String method) throws Exception {

            serviceResponse = null;
            this.method = method;
            if(response != null) {
                Gson gson = new Gson();
                serviceResponse = gson.fromJson(response,ServiceResponse.class);

                if(GET_EMERGENCY_CONTACTS.equals(method)) {
                    appPref.saveSOSNumbers(response);
                }
                Log.d("Emergency", response);
            }
    }

    private void getEmergencyContacts()
    {
        try {
            VehicleServices vehicleServices = new VehicleServices();
            vehicleServices.getSOSNumber(appPref, this, GET_EMERGENCY_CONTACTS);
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if(serviceResponse!=null)
        {
            if("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse()))
            {
                if (CH_Constant.DELETE_EMERGENCY_NO.equals(this.method))
                {
                    getEmergencyContacts();
                }
                else{
                    lstOfSOSContacts = appPref.getSOSServiceResponse(false);
                    if (lstOfSOSContacts != null && !lstOfSOSContacts.isEmpty()) {

                        adapter.clear();
                        adapter.notifyDataSetChanged();

                        for (int i = 0; i < lstOfSOSContacts.size(); i++) {
                            EmergencyContact item = new EmergencyContact(lstOfSOSContacts.get(i));
                            item.setId(i);
                            adapter.add(item);
                        }

                        adapter.notifyDataSetChanged();
                        listView.setAdapter(adapter);

                    } else {
                        Utility.makeToastBottom(this, "Please enter SOS contacts in your profile");
                    }
                }
            }
            else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                }
            }
        }
    }
}
