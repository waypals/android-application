package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;


public class FriendsAddFriendFragment extends Fragment implements AsyncFinishInterface {

    String FRIEND_REQUEST_ALREADY_SENT = "FRIEND_REQUEST_ALREADY_SENT";
    String ALREADY_FRIEND = "ALREADY_FRIEND";
    String FRIEND_REQUEST_SENT_SUCCESSFULLY = "FRIEND_REQUEST_SENT_SUCCESSFULLY";
    String FRIEND_NOT_EXIST_IN_SYSTEM = "FRIEND_NOT_EXIST_IN_SYSTEM";
    String FRIEND_REQUEST_CANNOT_SEND_TO_YOURSELF = "FRIEND_REQUEST_CANNOT_SEND_TO_YOURSELF";
    String mail;
    private Activity activity;
    private AsyncFinishInterface asyncFinishInterface;
    private EditText email;
    private Button send;
    private ApplicationPreferences prefs;
    private String method;
    private ServiceResponse serviceResponse;
    private String tag = "Add Friends Fragment";
    private TextView text;

    public FriendsAddFriendFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        asyncFinishInterface = this;
        prefs = new ApplicationPreferences(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_friends_add_friend, container, false);

        send = (Button) view.findViewById(R.id.sendFriendReqBtn);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = (EditText) view.findViewById(R.id.requestedEmailId);
                mail = email.getText().toString();

                if (validateEmail(mail)) {
                    if ("Send Invitation".equals(send.getText())) {
                        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.INVITE_FRIEND + mail, "INVITE_FRIEND", "", "add new friend fragment", asyncFinishInterface, true);
                        service.execute();
                    } else {
                        GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.NEW_FRIEND + mail, "ADD_FRIEND", "", "add new friend fragment", asyncFinishInterface, true);
                        service.execute();
                    }

                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean validateEmail(String mail) {
        boolean value = false;
        if (mail == null || mail.equals("")) {
            Utility.makeNewToast(getActivity(), "Please enter email Id ");
            value = false;
        } else if (!mail.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$")) {
            Utility.makeNewToast(getActivity(), "Please enter valid email Id ");
            value = false;
        } else {
            value = true;
        }
        return value;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        Gson gson = new Gson();
        if ("ADD_FRIEND".equals(method)) {
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
        if ("INVITE_FRIEND".equals(method)) {
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        try {

            if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(getActivity(),
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(getActivity(), Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(getActivity(), getString(R.string.server_error));
                }

                return;
            }

            if ("ADD_FRIEND".equals(method)) {
                if (serviceResponse != null) {
                    String success = serviceResponse.getResponse();
                    if (success == null || "ERROR".equals(success)) {
                        String errorMessage = serviceResponse.getErrorCode();
                        Utility.makeNewToast(getActivity(), CH_Constant.ERROR_Msg(errorMessage));
                    } else if ("SUCCESS".equals(success)) {
                        email = (EditText) getView().findViewById(R.id.requestedEmailId);
                        email.setText("");

                        String status = serviceResponse.getFriendRequestStatus();
                        if (FRIEND_REQUEST_ALREADY_SENT.equals(status)) {
                            Utility.makeNewToast(getActivity(), activity.getString(R.string.FRIEND_REQUEST_ALREADY_SENT) + mail);
                        } else if (ALREADY_FRIEND.equals(status)) {
                            Utility.makeNewToast(getActivity(), mail + " " + activity.getString(R.string.ALREADY_FRIEND));
                        } else if (FRIEND_REQUEST_SENT_SUCCESSFULLY.equals(status)) {
                            Utility.makeNewToast(getActivity(), activity.getString(R.string.FRIEND_REQUEST_SENT_SUCCESSFULLY) + mail);
                        } else if (FRIEND_NOT_EXIST_IN_SYSTEM.equals(status)) {
                            //Utility.makeNewToast(getActivity(),mail+" "+ activity.getString(R.string.FRIEND_NOT_EXIST_IN_SYSTEM));
                            email.setText(mail);
                            email.setEnabled(false);
                            text = (TextView) getView().findViewById(R.id.txtViewFriendList);
                            text.setText("User is not registered with Waypals.\nWould you like to send invitation");
                            send = (Button) getView().findViewById(R.id.sendFriendReqBtn);
                            send.setText("Send Invitation");
                        } else if (FRIEND_REQUEST_CANNOT_SEND_TO_YOURSELF.equals(status)) {
                            Utility.makeNewToast(getActivity(), activity.getString(R.string.FRIEND_REQUEST_CANNOT_SEND_TO_YOURSELF));
                        }
                    }
                }
            }
            if ("INVITE_FRIEND".equals(method)) {
                email = (EditText) getView().findViewById(R.id.requestedEmailId);
                email.setText("");
                email.setEnabled(true);
                if (serviceResponse != null && "SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                    Utility.makeNewToast(getActivity(), "Invitation has been sent");
                    text = (TextView) getView().findViewById(R.id.txtViewFriendList);
                    text.setText("Send Friend Request");

                    send = (Button) getView().findViewById(R.id.sendFriendReqBtn);
                    send.setText("Send Request");
                } else {
                    Utility.makeNewToast(getActivity(), "Could not send invitation");
                }
            }
        }catch (Exception ex)
        {
            Log.e("Adding Friend Error", ex.getMessage());
            ExceptionHandler.handlerException(ex, getActivity());
        }

    }

}
