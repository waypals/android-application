package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.Beans.ConstraintAlerts_Beans;
import com.waypals.gson.vo.RideLocation;
import com.waypals.rest.service.AddressService;
import com.waypals.rest.service.IAddress;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Alert_Detail_Screen extends Activity implements IAddress {
    private final static int REQUEST_CODE_IMAGE_CAPTURED = 11;
    private final static int REQUEST_CODE_IMAGE_GALLERY = 21;
    Uri capturedImageURI;
    Button back_btn, share_btn, add_pic_btn;
    TextView nav_title_txt;
    ImageView add_pic_img;
    EditText alert_type_edit, date_edit, hh_time_edit, mm_time_edit, pm_time_edit, vehicle_edit, description_edit, location_edit, pssible_sol_edit;
    int indx;
    ApplicationPreferences appPref;
    ArrayList<ConstraintAlerts_Beans> Alerts_list;
    Typeface tf;
    private Handler imageHandler = new Handler();
    private Button mDialogGalleryButton;
    private Button mDialogCameraButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_detail_page);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        CH_Constant.setTypeface((ViewGroup) findViewById(R.id.detail_root_layout), tf);

        back_btn = (Button) findViewById(R.id.left_btn);
//		share_btn = (Button) findViewById(R.id.right_btn);
//		add_pic_btn = (Button) findViewById(R.id.add_pic_btn);
        add_pic_img = (ImageView) findViewById(R.id.add_pic_img);
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        date_edit = (EditText) findViewById(R.id.date_edit);
        hh_time_edit = (EditText) findViewById(R.id.hh_time_edit);
        mm_time_edit = (EditText) findViewById(R.id.mm_time_edit);
        pm_time_edit = (EditText) findViewById(R.id.pm_time_edit);
        vehicle_edit = (EditText) findViewById(R.id.vehicle_edit);
        description_edit = (EditText) findViewById(R.id.description_edit);
        location_edit = (EditText) findViewById(R.id.location_edit);
        alert_type_edit = (EditText) findViewById(R.id.alert_type_edit);

        appPref = new ApplicationPreferences(this);

        indx = getIntent().getExtras().getInt("alert_index");

        Alerts_list = (ArrayList<ConstraintAlerts_Beans>) getIntent().getExtras().getSerializable("alert_list");

        Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText("ALERTS NO. " + (indx + 1));
        back_btn.setTypeface(tf);

//		share_btn.setVisibility(View.VISIBLE);
//		share_btn.setText("Share");

        alert_type_edit.setInputType(InputType.TYPE_NULL);

//		new GetAlert_Acknowlegde_Operation().execute("");

        Date date = null;
//		String dtStart = Alerts_list.get(indx).getConstraintTransgressionTime();		//	17/05/2013 14:16:55 IST 	"2010-10-15T09:27:37Z";
//		SimpleDateFormat  format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ZZZ");

        final String OLD_FORMAT = "dd/MM/yyyy HH:mm:ss ZZZ";
        final String NEW_FORMAT = "dd.MM.yyyy HH:mm:ss ZZZ";

        String oldDateString = null;
        String newDateString;

        try {

            oldDateString = Alerts_list.get(indx).getConstraintTransgressionTime();
        } catch (NullPointerException e) {
            finish();
        }


        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        try {

            date = sdf.parse(oldDateString);

            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(date);

            System.out.println("oldDateString :: " + oldDateString);
            System.out.println("newDateString :: " + newDateString);


        } catch (ParseException e) {
            e.printStackTrace();
        }


        try {
            Date Rdate = new Date(Alerts_list.get(indx).getConstraintTransgressionTime());
//			Date Idate = new Date(Inci_Alerts_list.get(indx).getIncidentTime());
            DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
//
//			inci_date_edt.setText(dateFormat.format(Idate));
            date_edit.setText(dateFormat.format(Rdate));
        } catch (Exception e) {
            e.printStackTrace();

            date_edit.setText(Alerts_list.get(indx).getConstraintTransgressionTime());
        }

        alert_type_edit.setText(getViewTye(Alerts_list.get(indx).getDP_Type()));
        vehicle_edit.setText(Alerts_list.get(indx).getMeta_RegistrationNo());
        description_edit.setText(Alerts_list.get(indx).getViolatedRule_Constraint_Name());
        RideLocation location = new RideLocation(new BigDecimal(Alerts_list.get(indx).getDP_Lat()), new BigDecimal(Alerts_list.get(indx).getDP_Long()));
        AddressService addressService = new AddressService(this, location);
        addressService.execute();

        location_edit.setText(Alerts_list.get(indx).getDP_Lat() + " , " + Alerts_list.get(indx).getDP_Long());
//		pssible_sol_edit.setText("");

        back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//		add_pic_btn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v)
//			{
//				showTakeImageDialog(add_pic_img, "");
//			}
//		});

    }

    private String getViewTye(String type) {

        String viewType = type;
        if (type != null) {
            if (type.equals(CH_Constant.Inbound_Geofence)) {
                viewType = "Enter GeoFence";
            } else if (type.equals(CH_Constant.Outbound_Geofence)) {
                viewType = "Leave GeoFence";
            } else if (type.equals(CH_Constant.System_Fence)) {
                viewType = "Parking Fence";
            } else if (type.equals(CH_Constant.SPEED_LIMIT)) {
                viewType = "Speed Limit";
            }


        }
        return viewType;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("resultCode", String.valueOf(resultCode));
        if (requestCode == REQUEST_CODE_IMAGE_CAPTURED)// capture image form camera
        {
            if (resultCode == RESULT_OK) {

                onPhotoTaken();
            }
        } else if (requestCode == REQUEST_CODE_IMAGE_GALLERY) // pick picture from gallary
        {

            onPhotoTakenFromGallery(data);
        }
    }


    public void onPhotoTaken() {
        try {
//			Vector localImageVector = new Vector();
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = this.managedQuery(capturedImageURI, projection, null, null, null);
            int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            this.startManagingCursor(cursor);
            cursor.moveToFirst();

            String imagePath = cursor.getString(column_index_data);
            File f = new File(imagePath);
            Bitmap image = Utility.decodeFile(f);
            add_pic_img.setImageBitmap(image);

            Log.w("imagePath", imagePath);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("imagePath", "onactivity result");
        }

    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            printThumbnails();

            Uri _uri = data.getData();
            if (_uri != null) {
                Cursor cursor = this.getContentResolver().query(_uri, new String[]
                        {android.provider.MediaStore.Images.ImageColumns.DATA
                        }, null, null, null);

                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                this.startManagingCursor(cursor);
                cursor.moveToFirst();

                final String imagePath = cursor.getString(column_index);

                new Thread() {
                    public void run() {

                        File f = new File(imagePath);
                        final Bitmap image = Utility.decodeFile(f);
                        imageHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                add_pic_img.setImageBitmap(image);
                            }
                        });
                    }
                }.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("imagePath", "onactivity result");
        }

    }

    public void printThumbnails() {
        Uri uri = MediaStore.Images.Thumbnails.getContentUri("external");


        Cursor cursor = MediaStore.Images.Thumbnails.queryMiniThumbnails
                (getContentResolver(), uri, MediaStore.Images.Thumbnails.MINI_KIND,
                        null);
        int count = cursor.getColumnCount();


        for (int i = 0; i < count; i++) {
            System.out.print(cursor.getColumnName(i) + ",");
        }
        System.out.println("");

        cursor.moveToFirst();
        while (true) {
            for (int i = 0; i < count; i++) {
                System.out.print(cursor.getString(i) + ",");

            }
            if (cursor.isLast()) {
                break;
            } else {
                cursor.moveToNext();
            }
        }


    }

    @Override
    public void setAddress(String address) {
        // TODO Auto-generated method stub
        if (address != null && address.length() != 0) {
            location_edit.setText(address);
        }

    }


}


