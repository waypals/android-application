package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.waypals.Beans.IncidentAlerts_Beans;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedImpl.AppController;
import com.waypals.feedImpl.FeedImageView;
import com.waypals.imageHelper.ImageResizer;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;


public class Incident_Detail_Screen extends Activity implements AsyncFinishInterface {
    public static final int CAMERA_PICTURE = 2;
    public static final int GALLERY_PICTURE = 1;
    private final static int REQUEST_CODE_IMAGE_CAPTURED = 11;
    private final static int REQUEST_CODE_IMAGE_GALLERY = 21;
    public static File Copy_sourceLocation;
    public static File Paste_Target_Location;
    public static File MY_IMG_DIR, Default_DIR;
    public static Uri uri;
    public static Intent pictureActionIntent = null;
    public static int connectionTimeOut = 0;
    static String imageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    Uri capturedImageURI;
    String method;
    TableRow add_picture_row, incidntType_row, ticketNo_row, damage_row, location_row, picture_row;
    ImageView back_btn, right_btn;
    TextView nav_title_txt, incidntType_txt;
    ImageView add_pic_img;
    //	Spinner vehicle_spnr;
    // EditText grp_edt;
    EditText inci_type_edit, inci_date_edt, ticketNo_edt, reprting_date_edt,
            driver_edt, vehicle_edt, description_edt, damage_edt, location_edt;
    AsyncFinishInterface asyncFinishInterface;
    String tag = "Incident Details";
    String inci_post_date;
    String class_val;
    int indx;
    CustomDateTimePicker custom;
    ApplicationPreferences appPref;
    List<IncidentAlerts_Beans> Inci_Alerts_list;
    Typeface tf;
    TextView reportingText, vehicleText;
    Drawable[] imageList = null;
    File tempFile;
    String tempFileName = "temp.jpg";
    String fileNamePath;
    ArrayList<IncidentImages> imagesSet = null;
    Activity activity;
    ServiceResponse serviceResponse;
    Context context;
    ImageView f1, f2, f3, f4;
    HashMap<Integer, IncidentImages> loadedImageObjects;

    FeedImageView v1, v2, v3, v4;
    ArrayList<String> imageUrlList = null;

    HashMap<Integer, ImageView> usedImageViews = new HashMap<Integer, ImageView>();
    String response, errorCode;
    String login_response;
    private Handler imageHandler = new Handler();
    private Button mDialogGalleryButton;
    private Button mDialogCameraButton;
    private Button mDialogCancelButton;

    public static String Get_Random_File_Name() {
        final Calendar c = Calendar.getInstance();
        int myYear = c.get(Calendar.YEAR);
        int myMonth = c.get(Calendar.MONTH);
        int myDay = c.get(Calendar.DAY_OF_MONTH);
        String Random_Image_Text = "" + myDay + myMonth + myYear + "_" + Math.random();
        return Random_Image_Text;
    }

    public static Bitmap decodeFile(File f) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            Log.e("decodeFile", "" + e);
        }
        return null;
    }

    public static File copyFile(File current_location, File destination_location) {
        Copy_sourceLocation = new File("" + current_location);
        Paste_Target_Location = new File("" + destination_location + "/" + Get_Random_File_Name() + ".jpg");

        Log.v("Purchase-File", "sourceLocation: " + Copy_sourceLocation);
        Log.v("Purchase-File", "targetLocation: " + Paste_Target_Location);
        try {
            // 1 = move the file, 2 = copy the file
            int actionChoice = 2;
            // moving the file to another directory
            if (actionChoice == 1) {
                if (Copy_sourceLocation.renameTo(Paste_Target_Location)) {
                    Log.i("Purchase-File", "Move file successful.");
                } else {
                    Log.i("Purchase-File", "Move file failed.");
                }
            }

            // we will copy the file
            else {
                // make sure the target file exists
                if (Copy_sourceLocation.exists()) {

                    InputStream in = new FileInputStream(Copy_sourceLocation);
                    OutputStream out = new FileOutputStream(Paste_Target_Location);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;

                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();

                    Log.i("copyFile", "Copy file successful.");

                } else {
                    Log.i("copyFile", "Copy file failed. Source file missing.");
                }
            }

        } catch (NullPointerException e) {
            Log.i("copyFile", "" + e);

        } catch (Exception e) {
            Log.i("copyFile", "" + e);
        }
        return Paste_Target_Location;
    }

    public static File Create_MY_IMAGES_DIR() {
        try {
            // Get SD Card path & your folder name
            MY_IMG_DIR = new File(Environment.getExternalStorageDirectory(), "/My_Image/");

            // check if exist
            if (!MY_IMG_DIR.exists()) {
                // Create New folder
                MY_IMG_DIR.mkdirs();
                Log.i("path", ">>.." + MY_IMG_DIR);
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Create_MY_IMAGES_DIR", "" + e);
        }
        return MY_IMG_DIR;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            String intakeType = getIntent().getExtras().getString("type");
            if (intakeType != null && intakeType.equalsIgnoreCase("get")) {
                setContentView(R.layout.incident_detail_page);
                location_row = (TableRow) findViewById(R.id.location_row);
                location_row.setVisibility(View.GONE);

                f1 = (ImageView) findViewById(R.id.image1);
                f2 = (ImageView) findViewById(R.id.image2);
                f3 = (ImageView) findViewById(R.id.image3);
                f4 = (ImageView) findViewById(R.id.image4);

            } else {
                setContentView(R.layout.incident_detail_page_get);


                imageUrlList = new ArrayList<String>();
            }
            tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            CH_Constant.setTypeface((ViewGroup) findViewById(R.id.detail_root_layout), tf);

            asyncFinishInterface = this;
            activity = this;
            context = this;
            back_btn = (ImageView) findViewById(R.id.left_btn);
            reportingText = (TextView) findViewById(R.id.reporting_textView);
            vehicleText = (TextView) findViewById(R.id.vehicle_text_view);

            //back_btn.setTypeface(tf);
            right_btn = (ImageView) findViewById(R.id.right_btn);
            //		add_pic_btn = (Button) findViewById(R.id.add_pic_btn);
            add_pic_img = (ImageView) findViewById(R.id.add_pic_img);
            nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
            inci_type_edit = (EditText) findViewById(R.id.alert_type_edit);
            inci_date_edt = (EditText) findViewById(R.id.inci_date_edit);
            reprting_date_edt = (EditText) findViewById(R.id.reporting_date_edit);
            vehicle_edt = (EditText) findViewById(R.id.vehicle_edit);
            //		vehicle_spnr = (Spinner) findViewById(R.id.vehicle_spinr);
            //		grp_edt = (EditText) findViewById(R.id.grp_edit);
            ticketNo_edt = (EditText) findViewById(R.id.tickt_edit);
            driver_edt = (EditText) findViewById(R.id.driver_edt);
            description_edt = (EditText) findViewById(R.id.description_edt);
            damage_edt = (EditText) findViewById(R.id.damage_edt);
            location_edt = (EditText) findViewById(R.id.location_edit);
            incidntType_txt = (TextView) findViewById(R.id.incidntType_txt);
            ticketNo_row = (TableRow) findViewById(R.id.ticketNo_row);
            damage_row = (TableRow) findViewById(R.id.damage_row);
            picture_row = (TableRow) findViewById(R.id.tableRow2);

            incidntType_row = (TableRow) findViewById(R.id.incidntType_row);
            /*		add_picture_row = (TableRow) findViewById(R.id.add_picture_row);
             */
            appPref = new ApplicationPreferences(this);

            Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            nav_title_txt.setTypeface(tf);
            right_btn.setVisibility(View.VISIBLE);


            inci_date_edt.setInputType(InputType.TYPE_NULL);
            inci_date_edt.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });
            inci_date_edt.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    return true;
                }
            });


            inci_type_edit.setInputType(InputType.TYPE_NULL);
            //		ticketNo_edt.setOnClickListener(new OnClickListener() {
            //			@Override
            //			public void onClick(View v) {
            //				inci_type_edit.setCursorVisible(true);
            //				inci_type_edit.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);
            //			}
            //		});


            //		Type "get" is to Add Incident Details  & Type "set" is to Show Incident Details

            if (intakeType != null && intakeType.equalsIgnoreCase("get")) {
                class_val = getIntent().getExtras().getString("add_incident");

                if("THEFT".equalsIgnoreCase(class_val))
                {
                    picture_row.setVisibility(View.GONE);
                }

                nav_title_txt.setText(class_val);

                //			add_picture_row.setVisibility(View.VISIBLE);
                //			vehicle_spnr.setVisibility(View.VISIBLE);
                incidntType_row.setVisibility(View.GONE);
                vehicle_edt.setVisibility(View.GONE);
                right_btn.setTag("Save");
                right_btn.setBackgroundResource(R.drawable.save_btn);
                // right_btn.setTypeface(tf);                    //		Right button in Add Incident is "Save" & in Show Incident is "Share"

                inci_type_edit.setEnabled(true);
                ticketNo_edt.setEnabled(true);
                inci_date_edt.setEnabled(true);
                reprting_date_edt.setEnabled(true);
                vehicle_edt.setEnabled(true);
                //				grp_edt.setEnabled(true);
                driver_edt.setEnabled(true);
                description_edt.setEnabled(true);
                damage_edt.setEnabled(true);
                location_edt.setEnabled(true);

                if (class_val.contains("TRAFFIC"))
                    ticketNo_row.setVisibility(View.VISIBLE);
                else
                    ticketNo_row.setVisibility(View.GONE);

                if (class_val.equalsIgnoreCase("BREAK DOWN"))
                    class_val = "BREAK_DOWN";
                else if (class_val.equalsIgnoreCase("TRAFFIC VIOLATION"))
                    class_val = "TRAFFIC_VIOLATION";

                if (class_val.contains("THEFT"))
                    damage_row.setVisibility(View.GONE);


                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyy, hh:mm a");
                String current_DateTime = dateFormat.format(new Date());

                //			String[] vehicle_array=;
                //			ArrayAdapter<String> vehicle_adapter =  new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, vehicle_array);
                //			vehicle_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //			vehicle_spnr.setAdapter(vehicle_adapter);

                incidntType_txt.setText("Ticket Number:");
                ticketNo_edt.setEnabled(true);
                ticketNo_edt.setText("");
                inci_date_edt.setText("");
                reprting_date_edt.setText(current_DateTime);
                reprting_date_edt.setEnabled(false);
                reprting_date_edt.setVisibility(View.GONE);
                reportingText.setVisibility(View.GONE);
                vehicleText.setVisibility(View.GONE);

                //			vehicle_edt.setText("");
                //			grp_edt.setText("");
                driver_edt.setText("");
                description_edt.setText("");
                damage_edt.setText("");
                location_edt.setText("");

                custom = new CustomDateTimePicker(this, new CustomDateTimePicker.ICustomDateTimeListener() {
                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec, String AM_PM) {

                        String set_DateTime;

                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                        set_DateTime = sdf.format(dateSelected);


                        //					28/06/2013 12:23:32 IST
                        inci_post_date = set_DateTime;

                        inci_date_edt.setText(Utility.getUTCtoIST(set_DateTime));

                        //					inci_date_edt.setText(calendarSelected.get(Calendar.DAY_OF_MONTH)+ "." + (monthNumber+1) + "." + year
                        //																+ ", " + hour12 + ":" + min + " " + AM_PM);

                    }

                    @Override
                    public void onCancel() {

                    }
                });

                /**
                 * Pass Directly current time format it will return AM and PM if you set
                 * false
                 */
                custom.set24HourFormat(false);
                /**
                 * Pass Directly current data and time to show when it pop up
                 */
                custom.setDate(Calendar.getInstance());

                inci_date_edt.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        custom.showDialog();
                    }
                });

            } else if (intakeType != null && intakeType.equalsIgnoreCase("set")) {
                indx = getIntent().getExtras().getInt("alert_index");
                Inci_Alerts_list = (ArrayList<IncidentAlerts_Beans>) getIntent().getExtras().getSerializable("alert_list");

                nav_title_txt.setText("INCIDENT NO. " + (indx + 1));
                //			add_picture_row.setVisibility(View.GONE);
                //			vehicle_spnr.setVisibility(View.GONE);
                vehicle_edt.setVisibility(View.VISIBLE);
                //			right_btn.setText("Share");										//		Right button in Add Incident is "Save" & in Show Incident is "Share"

                right_btn.setVisibility(View.GONE);
                //			new GetAlert_Acknowlegde_Operation().execute();

                ticketNo_row.setVisibility(View.GONE);


                if (Inci_Alerts_list != null && Inci_Alerts_list.size() > 0) {
                    inci_date_edt.setText(Inci_Alerts_list.get(indx).getIncidentTime());
                    reprting_date_edt.setText(Inci_Alerts_list.get(indx).getReportingTime());

                    final String IncitntType = Inci_Alerts_list.get(indx).getEvent_type();

                    if (IncitntType.contains("TRAFFIC")) {
                        ticketNo_row.setVisibility(View.VISIBLE);
                        ticketNo_edt.setText(Inci_Alerts_list.get(indx).getTicketNumber());
                    } else {
                        ticketNo_row.setVisibility(View.GONE);
                    }


                    if (IncitntType.contains("THEFT"))
                        damage_row.setVisibility(View.GONE);

                    inci_type_edit.setText(IncitntType);
                    vehicle_edt.setText(Inci_Alerts_list.get(indx).getMeta_RegistrationNo());
                    //				grp_edt.setText(Inci_Alerts_list.get(indx).getVehicleGroup());
                    driver_edt.setText(Inci_Alerts_list.get(indx).getDriver());
                    description_edt.setText(Inci_Alerts_list.get(indx).getDescription());
                    damage_edt.setText(Inci_Alerts_list.get(indx).getDamages());
                    location_edt.setText(Inci_Alerts_list.get(indx).getReportedLocation());
                    //		solution_edt.setText(Inci_Alerts_list.get(indx).get);
                    Log.d(tag, "location : " + location_edt.getText().toString());
                    if (location_edt.getText().toString() != null && !location_edt.getText().toString().trim().isEmpty()) {
                        location_edt.setEnabled(true);
                        location_edt.setFocusable(false);
                        location_edt.setClickable(true);


                        location_edt.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                try {
                                    Log.d(tag, "hey im here");
                                    String loc = Inci_Alerts_list.get(indx).getReportedLocation();
                                    String lat[] = loc.split(",");
                                    Intent i = Utility.openGoogleMap(context, Float.parseFloat(lat[0]), Float.parseFloat(lat[1]));
                                    context.startActivity(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }

                    IncidentImages[] images = Inci_Alerts_list.get(indx).getIncidentImages();
                    ImageLoader imageLoader = new AppController(this).getImageLoader();
                    if (images != null) {
                        imageUrlList = new ArrayList<String>();

                        for (IncidentImages i : images) {
                            imageUrlList.add(i.getImageLink());
                        }

                        //final TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT);
                        switch (imageUrlList.size()) {
                            case 1:
                                v1.setVisibility(View.VISIBLE);
                                v1.requestLayout();
                                v2.setVisibility(View.GONE);
                                v3.setVisibility(View.GONE);
                                v4.setVisibility(View.GONE);
                                v1.setImageUrl(imageUrlList.get(0), imageLoader);
                                v1.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v1.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v1.setLayoutParams(layoutParams);
                                        v1.requestLayout();
                                    }
                                });
                                break;

                            case 2:
                                v1.setImageUrl(imageUrlList.get(0), imageLoader);
                                v1.setVisibility(View.VISIBLE);
                                v2.setVisibility(View.VISIBLE);
                                v1.requestLayout();
                                v2.requestLayout();
                                v3.setVisibility(View.GONE);
                                v4.setVisibility(View.GONE);
                                v1.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v1.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v1.setLayoutParams(layoutParams);
                                        v1.requestLayout();
                                    }
                                });
                                v2.setImageUrl(imageUrlList.get(1), imageLoader);
                                v2.setVisibility(View.VISIBLE);
                                v2.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v2.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v2.setLayoutParams(layoutParams);
                                        v2.requestLayout();
                                    }
                                });
                                break;
                            case 3:
                                v1.setVisibility(View.VISIBLE);
                                v2.setVisibility(View.VISIBLE);
                                v3.setVisibility(View.VISIBLE);
                                v4.setVisibility(View.GONE);
                                v1.requestLayout();
                                v2.requestLayout();
                                v3.requestLayout();

                                v1.setImageUrl(imageUrlList.get(0), imageLoader);
                                v1.setVisibility(View.VISIBLE);
                                v1.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v1.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v1.setLayoutParams(layoutParams);
                                        v1.requestLayout();
                                    }
                                });
                                v2.setImageUrl(imageUrlList.get(1), imageLoader);
                                v2.setVisibility(View.VISIBLE);
                                v2.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v2.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v2.setLayoutParams(layoutParams);
                                        v2.requestLayout();
                                    }

                                });
                                v3.setImageUrl(imageUrlList.get(2), imageLoader);
                                v3.setVisibility(View.VISIBLE);
                                v3.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v3.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
                                        // v3.setLayoutParams(layoutParams);
                                        v3.requestLayout();
                                    }

                                });
                                break;

                            case 4:
                                v1.setVisibility(View.VISIBLE);
                                v2.setVisibility(View.VISIBLE);
                                v3.setVisibility(View.VISIBLE);
                                v4.setVisibility(View.VISIBLE);
                                v1.requestLayout();
                                v2.requestLayout();
                                v3.requestLayout();
                                v4.requestLayout();
                                v1.setImageUrl(imageUrlList.get(0), imageLoader);
                                v1.setVisibility(View.VISIBLE);
                                v1.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v1.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v1.setLayoutParams(layoutParams);
                                        v1.requestLayout();
                                    }

                                });
                                v2.setImageUrl(imageUrlList.get(1), imageLoader);
                                v2.setVisibility(View.VISIBLE);
                                v2.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v2.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v2.setLayoutParams(layoutParams);
                                        v2.requestLayout();
                                    }

                                });
                                v3.setImageUrl(imageUrlList.get(2), imageLoader);
                                v3.setVisibility(View.VISIBLE);
                                v3.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v3.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v3.setLayoutParams(layoutParams);
                                        v3.requestLayout();
                                    }

                                });
                                v4.setImageUrl(imageUrlList.get(3), imageLoader);
                                v4.setVisibility(View.VISIBLE);
                                v4.setResponseObserver(new FeedImageView.ResponseObserver() {
                                    @Override
                                    public void onError() {
                                        v4.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onSuccess() {
//                                        try{
//                                            Thread.sleep(1000);
//                                        }
//                                        catch (Exception e)
//                                        {
//                                            e.printStackTrace();
//                                        }
                                        //v4.setLayoutParams(layoutParams);
                                        v4.requestLayout();
                                    }

                                });
                                break;


                        }
                    }

                }


            }

            right_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //		Right button in Add Incident is "Save" & in Show Incident is "Share"

                    // System.out.println("Right btn :: " + right_btn.getText().toString());

                    if (right_btn.getTag().toString().equalsIgnoreCase("Save")) {
                        InputMethodManager inputManager = (InputMethodManager)
                                Incident_Detail_Screen.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(Incident_Detail_Screen.this.getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                        Date d = null;
                        try
                        {
                            if(inci_post_date != null) {
                                d = sdf.parse(inci_post_date);
                            }
                        }
                        catch (ParseException e)
                        {
                            ExceptionHandler.handlerException(e, activity);
                        }
                        if ( d == null || d.after(new Date())) {
                                String msg = "Please enter valid date";
                                if(d != null)
                                {
                                  msg = "Future Date is not allowed";
                                }
                                Utility.makeNewToast(activity, msg);
                        }
                        else if (inci_date_edt.getText().toString().equals("") || inci_date_edt.getText().toString().trim().length() == 0 || reprting_date_edt.getText().toString().equals("") || reprting_date_edt.getText().toString().trim().length() == 0 || driver_edt.getText().toString().equals("") || driver_edt.getText().toString().trim().length() == 0 || description_edt.getText().toString().equals("") || description_edt.getText().toString().trim().length() == 0) {
                            try {
                                Utility.makeNewToast(activity, "All fields mandatory");
                            } catch (Exception e) {
                                ExceptionHandler.handlerException(e, activity);
                            }
                        } else if (class_val.contains("TRAFFIC") && ticketNo_edt.getText().toString().equals("")) {
                            try {
                                Utility.makeNewToast(activity, "Please fill ticket number");
                            } catch (Exception e) {
                                ExceptionHandler.handlerException(e, activity);
                            }
                        } else {
                            if (CH_Constant.isNetworkAvailable(Incident_Detail_Screen.this)) {
                                if (class_val != null) {

                                    String ticketNo = "null";
                                    String description = description_edt.getText().toString();
                                    String damage = damage_edt.getText().toString();
                                    String location = location_edt.getText().toString();
                                    description = description.replace("\n", "\\n");
                                    description = description.replace("\"", "\\\"");
                                    damage = damage.replace("\n", "\\n");
                                    damage = damage.replace("\"", "\\\"");
                                    location = location.replace("\n", "\\n");
                                    location = location.replace("\"", "\\\"");

                                    if (class_val.contains("TRAFFIC"))
                                        ticketNo = ticketNo_edt.getText().toString(); // Ticket

                                    try {
                                        if (loadedImageObjects != null && imagesSet.size() > 0) {
                                            imagesSet = new ArrayList<IncidentImages>();
                                            for (IncidentImages i : loadedImageObjects.values()) {
                                                imagesSet.add(i);
                                            }
                                        }

                                        Gson gson = new Gson();

                                        IncidentRequest request = new IncidentRequest();
                                        request.setDamages(damage);
                                        request.setDescription(description);
                                        request.setVehicleId(appPref.getVehicle_Selected());
                                        request.setIncidentImages(imagesSet);
                                        request.setIncidentLocation(location);
                                        request.setIncidentTime(inci_post_date);
                                        request.setTicketNumber(ticketNo);
                                        request.setIncidentType(class_val);
                                        request.setDriver(driver_edt.getText().toString());

                                        String input = gson.toJson(request);

                                        Log.d(tag, "Input Json : " + input);

//                                        SaveIncidentService saveIncidentService = new SaveIncidentService(appPref, activity, input, asyncFinishInterface);
//                                        saveIncidentService.execute();

                                        GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.Add_Incident_Api, "addincident", input.toString(), tag, asyncFinishInterface, true);
                                        service.execute();
                                    } catch (Exception e) {
                                        ExceptionHandler.handlerException(e, activity);
                                    }
                                }
                            } else {
                                try {
                                    Utility.makeNewToast(activity, CH_Constant.NO_NETWORK);
                                } catch (Exception e) {
                                    ExceptionHandler.handlerException(e, activity);
                                }
                            }
                        }
                    }
                }
            });

            back_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

			/*add_pic_btn.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v)
			{
				showTakeImageDialog(add_pic_img);
			}
		});*/
        } catch (Exception e) {
            e.printStackTrace();
            //create Exception Handler method to show Exceptions
        }

        Button pictureBox = (Button) findViewById(R.id.pic_button);

        if (pictureBox != null) {
            pictureBox.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    selectImage();
                }
            });
        }

    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        if (method.equals("addincident")) {
            if (response != null) {
                Gson gson = new Gson();
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            } else {
                serviceResponse = null;
                try {
                    Utility.makeNewToast(activity, getString(R.string.err_no_response));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, activity);
                }
            }
        }
        if (method.endsWith("GetLocation")) {
            this.response = response;
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, getString(R.string.server_error));
            }

            return;
        }

        if (method.equals("addincident")) {
            if (serviceResponse != null) {
                if (serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    try {
                        Utility.makeNewToast(activity, "Incident added");
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }

                    loadedImageObjects = null;
                    imagesSet = null;
                    f1.setVisibility(View.GONE);
                    f2.setVisibility(View.GONE);
                    f3.setVisibility(View.GONE);
                    f4.setVisibility(View.GONE);

                    finish();
                } else {
                    try {
                        Utility.makeNewToast(activity, CH_Constant.ERROR_Msg(serviceResponse.getErrorCode()));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                    finish();
                }
            }
        }
        if (method.endsWith("GetLocation")) {
            location_edt = (EditText) findViewById(R.id.location_edit);
            location_edt.setText(this.response);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getImageList() {
        try {
            FileFilter jpg = new FileFilter() {

                @Override
                public boolean accept(File file) {
                    if (file.getName().endsWith(".jpg"))
                        return true;
                    else
                        return false;
                }
            };
            File[] list = new File(Environment.getExternalStorageDirectory() + "/My_Image/").listFiles();

            if (list != null) {
                imageList = new Drawable[list.length];
                for (int i = 0; i < list.length; i++) {
                    Drawable d = Drawable.createFromPath(list[i].getAbsolutePath());
                    imageList[i] = d;
                }
            }

        } catch (Exception ex) {
            imageList = null;
        }
    }

    public void Image_Selecting_Task(Intent data) {
        Create_MY_IMAGES_DIR();
        try {
            if (data != null) {
                uri = data.getData();
            }

            if (uri != null) {
                // User had pick an image.
                Cursor cursor = getContentResolver().query(uri, new String[]
                        {MediaStore.Images.ImageColumns.DATA}, null, null, null);
                cursor.moveToFirst();
                // Link to the image
                final String imageFilePath = cursor.getString(0);

                //Assign string path to File
                Default_DIR = new File(imageFilePath);

                // Create new dir MY_IMAGES_DIR if not created and copy image into that dir and store that image path in valid_photo
                Create_MY_IMAGES_DIR();

                // Copy your image
                copyFile(Default_DIR, MY_IMG_DIR);


            } else {
                Toast toast = Toast.makeText(this, "Sorry!!! You haven't selecet any image.", Toast.LENGTH_LONG);
                toast.show();
            }
        } catch (Exception e) {
            // you get this when you will not select any single image
            Log.e("onActivityResult", "" + e);
            e.printStackTrace();

        }
    }

    /*added here new*/
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/" + timeStamp + "_temp.jpg");

        // Save a file: path for use with ACTION_VIEW intents
        fileNamePath = image.getAbsolutePath();
        return image;
    }

    public String checkImageSize(String fileNamePath) {
        try {
            File f = new File(fileNamePath);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            is.close();

            int kb = size / 1024;

            if (kb > 256) {
                File file = Utility.createFile();
                Bitmap b = ImageResizer.resize(new File(fileNamePath), 256, 256);
                FileOutputStream fout = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                fout.flush();
                b.recycle();
                b = null;
                fout.close();
                return file.getAbsolutePath();
            } else {
                return fileNamePath;
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, activity);
            return null;
        }
    }

    private Integer getAvailbleImagePosition() {
        int ret = 0;

        Set<Integer> pos = usedImageViews.keySet();

        for (int i = 0; i < usedImageViews.size(); i++) {
            ImageView img = usedImageViews.get(i);
            if (img == null) {
                ret = i;
                break;
            }
        }
        return ret;
    }
//
//    public static Bitmap decodeSampledBitmapFromResource(File res,int reqWidth, int reqHeight) {
//
//        // First decode with inJustDecodeBounds=true to check dimensions
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(res options);
//
//        // Calculate inSampleSize
//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
//
//        // Decode bitmap with inSampleSize set
//        options.inJustDecodeBounds = false;
//        return BitmapFactory.decodeResource(res, resId, options);
//    }

    public void selectImage() {
        if (imagesSet == null) {
            imagesSet = new ArrayList<IncidentImages>();
            loadedImageObjects = new HashMap<Integer, IncidentImages>();
            usedImageViews.put(0, null);
            usedImageViews.put(1, null);
            usedImageViews.put(2, null);
            usedImageViews.put(3, null);
        }
        if (loadedImageObjects.size() >= 4) {
            try {
                Utility.makeNewToast(activity, "You can not add more than 4 images");
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, activity);
            } finally {
                return;
            }
        }
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Incident_Detail_Screen.this);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                fileNamePath = null;
                Log.d(tag, "image selection canceled");
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {

        }

        Log.i("resultCode", String.valueOf(resultCode));
        if (requestCode == CAMERA_PICTURE && resultCode == RESULT_OK)// capture image form camera
        {
            onPhotoTaken();
        } else if (requestCode == GALLERY_PICTURE && resultCode == RESULT_OK) // pick picture from gallary
        {
            onPhotoTakenFromGallery(data);
        }
    }

    public void onPhotoTaken() {
        try {
            if (fileNamePath != null) {
                fileNamePath = checkImageSize(fileNamePath);
                {
                    String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                    String type = name.substring(name.indexOf(".") + 1);
                    String imageStr = Utility.getImageToHex(activity, fileNamePath);
                    IncidentImages images = new IncidentImages();
                    images.setImageName(name);
                    images.setImageStr(imageStr);
                    if (type.equalsIgnoreCase(ImageType.JPEG.name())) {
                        images.setImageType(ImageType.JPEG);
                    } else if (type.equalsIgnoreCase(ImageType.PNG.name())) {
                        images.setImageType(ImageType.PNG);
                    } else if (type.equalsIgnoreCase(ImageType.JPG.name())) {
                        images.setImageType(ImageType.JPG);
                    } else {
                        Utility.makeNewToast(activity, "Image format not supported");
                        return;
                    }
                    int loaded = imagesSet.size();

                    final int emptyPosition = getAvailbleImagePosition();

                    loadedImageObjects.put(emptyPosition, images);
                    switch (emptyPosition) {
                        case 0:
                            f1.setImageBitmap(Utility.decodeSampledBitmapFromPath(fileNamePath, 256, 256));
                            f1.setVisibility(View.VISIBLE);
                            f1.setOnLongClickListener(new OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    try {
                                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                        dialog.setTitle("Delete Image");
                                        dialog.setMessage("Do you want to delete this image ?");
                                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Log.d(tag, "deleting image");
                                                try {
                                                    loadedImageObjects.remove(emptyPosition);
                                                    usedImageViews.put(0, null);
                                                    f1.setVisibility(View.GONE);
                                                } catch (Exception ex) {
                                                    ExceptionHandler.handleException(ex, activity);
                                                }
                                            }
                                        });
                                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                return;
                                            }
                                        });
                                        dialog.setCancelable(false);
                                        dialog.create();
                                        dialog.show();
                                    } catch (Throwable e) {
                                        ExceptionHandler.handleException(e);
                                    }
                                    return false;
                                }
                            });
                            usedImageViews.put(0, f1);
                            break;

                        case 1:
                            f2.setImageBitmap(Utility.decodeSampledBitmapFromPath(fileNamePath, 256, 256));
                            f2.setVisibility(View.VISIBLE);
                            f2.setOnLongClickListener(new OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    try {
                                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                        dialog.setTitle("Delete Image");
                                        dialog.setMessage("Do you want to delete this image ?");
                                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Log.d(tag, "deleting image");
                                                try {
                                                    loadedImageObjects.remove(emptyPosition);
                                                    f2.setVisibility(View.GONE);
                                                    usedImageViews.put(1, null);
                                                } catch (Exception ex) {
                                                    ExceptionHandler.handleException(ex, activity);
                                                }
                                            }
                                        });
                                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                return;
                                            }
                                        });
                                        dialog.setCancelable(false);
                                        dialog.create();
                                        dialog.show();
                                    } catch (Throwable e) {
                                        ExceptionHandler.handleException(e);
                                    }
                                    return false;
                                }
                            });
                            usedImageViews.put(1, f2);
                            break;

                        case 2:
                            f3.setImageBitmap(Utility.decodeSampledBitmapFromPath(fileNamePath, 256, 256));
                            f3.setVisibility(View.VISIBLE);
                            f3.setOnLongClickListener(new OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    try {
                                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                        dialog.setTitle("Delete Image");
                                        dialog.setMessage("Do you want to delete this image ?");
                                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Log.d(tag, "deleting image");
                                                try {
                                                    loadedImageObjects.remove(emptyPosition);
                                                    f3.setVisibility(View.GONE);
                                                    usedImageViews.put(2, null);
                                                } catch (Exception ex) {
                                                    ExceptionHandler.handleException(ex, activity);
                                                }
                                            }
                                        });
                                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                return;
                                            }
                                        });
                                        dialog.setCancelable(false);
                                        dialog.create();
                                        dialog.show();
                                    } catch (Throwable e) {
                                        ExceptionHandler.handleException(e);
                                    }
                                    return false;
                                }
                            });
                            usedImageViews.put(2, f3);
                            break;
                        case 3:
                            f4.setImageBitmap(Utility.decodeSampledBitmapFromPath(fileNamePath, 256, 256));
                            f4.setVisibility(View.VISIBLE);
                            f4.setOnLongClickListener(new OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    try {
                                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                        dialog.setTitle("Delete Image");
                                        dialog.setMessage("Do you want to delete this image ?");
                                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Log.d(tag, "deleting image");
                                                try {
                                                    loadedImageObjects.remove(emptyPosition);
                                                    f4.setVisibility(View.GONE);
                                                    usedImageViews.put(3, null);
                                                } catch (Exception ex) {
                                                    ExceptionHandler.handleException(ex, activity);
                                                }
                                            }
                                        });
                                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                return;
                                            }
                                        });
                                        dialog.setCancelable(false);
                                        dialog.create();
                                        dialog.show();
                                    } catch (Throwable e) {
                                        ExceptionHandler.handleException(e);
                                    }
                                    return false;
                                }
                            });
                            usedImageViews.put(3, f3);
                            break;
                    }
                    imagesSet.add(images);
                }

            }
            Log.d("camera imagePath", fileNamePath);
            Log.d(tag, "Total Image files: " + imagesSet);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("camera imagePath", "onactivity result");
        }

    }

    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            Uri _uri = data.getData();
            if (_uri != null) {
                fileNamePath = getPath(_uri);
                Log.d(tag, "gallary image path: " + fileNamePath);
                if (fileNamePath != null) {
                    fileNamePath = checkImageSize(fileNamePath);
                    {
                        String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                        String type = name.substring(name.indexOf(".") + 1);
                        String imageStr = Utility.getImageToHex(activity, fileNamePath);
                        IncidentImages images = new IncidentImages();
                        images.setImageName(name);
                        images.setImageStr(imageStr);

                        if (type.equalsIgnoreCase(ImageType.JPEG.name())) {
                            images.setImageType(ImageType.JPEG);
                        } else if (type.equalsIgnoreCase(ImageType.PNG.name())) {
                            images.setImageType(ImageType.PNG);
                        } else if (type.equalsIgnoreCase(ImageType.JPG.name())) {
                            images.setImageType(ImageType.JPG);
                        } else {
                            Utility.makeNewToast(activity, "Image format not supported");
                            return;
                        }
                        int loaded = imagesSet.size();

                        int emptyPosition = getAvailbleImagePosition();
                        loadedImageObjects.put(emptyPosition, images);
                        switch (emptyPosition) {
                            case 0:
                                f1.setImageURI(Uri.fromFile(new File(fileNamePath)));
                                f1.setVisibility(View.VISIBLE);
                                f1.setOnLongClickListener(new OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        try {
                                            AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                            dialog.setTitle("Delete Image");
                                            dialog.setMessage("Do you want to delete this image ?");
                                            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    Log.d(tag, "deleting image");
                                                    try {
                                                        loadedImageObjects.remove(0);
                                                        usedImageViews.put(0, null);
                                                        f1.setVisibility(View.GONE);
                                                    } catch (Exception ex) {
                                                        ExceptionHandler.handleException(ex, activity);
                                                    }
                                                }
                                            });
                                            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    return;
                                                }
                                            });
                                            dialog.setCancelable(false);
                                            dialog.create();
                                            dialog.show();
                                        } catch (Throwable e) {
                                            ExceptionHandler.handleException(e);
                                        }
                                        return false;
                                    }
                                });
                                usedImageViews.put(0, f1);
                                break;

                            case 1:
                                f2.setImageURI(Uri.fromFile(new File(fileNamePath)));
                                f2.setVisibility(View.VISIBLE);
                                f2.setOnLongClickListener(new OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        try {
                                            AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                            dialog.setTitle("Delete Image");
                                            dialog.setMessage("Do you want to delete this image ?");
                                            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    Log.d(tag, "deleting image");
                                                    try {
                                                        loadedImageObjects.remove(1);
                                                        f2.setVisibility(View.GONE);
                                                        usedImageViews.put(1, null);
                                                    } catch (Exception ex) {
                                                        ExceptionHandler.handleException(ex, activity);
                                                    }
                                                }
                                            });
                                            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    return;
                                                }
                                            });
                                            dialog.setCancelable(false);
                                            dialog.create();
                                            dialog.show();
                                        } catch (Throwable e) {
                                            ExceptionHandler.handleException(e);
                                        }
                                        return false;
                                    }
                                });
                                usedImageViews.put(1, f2);
                                break;

                            case 2:
                                f3.setImageURI(Uri.fromFile(new File(fileNamePath)));
                                f3.setVisibility(View.VISIBLE);
                                f3.setOnLongClickListener(new OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        try {
                                            AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                            dialog.setTitle("Delete Image");
                                            dialog.setMessage("Do you want to delete this image ?");
                                            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    Log.d(tag, "deleting image");
                                                    try {
                                                        loadedImageObjects.remove(2);
                                                        f3.setVisibility(View.GONE);
                                                        usedImageViews.put(2, null);
                                                    } catch (Exception ex) {
                                                        ExceptionHandler.handleException(ex, activity);
                                                    }
                                                }
                                            });
                                            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    return;
                                                }
                                            });
                                            dialog.setCancelable(false);
                                            dialog.create();
                                            dialog.show();
                                        } catch (Throwable e) {
                                            ExceptionHandler.handleException(e);
                                        }
                                        return false;
                                    }
                                });
                                usedImageViews.put(2, f3);
                                break;
                            case 3:
                                f4.setImageURI(Uri.fromFile(new File(fileNamePath)));
                                f4.setVisibility(View.VISIBLE);
                                f4.setOnLongClickListener(new OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View view) {
                                        try {
                                            AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                                            dialog.setTitle("Delete Image");
                                            dialog.setMessage("Do you want to delete this image ?");
                                            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    Log.d(tag, "deleting image");
                                                    try {
                                                        loadedImageObjects.remove(3);
                                                        f4.setVisibility(View.GONE);
                                                        usedImageViews.put(3, null);
                                                    } catch (Exception ex) {
                                                        ExceptionHandler.handleException(ex, activity);
                                                    }
                                                }
                                            });
                                            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    return;
                                                }
                                            });
                                            dialog.setCancelable(false);
                                            dialog.create();
                                            dialog.show();
                                        } catch (Throwable e) {
                                            ExceptionHandler.handleException(e);
                                        }
                                        return false;
                                    }
                                });
                                usedImageViews.put(3, f3);
                                break;
                        }
                        imagesSet.add(images);
                    }
//                    else
//                    {
//                        fileNamePath = null;
//                        try
//                        {
//                            Utility.makeNewToast(activity,"Image size exceeds");
//                        }
//                        catch(Exception e)
//                        {
//                            ExceptionHandler.handlerException(e,activity);
//                        }
//
//                    }

                }
                Log.d(tag, "images list : " + imagesSet);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(tag, "onactivity result");
        }

    }

    enum ImageType {
        PNG, JPG, JPEG;
    }

    public static class IncidentImages implements Serializable {

        private String imageStr;
        private String imageName;
        private ImageType imageType;
        private String imageLink;

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }

        public String getImageStr() {
            return imageStr;
        }

        public void setImageStr(String imageStr) {
            this.imageStr = imageStr;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public ImageType getImageType() {
            return imageType;
        }

        public void setImageType(ImageType imageType) {
            this.imageType = imageType;
        }

        @Override
        public String toString() {
            return "IncidentImages{" +
                    "imageStr='" + imageStr + '\'' +
                    ", imageName='" + imageName + '\'' +
                    ", imageType='" + imageType + '\'' +
                    '}';
        }
    }

    public class GetAlert_Acknowlegde_Operation extends
            AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            try {
                String alert_request_json = "{\"vehicleId\":"
                        + Inci_Alerts_list.get(indx).getVehicleId()
                        + ",\"alertType\":" + CH_Constant.incidentAlert
                        + ",\"alertIds\":["
                        + Inci_Alerts_list.get(indx).getIncidentId() + "]}";
                //   CH_Constant.AlertAcknowledge(appPref, alert_request_json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    public class ImageAdapter extends BaseAdapter {

        int imageBackground;
        private Context ctx;

        public ImageAdapter(Context c) {
            ctx = c;
        }


        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            ImageView iv = new ImageView(ctx);
            iv.setImageDrawable(imageList[arg0]);
            return iv;
        }


        @Override
        public int getCount() {

            return imageList.length;
        }

        @Override
        public Object getItem(int arg0) {

            return arg0;
        }

        @Override
        public long getItemId(int arg0) {

            return arg0;
        }
    }

    private class IncidentRequest {
        String driver;
        String damages;
        Long vehicleId;
        String description;
        String incidentTime;
        String incidentType;
        String incidentLocation;
        String ticketNumber;
        List<IncidentImages> incidentImages;

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getDamages() {
            return damages;
        }

        public void setDamages(String damages) {
            this.damages = damages;
        }

        public Long getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(Long vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIncidentTime() {
            return incidentTime;
        }

        public void setIncidentTime(String incidentTime) {
            this.incidentTime = incidentTime;
        }

        public String getIncidentType() {
            return incidentType;
        }

        public void setIncidentType(String incidentType) {
            this.incidentType = incidentType;
        }

        public String getIncidentLocation() {
            return incidentLocation;
        }

        public void setIncidentLocation(String incidentLocation) {
            this.incidentLocation = incidentLocation;
        }

        public String getTicketNumber() {
            return ticketNumber;
        }

        public void setTicketNumber(String ticketNumber) {
            this.ticketNumber = ticketNumber;
        }

        public List<IncidentImages> getIncidentImages() {
            return incidentImages;
        }

        public void setIncidentImages(List<IncidentImages> incidentImages) {
            this.incidentImages = incidentImages;
        }
    }


}
