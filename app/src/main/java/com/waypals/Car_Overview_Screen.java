package com.waypals;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.services.FetchAddressIntentService;
import com.waypals.services.LocationAddress;
import com.waypals.signup.Constants;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Car_Overview_Screen extends FragmentActivity implements
        OnMarkerClickListener, AsyncFinishInterface {
    public static GoogleMap map;
    static Marker selCar_mrker;
    static String tag = "Car_Overview_Screen Screen";
    static Collection<VehicleInformationVO> vehicleInfoVO;
    static LatLng MY_Position;
    static double x;
    static double y;
    static ImageView maplayer_img;
    static ImageView maploc_img;
    // static LatLng MY_Position;
    static String pinLoc = "none";
    ImageView back_btn;
    TextView cov_title_txt;
    // double x,y;
//	Timer timer;
    ImageView myloc_img, map_layer_img;
    int maplayer = 1;
    Marker myPosition;
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            // timer.cancel();
            x = location.getLatitude();
            y = location.getLongitude();
            // locationManager.removeUpdates(this);
            // locationManager.removeUpdates(locationListenerGps);

            if (myPosition != null) {
                myPosition.remove();
            }

            pinLoc = "net";
            MY_Position = new LatLng(x, y);
            myPosition = map.addMarker(new MarkerOptions()
                    .position(MY_Position)
                    .title("Me")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.blue_person)));

            // System.out.println("Network ::" + x + y);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            // timer.cancel();
            x = location.getLatitude();
            y = location.getLongitude();

            if (myPosition != null) {
                myPosition.remove();
            }

            pinLoc = "gps";
            MY_Position = new LatLng(x, y);
            myPosition = map.addMarker(new MarkerOptions()
                    .position(MY_Position)
                    .title("Me")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.blue_person)));

            // System.out.println("GPS ::" + x + y);

        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    boolean me_position = false;
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    ArrayList<LatLng> cars_latlng;
    ArrayList<Marker> carMarker;
    ArrayList<Long> carIds;
    CameraUpdate cu;
    ApplicationPreferences appPref;
    LatLng selectedCar_latlng;

    private AddressResultReceiver mResultReceiver;
    private String mAddressOutput;

    // ...

    protected void startIntentService(int id, double lat, double lon) {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra("receiver", mResultReceiver);
        intent.putExtra("id", id);
        intent.putExtra("lat", lat);
        intent.putExtra("lon", lon);
        startService(intent);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            String address = resultData.getString("address");
            int id = resultData.getInt("id");
            //displayAddressOutput();

            String newSnippet = "";
            if (carMarker != null && carMarker.size() > 0) {
                VehicleInformationVO[] lstVehicles = new VehicleInformationVO[carMarker.size()];
                vehicleInfoVO.toArray(lstVehicles);
                String title = lstVehicles[id].getRegistrationNumber();
                newSnippet = newSnippet + "\n" +
                        lstVehicles[id].getState() + "\n" +
                        Utility.parseServerDate(lstVehicles[id].getDateTime());
                newSnippet = newSnippet + "\n" + address;

                carMarker.get(id).setTitle(title);
                carMarker.get(id).setSnippet(newSnippet);
            }
           // carMarker.get(id).showInfoWindow();
           // carMarker.get(id).setVisible(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.car_overview);
            back_btn = (ImageView) findViewById(R.id.left_btn);

            mResultReceiver = new AddressResultReceiver(new Handler());


            cov_title_txt = (TextView) findViewById(R.id.nav_title_txt);

            Typeface tf = Typeface.createFromAsset(getAssets(),
                    CH_Constant.Font_Typface_Path);
            cov_title_txt.setTypeface(tf);
            cov_title_txt.setText("VEHICLE OVERVIEW");

            back_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;
                    map.setMapType(maplayer);
                    map.getUiSettings().setCompassEnabled(true);
                    map.setOnMarkerClickListener(Car_Overview_Screen.this);
                    // map.getUiSettings().setZoomControlsEnabled(false);
                    // map.getUiSettings().setScrollGesturesEnabled(false);

                    if (ActivityCompat.checkSelfPermission(Car_Overview_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Car_Overview_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    map.setMyLocationEnabled(false);

                    map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {

                        }
                    });

                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {

                            LinearLayout info = new LinearLayout(Car_Overview_Screen.this);
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(Car_Overview_Screen.this);
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());

                            TextView snippet = new TextView(Car_Overview_Screen.this);
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());

                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });

                    VehicleServices vehicleServices = new VehicleServices();
                    try {
                        vehicleServices.getVehiclesData(appPref, Car_Overview_Screen.this, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            appPref = new ApplicationPreferences(this);

            cars_latlng = new ArrayList<LatLng>();
            carMarker = new ArrayList<Marker>();
            carIds = new ArrayList<Long>();


            // ArrayList<Marker> markers = new ArrayList<Marker>();

            // / Getting LocationManager object from System Service LOCATION_SERVICE
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            gps_enabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            VehicleServices vehicleServices = new VehicleServices();
            vehicleServices.getVehiclesData(appPref, this, true);


            if (myPosition != null)
                myPosition.remove();

        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //int pos = -1;
        //Log.d("Car Overview: ", "Clicked");
       // selCar_mrker = marker;
        return false;
    }

    @Override
    public void finish(String response, String method) {

        Log.i(tag, "method RES: " + method);
        Gson gson = new Gson();
        if (method.contains("vehiclesdata") && response != null) {
            Log.i(tag, "getVehiclesData RES: " + response);
            VehicleServiceResponse vehicleServiceResponse = gson.fromJson(
                    response, VehicleServiceResponse.class);
            System.out.println("JNA................."
                    + vehicleServiceResponse.toString());
            if (vehicleServiceResponse.getResponse()
                    .equalsIgnoreCase("SUCCESS")) {
                vehicleInfoVO = vehicleServiceResponse.getVehicles();
            }
        }

    }

    @Override
    public void onPostExecute(Void Result) {
        setUpMapIfNeeded();

    }

    public boolean isGoogleMapsInstalled() {
        try {
            Log.i(tag, "check Google Map installed");
            ApplicationInfo info;
            info = getPackageManager().getApplicationInfo(
                    "com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;

        }
    }

    private void setUpMapIfNeeded() {
        if (map == null) {
            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mapFrag)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;
                    if (map != null) {
                        map.setMapType(maplayer);
                        map.getUiSettings().setCompassEnabled(true);
                        map.setOnMarkerClickListener(Car_Overview_Screen.this);
                        if (ActivityCompat.checkSelfPermission(Car_Overview_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Car_Overview_Screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            return;
                        }
                        map.setMyLocationEnabled(false);
                    }
                }
            });

        }

        if (isGoogleMapsInstalled()) {
            if (map != null) {
                // Getting LocationManager object from System Service
                // LOCATION_SERVICE
                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

                gps_enabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                network_enabled = locationManager
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (gps_enabled)
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 0, 0,
                        locationListenerGps);
                if (network_enabled)
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER, 0, 0,
                            locationListenerNetwork);

				/*if (timer == null) {
                    timer = new Timer();
					timer.schedule(new GetLastLocation(), 20000, 20000);
				}
*/
				carMarker.clear();
                Iterator<VehicleInformationVO> vehicleInfoItr = vehicleInfoVO
                        .iterator();
                if (vehicleInfoItr != null) {
                    if (selCar_mrker != null) {
                      //  selCar_mrker.remove();
                    }
                    int i = 0;
                    LatLngBounds.Builder bc = new LatLngBounds.Builder();
                    while (vehicleInfoItr.hasNext()) {
                        VehicleInformationVO vehicleInfo = vehicleInfoItr
                                .next();
                        if (vehicleInfo.getLocation() != null) {
                            String location = vehicleInfo.getLocation().toString();
                            Log.i(tag, location);
                            LatLng latlng = new LatLng(Double.parseDouble(location
                                    .split("/")[0]), Double.parseDouble(location
                                    .split("/")[1]));

						/*if (appPref.getVehicle_Selected() == vehicleInfo
                                .getVehicleId()) {*/
                            int res_icon = CH_Constant.getCarCurrentIcon(
                                    vehicleInfo.getState(),
                                    vehicleInfo.isSystemFenceEnabled, vehicleInfo.getVehicleType());
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(latlng).icon(
                                            BitmapDescriptorFactory
                                                    .fromResource(res_icon)));

                           // LocationAddress locationAddress = new LocationAddress();

                            String snippet = vehicleInfo.getState() + "\n" + Utility.parseServerDate(vehicleInfo.getDateTime());
                            marker.setTitle(vehicleInfo.getRegistrationNumber());
                            marker.setSnippet(snippet);
                            marker.setVisible(true);

                            //locationAddress.getAddressFromLocation(i++, marker.getPosition().latitude, marker.getPosition().longitude,
                              //      getApplicationContext(), new GeocoderHandler());
                            startIntentService(i, marker.getPosition().latitude, marker.getPosition().longitude);
                            i++;

                            carMarker.add(marker);
                           // selCar_mrker = marker;
                           // selCar_mrker.setVisible(true);
                            marker.setVisible(true);
                            bc.include(latlng);
//							appPref.saveSelectedCarLatLng(location);
                            /*CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latlng).build();
							map.moveCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));*/
//						}
                           // i++;
                        }


                    }
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
//					CameraPosition cameraPosition = new CameraPosition.Builder()
//							.target(selectedCar_latlng).zoom(11).build();
//					map.moveCamera(CameraUpdateFactory
//							.newCameraPosition(cameraPosition));
               //     map.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedCar_latlng, 18));
                    maplayer_img = (ImageView) Car_Overview_Screen.this
                            .findViewById(R.id.map_layer_img);
                    maploc_img = (ImageView) Car_Overview_Screen.this
                            .findViewById(R.id.myloc_img);
                    maplayer_img.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            maplayer++;
                            map.setMapType(maplayer);

                            if (maplayer == 4) {
                                maplayer = 0;
                            }

                        }
                    });

                    maploc_img.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int status = GooglePlayServicesUtil
                                    .isGooglePlayServicesAvailable(getBaseContext());
                            if (status != ConnectionResult.SUCCESS) {
                                // Google Play Services are not available
                                int requestCode = 10;
                                Dialog dialog = GooglePlayServicesUtil
                                        .getErrorDialog(status,
                                                Car_Overview_Screen.this, requestCode);
                                dialog.show();
                            } else {

                                if (!me_position) {
                                    maploc_img
                                            .setImageResource(R.drawable.car_icon);
                                    me_position = true;

                                    LatLng latLng = new LatLng(x, y);

                                    LatLng MY_Position = latLng;

                                    if (myPosition != null) {
                                        myPosition.remove();
                                    }

                                    myPosition = map
                                            .addMarker(new MarkerOptions()
                                                    .position(MY_Position)
                                                    .title("Me")
                                                    .icon(BitmapDescriptorFactory
                                                            .fromResource(R.drawable.blue_person)));

                                    map.moveCamera(CameraUpdateFactory
                                            .newLatLngZoom(latLng, 13));
                                    // map.animateCamera(CameraUpdateFactory.zoomTo(15));
                                    map.animateCamera(
                                            CameraUpdateFactory.zoomTo(12),
                                            1000, null);

                                    // CameraPosition cameraPosition = new
                                    // CameraPosition.Builder()
                                    // .target(MY_Position) // Sets the center
                                    // of the map to Mountain View
                                    // .zoom(17) // Sets the zoom
                                    // .bearing(90) // Sets the orientation of
                                    // the camera to east
                                    // .tilt(90) // Sets the tilt of the camera
                                    // to 30 degrees
                                    // .build(); // Creates a CameraPosition
                                    // from the builder
                                    // map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                                } else {
                                    try {
//										if (selectedCar_latlng != null) {
                                        maploc_img
                                                .setImageResource(R.drawable.man_icon);
                                        me_position = false;
//											map.moveCamera(CameraUpdateFactory
//													.newLatLngZoom(
//															selectedCar_latlng,
//															13));
                                        map.animateCamera(
                                                CameraUpdateFactory
                                                        .zoomTo(12), 1000,
                                                null);
                                        if (myPosition != null) {
                                            myPosition.remove();
                                        }
//										}
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            }

                        }
                    });

                }

            }

        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            int position = 0;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    position = bundle.getInt("position");
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }

            if (locationAddress != null) {
                String newSnippet = "";
                if (carMarker != null && carMarker.size() > 0) {
                    VehicleInformationVO[] lstVehicles = new VehicleInformationVO[carMarker.size()];
                    vehicleInfoVO.toArray(lstVehicles);
                    newSnippet = lstVehicles[position].getRegistrationNumber();
                    newSnippet = newSnippet +  "\n" +
                            lstVehicles[position].getState() + "\n" +
                            Utility.parseServerDate(lstVehicles[position].getDateTime());
                    newSnippet = newSnippet + "\n" + locationAddress;
                }

//                if (selCar_mrker.getSnippet() != null) {
//                    //newSnippet = selCar_mrker.getSnippet() + "\n" + locationAddress;
//                    newSnippet = locationAddress;
//                }else{
//                    newSnippet = locationAddress;
//                }
                carMarker.get(position).setSnippet(newSnippet);


            }
            //tvAddress.setText(locationAddress);
        }
    }

}
