package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;
import com.waypals.utils.Utility;

import org.json.JSONObject;

public class RemoveGeofenceConfirm extends Activity {

    static String tag = "Remove Geofence Confirm";
    String ids;
    String request;
    ApplicationPreferences appPref;
    String response;
    private ImageView iv_close, iv_delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        appPref = new ApplicationPreferences(this);
        setContentView(R.layout.remove_geofence_confirm_screen);
        String fenceName = "";
        String fenceID = "";

        String data[] = (String[]) getIntent().getExtras().get("data");

        if (data != null) {
            Log.i(tag, "1: " + data[0] + "2: " + data[1]);

            if (data[0] != null) {
                fenceName = data[0];
            }
            if (data[1] != null) {
                fenceID = data[1];
            }

        }

        Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        TextView geofencename = (TextView) findViewById(R.id.GeofenceName);

        geofencename.setTypeface(tf);
        geofencename.setText(fenceName);
        geofencename.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

        iv_close = (ImageView) findViewById(R.id.iv_closeBtn);
        iv_delete = (ImageView) findViewById(R.id.iv_delete);
        //ids=getIntent().getExtras().getString("IDS_GEO_FENCE");
        request = "{\"vehicleId\":" + appPref.getVehicle_Selected() + ",\"fenceIds\":[" + fenceID + "]}";
        ids = new String(fenceID);
        Log.d("Deleted GeoFences are:", ids);


        iv_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                setResult(RESULT_CANCELED, i);
                finish();

            }
        });

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                CallService call = new CallService();
                call.execute();
            }
        });

    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent i = new Intent();
        setResult(RESULT_CANCELED, i);
        finish();
    }

    class CallService implements AsyncFinishInterface {//extends AsyncTask<Void, Void, Void> {
        String errorMessage;
        boolean enable;

        public void execute() {
            try {
                WplHttpClient.callVolleyService(RemoveGeofenceConfirm.this,
                        CH_Constant.Remove_Geofence_Api,
                        request, appPref, this, null, true);
            } catch (NetworkException e) {
                errorMessage = e.getLocalizedMessage();
                ExceptionHandler.handlerException(e, RemoveGeofenceConfirm.this);

            } catch (Exception e) {
                errorMessage = e.getLocalizedMessage();
                ExceptionHandler.handlerException(e, RemoveGeofenceConfirm.this);
            }
        }

        @Override
        public void finish(String response, String method) throws Exception {
            RemoveGeofenceConfirm.this.response = response;
            JSONObject resultJSON = new JSONObject(response);
            if (response != null && "SUCCESS".equals(resultJSON.optString("response"))) {
                enable = true;
            } else {
                enable = false;
            }
        }

        @Override
        public void onPostExecute(Void result) {
            try {
                if (enable) {
                    Utility.makeNewToast(RemoveGeofenceConfirm.this, RemoveGeofenceConfirm.this.getString(R.string.geofences_deleted));
                } else if (!enable || errorMessage != null) {
                    Utility.makeNewToast(RemoveGeofenceConfirm.this, RemoveGeofenceConfirm.this.getString(R.string.please_try_again));
                }
            } catch (Exception ex) {
                ExceptionHandler.handleException(ex);
            }
            Intent i = new Intent();
            setResult(RESULT_OK, i);
            RemoveGeofenceConfirm.this.finish();
        }
    }

}
