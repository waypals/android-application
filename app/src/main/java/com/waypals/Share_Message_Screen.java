package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.waypals.adapters.ContactsCompletionView;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedImpl.AppController;
import com.waypals.feedImpl.FeedImageView;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.feedItems.Friend;
import com.waypals.feedItems.ImageContent;
import com.waypals.feedItems.Images;
import com.waypals.request.FeedRequest;
import com.waypals.response.AcceptedFriendResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;
import com.wareninja.opensource.common.ObjectSerializer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class Share_Message_Screen extends Activity implements TokenCompleteTextView.TokenListener, AsyncFinishInterface {

    ContactsCompletionView completionView;
    Friend[] friends;
    ArrayAdapter<Friend> adapter;
    JSONArray friendsArray;
    Button actionButton;
    ImageView back_btn;
    TextView title, feedText;
    String tag = "Share message screen";
    ArrayList<Friend> selectedFriends = new ArrayList<Friend>();
    EditText message;
    Activity context;
    String method;
    String latt = null;
    String longg = null;
    Long feedId;
    String content;
    ServiceResponse serviceResponse;
    AcceptedFriendResponse friendResponse;
    FeedWrapper feedWrapper;
    FeedImageView imageView1;
    FeedImageView imageView2;
    FeedImageView imageView3;
    TableRow firstImageRow;
    TableRow secondImageRow;
    ImageLoader imageLoader = null;
    private AsyncFinishInterface asyncFinishInterface;
    private ApplicationPreferences appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        asyncFinishInterface = this;

        final String screen = getIntent().getStringExtra("screen");
        context = this;

        if (screen.equals("sharelocation")) {
            setContentView(R.layout.share_message_screen);
            findViewById(R.id.camera_button).setVisibility(View.INVISIBLE);
        } else {
            setContentView(R.layout.share_message_forward);
        }
        actionButton = (Button) findViewById(R.id.postButton);
        title = (TextView) findViewById(R.id.nav_title_txt);
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Typeface tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);
        title.setTypeface(tf);
        message = (EditText) findViewById(R.id.feedEditText);
        if (screen.equals("sharelocation")) {
            title.setText("SHARE LOCATION");
            actionButton.setText("share");
            message.setText(R.string.default_sharelocation_msg);
            latt = getIntent().getStringExtra("lat");
            longg = getIntent().getStringExtra("long");
        }
        if (screen.equals("feeds")) {
            title.setText("SHARE FEED");
            actionButton.setText("share");
            feedId = getIntent().getLongExtra("feedId", 0);
            content = getIntent().getStringExtra("content");
            feedText = (TextView) findViewById(R.id.feedText);
            if (content != null) {
                if (imageLoader == null) {
                    imageLoader = AppController.getInstance().getImageLoader();
                }
                feedWrapper = (FeedWrapper) ObjectSerializer.deserialize(content);
                String txt = feedWrapper.getFeed().getContent().getDescription();
                if (txt != null && !txt.trim().isEmpty()) {
                    feedText.setText(txt);
                } else {
                    feedText.setVisibility(View.GONE);
                }
                String type = feedWrapper.getFeed().getType();
                RelativeLayout container = (RelativeLayout) findViewById(R.id.imageContainer);



                /*Now Set Images If exists*/
                if (type.equals("TEXT")) {
                    container.setVisibility(View.GONE);
                }
                if (type.equals("IMAGE")) {
                    imageView1 = (FeedImageView) findViewById(R.id.feedImage1);
                    imageView2 = (FeedImageView) findViewById(R.id.feedImage2);
                    imageView3 = (FeedImageView) findViewById(R.id.feedImage3);
                    firstImageRow = (TableRow) findViewById(R.id.firstRow);
                    secondImageRow = (TableRow) findViewById(R.id.secondRow);

                    ImageContent imageContent = (ImageContent) feedWrapper.getFeed().getContent();
                    Images[] images = imageContent.getImages();

                    if (images != null && images.length == 0) {
                        container.setVisibility(View.GONE);
                    } else {
                        if (images.length == 1) {
                            secondImageRow.setVisibility(View.GONE);
                        }


                        try {
                            switch (images.length) {
                                case 1:

                                    secondImageRow.setVisibility(View.GONE);

                                    if (images[0].getImageLink() != null) {
                                        imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        imageView1.setVisibility(View.VISIBLE);
                                        imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        firstImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        imageView1.setVisibility(View.GONE);
                                    }
                                    break;


                                case 2:
                                    firstImageRow.setVisibility(View.GONE);
                                    if (images[0].getImageLink() != null) {
                                        imageView2.setImageUrl(images[0].getImageLink(), imageLoader);
                                        imageView2.setVisibility(View.VISIBLE);
                                        imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        secondImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[1].getImageLink() != null) {
                                        imageView3.setImageUrl(images[1].getImageLink(), imageLoader);
                                        imageView3.setVisibility(View.VISIBLE);
                                        imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        secondImageRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        imageView3.setVisibility(View.GONE);
                                    }

                                    break;

                                default:

                                    if (images[0].getImageLink() != null) {
                                        imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        imageView1.setVisibility(View.VISIBLE);
                                        imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        firstImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {

                                                        firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        imageView1.setVisibility(View.GONE);
                                    }
                                    if (images[1].getImageLink() != null) {
                                        imageView2.setImageUrl(images[1].getImageLink(), imageLoader);
                                        imageView2.setVisibility(View.VISIBLE);
                                        imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {

                                                    }
                                                });
                                    } else {
                                        imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[2].getImageLink() != null) {
                                        imageView3.setImageUrl(images[2].getImageLink(), imageLoader);
                                        imageView3.setVisibility(View.VISIBLE);
                                        imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {

                                                    }
                                                });
                                    } else {
                                        imageView3.setVisibility(View.GONE);
                                    }


                                    break;

                            }

                        } catch (RuntimeException error) {
                            Log.d(tag, "Found Error while loading images|Hiding Image container");
                            container.setVisibility(View.GONE);
                        }
                    }


                }

            }

        }
        if (screen.equals("messages")) {
            title.setText("MESSAGE(S)");
            actionButton.setText("send");
            feedId = getIntent().getLongExtra("feedId", 0);
            content = getIntent().getStringExtra("content");
            feedText = (TextView) findViewById(R.id.feedText);
            if (content != null) {
                if (imageLoader == null) {
                    imageLoader = AppController.getInstance().getImageLoader();
                }
                feedWrapper = (FeedWrapper) ObjectSerializer.deserialize(content);
                String txt = feedWrapper.getFeed().getContent().getDescription();
                if (txt != null && !txt.trim().isEmpty()) {
                    feedText.setText(txt);
                } else {
                    feedText.setVisibility(View.GONE);
                }
                String type = feedWrapper.getFeed().getType();
                RelativeLayout container = (RelativeLayout) findViewById(R.id.imageContainer);



                /*Now Set Images If exists*/
                if (type.equals("TEXT")) {
                    container.setVisibility(View.GONE);
                }
                if (type.equals("IMAGE")) {
                    imageView1 = (FeedImageView) findViewById(R.id.feedImage1);
                    imageView2 = (FeedImageView) findViewById(R.id.feedImage2);
                    imageView3 = (FeedImageView) findViewById(R.id.feedImage3);
                    firstImageRow = (TableRow) findViewById(R.id.firstRow);
                    secondImageRow = (TableRow) findViewById(R.id.secondRow);

                    ImageContent imageContent = (ImageContent) feedWrapper.getFeed().getContent();
                    Images[] images = imageContent.getImages();


                    if (images == null) {
                        container.setVisibility(View.GONE);
                    } else if (images != null && images.length == 0) {
                        container.setVisibility(View.GONE);
                    } else {
                        if (images.length == 1) {

                            secondImageRow.setVisibility(View.GONE);
                        }


                        try {
                            switch (images.length) {
                                case 1:

                                    secondImageRow.setVisibility(View.GONE);

                                    if (images[0].getImageLink() != null) {
                                        imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        imageView1.setVisibility(View.VISIBLE);
                                        imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        firstImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        imageView1.setVisibility(View.GONE);
                                    }
                                    break;


                                case 2:
                                    firstImageRow.setVisibility(View.GONE);
                                    if (images[0].getImageLink() != null) {
                                        imageView2.setImageUrl(images[0].getImageLink(), imageLoader);
                                        imageView2.setVisibility(View.VISIBLE);
                                        imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        secondImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[1].getImageLink() != null) {
                                        imageView3.setImageUrl(images[1].getImageLink(), imageLoader);
                                        imageView3.setVisibility(View.VISIBLE);
                                        imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        secondImageRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        imageView3.setVisibility(View.GONE);
                                    }

                                    break;

                                default:

                                    if (images[0].getImageLink() != null) {
                                        imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        imageView1.setVisibility(View.VISIBLE);
                                        imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        firstImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {

                                                        firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        imageView1.setVisibility(View.GONE);
                                    }
                                    if (images[1].getImageLink() != null) {
                                        imageView2.setImageUrl(images[1].getImageLink(), imageLoader);
                                        imageView2.setVisibility(View.VISIBLE);
                                        imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {

                                                    }
                                                });
                                    } else {
                                        imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[2].getImageLink() != null) {
                                        imageView3.setImageUrl(images[2].getImageLink(), imageLoader);
                                        imageView3.setVisibility(View.VISIBLE);
                                        imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {

                                                        secondImageRow.setVisibility(View.GONE);

                                                    }

                                                    @Override
                                                    public void onSuccess() {

                                                    }
                                                });
                                    } else {
                                        imageView3.setVisibility(View.GONE);
                                    }


                                    break;

                            }

                        } catch (RuntimeException error) {
                            Log.d(tag, "Found Error while loading images|Hiding Image container");
                            container.setVisibility(View.GONE);
                        }
                    }
                } else {
                    container.setVisibility(View.GONE);
                }
            }
        }


        appPrefs = new ApplicationPreferences(this);

        GenericAsyncService service = new GenericAsyncService(appPrefs, this, CH_Constant.GET_FRIEND_LIST, "listacceptedfriends", "", tag, this, false);
        service.execute();

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ("sharelocation".equals(screen)) {
                    message = (EditText) findViewById(R.id.feedEditText);
                    String text = message.getText().toString();

                    if (selectedFriends == null) {
                        selectedFriends = new ArrayList<Friend>();
                    }
                    if (text.isEmpty()) {
                        try {
                            Utility.makeNewToast(context, getString(R.string.inp_alert_message));
                        } catch (Exception e) {
                            ExceptionHandler.handlerException(e, context);
                        }
                    } else {
                        if (selectedFriends != null && selectedFriends.size() < 1) {
                            try {
                                Utility.makeNewToast(context, getString(R.string.inp_select_friend));
                            } catch (Exception e) {
                                ExceptionHandler.handlerException(e, context);
                            }
                        } else {
                            try {
                                JSONArray users = new JSONArray();

                                for (Friend f : selectedFriends) {
                                    users.put(f.getUserId());
                                }

                                if (screen.equals("sharelocation")) {
                                    JSONObject input = new JSONObject();
                                    input.put("latitude", Double.parseDouble(latt));
                                    input.put("longitude", Double.parseDouble(longg));
                                    input.put("userIds", users);
                                    input.put("timeStamp", Utility.getCurrentUtcTime());

                                    Log.d(tag, "input :" + input);
//                                /*Service call*/
//                                    ShareLocationService shareLocationService = new ShareLocationService(appPrefs, context, input);
//                                    shareLocationService.execute();

                                    GenericAsyncService service = new GenericAsyncService(appPrefs, context, CH_Constant.SHARE_LOCATION, "shareposition", input.toString(), tag, asyncFinishInterface, false);
                                    service.execute();


                                    selectedFriends.clear();
                                    message.setText("");
                                    completionView.clear();
                                }

                            } catch (Throwable e) {
                                ExceptionHandler.handleException(e);
                            }
                        }

                    }
                } else {

                    if (selectedFriends == null) {
                        selectedFriends = new ArrayList<Friend>();
                    }
                    {
                        if (selectedFriends != null && selectedFriends.size() < 1) {
                            try {
                                Utility.makeNewToast(context, getString(R.string.inp_select_friend));
                            } catch (Exception e) {
                                ExceptionHandler.handlerException(e, context);
                            }
                        } else {
                            try {
                                JSONArray users = new JSONArray();

                                for (Friend f : selectedFriends) {
                                    users.put(f.getUserId());
                                }

                                if (screen.equals("feeds")) {
                                    String input = new FeedRequest().shareFeed(feedId, selectedFriends);
                                    Log.d(tag, "input :" + input);
                                    if (input != null) {
                                        GenericAsyncService service = new GenericAsyncService(appPrefs, context, CH_Constant.SHARE_FEED, "sharefeed", input.toString(), tag, asyncFinishInterface, false);
                                        service.execute();

                                        selectedFriends.clear();
                                        completionView.clear();
                                        finish();
                                    }
                                }
                                if (screen.equals("messages")) {
                                    String input = new FeedRequest().shareFeed(feedId, selectedFriends);
                                    Log.d(tag, "input :" + input);
                                    if (input != null) {
                                        GenericAsyncService service = new GenericAsyncService(appPrefs, context, CH_Constant.SHARE_MESSAGE, "sharefeedmessage", input.toString(), tag, asyncFinishInterface, false);
                                        service.execute();

                                        selectedFriends.clear();
                                        completionView.clear();
                                        finish();
                                    }
                                }

                            } catch (Throwable e) {
                                ExceptionHandler.handleException(e);
                            }
                        }

                    }
                }


            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish(String response, String method) {

        try {
            this.method = method;
            Gson gson = new Gson();
            if (method != null && method.endsWith("listacceptedfriends")) {
                friendResponse = gson.fromJson(response, AcceptedFriendResponse.class);

                if (friendResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    friends = friendResponse.getAcceptedFriendsResult();
                }
                Log.d(tag, friendResponse.toString());
            }
            if (method != null && method.equals("shareposition")) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
            if (method != null && method.endsWith("sharefeed")) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
            if (method != null && method.endsWith("sharefeedmessage")) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        } catch (Throwable ex) {
            ExceptionHandler.handleException(ex);
        }

    }


    public void createFriendsList(Friend[] friends) {
        try {
            if (friends != null) {
                adapter = new FilteredArrayAdapter<Friend>(this, R.layout.friend_list_row, friends) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {

                            LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                            convertView = (View) l.inflate(R.layout.friend_list_row, parent, false);
                        }

                        Friend p = getItem(position);
                        ((TextView) convertView.findViewById(R.id.name)).setText(p.getFirstName() + " " + p.getLastName());
                        ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());

                        return convertView;
                    }

                    @Override
                    protected boolean keepObject(Friend obj, String mask) {
                        mask = mask.toLowerCase();
                        if (!selectedFriends.contains(obj)) {
                            return obj.getFirstName().toLowerCase().startsWith(mask) || obj.getLastName().toLowerCase().startsWith(mask) || obj.getEmail().toLowerCase().startsWith(mask);
                        } else
                            return false;
                    }
                };


                completionView = (ContactsCompletionView) findViewById(R.id.contacts);
                completionView.setDuplicateParentStateEnabled(false);
                completionView.setAdapter(adapter);
                completionView.setThreshold(1);
                completionView.setTokenListener(this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            } else {
                try {
                    Utility.makeNewToast(context, getString(R.string.err_no_friends));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, context);
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, context);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        // TODO Auto-generated method stub
        try {
            if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) || (friendResponse != null && "ERROR".equalsIgnoreCase(
                    friendResponse.getResponse())))  {
                String errorMessage = null;
                if(serviceResponse != null) {
                    errorMessage = serviceResponse.getErrorCode();
                }else if(friendResponse != null)
                {
                    errorMessage = friendResponse.getErrorCode();
                }
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }

                return;
            }

            if (method != null && method.endsWith("listacceptedfriends")) {
                if (friendResponse != null && "SUCCESS".equalsIgnoreCase(friendResponse.getResponse())) {
                    createFriendsList(friends);
                } else {
                    try {
                        Utility.makeNewToast(context, CH_Constant.ERROR_Msg(friendResponse.getErrorCode()));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                }

            }
            if (method != null && method.endsWith("shareposition")) {
                if (serviceResponse != null) {
                    if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                        if (serviceResponse.getNotAUser() != null && serviceResponse.getNotAUser().length > 0) {
                            StringBuilder namelist = new StringBuilder();
                            for (String name : serviceResponse.getNotAUser()) {
                                if (namelist.length() == 0) {
                                    namelist.append(name);
                                } else {
                                    namelist.append(",");
                                    namelist.append(name);
                                }
                            }
                            String msg = getString(R.string.err_no_rideamigos_app);
                            msg = msg.replace("#FRIENDS", namelist.toString());
                            Utility.makeNewToast(this, msg);
                        } else {
                            Utility.makeNewToast(this, getString(R.string.op_alert_loc_shared));
                        }
                    } else {
                        Utility.makeNewToast(this, getString(R.string.op_alert_msg_share_failed));
                    }
                }
                selectedFriends.clear();

            }
            if (method != null && method.endsWith("sharefeed")) {
                if (serviceResponse != null) {
                    if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                        Utility.makeNewToast(this, getString(R.string.op_alert_feed_shared));
                    } else {
                        Utility.makeNewToast(this, getString(R.string.op_alert_feed_share_failed));
                    }
                }
                selectedFriends.clear();
            }
            if (method.endsWith("sharefeedmessage")) {
                if (serviceResponse != null) {
                    if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                        Utility.makeNewToast(this, getString(R.string.op_alert_msg_shared));
                    } else {
                        Utility.makeNewToast(this, getString(R.string.op_alert_msg_share_failed));
                    }
                }
                selectedFriends.clear();
            }

        } catch (Exception ex) {
            ExceptionHandler.handleException(ex, context);
        }
    }

    @Override
    public void onTokenAdded(Object token) {
        if (selectedFriends == null) {
            selectedFriends = new ArrayList<Friend>();
        }

        if (!selectedFriends.contains((Friend) token)) {
            selectedFriends.add((Friend) token);
        }
    }

    @Override
    public void onTokenRemoved(Object token) {
        if (selectedFriends != null && !selectedFriends.isEmpty() && selectedFriends.contains((Friend) token)) {
            selectedFriends.remove((Friend) token);
        }

    }

}
