package com.waypals.documentcase;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.adapters.DocumentImageAdapter;
import com.waypals.adapters.ImageViewerAdapter;
import com.waypals.adapters.SpinnerAdapter;
import com.waypals.exception.ExceptionHandler;
import com.waypals.imageHelper.ImageResizer;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * A placeholder fragment containing a simple view.
 */
public class DocumentCaseFragment extends Fragment implements AsyncFinishInterface{

    private Spinner spinnerCode;
    private String fileNamePath;

    public static final int CAMERA_PICTURE = 2;
    public static final int GALLERY_PICTURE = 1;
    private GridView grid;
    DocumentImageAdapter docAdapter;
    TextView txtCode;
    private String lastImageType;
    private String lastImageHashMap;
    private ApplicationPreferences appPref;
    private ServiceResponse serviceResponse;
    private LinearLayout lytAddDocPanel;
    private LinearLayout lytAddMore;
    private String method;

    private final String UPLOAD_IMAGE = "UPLOAD_IMAGE";
    private final String FETCH_IMAGES = "FETCH_IMAGES";
    private ViewPager mPager;
    private ImageViewerAdapter adapter;
    private Stack<ImageUrlInfo> stackImages;

    private BroadcastReceiver fragmentHelper = null;
    private boolean lastIsLink;
    private long lastId;
    private String recentAddedType;
    private String recentAddedImage;
    private TextView txtNoDoc;

    public DocumentCaseFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document_case, container, false);

        fragmentHelper = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if("DOC_DELETED".equals(action))
                {
                    mPager.setVisibility(View.GONE);
                    grid.setVisibility(View.VISIBLE);
                    String type = intent.getStringExtra("TYPE");
                    fetchImages();

                    //updateImages(type, null, false, -1, true);
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("DOC_DELETED");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(fragmentHelper, filter);

        ImageView back_btn = (ImageView) view.findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               handleBackPress();
            }
        });

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), CH_Constant.Font_Typface_Path);
        TextView nav_title_txt = (TextView) view.findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText(getString(R.string.documents_case));

        RelativeLayout lytCode = (RelativeLayout) view.findViewById(R.id.layout_doc_type);
        spinnerCode = (Spinner) lytCode.findViewById(R.id.spinner_drop_down);
        txtCode = (TextView) lytCode.findViewById(R.id.text_spinner);
        txtCode.setText(DocumentType.DL.getValue());

        spinnerCode.setPrompt("Type");
        String[] codes = {DocumentType.DL.getValue(), DocumentType.VPC.getValue(), DocumentType.VIPC.getValue(), DocumentType.VRC.getValue()};
        SpinnerAdapter<String> spinnerCodeAdapter = new SpinnerAdapter<String>(getActivity(), R.layout.spinner_text_layout, codes, txtCode, spinnerCode);

        spinnerCode.setAdapter(spinnerCodeAdapter);

        txtCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerCode.performClick();
            }
        });

        lytAddDocPanel = (LinearLayout) view.findViewById(R.id.layout_add_more_document_panel);

        lytAddMore = (LinearLayout) view.findViewById(R.id.lay_add_document_button);
        lytAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lytAddDocPanel.setVisibility(View.VISIBLE);
                lytAddMore.setVisibility(View.GONE);
            }
        });

        appPref = new ApplicationPreferences(getActivity());

        txtNoDoc = (TextView) view.findViewById(R.id.text_no_doc_available);

        TextView btnCancel = (TextView) view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lytAddDocPanel.setVisibility(View.GONE);
                lytAddMore.setVisibility(View.VISIBLE);

                if(lastImageHashMap != null && lastImageType != null) {
                    onCancel();
                }
            }
        });

        TextView btnSave = (TextView) view.findViewById(R.id.button_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isValidDoc())
                {
                    HashMap<String, String> imgMap = new HashMap<String, String>();

                    String imageStr = Utility.getImageToHex(getActivity(), recentAddedImage);
                    imgMap.put(recentAddedType, imageStr);

                    VehicleUploadDocument vehicleUploadDocument = new VehicleUploadDocument();
                    vehicleUploadDocument.setVehicleId(appPref.getVehicle_Selected());
                    vehicleUploadDocument.setType(getIntType(recentAddedType));
                    vehicleUploadDocument.setImageHexString(imgMap);

                    Gson gson = new Gson();
                    String input = gson.toJson(vehicleUploadDocument).toString();

                    GenericAsyncService service = new GenericAsyncService(appPref, getActivity(),
                            CH_Constant.VEHICLE_DOCUMENT_UPLOAD, UPLOAD_IMAGE, input, null, DocumentCaseFragment.this, true);
                    service.execute();
                }
                else {
                    Utility.makeNewToast(getActivity(), "Please upload an image!!!");
                }
            }
        });



        TextView picture = (TextView) view.findViewById(R.id.picture);
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
       // picture.setEnabled(false);

        grid = (GridView) view.findViewById(R.id.grid);

        String[] content = new String[]{DocumentType.DL.getValue(), DocumentType.VIPC.getValue(), DocumentType.VPC.getValue(), DocumentType.VRC.getValue()};
        docAdapter = new DocumentImageAdapter(getActivity(), R.layout.document_image_layout);

        grid.setAdapter(docAdapter);
        mPager = (ViewPager) view.findViewById(R.id.pager);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(lytAddDocPanel.getVisibility() == View.VISIBLE)
                {
                    onCancel();
                }

                grid.setVisibility(View.GONE);
                mPager.setVisibility(View.VISIBLE);
                adapter = new ImageViewerAdapter(((FragmentActivity) getActivity()).getSupportFragmentManager(), getDocImages(), getActivity());
                mPager.setAdapter(adapter);
                mPager.setCurrentItem(i);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if( keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK )
                {
                    if(keyEvent.getAction() == KeyEvent.ACTION_DOWN)
                    {
                        return true;
                    }
                    Log.d("Document Case", "Back Pressed");
                    handleBackPress();
                    return true;
                }
                return false;
            }
        });

        initImages();
        fetchImages();
        return view;
    }


    private void handleBackPress() {
        if(mPager.getVisibility() == View.VISIBLE)
        {
            mPager.setVisibility(View.GONE);
            grid.setVisibility(View.VISIBLE);
        }
        else {
            getActivity().finish();
        }
    }

    private Integer getIntType(String lastImageType) {

        if(DocumentType.DL.getValue().equals(lastImageType))
        {
            return 3;
        }else if(DocumentType.VIPC.getValue().equals(lastImageType))
        {
            return 2;
        }else if(DocumentType.VPC.getValue().equals(lastImageType))
        {
            return 1;
        }else if(DocumentType.VRC.getValue().equals(lastImageType))
        {
            return 0;
        }

        return 3;
    }

    private void fetchImages()
    {
        GenericAsyncService service = null;
        try {
            service = new GenericAsyncService(appPref, getActivity(),
                    CH_Constant.GET_VEHICLE_DOCUMENT+"/"+ URLEncoder.encode("" + appPref.getVehicle_Selected(), "UTF-8"), FETCH_IMAGES, null, null, DocumentCaseFragment.this, false);
            service.execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    private boolean isValidDoc()
    {
        boolean result = false;
        if(!Utility.isStringNullEmpty(recentAddedType) && !Utility.isStringNullEmpty(recentAddedImage))
        {
            result = true;
        }
        return result;
    }

    public void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                fileNamePath = null;
                Log.d("Document Case", "image selection canceled");
            }
        });
        builder.show();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/" + timeStamp + "_temp.jpg");

        // Save a file: path for use with ACTION_VIEW intents
        fileNamePath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_CANCELED) {
        }
        Log.i("resultCode", String.valueOf(resultCode));
        if (requestCode == CAMERA_PICTURE && resultCode == getActivity().RESULT_OK)// capture image form camera
        {
            onPhotoTaken();
        } else if (requestCode == GALLERY_PICTURE && resultCode == getActivity().RESULT_OK) // pick picture from gallary
        {
            onPhotoTakenFromGallery(data);
        }
    }

    public void onPhotoTaken() {
        try {
            if (fileNamePath != null) {
                fileNamePath = checkImageSize(fileNamePath);
                {
                    String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                    String type = name.substring(name.indexOf(".") + 1);
                    String imageStr = Utility.getImageToHex(getActivity(), fileNamePath);
               //     final IncidentImages images = new IncidentImages();
                 //   images.setImageName(name);
                //    images.setImageStr(imageStr);
                    if (type.equalsIgnoreCase(ImageType.JPEG.name())) {
               //         images.setImageType(ImageType.JPEG);
                  } else if (type.equalsIgnoreCase(ImageType.PNG.name())) {
                //        images.setImageType(ImageType.PNG);
                    } else if (type.equalsIgnoreCase(ImageType.JPG.name())) {
                 //       images.setImageType(ImageType.JPG);
                    } else {
                        Utility.makeNewToast(getActivity(), "Image format not supported");
                        return;
                    }
                    updateImages(txtCode.getText().toString(), fileNamePath, false, -1, false);
                }

            }
            Log.d("camera imagePath", fileNamePath);
         //   Log.d(tag, "Total Image files: " + imagesSet);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("camera imagePath", "onactivity result");
        }

    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            Uri _uri = data.getData();
            if (_uri != null) {
                fileNamePath = getPath(_uri);
                Log.d("Document Case", "gallary image path: " + fileNamePath);
                if (fileNamePath != null) {
                    fileNamePath = checkImageSize(fileNamePath);
                    {
                        String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                        String type = name.substring(name.indexOf(".") + 1);
                        String imageStr = Utility.getImageToHex(getActivity(), fileNamePath);
                      //  final IncidentImages images = new IncidentImages();
                     //   images.setImageName(name);
                     //   images.setImageStr(imageStr);

                        if (type.equalsIgnoreCase(ImageType.JPEG.name())) {
                    //        images.setImageType(ImageType.JPEG);
                        } else if (type.equalsIgnoreCase(ImageType.PNG.name())) {
                         //   images.setImageType(ImageType.PNG);
                        } else if (type.equalsIgnoreCase(ImageType.JPG.name())) {
                           // images.setImageType(ImageType.JPG);
                        } else {
                            Utility.makeNewToast(getActivity(), "Image format not supported");
                            return;
                        }

                        updateImages(txtCode.getText().toString(), fileNamePath, false, -1, false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Document Case", "On activity result");
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        Gson gson = new Gson();
        this.method = method;

        if (response != null) {
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
            Log.d("Response", response.toString());
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse()))) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(getActivity(),
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(getActivity(), Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                setAsNoDoc(null);
//                Utility.makeNewToast(getActivity(), "Server not responding, please try after some time!!");
            }

            return;
        }

        if (serviceResponse != null) {
            if (serviceResponse.getResponse().equals("SUCCESS")) {

                if(UPLOAD_IMAGE.equals(method)) {
                    List<Document> document = serviceResponse.getDocuments();
                    if(document != null && !document.isEmpty())
                    {
                        Document doc = document.get(0);
                       // updateImages(getType(doc.getType()), doc.getImageUrl(), true, doc.getDocumentId(), false);
                    }
                    fetchImages();
                    Utility.makeNewToast(getActivity(), "Information updated");
                    lytAddMore.setVisibility(View.VISIBLE);
                    lytAddDocPanel.setVisibility(View.GONE);
                    lastImageHashMap = null;
                    lastImageType = null;
                    recentAddedImage = null;
                    recentAddedType = null;
                }else if(FETCH_IMAGES.equals(method))
                {
                    List<Document> documents = serviceResponse.getDocuments();

                    if(documents != null)
                    {
                        initImages();
                        for(Document document : documents)
                        {
                            updateImages(getType(document.getType()), document.getImageUrl(), true, document.getDocumentId(), false);
                        }
                    }else {
                        setAsNoDoc(null);
                    }
                }
            } else {
                Utility.makeNewToast(getActivity(), "Could not update information");
            }
        }
    }

    private void setAsNoDoc(List<DocImageData> lstData)
    {
        if(lstData != null)
        {
            if(!lstData.isEmpty())
            {
                txtNoDoc.setVisibility(View.GONE);
                if(mPager.getVisibility() == View.GONE)
                {
                    grid.setVisibility(View.VISIBLE);
                }
            }
            else {
                mPager.setVisibility(View.GONE);
                grid.setVisibility(View.GONE);
                txtNoDoc.setVisibility(View.VISIBLE);
            }
        }
        else {
            mPager.setVisibility(View.GONE);
            grid.setVisibility(View.GONE);
            txtNoDoc.setVisibility(View.VISIBLE);
        }
    }

    enum ImageType {
        PNG, JPG, JPEG;
    }

    public enum DocumentType {
        DL("Driving Licence"), VPC("Vehicle Pollution Certificate"),
        VIPC("Vehicle Insurance Policy Certificate"), VRC("Vehicle Registration Certificate");

        private String value;

        DocumentType(String value)
        {
            this.value = value;
        }

        public String getValue()
        {
            return this.value;
        }
    }

    private String getType(int type)
    {
        DocumentType documentType = DocumentType.DL;
        switch (type)
        {
            case 0: documentType = DocumentType.VRC; break;
            case 1: documentType = DocumentType.VPC; break;
            case 2: documentType = DocumentType.VIPC; break;
            case 3: documentType = DocumentType.DL; break;
        }

        return documentType.getValue();
    }

    private Map<String, ImageUrlInfo> mapImages = new HashMap<>();

    private void initImages()
    {
        mapImages.clear();
        mapImages.put(DocumentType.DL.getValue(), null);
        mapImages.put(DocumentType.VPC.getValue(), null);
        mapImages.put(DocumentType.VIPC.getValue(), null);
        mapImages.put(DocumentType.VRC.getValue(), null);

        stackImages = new Stack<>();
    }

    private List<DocImageData> getDocImages()
    {
        Set<Map.Entry<String, ImageUrlInfo>> entries = mapImages.entrySet();
        Iterator<Map.Entry<String, ImageUrlInfo>> iterator = entries.iterator();

        List<DocImageData> lstData = new ArrayList<>();
        while (iterator.hasNext())
        {
            Map.Entry<String, ImageUrlInfo> next = iterator.next();
            ImageUrlInfo value = next.getValue();
            if(value != null && value.getImageUrl() != null)
            {
                lstData.add(new DocImageData(next.getKey(), value.getImageUrl(), value.isLink(), value.getDocumentId()));
            }
        }

        return lstData;
    }

    private void onCancel()
    {
        fetchImages();
//        if(lastImageType != null) {
//            updateImages(lastImageType, lastImageHashMap, lastIsLink, lastId, false);
//        }
    }

    private void updateImages(String type, String image, boolean isLink, long id, boolean isDeleted)
    {
        docAdapter.clear();
        //docAdapter.notifyDataSetChanged();
        recentAddedType = type;
        recentAddedImage = image;

        ImageUrlInfo imageUrlInfo = new ImageUrlInfo();
        imageUrlInfo.setImageUrl(image);
        imageUrlInfo.setDocumentId(id);
        imageUrlInfo.setIsLink(isLink);

        if(image != null)
        {
            mapImages.put(type, imageUrlInfo);
        }
        Log.d("Image Size", "" + mapImages.size());

        Set<Map.Entry<String, ImageUrlInfo>> entries = mapImages.entrySet();
        Iterator<Map.Entry<String, ImageUrlInfo>> iterator = entries.iterator();

        List<DocImageData> lstData = new ArrayList<>();
        while (iterator.hasNext())
        {
            Map.Entry<String, ImageUrlInfo> next = iterator.next();
            ImageUrlInfo value = next.getValue();
            if(value != null && value.getImageUrl() != null)
            {
                lstData.add(new DocImageData(next.getKey(), value.getImageUrl(), value.isLink(), value.getDocumentId()));
            }
        }
        docAdapter.addAll(lstData);
        docAdapter.notifyDataSetChanged();

        if(lstData != null && !lstData.isEmpty() && ((FragmentActivity) getActivity()) != null) {
            adapter = new ImageViewerAdapter(((FragmentActivity) getActivity()).getSupportFragmentManager(), lstData, getActivity());
            mPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        else {
            mPager.setVisibility(View.GONE);
            grid.setVisibility(View.VISIBLE);
           // mPager.setAdapter(null);
        }

        if(mPager.getVisibility() == View.VISIBLE)
        {
            if(lstData != null && !lstData.isEmpty()) {
                mPager.setCurrentItem(0);
            }else {
                mPager.setVisibility(View.GONE);
                grid.setVisibility(View.VISIBLE);
            }
        }

        if(adapter != null) {
            adapter.notifyDataSetChanged();
        }

        setAsNoDoc(lstData);
    }

    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public String checkImageSize(String fileNamePath) {
        try {
            File f = new File(fileNamePath);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            is.close();

            int kb = size / 1024;

            if (kb > 256) {
                File file = Utility.createFile();
                Bitmap b = ImageResizer.resize(new File(fileNamePath), 256, 256);
                FileOutputStream fout = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                fout.flush();
                b.recycle();
                b = null;
                fout.close();
                return file.getAbsolutePath();
            } else {
                return fileNamePath;
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, getActivity());
            return null;
        }
    }
}
