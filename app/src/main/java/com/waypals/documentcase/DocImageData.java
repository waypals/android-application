package com.waypals.documentcase;

import java.io.Serializable;

/**
 * Created by surya on 18/1/16.
 */
public class DocImageData implements Serializable{

    private String imageUrl;
    private String title;
    private boolean isLink;
    private long documentId;

    public DocImageData(String title, String url, boolean isLink, long documentId)
    {
        this.title = title;
        this.imageUrl = url;
        this.isLink = isLink;
        this.documentId = documentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isLink() {
        return isLink;
    }

    public void setIsLink(boolean isLink) {
        this.isLink = isLink;
    }

    public long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }
}
