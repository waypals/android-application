package com.waypals.documentcase;

import java.util.HashMap;

/**
 * Created by surya on 19/1/16.
 */
public class VehicleUploadDocument {
    private Long vehicleId;
    private HashMap<String, String> imageHexString;
    private Integer Type;

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public HashMap<String, String> getImageHexString() {
        return imageHexString;
    }

    public void setImageHexString(HashMap<String, String> imageHexString) {
        this.imageHexString = imageHexString;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

}
