package com.waypals.documentcase;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedAdapter.FeedMethodEnum;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by surya on 25/1/16.
 */
public class DocumentImageAsyncDownloader extends AsyncTask<String, Integer, Bitmap> {

    private Context context;
    private String title;
    private PowerManager.WakeLock mWakeLock;
    private ProgressDialog progressDialog;
    private File filePath;

    public DocumentImageAsyncDownloader(Context context,  ProgressDialog progressDialog)
    {
        this.context = context;
        this.progressDialog = progressDialog;
    }

    @Override
    protected Bitmap doInBackground(String... voids) {

        String url = voids[0];
        title = voids[1];

        try {
            URL u = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            myBitmap = getResizedBitmap(myBitmap, 256, 256);

            return myBitmap;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(final Bitmap aVoid) {

        mWakeLock.release();
        progressDialog.dismiss();

        try {
            AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
            dialog.setTitle("Downloaded");
            dialog.setMessage("Do you want to see the document?");
            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(Utility.createTempImageFile(aVoid)), "image/*");
                        context.startActivity(intent);
                    } catch (Exception ex) {
                        ExceptionHandler.handleException(ex, (Activity)context);
                    }
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    return;
                }
            });
            dialog.setCancelable(false);
            dialog.create();
            dialog.show();
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }

//        Intent intent = new Intent(context, DocumentCase.class);
//        intent.putExtra("key", "value");
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//                context).setAutoCancel(true)
//                .setContentTitle(title)
//                .setContentIntent(pendingIntent)
//                .setSmallIcon(R.drawable.logo)
//                .setContentText(title);
//
//        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
//        bigPicStyle.bigPicture(aVoid);
//        bigPicStyle.setBigContentTitle(title);
//        mBuilder.setStyle(bigPicStyle);
//
//        NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);
//        mNotificationManager.notify((int) (new Date().getTime()), mBuilder.build());
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();
        progressDialog.show();
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }
}
