package com.waypals.documentcase;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.waypals.R;

public class DocumentCase extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_case);
    }

}
