package com.waypals.documentcase;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.io.File;
import java.net.URLEncoder;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DocumentViewerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DocumentViewerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DocumentViewerFragment extends Fragment implements AsyncFinishInterface{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static String TYPE = "type";
    private static String IMG_LINK = "img_link";
    private static String IS_LINK = "is_link";
    private static String DOC_ID = "doc_id";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String type;
    private String imgLink;
    private boolean isLink;
    private long docId;

    private ApplicationPreferences appPref;

    private OnFragmentInteractionListener mListener;
    private ServiceResponse serviceResponse;
    private ImageLoader mImageLoader;

    ProgressDialog mProgressDialog;
    DocumentImageAsyncDownloader documentImageAsyncDownloader;
    private static Context context;

    public DocumentViewerFragment() {
        // Required empty public constructor
        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Download");
        //mProgressDialog.setIndeterminate(true);
        //mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                if (documentImageAsyncDownloader != null) {
                    documentImageAsyncDownloader.cancel(true);
                }
            }
        });

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DocumentViewerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DocumentViewerFragment newInstance(int position, String type, String imgLink, boolean isLink, long docId, Context context) {

        DocumentViewerFragment.context = context;
        DocumentViewerFragment fragment = new DocumentViewerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        args.putString(TYPE, type);
        args.putString(IMG_LINK, imgLink);
        args.putBoolean(IS_LINK, isLink);
        args.putLong(DOC_ID, docId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            type = getArguments().getString(TYPE);
            imgLink = getArguments().getString(IMG_LINK);
            isLink = getArguments().getBoolean(IS_LINK);
            docId = getArguments().getLong(DOC_ID);
        }

        appPref = new ApplicationPreferences(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_document_viewer, container, false);

        TextView textView = (TextView) view.findViewById(R.id.text_title);
        textView.setText(type);

        ImageView imgShare = (ImageView) view.findViewById(R.id.wallet_doc_share);

        final NetworkImageView imageView = (NetworkImageView) view.findViewById(R.id.document_image);

        ImageView imageViewNormal = (ImageView) view.findViewById(R.id.document_image_normal);

        imageView.setDrawingCacheEnabled(true);
        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);

                if (!isLink) {
                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(imgLink)));
                    shareIntent.setType("image/jpeg");
                } else {
                    // shareIntent.putExtra(Intent.EXTRA_TEXT, imgLink);
                    // shareIntent.setType("text/plain");
                    File file = Utility.createTempImageFile(imageView.getDrawingCache());
                    if (file != null) {
                        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                        shareIntent.setType("image/jpeg");
                    } else {
                        shareIntent.putExtra(Intent.EXTRA_TEXT, imgLink);
                        shareIntent.setType("text");
                    }
                }

                startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));
            }
        });

        ImageView imgDownload = (ImageView) view.findViewById(R.id.wallet_doc_download);
        imgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isLink) {
                    mProgressDialog.show();
                    documentImageAsyncDownloader = new DocumentImageAsyncDownloader(getContext(), mProgressDialog);
                    documentImageAsyncDownloader.execute(imgLink, type);
                }else {
                    try {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                        dialog.setTitle("Downloaded");
                        dialog.setMessage("Do you want to see the document?");
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    Intent intent = new Intent();
                                    intent.setAction(Intent.ACTION_VIEW);
                                    intent.setDataAndType(Uri.fromFile(new File(imgLink)), "image/*");
                                    context.startActivity(intent);
                                } catch (Exception ex) {
                                    ExceptionHandler.handleException(ex, (Activity)context);
                                }
                            }
                        });
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        dialog.setCancelable(false);
                        dialog.create();
                        dialog.show();
                    } catch (Throwable e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            }
        });

        ImageView imgDelete = (ImageView) view.findViewById(R.id.wallet_doc_delete);

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDoc(docId);
            }
        });

        if(isLink) {
            RequestQueue queue = VolleySingleton.getInstance(getContext().getApplicationContext()).getRequestQueue();
            mImageLoader = VolleySingleton.getInstance(getContext().getApplicationContext()).getImageLoader();
            imageView.setImageUrl(imgLink, mImageLoader);
            imageView.setVisibility(View.VISIBLE);
            imageViewNormal.setVisibility(View.GONE);
        }else {
            //imageView.setImageUrl(imgLink, new ImageLoader(Volley.newRequestQueue(getContext().getApplicationContext()), new ImageCache()));
//            imageView.setImageBitmap(Utility.decodeFile(new File(imgLink)));
         //   imageView.setImageBitmap(Utility.decodeFile(new File(imgLink)));
            imageViewNormal.setImageURI(Uri.fromFile(new File(imgLink)));
            imageViewNormal.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void deleteDoc(long id)
    {
        try {
            GenericAsyncService service = new GenericAsyncService(appPref, getActivity(),
                    CH_Constant.DELETE_DOCUMENTS + URLEncoder.encode(""+id,"UTF-8"), null, null, null, this, true);
            service.execute();
        }catch (Exception ex)
        {
            ExceptionHandler.handleException(ex, getActivity());
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        Gson gson = new Gson();

        if (response != null) {
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
            Log.d("Response", response.toString());
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse()))) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(getActivity(),
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(getActivity(), Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(getActivity(), "Server not responding, please try after some time!!");
            }

            return;
        }

        if (serviceResponse != null) {
            if (serviceResponse.getResponse().equals("SUCCESS")) {
                Intent docDeleted = new Intent("DOC_DELETED");
                docDeleted.putExtra("TYPE", type);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(docDeleted);

            } else {
                Utility.makeNewToast(getActivity(), "Could not delete the document");
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
