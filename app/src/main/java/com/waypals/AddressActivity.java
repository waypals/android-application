package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrus.sdk.Environment;
import com.citrus.sdk.ui.utils.CitrusFlowManager;
import com.google.gson.Gson;
import com.waypals.fragments.AddressDetailFragment;
import com.waypals.objects.Address;
import com.waypals.objects.OrderPaymentRequest;
import com.waypals.objects.Phone;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddressActivity extends FragmentActivity implements AsyncFinishInterface, AddressDetailFragment.OnAddressDetailFragmentInteractionListener{

    AddressDetailFragment addressDetailFragment;
    LinearLayout lytContinue;
    ApplicationPreferences appPref;
    private float amount;
    private ServiceResponse serviceResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        ImageView back = (ImageView) findViewById(R.id.left_btn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView text = (TextView) findViewById(R.id.nav_title_txt);
        text.setText("Address Detail");

        appPref = new ApplicationPreferences(this);

        Intent intent = getIntent();

        amount = intent.getFloatExtra("Amount", 0);
        final String[] pCodes = intent.getStringArrayExtra("pcodes");
        final String couponCode = intent.getStringExtra("couponCode");

        addressDetailFragment = new AddressDetailFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, addressDetailFragment);
        transaction.commit();

        lytContinue = (LinearLayout) findViewById(R.id.continue_layout);

        lytContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addressDetailFragment != null) {

                    if (addressDetailFragment.isValid()) {
                        Address address = addressDetailFragment.getAddress();
                        Phone phone = addressDetailFragment.getPhone();
                        String email = addressDetailFragment.getEmail();
                        String name = addressDetailFragment.getName();

                        OrderPaymentRequest orderPaymentRequest = new OrderPaymentRequest();
                        orderPaymentRequest.setAddress(address);
                        orderPaymentRequest.setName(name);

                        if (couponCode != null) {
                            orderPaymentRequest.setCouponUsed(couponCode);
                        }
                        if (pCodes != null){
                            List<String> lstCodes = new ArrayList<String>();
                            Collections.addAll(lstCodes, pCodes);

                            orderPaymentRequest.setProductCodes(lstCodes);
                        }

                        orderPaymentRequest.setTotalAmout(amount);
                        orderPaymentRequest.setEmail(email);
                        orderPaymentRequest.setPhoneNumber(phone);

                        Gson gson = new Gson();
                        String request = gson.toJson(orderPaymentRequest);

                        initiatePayment(request);
                    }
                }
            }
        });

        initPayment();
    }

    private void initPayment() {
        CitrusFlowManager.initCitrusConfig(CH_Constant.SIGN_UP_ID,
                CH_Constant.SIGN_UP_SECRET_KEY, CH_Constant.SIGN_IN_ID,
                CH_Constant.SIGN_IN_SECREY_KEY,
                getResources().getColor(R.color.white), this,
                Environment.PRODUCTION, CH_Constant.VANITY, CH_Constant.BILL_GENERATOR_URL, CH_Constant.RETURN_URL);
    }

    private void makePayment(String tnxId) {
        if (tnxId != null) {
            String emailId = "";
            String phoneNum = "";

            if (addressDetailFragment.isValid()) {
                emailId = addressDetailFragment.getEmail();
                phoneNum = addressDetailFragment.getPhone().getPhoneNo();
            }

            String token = "";
            CitrusFlowManager.startShoppingFlow(AddressActivity.this,
                    emailId, phoneNum, ""+amount, tnxId, token);
        }
    }

    private void initiatePayment(String input){

            GenericAsyncService getTnxIdService = new GenericAsyncService(appPref, this, CH_Constant.SERVER+CH_Constant.ITEM_ORDER_STORE, "", input,
                    "", this, true);
            getTnxIdService.execute();
    }

    @Override
    public void onAddressDetailFragmentInteraction(Address uri) {

    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (!Utility.isStringNullEmpty(response)) {
            Gson gson = new Gson();
            Log.d("Raw Response", response);
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (serviceResponse != null) {
            if (CH_Constant.SUCCESS.equalsIgnoreCase(serviceResponse.getResponse())) {
                makePayment(serviceResponse.getRefId());

            } else {
                Utility.makeNewToast(this, "Payment failed, please try again");
            }
        }
    }
}
