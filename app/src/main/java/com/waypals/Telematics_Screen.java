package com.waypals;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.Json.ObdJson;
import com.waypals.response.ObdResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

public class Telematics_Screen extends NavigationDrawer implements AsyncFinishInterface {
    static Typeface tf;
    ImageView back_btn;
    TextView title;
    VehicleServices vs = new VehicleServices();
    String tag = "Telematics";
    ContentAdapter fd;
    ObdJson obd;
    Integer state;
    ObdResponse obdResponse;
    private ApplicationPreferences appPref;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.telematics_screen_new);

        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        appPref = new ApplicationPreferences(this);
        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setTypeface(tf);
        title.setText("TELEMATICS");

        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setupContentListener();

        try {
            vs.getTelematicsData(appPref, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void finish(String response, String method) throws Exception {
        Log.d(tag, "Response: " + response);
        if (method.endsWith("obd")) {

            Gson gson = new Gson();
            obdResponse = gson.fromJson(response, ObdResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if ((obdResponse != null && "ERROR".equalsIgnoreCase(
                obdResponse.getResponse())))  {
            String errorMessage = obdResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, "Data is not available");
            }

            return;
        }

        if ("SUCCESS".equalsIgnoreCase(obdResponse.getResponse())) {
            udpateContentListener();
        }
    }


    public void setupContentListener() {
        System.out.println("telematics list ");
        ListView rightList = (ListView) this.findViewById(R.id.telematicsList);
        fd = new ContentAdapter(context, R.layout.telematics_content_row, setupContent());
        rightList.setAdapter(fd);
    }

    public void udpateContentListener() {
        Log.d(tag, "updating telemetics data");
        ListView rightList = (ListView) this.findViewById(R.id.telematicsList);
        fd = new ContentAdapter(context, R.layout.telematics_content_row, updateContent());
        fd.notifyDataSetChanged();
        rightList.setAdapter(fd);

    }

    public Content[] setupContent() {
        Log.i(tag, "creating list of items for right drawers");

		/* setup drawer list */
        Content fr[] = new Content[]{
                new Content(R.drawable.engin_state_img, "ENGINE STATE", "NA"),
                new Content(R.drawable.fuel_level_img, "FUEL LEVEL", "NA"),
                new Content(R.drawable.speed_img, "SPEED", "NA"),
                new Content(R.drawable.battery_img, "BATTERY SLEEP VOL", "NA"),
                new Content(R.drawable.battery_img, "BATTERY RUNNING VOL", "NA"),
                new Content(R.drawable.rpm_img, "RPM", "NA"),
                new Content(R.drawable.engin_load_img, "ENGINE LOAD", "NA")
        };
        return fr;

    }

    public Content[] updateContent() {
        Log.i(tag, "creating list of items for right drawers");

        if (obdResponse != null && obdResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
            String enginestate = "NA";
            if (obdResponse.getState() != null) {
                if (obdResponse.getState().toString().equals("PARKED")) {
                    state = 0;
                } else {
                    state = 1;
                }
                enginestate = state == 0 ? "OFF" : "ON";
            } else {
                enginestate = "NA";
            }

            String fuelValue = "NA";
            String rpmValue = "NA";
            String speedValue = "NA";
            String obdSleepVol = "NA";
            String obdRunningVol = "NA";
            String engineLoad = "NA";
            if(obdResponse.getObd() != null)
            {
                if(obdResponse.getObd().getObdFuelLevel() != null) {
                    fuelValue = obdResponse.getObd().getObdFuelLevel().toString() + " %";
                }

                if(obdResponse.getObd().getRpm() != null && obdResponse.getObd().getRpm().getRpm() != null)
                {
                    rpmValue = obdResponse.getObd().getRpm().getRpm().toString();
                }

                if(obdResponse.getObd().getVehicleSpeed() != null && obdResponse.getObd().getVehicleSpeed().getSpeed() != null)
                {
                    speedValue = obdResponse.getObd().getVehicleSpeed().getSpeed().toString();
                }

                if(obdResponse.getObd().getObdSleepVoltage() != null)
                {
                    obdSleepVol = ""+obdResponse.getObd().getObdSleepVoltage()+" V";
                }

                if(obdResponse.getObd().getObdRunningVoltage() != null)
                {
                    obdRunningVol = ""+obdResponse.getObd().getObdRunningVoltage()+" V";
                }

                if(obdResponse.getObd().getObdEngineLoad() != null)
                {
                    engineLoad = obdResponse.getObd().getObdEngineLoad().toString() + " %";
                }
            }

            /* setup drawer list */
            Content fr[] = new Content[]{
                    new Content(R.drawable.engin_state_img, "ENGINE STATE", enginestate),
                    new Content(R.drawable.fuel_level_img, "FUEL LEVEL", fuelValue),
                    new Content(R.drawable.speed_img, "SPEED", speedValue),
                    new Content(R.drawable.battery_img, "BATTERY SLEEP VOL", obdSleepVol),
                    new Content(R.drawable.battery_img, "BATTERY RUNNING VOL", obdRunningVol),
                    new Content(R.drawable.rpm_img, "RPM", rpmValue),
                    new Content(R.drawable.engin_load_img, "ENGINE LOAD", engineLoad)
            };
            return fr;
        } else {
            return null;
        }
    }

    @Override
    protected void setImmobilizer() {

    }


    class Content {
        public int icon;
        public String title;
        public String value;


        public Content(int icon, String title, String value) {
            super();
            this.icon = icon;
            this.title = title;
            this.value = value;
        }
    }

    class ContentAdapter extends ArrayAdapter<Content> {
        Context context;
        int layoutResourceId;
        Content data[] = null;
        Typeface tf;

        public ContentAdapter(Context context, int resource,
                              Content[] objects) {
            super(context, resource, objects);
            this.layoutResourceId = resource;
            this.context = context;
            this.data = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ContentsHolder holder;
            LayoutInflater inflater = ((NavigationDrawer) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);


            holder = new ContentsHolder();
            holder.imgIcon = (ImageView) row
                    .findViewById(R.id.telematics_itemIcon);
            holder.txtTitle = (TextView) row
                    .findViewById(R.id.telematics_itemText);
            holder.itemValue = (TextView) row.findViewById(R.id.telematics_itemValue);


            Content drawerContent = data[position];

            holder.txtTitle.setTypeface(tf);
            holder.txtTitle.setTextSize(getDefaultTextSize());
            holder.txtTitle.setText(drawerContent.title);


            holder.imgIcon.setImageResource(drawerContent.icon);

            holder.itemValue.setTypeface(tf);
            holder.itemValue.setTextSize(getDefaultTextSize());
            holder.itemValue.setText(drawerContent.value);

            row.setTag(holder);
            return row;

        }

        class ContentsHolder {
            ImageView imgIcon;
            TextView txtTitle;
            TextView itemValue;
        }
    }
}