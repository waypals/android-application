package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.analytics.ScoreActivity;
import com.waypals.exception.ExceptionHandler;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.signup.SignUp;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONObject;

/**
 * Created by shantanu on 30/1/15.
 */
public class ForgetPassword extends Activity implements AsyncFinishInterface {

    private EditText emailText;
    private TextView backToLogin, tvNewUser;
    private Button reset;
    private Activity activity;
    private ApplicationPreferences preferences;
    private AsyncFinishInterface asyncFinishInterface;
    private String tag = "Forget password";
    private ServiceResponse serviceResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password);
        activity = this;
        preferences = new ApplicationPreferences(this);
        asyncFinishInterface = this;

        reset = (Button) findViewById(R.id.login_btn);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailText = (EditText) findViewById(R.id.username_et);
                String email = emailText.getText().toString();

                if (validateEmail(email)) {
                    try {
                        JSONObject input = new JSONObject();
                        input.put("emailId", email);

                        GenericAsyncService service = new GenericAsyncService(preferences, activity, CH_Constant.FORGET_PASSWORD, "FORGET", input.toString(), tag, asyncFinishInterface, true);
                        service.execute();
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            }
        });


        TextView termService = (TextView) findViewById(R.id.term_service);

        if(termService != null)
        {
            termService.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(ForgetPassword.this, AboutUs.class);
                    intent.putExtra(AboutUs.ABOUT_US_TITLE, "Terms & Conditions");
                    intent.putExtra(AboutUs.ABOUT_US_TEXT, getString(R.string.terms_conditions));
                    startActivity(intent);
                }
            });
        }

        backToLogin = (TextView) findViewById(R.id.loginText);
        backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, Login_Screen.class);
                startActivity(i);
                finish();
            }
        });

        tvNewUser = (TextView) findViewById(R.id.title_newUSer);
        tvNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Utility.openUrl(activity, getString(R.string.register_user_url));
                Intent signUpIntent = new Intent(ForgetPassword.this, SignUp.class);
                startActivity(signUpIntent);
            }
        });

    }


    public boolean validateEmail(String mail) {
        boolean value = false;
        if (mail == null || mail.equals("")) {
            Utility.makeNewToast(activity, "Please enter email Id ");
            value = false;
        } else if (!mail.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$")) {
            Utility.makeNewToast(activity, "Please enter valid email Id ");
            value = false;
        } else {
            value = true;
        }
        return value;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (method.equals("FORGET")) {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
            emailText = (EditText) findViewById(R.id.username_et);
            emailText.setText("");
            Utility.makeNewToast(activity, "Email to reset your password is sent !");
        } else {
            Utility.makeNewToast(activity, "Could not reset your password at this time");
        }
    }
}
