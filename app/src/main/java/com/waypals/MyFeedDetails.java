package com.waypals;

import java.util.ArrayList;

public class MyFeedDetails {

    private String feedId;
    private String owner;
    private String userId;
    private String sharedBy;
    private String sharedByID;
    private String type;
    private String description;
    private String timeStamp;
    private ArrayList<CommentDetails> commentDetailsList = new ArrayList<CommentDetails>();


    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public ArrayList<CommentDetails> getCommentDetailsList() {
        return commentDetailsList;
    }

    public void setCommentDetailsList(ArrayList<CommentDetails> commentDetailsList) {
        this.commentDetailsList = commentDetailsList;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(String sharedBy) {
        this.sharedBy = sharedBy;
    }

    public String getSharedByID() {
        return sharedByID;
    }

    public void setSharedByID(String sharedByID) {
        this.sharedByID = sharedByID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

}
