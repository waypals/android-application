package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.waypals.fragments.GeofenceFragment;
import com.waypals.fragments.dummy.DummyContent;
import com.waypals.gson.vo.GeoFenceVO;
import com.waypals.gson.vo.ListGeoFenceResponse;
import com.waypals.gson.vo.VehicleStateResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.Utility;

import java.util.List;

public class GeofenceActivity extends FragmentActivity implements GeofenceFragment.OnListFragmentInteractionListener {

    ApplicationPreferences appPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geofence);
        appPref = new ApplicationPreferences(this);

        ImageView back = (ImageView) findViewById(R.id.left_btn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView addGeofence = (ImageView) findViewById(R.id.right_btn);
        addGeofence.setVisibility(View.VISIBLE);
        addGeofence.setImageResource(R.drawable.addicon);

        addGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(GeofenceActivity.this, Add_Geofence.class);
                    startActivity(intent);
            }
        });


        TextView text = (TextView) findViewById(R.id.nav_title_txt);
        text.setText("Geofence");
    }

    @Override
    public void onListFragmentInteraction(GeoFenceVO item) {
        Intent intent = new Intent(this, ShowGeoFenceOnMap.class);
        intent.putExtra("ID", item.getId());
        startActivity(intent);
    }

}
