package com.waypals.request;

import java.io.Serializable;

public class VehicleSpeedLimit implements Serializable {
    Long vehicleId;
    SpeedLimit speedLimit;

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public SpeedLimit getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(SpeedLimit speedLimit) {
        this.speedLimit = speedLimit;
    }
}
