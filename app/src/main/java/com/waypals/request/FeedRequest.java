package com.waypals.request;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Content;
import com.waypals.feedItems.Feed;
import com.waypals.feedItems.FeedUsers;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.feedItems.Friend;
import com.waypals.feedItems.LocationFeedContent;
import com.waypals.feedItems.Name;
import com.waypals.feedItems.SharedBy;
import com.waypals.feedItems.SharedWith;
import com.waypals.utils.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class FeedRequest {

    FeedWrapper format;


    public String createNewFeed(String text, HashMap selectedImages) {
        Feed feed = new Feed();

        feed.setType("IMAGE");

        feed.setTimeStamp(Utility.getCurrentUtcTime());

        Content content = new Content();
        content.setDescription(text);
        // feed.setContent(content);

        HashMap<String, String> img = new HashMap<String, String>();
        if (selectedImages != null) {
            Set<String> keys = selectedImages.keySet();
            for (String key : keys) {
                img.put(key, selectedImages.get(key).toString());
            }
        } else {
            //img=null;
        }


        FeedUsers[] feedUsers = new FeedUsers[1];
        feedUsers[0] = new FeedUsers();
        SharedBy sharedBy = new SharedBy();
        Name name = new Name();
        sharedBy.setName(name);

        SharedWith[] sharedWiths = new SharedWith[1];
        sharedWiths[0] = new SharedWith();
        feedUsers[0].setSharedWith(sharedWiths);
        feedUsers[0].setSharedBy(sharedBy);

//        feed.setFeedUsers(feedUsers);


        feed.setFeedType("POST");
        format = new FeedWrapper();
        format.setFeed(feed);
        format.setDescription(text);
        format.setImageHexString(img);
        //format.setJourneyFeedContent(new JourneyFeedContent());
        format.setLocationFeedContent(new LocationFeedContent());

        Gson gson = new Gson();
        String request = gson.toJson(format);

        return request;
    }

    public String shareFeed(Long feedId, ArrayList<Friend> selectedFriends) {
        ShareFeedFormat shareFeedFormat = new ShareFeedFormat();

        FeedUsers feedUsers = new FeedUsers();

        SharedWith[] sharedWith = new SharedWith[selectedFriends.size()];

        for (int i = 0; i < selectedFriends.size(); i++) {
            Friend f = selectedFriends.get(i);
            sharedWith[i] = new SharedWith();
            sharedWith[i].setEmail(f.getEmail());
            sharedWith[i].setUserId(f.getUserId());
            Name name = new Name();
            name.setFirstName(f.getFirstName());
            name.setLastName(f.getLastName());
            sharedWith[i].setName(name);
        }

        feedUsers.setSharedWith(sharedWith);

        shareFeedFormat.setFeedId(feedId);
        shareFeedFormat.setFeedUsers(feedUsers);
        try {
            Gson gson = new Gson();
            String request = gson.toJson(shareFeedFormat);


            return request;
        } catch (Exception e) {
            ExceptionHandler.handleException(e);
            return null;
        }

    }


    public String createNewMessage(String text, Long[] friendIds, HashMap selectedImages) {
        Feed feed = new Feed();

        feed.setType("IMAGE");

        Content content = new Content();
        content.setDescription(text);
        // feed.setContent(content);

        HashMap<String, String> img = new HashMap<String, String>();
        if (selectedImages != null) {
            Set<String> keys = selectedImages.keySet();
            for (String key : keys) {
                img.put(key, selectedImages.get(key).toString());
            }
        }

        FeedUsers[] feedUsers = new FeedUsers[1];
        feedUsers[0] = new FeedUsers();
        SharedBy sharedBy = new SharedBy();
        Name name = new Name();
        sharedBy.setName(name);

        SharedWith[] sharedWiths = new SharedWith[1];
        sharedWiths[0] = new SharedWith();
        feedUsers[0].setSharedWith(sharedWiths);
        feedUsers[0].setSharedBy(sharedBy);

//        feed.setFeedUsers(feedUsers);


        feed.setFeedType("POST");
        format = new FeedWrapper();
        format.setFriendIds(friendIds);
        format.setFeed(feed);
        format.setDescription(text);
        format.setImageHexString(img);
      //  format.setJourneyFeedContent(new JourneyFeedContent());
        format.setLocationFeedContent(new LocationFeedContent());

        Gson gson = new Gson();
        String request = gson.toJson(format);

        return request;
    }

    class ShareFeedFormat {
        Long feedId;
        FeedUsers feedUsers;

        public Long getFeedId() {
            return feedId;
        }

        public void setFeedId(Long feedId) {
            this.feedId = feedId;
        }

        public FeedUsers getFeedUsers() {
            return feedUsers;
        }

        public void setFeedUsers(FeedUsers feedUsers) {
            this.feedUsers = feedUsers;
        }
    }

}
