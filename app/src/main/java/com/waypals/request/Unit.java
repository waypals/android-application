package com.waypals.request;

import java.io.Serializable;

public class Unit implements Serializable {
    String unit;
    Float speed;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }
}