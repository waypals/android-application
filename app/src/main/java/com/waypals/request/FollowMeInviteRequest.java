package com.waypals.request;

import java.util.Set;

/**
 * Created by shantanu on 14/1/15.
 */
public class FollowMeInviteRequest {
    Long vehicleId;
    Set<Long> followWith;
    TimeRange range;

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Set<Long> getFollowWith() {
        return followWith;
    }

    public void setFollowWith(Set<Long> followWith) {
        this.followWith = followWith;
    }

    public TimeRange getRange() {
        return range;
    }

    public void setRange(TimeRange range) {
        this.range = range;
    }
}
