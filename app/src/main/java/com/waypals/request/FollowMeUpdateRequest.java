package com.waypals.request;

/**
 * Created by shantanu on 4/2/15.
 */
public class FollowMeUpdateRequest {
    private Long userId;
    private String token;
    private TimeRange timeRange;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TimeRange getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
    }
}
