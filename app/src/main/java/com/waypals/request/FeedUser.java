package com.waypals.request;

public class FeedUser {

    private Name sharedWith;

    private String emailId;

    private String userId;

    public Name getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(Name sharedWith) {
        this.sharedWith = sharedWith;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
