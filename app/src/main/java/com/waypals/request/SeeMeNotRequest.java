package com.waypals.request;

/**
 * Created by shantanu on 14/1/15.
 */
public class SeeMeNotRequest {

    String vehicleId;
    TimeRange range;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public TimeRange getRange() {
        return range;
    }

    public void setRange(TimeRange range) {
        this.range = range;
    }

}
