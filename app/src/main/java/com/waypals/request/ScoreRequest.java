package com.waypals.request;

import java.io.Serializable;

/**
 * Created by surya on 14/02/2017.
 */
public class ScoreRequest implements Serializable{

    String startTime;
    String endTime;
    String vehicleId;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }
}
