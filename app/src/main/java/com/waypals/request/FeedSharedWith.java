package com.waypals.request;

public class FeedSharedWith {

    private FeedUser[] sharedWith;

    private String caption;

    public FeedUser[] getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(FeedUser[] sharedWith) {
        this.sharedWith = sharedWith;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }


}
