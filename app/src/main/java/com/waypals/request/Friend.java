package com.waypals.request;

public class Friend {

    private String friendId;

    private String friendCollectionId;

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getFriendCollectionId() {
        return friendCollectionId;
    }

    public void setFriendCollectionId(String friendCollectionId) {
        this.friendCollectionId = friendCollectionId;
    }


}
