package com.waypals.request;

/**
 * Created by shantanu on 14/1/15.
 */
public class TimeRange {
    String startTime;
    String endTime;

    public String getStartTime() {
        return this.startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return this.endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
