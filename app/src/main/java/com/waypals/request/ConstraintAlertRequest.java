package com.waypals.request;

import com.waypals.gson.vo.ConstraintAlertVO;

public class ConstraintAlertRequest {

    ConstraintAlertVO[] constraintAlertVO;

    public ConstraintAlertVO[] getConstraintAlertVO() {
        return constraintAlertVO;
    }

    public void setConstraintAlertVO(ConstraintAlertVO[] constraintAlertVO) {
        this.constraintAlertVO = constraintAlertVO;
    }


}
