package com.waypals.request;

public class Comment {

    private FeedUser commentedBy;

    private String comment;

    public FeedUser getCommentedBy() {
        return commentedBy;
    }

    public void setCommentedBy(FeedUser commentedBy) {
        this.commentedBy = commentedBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
