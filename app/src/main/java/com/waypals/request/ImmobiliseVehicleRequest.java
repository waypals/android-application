package com.waypals.request;

/**
 * Created by surya on 20/09/2017.
 */
public class ImmobiliseVehicleRequest {

    private String vehicleId;
    private Boolean immobilise;

    public Boolean getImmobilise() {
        return immobilise;
    }

    public void setImmobilise(Boolean immobilise) {
        this.immobilise = immobilise;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

}
