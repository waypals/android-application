package com.waypals.request;

import java.io.Serializable;

public class SettingsUpdateRequest implements Serializable {

    private Long vehicleId;

    private Long enableParkingFenceVehicleId;
    private Long disableParkingFenceVehicleId;
    private Notification notificationType;
    private VehicleSpeedLimit vehicleSpeedLimit;

    public String lastServiceDate;
    public String pollutionDueDate;
    public String policyDueDate;
    public String licenceExpDate;

    public Long getDisableParkingFenceVehicleId() {
        return disableParkingFenceVehicleId;
    }

    public void setDisableParkingFenceVehicleId(Long disableParkingFenceVehicleId) {
        this.disableParkingFenceVehicleId = disableParkingFenceVehicleId;
    }

    public String getLastServiceDate() {
        return lastServiceDate;
    }

    public void setLastServiceDate(String lastServiceDate) {
        this.lastServiceDate = lastServiceDate;
    }

    public String getPollutionDueDate() {
        return pollutionDueDate;
    }

    public void setPollutionDueDate(String pollutionDueDate) {
        this.pollutionDueDate = pollutionDueDate;
    }

    public String getPolicyDueDate() {
        return policyDueDate;
    }

    public void setPolicyDueDate(String policyDueDate) {
        this.policyDueDate = policyDueDate;
    }

    public String getLicenceExpDate() {
        return licenceExpDate;
    }

    public void setLicenceExpDate(String licenceExpDate) {
        this.licenceExpDate = licenceExpDate;
    }

    public Long getEnableParkingFenceVehicleId() {
        return enableParkingFenceVehicleId;
    }

    public void setEnableParkingFenceVehicleId(Long enableParkingFenceVehicleId) {
        this.enableParkingFenceVehicleId = enableParkingFenceVehicleId;
    }

    public Notification getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Notification notificationType) {
        this.notificationType = notificationType;
    }

    public VehicleSpeedLimit getVehicleSpeedLimit() {
        return vehicleSpeedLimit;
    }

    public void setVehicleSpeedLimit(VehicleSpeedLimit vehicleSpeedLimit) {
        this.vehicleSpeedLimit = vehicleSpeedLimit;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }
}
