package com.waypals.request;

import java.io.Serializable;

/**
 * Created by surya on 10/5/16.
 */
public class VehicleTripRequest implements Serializable {

    String vehicleId;
    String startTime;
    String endTime;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
