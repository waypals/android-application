package com.waypals.request;

import java.io.Serializable;

public class SpeedLimit implements Serializable {
    boolean isActivated;
    Unit min;
    Unit max;
    Unit allowedDeviation;
    Unit violatedSpeed;


    public Unit getAllowedDeviation() {
        return allowedDeviation;
    }

    public void setAllowedDeviation(Unit allowedDeviation) {
        this.allowedDeviation = allowedDeviation;
    }

    public Unit getViolatedSpeed() {
        return violatedSpeed;
    }

    public void setViolatedSpeed(Unit violatedSpeed) {
        this.violatedSpeed = violatedSpeed;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Unit getMin() {
        return min;
    }

    public void setMin(Unit min) {
        this.min = min;
    }

    public Unit getMax() {
        return max;
    }

    public void setMax(Unit max) {
        this.max = max;
    }


}