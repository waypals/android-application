package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waypals.adapters.ContactsCompletionView;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedAdapter.FeedAsyncInterface;
import com.waypals.feedAdapter.FeedMethodEnum;
import com.waypals.feedAdapter.MessageHelper;
import com.waypals.feedImpl.AppController;
import com.waypals.feedImpl.FeedImageView;
import com.waypals.feedItems.Comments;
import com.waypals.feedItems.Feed;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.feedItems.Friend;
import com.waypals.feedItems.ImageContent;
import com.waypals.feedItems.Images;
import com.waypals.feedItems.JourneyFeedContent;
import com.waypals.feedItems.RouteCoordinates;
import com.waypals.feedItems.WayPoints;
import com.waypals.gson.vo.FeedContentDeserializer;
import com.waypals.map.TrailActivity;
import com.waypals.request.FeedRequest;
import com.waypals.request.Pagination;
import com.waypals.response.AcceptedFriendResponse;
import com.waypals.response.FeedContentResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.services.MessageService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;
import com.wareninja.opensource.common.ObjectSerializer;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static com.wareninja.opensource.common.ObjectSerializer.serialize;

public class MyMessages extends MessageHelper implements FeedAsyncInterface, AsyncFinishInterface, TokenCompleteTextView.TokenListener {

    public static final int CAMERA_PICTURE = 2;
    public static final int GALLERY_PICTURE = 1;
    final static String fromBottom = "BOTTOM";
    final static String fromTop = "TOP";
    private static final int IMAGE_SELECT_ACTION = 100;
    ArrayList<Friend> selectedFriends = new ArrayList<Friend>();
    ContactsCompletionView completionView;
    Friend[] friends;
    EditText message;
    ArrayAdapter<Friend> adapter;
    AsyncFinishInterface asyncFinishInterface;
    FeedAsyncInterface feedAsyncInterface;
    FeedContentResponse feedContentResponse;
    List<FeedWrapper> feedWrappers;
    Button right_btn;
    ImageView back_btn;
    MessageAdapter feedAdapter;
    TextView title;
    Context context;
    Activity activity;
    EditText feedText;
    String fileNamePath = null;
    Boolean isImageAvailable = false;
    Button postFeed;
    AppController appController;
    TextView imageSelected;
    ServiceResponse serviceResponse;
    View footerView;
    String method;
    Typeface tf;
    ImageView cameraButton;
    FeedMethodEnum feedMethodEnum;
    String requestFrom;
    FeedWrapper selectedFordelete;
    TextView loadMore;
    SwipeRefreshLayout swipeView;
    HashMap<String, String> images = new HashMap<String, String>();
    HashMap<String, String> filedetails = new HashMap<String, String>();
    private ApplicationPreferences appPref;
    private String tag = "My Messages";
    private ListView listView;
    private int pageSize = 10;
    private int currentPage = 1;
    private boolean checkingFeeds = false;
    private int lostFeed = 0;
    private boolean showNotification = true;
    private Boolean onResumeRefresh = true;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_message);


        asyncFinishInterface = this;
        feedAsyncInterface = this;
        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipe);


        context = this;
        activity = this;
        listView = (ListView) findViewById(R.id.list_my_feed);
        appPref = new ApplicationPreferences(this);
        currentPage = appPref.getCurrentFeedPage();

        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setTypeface(tf);

        title.setText("MESSAGE(S)");
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        LayoutInflater layoutInflater = getLayoutInflater();
        footerView = layoutInflater.inflate(R.layout.footer_layout, null, false);

        loadMore = (TextView) footerView.findViewById(R.id.loadmore);
        loadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNewMessage(true, "BOTTOM");
            }
        });

        message = (EditText) findViewById(R.id.feedEditText);

         /*call for list of friends*/
        GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_FRIEND_LIST, "listacceptedfriends", "", tag, this, false);
        service.execute();

        cameraButton = (ImageView) findViewById(R.id.camera_button);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, Camera_Image_Container.class);
                i.putExtra("files", ObjectSerializer.serialize(images));
                setOnResumeRefresh(false);
                i.putExtra("filesDetails", ObjectSerializer.serialize(filedetails));
                startActivityForResult(i, IMAGE_SELECT_ACTION);
            }
        });


        feedAdapter = new MessageAdapter(appPref, activity);
        listView.setAdapter(feedAdapter);

        imageSelected = (TextView) findViewById(R.id.selectedImageName);
        postFeed = (Button) findViewById(R.id.postButton);
        postFeed.setText("SEND");
        postFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (selectedFriends != null && selectedFriends.size() > 0) {
                    try {
                        message = (EditText) findViewById(R.id.feedEditText);
                        String text = message.getText().toString();
                        if (text.isEmpty()) {
                            try {
                                Utility.makeNewToast(activity, getString(R.string.inp_alert_message));
                            } catch (Exception e) {
                                ExceptionHandler.handlerException(e, activity);
                            }
                            return;
                        }
                        Long[] users = new Long[selectedFriends.size()];
                        for (int i = 0; i < users.length; i++) {
                            users[i] = Long.parseLong(selectedFriends.get(i).getUserId());
                        }

                        if (images == null) {
                            images = new HashMap<String, String>();
                        }

                        if (filedetails != null) {
                            Set<String> keys = filedetails.keySet();
                            Log.d(tag, "File Details: " + filedetails.values());
                            for (String filename : keys) {
                                Log.d(tag, "File:" + filename + "|Image hex string : " + Utility.getImageToHex(activity, filedetails.get(filename).toString()));
                                images.put(filename, Utility.getImageToHex(activity, filedetails.get(filename).toString()));
                            }
                        } else {
                            images = null;
                        }


                        String input = new FeedRequest().createNewMessage(text, users, images);
                        Log.d(tag, "input :" + input);
                        if (input != null) {
//                            CreateNewMessage createNewMessage = new CreateNewMessage(appPref, activity, input, asyncFinishInterface);
//                            createNewMessage.execute();
                            selectedFriends.clear();
                            message.setText("");
                            completionView.clear();
//                            if(completionView != null && completionView.getText() != null) {
//                                if(!completionView.getText().toString().trim().isEmpty())
//                                {
//                                    completionView.setSelection(completionView.getText().length(), completionView.getText().length());
//                                }
//                            }
                            GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.CREATE_NEW_MESSAGE, FeedMethodEnum.NEW_MESSAGE.name(), input, tag, asyncFinishInterface, true);
                            service.execute();

                        }
                    } catch (Throwable e) {
                        ExceptionHandler.handleException(e, activity);
                    }
                } else {
                    try {
                        Utility.makeNewToast(activity, getString(R.string.inp_select_friend));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                }

            }
        });

//        cameraButton = (ImageView) findViewById(R.id.camera_button);
//        cameraButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectImage();
//            }
//        });


        // We first check for cached request
        appController = new AppController(context).getInstance();
        if (appController != null) {
            loadCachedMessage();
        } else {
            Log.d(tag, "getting null controller");
        }

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (listView != null && listView.getChildCount() > 0) {
                    boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    boolean enable = firstItemVisible && topOfFirstItemVisible;
                    swipeView.setEnabled(enable);
                }

                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    if (listView.getFooterViewsCount() == 0) {
                        Log.d(tag, "adding new footer view");
                        listView.addFooterView(footerView);
                    } else {
                        //Log.d(tag,"footer view already exists");
                    }
                    Log.d(tag, "I am on the bottom...");
                }

            }
        });

        swipeView.setColorSchemeColors(Color.parseColor("#406A8C"), Color.parseColor("#F213A1"), Color.parseColor("#13F264"), Color.parseColor("#1317F2"), Color.parseColor("#F2EE13"));
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cleanLoad(true);
                        swipeView.setRefreshing(false);

                    }
                }, 10000);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        onResumeRefresh = getOnResumeRefresh();
    }

    public Boolean getOnResumeRefresh() {
        return onResumeRefresh;
    }

    public void setOnResumeRefresh(Boolean onResumeRefresh) {
        this.onResumeRefresh = onResumeRefresh;
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (onResumeRefresh) {
                Log.d(tag, "Resuming...");
                cleanLoad(false);
            }
            //TODO: WorkAround: http://stackoverflow.com/questions/2214336/java-lang-indexoutofboundsexception-getchars-7-0-has-end-before-start
//            if(completionView != null && completionView.getText() != null) {
//                if(!completionView.getText().toString().trim().isEmpty())
//                {
//                   // completionView.setSelection(completionView.getText().toString().length());
//                }
//            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(tag, "distroying...");
    }

    @Override
    public void cleanLoad(boolean b) {
        appPref.clearMessages();
        appPref.saveLoadedMessages(0);
        checkNewMessage(b, fromTop);
    }

    @Override
    public void finish(String response, FeedMethodEnum method, String requestFrom, Boolean showNotification) throws Exception {
        try {
            this.feedMethodEnum = method;
            this.showNotification = showNotification;
            this.requestFrom = requestFrom;

            Gson gson = new GsonBuilder().registerTypeAdapter(Feed.class, new FeedContentDeserializer()).create();
            if (this.feedMethodEnum == FeedMethodEnum.MY_MESSAGES) {
                feedContentResponse = gson.fromJson(response, FeedContentResponse.class);
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        try {
            this.method = method;
            Gson gson = new GsonBuilder().registerTypeAdapter(Feed.class, new FeedContentDeserializer()).create();
            if (method.equals(FeedMethodEnum.NEW_MESSAGE.name())) {
                this.feedMethodEnum = FeedMethodEnum.NEW_MESSAGE;
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
            if (method.equalsIgnoreCase(FeedMethodEnum.DELETE_MESSAGE.name())) {
                this.feedMethodEnum = FeedMethodEnum.DELETE_MESSAGE;
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
            if (method.endsWith("listacceptedfriends")) {
                AcceptedFriendResponse friendResponse = gson.fromJson(response, AcceptedFriendResponse.class);

                if (friendResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    friends = friendResponse.getAcceptedFriendsResult();
                }
                Log.d(tag, friendResponse.toString());
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        try {

            if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) || (feedContentResponse != null && "ERROR".equalsIgnoreCase(
                    feedContentResponse.getResponse())))  {
                String errorMessage = null;
                if(serviceResponse != null) {
                    errorMessage = serviceResponse.getErrorCode();
                }else if(feedContentResponse != null)
                {
                    errorMessage = feedContentResponse.getErrorCode();
                }
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }

                return;
            }


            if (method != null && method.endsWith("listacceptedfriends")) {
                createFriendsList(friends);
            }
            if (this.feedMethodEnum == FeedMethodEnum.DELETE_MESSAGE) {
                if (serviceResponse != null && "SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                    Log.d(tag, "selected for delete: " + selectedFordelete);
                    appPref.removeMessageFromCache(selectedFordelete);
                    Utility.makeNewToast(activity, "Message deleted !");
                    feedAdapter.remove(selectedFordelete);
                    feedAdapter.notifyDataSetChanged();
                } else {
                    Utility.makeNewToast(activity, "Please try again later!");
                }
            }

            if (this.feedMethodEnum == FeedMethodEnum.MY_MESSAGES) {
                /*Now allow to check new feeds*/
                requestExists = false;

                if (feedContentResponse != null && "SUCCESS".equalsIgnoreCase(feedContentResponse.getResponse())) {
                    appPref.saveAvailableMessageCount(feedContentResponse.getTotalNumberOfRecords());
                    if (checkingFeeds) {
                        if (appPref.getAvailableMessageCount() == 0) {
                            try {
                                Utility.makeNewToast(activity, "You do not have any message.\nStart posting messages");
                                feedAdapter.clear();
                                feedAdapter.notifyDataSetChanged();
                                appPref.clearMessages();
                                appPref.saveLoadedMessages(0);
                                return;
                            } catch (Exception e) {
                                ExceptionHandler.handleException(e, activity);
                            }
                        }
                        Log.d(tag, "Checking Messages response");
                        if (appPref.getLoadedMessages() < appPref.getAvailableMessageCount()) {
                            getMessages();
                            return;
                        } else {
                            Log.d(tag, "Checking Messages response| No more data");
                            if (requestFrom.equals(fromBottom)) {
                                loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                                loadMore.setText(getString(R.string.inp_loading));
                                listView.removeFooterView(footerView);
                            }

                            if (this.showNotification) {
                                Utility.makeNewToast(activity, "Messages are up to date");
                                loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                                loadMore.setText(getString(R.string.inp_loadmore));
                                checkingFeeds = false;
                            }
                            return;
                        }
                    } else {

                        requestExists = false;
                        if (requestFrom.equals(fromBottom)) {
                            listView.removeFooterView(footerView);
                        }
                        if (appPref.getLoadingMessagesForFirstTime()) {
                            Log.d(tag, "messages loaded for first time");
                            appPref.saveLoadingMessagesForFirstTime(false);
                        }
                        /*depending upon request from call methods*/
                        if (this.requestFrom.equalsIgnoreCase(fromTop)) {
                            Log.d(tag, "loading New messages");
                            loadNewMessage();
                            if (listView.getFooterViewsCount() == 0) {
                                listView.addFooterView(footerView);
                            }
                        }
                        if (this.requestFrom.equalsIgnoreCase(fromBottom)) {
                            Log.d(tag, "loading Old Messages");
                            loadOldMessage();
                            if (this.requestFrom.equals(fromBottom)) {
                                loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                                loadMore.setText(getString(R.string.inp_loadmore));
                            }
                        }
                    }

                } else {
                    try {
                        Utility.makeNewToast(activity, CH_Constant.ERROR_Msg(feedContentResponse.getErrorCode()));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                }


            }
            if (this.feedMethodEnum == FeedMethodEnum.NEW_MESSAGE) {
                if (serviceResponse != null && "SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                    cleanLoad(false);
                    requestExists = false;
                    Utility.makeNewToast(this, "Message sent !");
                } else {
                    Utility.makeNewToast(this, "Server not responding. Please try again later");
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void onTokenAdded(Object token) {
        if (selectedFriends == null) {
            selectedFriends = new ArrayList<Friend>();
        }

        if (!selectedFriends.contains((Friend) token)) {
            selectedFriends.add((Friend) token);
        }
    }

    @Override
    public void onTokenRemoved(Object token) {
        if (selectedFriends != null && !selectedFriends.isEmpty() && selectedFriends.contains((Friend) token)) {
            selectedFriends.remove((Friend) token);
        }

    }


    @Override
    public Pagination getPaginator() {
        Pagination pg = new Pagination();
        if (appPref.getLoadingMessagesForFirstTime()) {
            pg.setPageNumber(String.valueOf(currentPage));
            pg.setPageSize(String.valueOf(pageSize));
            appPref.saveLoadingMessagesForFirstTime(false);
        } else {

            int available = appPref.getAvailableMessageCount();
            int loaded = appPref.getLoadedMessages();
            int remaining = available - loaded;
            lostFeed = pageSize - (loaded % pageSize);
            int pageNumber = loaded / pageSize;
            pageNumber++;

            if (lostFeed == 0) {
                ++pageNumber;
            }

            if (remaining < pageSize) {
                lostFeed = remaining;
                appPref.saveIsLostMessageAvailable(true);
            } else {
                appPref.saveIsLostMessageAvailable(false);
            }
            pg.setPageNumber(String.valueOf(pageNumber));
            pg.setPageSize(String.valueOf(pageSize));
            Log.d(tag, "available:" + available + "|loaded:" + loaded + "|remaining:" + remaining + "|pageNumber:" + pageNumber + "|lost:" + lostFeed);
        }
        return pg;
    }

    @Override
    public synchronized void checkNewMessage(boolean overrideShowNotification, String requestFrom) {
        try {
            if (!requestExists) {
                if (requestFrom.equals(fromBottom)) {
                    loadMore = (TextView) footerView.findViewById(R.id.loadmore);
                    loadMore.setText(getString(R.string.inp_loading));
                }

                Log.d(tag, "checkNewFeeds|checking for available feeds");
                checkingFeeds = true;
                showNotification = overrideShowNotification;
                Pagination pg = new Pagination();
                pg.setPageNumber("1");
                pg.setPageSize("1");
                requestExists = true;

                MessageService messageService = new MessageService(appPref, activity, pg, feedAsyncInterface, requestFrom, overrideShowNotification);
                messageService.execute();
            }

        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }
    }

    @Override
    public synchronized void loadNewMessage() {
        Log.d(tag, "**********************************NEW MESSAGE LOADING************************************************************");
        Log.d(tag, " before loaded feeds:" + appPref.getLoadedMessages() + "|available: " + appPref.getAvailableMessageCount());
        if (appPref.getLoadedMessages() < appPref.getAvailableMessageCount()) {
            feedAdapter.clear();
            feedAdapter.notifyDataSetChanged();

            int count = 0;
            for (FeedWrapper feedWrapper : feedContentResponse.getFeedResponseList()) {
                count = count + 1;
                feedAdapter.add(feedWrapper);
                appPref.addMessage(feedWrapper);
                Log.d("Msg", feedWrapper.toString() + " | count : " + count);
            }

            feedAdapter.notifyDataSetChanged();
            appPref.saveLoadedMessages(count + appPref.getLoadedMessages());

            if (this.showNotification) {
                try {
                    Utility.makeNewToast(activity, "Messages updated");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, activity);
                }
            }
        } else {
            if (this.showNotification) {
                try {
                    Utility.makeNewToast(activity, "Messages are up to date");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, activity);
                }
            }
        }
        requestExists = false;
        Log.d(tag, "after loaded feeds:" + appPref.getLoadedMessages() + "|availble: " + appPref.getAvailableMessageCount());
    }

    @Override
    public synchronized void loadOldMessage() {
        Log.d(tag, "**********************************OLD MESSAGES LOADING************************************************************");
        Log.d(tag, " before loaded MSGS:" + appPref.getLoadedMessages() + "|available: " + appPref.getAvailableMessageCount());
        if (appPref.getIsLostMessageAvailable()) {
            Log.d(tag, "Newly added msgs available of size: " + lostFeed);
            int count = 0;
            for (FeedWrapper feedWrapper : feedContentResponse.getFeedResponseList()) {
                feedAdapter.add(feedWrapper);
                appPref.addMessage(feedWrapper);
                Log.d("Msgs", feedWrapper.toString() + " | count : " + count);
                if (count == lostFeed) {
                    break;
                }
                ++count;
            }
            feedAdapter.notifyDataSetChanged();
            appPref.saveLoadedMessages(count + appPref.getLoadedMessages());
            lostFeed = 0;
            appPref.saveIsLostMessageAvailable(false);
        } else if (appPref.getLoadedMessages() < appPref.getAvailableMessageCount()) {
            int count = 0;
            for (FeedWrapper feedWrapper : feedContentResponse.getFeedResponseList()) {
                count = count + 1;
                feedAdapter.add(feedWrapper);
                appPref.addMessage(feedWrapper);
                Log.d("Msgs", feedWrapper.toString() + " | count : " + count);

            }
            feedAdapter.notifyDataSetChanged();
            appPref.saveLoadedMessages(count + appPref.getLoadedMessages());
            if (showNotification) {
                try {
                    Utility.makeNewToast(activity, "Messages updated");
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, activity);
                }
            }
        } else {
            try {
                Utility.makeNewToast(activity, "No more data available !");
            } catch (Exception e) {
                ExceptionHandler.handleException(e, activity);
            }
        }
        listView.removeFooterView(footerView);
        requestExists = false;
        Log.d(tag, "after loaded messages:" + appPref.getLoadedMessages() + "|available: " + appPref.getAvailableMessageCount());
    }

    @Override
    public synchronized void getMessages() {
        try {
            Log.d(tag, "Checking msg response| Now gettings msg");
            checkingFeeds = false;
            requestExists = true;
            MessageService messageService = new MessageService(appPref, activity, getPaginator(), this, this.requestFrom, this.showNotification);
            messageService.execute();
            appPref.saveLastMessagedUpdateTime(System.currentTimeMillis());
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    @Override
    public void loadCachedMessage() {
        try {
            feedWrappers = appPref.getMessages();
            if (feedWrappers != null) {
                feedAdapter.clear();
                feedAdapter.notifyDataSetChanged();
                for (FeedWrapper f : feedWrappers) {
                    feedAdapter.add(f);
                    Log.d(tag, "loading.. " + f);
                }
                feedAdapter.notifyDataSetChanged();
            } else {
                checkNewMessage(false, fromTop);
            }
        } catch (Exception e) {
            ExceptionHandler.handleException(e, activity);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/" + "temp.jpg");

        // Save a file: path for use with ACTION_VIEW intents
        fileNamePath = image.getAbsolutePath();
        return image;
    }

    public void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MyMessages.this);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                imageSelected = (TextView) findViewById(R.id.selectedImageName);
                imageSelected.setText("");
                fileNamePath = null;
                Log.d(tag, "image selection canceled");
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_SELECT_ACTION) {
            if (resultCode == RESULT_OK) {
                Log.d(tag, "User added images");
                if (data != null) {
                    filedetails = (HashMap<String, String>) ObjectSerializer.deserialize(data.getStringExtra("filesDetails"));


                    Set<String> keys = filedetails.keySet();

                    for (String f : keys) {
                        Log.d(tag, "file: " + f + "|path: " + filedetails.get(f));
                    }


                    if (filedetails.size() > 0) {
                        imageSelected = (TextView) findViewById(R.id.selectedImageName);
                        imageSelected.setText("added images");
                    }
                    Log.d(tag, "User added images size: " + filedetails.size());
                }
            }
            if (resultCode == RESULT_CANCELED) {
                imageSelected = (TextView) findViewById(R.id.selectedImageName);
                imageSelected.setText("");
                images = null;
                filedetails = null;
            }
            setOnResumeRefresh(true);
        }
    }

    public void onPhotoTaken() {
        imageSelected = (TextView) findViewById(R.id.selectedImageName);
        try {
            Log.d("camera imagePath", fileNamePath);
            isImageAvailable = true;
            imageSelected.setText("image added");
        } catch (Exception e) {
            e.printStackTrace();
            imageSelected.setText("Could not capture image");
            Log.e("camera imagePath", "onactivity result");
        }

    }

    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            Uri _uri = data.getData();
            if (_uri != null) {
                fileNamePath = getPath(_uri);
                isImageAvailable = true;
                imageSelected = (TextView) findViewById(R.id.selectedImageName);
                imageSelected.setText("image added");
                Log.d(tag, "gallary image path: " + fileNamePath);
            }
        } catch (Exception e) {
            e.printStackTrace();
            imageSelected.setText("Could not select image");
            Log.e("imagePath", "onactivity result");
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    public void createFriendsList(Friend[] friends) {
        try {
            if (friends != null) {
                adapter = new FilteredArrayAdapter<Friend>(this, R.layout.friend_list_row, friends) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {

                            LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                            convertView = (View) l.inflate(R.layout.friend_list_row, parent, false);
                        }

                        Friend p = getItem(position);
                        ((TextView) convertView.findViewById(R.id.name)).setText(p.getFirstName() + " " + p.getLastName());
                        ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());

                        return convertView;
                    }

                    @Override
                    protected boolean keepObject(Friend obj, String mask) {
                        mask = mask.toLowerCase();
                        if (!selectedFriends.contains(obj)) {
                            return obj.getFirstName().toLowerCase().startsWith(mask) || obj.getLastName().toLowerCase().startsWith(mask) || obj.getEmail().toLowerCase().startsWith(mask);
                        } else
                            return false;
                    }
                };


                completionView = (ContactsCompletionView) findViewById(R.id.contacts);

                completionView.setDuplicateParentStateEnabled(false);
                completionView.setAdapter(adapter);
                completionView.setThreshold(1);
                completionView.setTokenListener(this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            } else {
                try {
                    Utility.makeNewToast(activity, getString(R.string.err_no_friends));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, activity);
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, activity);
        }
    }

    @Override
    protected void setImmobilizer() {

    }

    class MessageAdapter extends ArrayAdapter<FeedWrapper> {
        ApplicationPreferences appPref;
        Activity activity;
        MessageAdapter fd;
        ImageLoader imageLoader = null;
        String tag = "Message adapter";
        private boolean mHasMoreItems;

        public MessageAdapter(ApplicationPreferences appPref, Activity activity) {
            super(activity, android.R.layout.two_line_list_item);
            mHasMoreItems = false;
            this.activity = activity;
            this.appPref = appPref;
            this.fd = this;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (imageLoader == null) {
                imageLoader = AppController.getInstance().getImageLoader();
            }
            //if(convertView==null)
            {
                final Holder holder = new Holder();
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.message_content_row, parent, false);

                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.extraText = (TextView) convertView.findViewById(R.id.extraText);
                holder.feedText = (TextView) convertView.findViewById(R.id.feedText);
                holder.timeStamp = (TextView) convertView.findViewById(R.id.timestamp);
                holder.deleteTextView = (TextView) convertView.findViewById(R.id.deleteFeedtxt);
                holder.deleteRow = (TableRow) convertView.findViewById(R.id.deleteRow);
                holder.imageView1 = (FeedImageView) convertView.findViewById(R.id.feedImage1);
                holder.imageView2 = (FeedImageView) convertView.findViewById(R.id.feedImage2);
                holder.imageView3 = (FeedImageView) convertView.findViewById(R.id.feedImage3);
                holder.commentText = (TextView) convertView.findViewById(R.id.commentOnFeedtxt);
                holder.commentRow = (TableRow) convertView.findViewById(R.id.commentRow);
                holder.firstImageRow = (TableRow) convertView.findViewById(R.id.firstRow);
                holder.secondImageRow = (TableRow) convertView.findViewById(R.id.secondRow);
                holder.container = (RelativeLayout) convertView.findViewById(R.id.imageContainer);
                holder.shareFeedRow = (TableRow) convertView.findViewById(R.id.shareFeedRow);
                holder.defaultRow = (TableRow) convertView.findViewById(R.id.defaultRow);
                holder.trailContainer =  (LinearLayout) convertView.findViewById(R.id.trail_container);
                holder.trailImage = (FeedImageView) convertView.findViewById(R.id.trail_image);

                final FeedWrapper feedWrapper = getItem(position);

                String name = feedWrapper.getFeedUsers().getSharedBy().getName().getFullName();
                Long Id = feedWrapper.getFeed().getOwner().getName().getId();
                final String feedText = feedWrapper.getFeed().getContent().getDescription();
                String trailDescription = feedWrapper.getDescription();
                String type = feedWrapper.getFeed().getType();
                String timestamp = feedWrapper.getFeedUpdatedTime();

                final Comments comment[] = feedWrapper.getFeed().getComments();
                if (comment != null) {
                    holder.commentText.setText("Reply(" + comment.length + ")");
                }

                holder.name.setText(name);
                if (feedText != null && !feedText.trim().isEmpty()) {
                    holder.feedText.setText(feedText);
                } else {
                    holder.feedText.setHeight(10);
                    holder.feedText.setVisibility(View.INVISIBLE);
                }

                if (type.equals("TEXT")) {
                    holder.extraText.setText("shared a");
                    holder.firstImageRow.setVisibility(View.GONE);
                    holder.secondImageRow.setVisibility(View.GONE);
                    holder.container.setVisibility(View.GONE);
                }
                if (type.equals("IMAGE")) {


                    holder.extraText.setText("shared a");
                    ImageContent imageContent = (ImageContent) feedWrapper.getFeed().getContent();
                    final Images[] images = imageContent.getImages();

                    if (images != null && images.length == 0) {
                        holder.extraText.setText("shared a");
                        holder.container.setVisibility(View.GONE);
                    } else {
                        if (images.length == 1) {
                            holder.extraText.setText("shared a");
                            holder.defaultRow.setVisibility(View.VISIBLE);
                            holder.secondImageRow.setVisibility(View.GONE);
                        }
                        if (images.length > 1) {
                            holder.extraText.setText("shared a");
                        }
                     /*Depending upone number of images things to do*/

                        holder.container.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (view instanceof FeedImageView) {
                                    Intent i = new Intent(activity, View_Feed_Images.class);
                                    i.putExtra("images", ObjectSerializer.serialize(images));
                                    activity.startActivity(i);
                                }

                            }
                        });


                        try {
                            final ProgressBar p = (ProgressBar) convertView.findViewById(R.id.progressBar);
                            final TextView imgErr = (TextView) convertView.findViewById(R.id.text);
                            switch (images.length) {
                                case 1:
                                    holder.secondImageRow.setVisibility(View.GONE);
                                    holder.imageView1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[0].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    if (images[0].getImageLink() != null) {
                                        holder.imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        holder.imageView1.setVisibility(View.VISIBLE);
                                        holder.imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        //holder.container.setVisibility(View.GONE);
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView1.setVisibility(View.GONE);
                                    }
                                    break;


                                case 2:
                                    holder.firstImageRow.setVisibility(View.GONE);
                                    holder.imageView2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[0].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    holder.imageView3.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[1].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    if (images[0].getImageLink() != null) {
                                        holder.imageView2.setImageUrl(images[0].getImageLink(), imageLoader);
                                        holder.imageView2.setVisibility(View.VISIBLE);
                                        holder.imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        //holder.container.setVisibility(View.GONE);
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[1].getImageLink() != null) {
                                        holder.imageView3.setImageUrl(images[1].getImageLink(), imageLoader);
                                        holder.imageView3.setVisibility(View.VISIBLE);
                                        holder.imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        //holder.container.setVisibility(View.GONE);
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView3.setVisibility(View.GONE);
                                    }

                                    break;

                                default:
                                    holder.imageView1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[0].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    holder.imageView2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[1].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    holder.imageView3.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(activity, View_Feed_Images.class);
                                            i.putExtra("images", ObjectSerializer.serialize(images));
                                            i.putExtra("selected", images[2].getImageLink());
                                            activity.startActivity(i);
                                        }
                                    });
                                    if (images[0].getImageLink() != null) {
                                        holder.imageView1.setImageUrl(images[0].getImageLink(), imageLoader);
                                        holder.imageView1.setVisibility(View.VISIBLE);
                                        holder.imageView1
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.firstImageRow.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView1.setVisibility(View.GONE);
                                    }
                                    if (images[1].getImageLink() != null) {
                                        holder.imageView2.setImageUrl(images[1].getImageLink(), imageLoader);
                                        holder.imageView2.setVisibility(View.VISIBLE);
                                        holder.imageView2
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView2.setVisibility(View.GONE);
                                    }

                                    if (images[2].getImageLink() != null) {
                                        holder.imageView3.setImageUrl(images[2].getImageLink(), imageLoader);
                                        holder.imageView3.setVisibility(View.VISIBLE);
                                        holder.imageView3
                                                .setResponseObserver(new FeedImageView.ResponseObserver() {
                                                    @Override
                                                    public void onError() {
                                                        holder.defaultRow.setVisibility(View.VISIBLE);
                                                        holder.secondImageRow.setVisibility(View.GONE);
                                                        p.setVisibility(View.GONE);
                                                        imgErr.setVisibility(View.VISIBLE);
                                                    }

                                                    @Override
                                                    public void onSuccess() {
                                                        holder.container.setVisibility(View.VISIBLE);
                                                        holder.defaultRow.setVisibility(View.GONE);
                                                    }
                                                });
                                    } else {
                                        holder.imageView3.setVisibility(View.GONE);
                                    }


                                    break;

                            }

                        } catch (RuntimeException error) {
                            Log.d(tag, "Found Error while loading images|Hiding Image container");
                            holder.container.setVisibility(View.GONE);
                        }
                    }


                }

                if ("JOURNEY".equals(type)) {
                    holder.feedText.setText(trailDescription);
                    holder.extraText.setText("shared a trail");
                    JourneyFeedContent journeyFeedContent = (JourneyFeedContent) feedWrapper.getFeed().getContent();
                    Images imgs = journeyFeedContent.getTrailImage();
                    final RouteCoordinates[] rootCoord = journeyFeedContent.getRouteCoordinates();//feedWrapper.getFeed().getContent().getRouteCoordinates();
                    final WayPoints[] wayPointses = journeyFeedContent.getWaypoints();
                    if (holder.container != null && holder.trailContainer != null && imgs != null && imgs.getImageLink() != null) {
                        holder.trailContainer.setVisibility(View.VISIBLE);
                        holder.container.setVisibility(View.GONE);
                        holder.trailImage.setImageUrl(imgs.getImageLink().replace(" ","%20"), imageLoader);

                        holder.trailImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (rootCoord != null) {
                                    Intent mapIntent = new Intent(getContext(), TrailActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("coordinates", rootCoord);
                                    if (wayPointses != null) {
                                        bundle.putSerializable("waypoints", wayPointses);
                                    }

                                    mapIntent.putExtras(bundle);
                                    startActivity(mapIntent);
                                }
                            }
                        });

                        holder.trailImage.setResponseObserver(new FeedImageView.ResponseObserver() {
                            @Override
                            public void onError() {
                                holder.trailContainer.setVisibility(View.GONE);
                            }

                            @Override
                            public void onSuccess() {
                                holder.trailContainer.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    else if( holder.trailContainer != null && (imgs == null || imgs.getImageLink() == null))
                    {
                        holder.trailContainer.setVisibility(View.VISIBLE);
                        holder.container.setVisibility(View.GONE);
                        holder.trailImage.setDefaultImageResId(R.drawable.default_map);
                        holder.trailImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (rootCoord != null) {
                                    Intent mapIntent = new Intent(getContext(), TrailActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("coordinates", rootCoord);
                                    if (wayPointses != null) {
                                        bundle.putSerializable("waypoints", wayPointses);
                                    }

                                    mapIntent.putExtras(bundle);
                                    startActivity(mapIntent);
                                }
                            }
                        });

                    }
                }

                Long time = Utility.getDateToLong(timestamp);
                if (time != null) {
                    CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
                    holder.timeStamp.setText(timeAgo);
                } else {
                    holder.timeStamp.setText(timestamp);
                }

                if (holder.commentRow != null) {
                    holder.commentRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            {
                                setOnResumeRefresh(true);
                                ArrayList<Comments> list = null;
                                if (comment != null) {
                                    list = new ArrayList<Comments>(Arrays.asList(comment));
                                }
                                Intent i = new Intent(activity, CommentActivity.class);
                                String comment = serialize(list);
                                i.putExtra("from", "mymessage");
                                i.putExtra("comments", comment);
                                i.putExtra("feedId", feedWrapper.getFeed().getId());
                                activity.startActivity(i);
                            }

                        }
                    });
                }

                if (holder.shareFeedRow != null) {
                    holder.shareFeedRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(activity, Share_Message_Screen.class);
                            i.putExtra("screen", "messages");
                            i.putExtra("feedId", feedWrapper.getFeed().getId());
                            i.putExtra("content", ObjectSerializer.serialize(feedWrapper));
                            activity.startActivity(i);
                        }
                    });
                }


                if (holder.deleteRow != null) {
                    holder.deleteRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                                dialog.setTitle("Delete Message");
                                dialog.setMessage("Do you want to delete this message ?");
                                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Log.d(tag, "deleting message");
                                        try {
                                            JSONObject input = new JSONObject();
                                            input.put("feedId", feedWrapper.getFeedUsers().getFeedUsersId());
                                            selectedFordelete = feedWrapper;
                                            Log.d(tag, "input: " + input);

                                            GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.DELETE_MESSAGE, FeedMethodEnum.DELETE_MESSAGE.name(), input.toString(), tag, asyncFinishInterface, true);
                                            service.execute();
                                        } catch (Exception ex) {
                                            ExceptionHandler.handleException(ex, activity);
                                        }
                                    }
                                });
                                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        return;
                                    }
                                });
                                dialog.setCancelable(false);
                                dialog.create();
                                dialog.show();
                            } catch (Throwable e) {
                                ExceptionHandler.handleException(e);
                            }

                        }
                    });
                }
                convertView.setTag(holder);
            }

            return convertView;
        }

        class Holder {
            ImageLoader imageLoader;
            TextView name;
            TextView extraText;
            TextView feedText;
            TextView timeStamp;
            TextView commentText;
            TextView deleteTextView;
            FeedImageView imageView1;
            FeedImageView imageView2;
            FeedImageView imageView3;
            TableRow deleteRow;
            TableRow commentRow;
            TableRow shareFeedRow;
            TableRow firstImageRow;
            TableRow secondImageRow;
            TableRow defaultRow;
            RelativeLayout container;
            LinearLayout trailContainer;
            FeedImageView trailImage;
        }
    }


}
