package com.waypals.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.waypals.utils.CH_Constant;
import com.waypals.R;

/**
 * Created by surya on 8/10/15.
 */
public class GCMRegisterationService extends IntentService{

    private static final String TAG = "RegIntentService";

    private String token = null;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     **/

    public GCMRegisterationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent != null) {
            try {

                if(CH_Constant.REGISTRATION_COMPLETE.equalsIgnoreCase(intent.getStringExtra("type"))) {

                    InstanceID instanceID = InstanceID.getInstance(this);
                    token = instanceID.getToken(getString(R.string.sender_id),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.i(TAG, "GCM Registration Token: " + token);

                    // Notify UI that registration has completed, so the progress indicator can be hidden.
                    Intent registrationComplete = new Intent(CH_Constant.REGISTRATION_COMPLETE);
                    registrationComplete.putExtra("token", token);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
                }

                else if(CH_Constant.DEREGISTRATION_COMPLETE.equalsIgnoreCase(intent.getStringExtra("type")))
                {
                    String t = intent.getStringExtra("token");
                    InstanceID instanceID = InstanceID.getInstance(this);
                    instanceID.deleteToken(t, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                    // Notify UI that registration has completed, so the progress indicator can be hidden.
                    Intent deregistrationComplete = new Intent(CH_Constant.DEREGISTRATION_COMPLETE);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(deregistrationComplete);
                }
            } catch (Exception ex) {
                Log.e("Error", ex.getMessage());

                if(CH_Constant.REGISTRATION_COMPLETE.equalsIgnoreCase(intent.getStringExtra("type")))
                {
                    Intent deregistrationComplete = new Intent(CH_Constant.REGISTRATION_FAILED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(deregistrationComplete);
                }
            }


        }
    }
}
