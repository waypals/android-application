package com.waypals.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.WplHttpClient;

public class GenericAsyncService {

    String input;
    String response, methodName;
    String tag;
    private ApplicationPreferences appPref;
    private AsyncFinishInterface asynFinishIterface;
    private ProgressDialog dialog;
    private String serviceName;
    private Activity context;
    private Boolean dialogShow = true;

    public Boolean getDialogShow() {
        return dialogShow;
    }

    public void setDialogShow(Boolean dialogShow) {
        this.dialogShow = dialogShow;
    }

    public GenericAsyncService(ApplicationPreferences appPref, Activity context, String API, String methodName, String input, String tag, AsyncFinishInterface asyncFinishInterface, Boolean dialog) {
        this.serviceName = API;
        this.appPref = appPref;
       // this.dialog = new ProgressDialog(context);
        this.asynFinishIterface = asyncFinishInterface;
        this.input = input;
        this.context = context;
        this.tag = tag;
        this.dialogShow = dialog;
        this.methodName = methodName;
    }

    public GenericAsyncService(Activity context, String url, AsyncFinishInterface asyncFinishInterface, Boolean dialog) {
        this.serviceName = url;
        this.asynFinishIterface = asyncFinishInterface;
        this.context = context;
        this.dialogShow = dialog;
    }

    public void executeGet()
    {
        try {
            WplHttpClient.callVolleyGetService(context, serviceName, this.asynFinishIterface, dialogShow);

        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }


    public void execute()
    {
        try {
            Log.d(this.tag, "REQ|" + this.serviceName);
            WplHttpClient.callVolleyService(context, this.serviceName, this.input, appPref, asynFinishIterface, methodName, dialogShow);
        } catch (NetworkException ex) {
            ExceptionHandler.handleException(ex, context);
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }
        Log.d(tag, "Response: " + response);
    }


}
