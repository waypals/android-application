package com.waypals.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by surya on 18/01/2018.
 */

public class FetchAddressIntentService extends IntentService {

    protected ResultReceiver mReceiver;

    public FetchAddressIntentService() {
        super("FetchAddressService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        // ...

        String errorMessage = "";

        // Get the location passed to this service through an extra.
        mReceiver = intent.getParcelableExtra("receiver");
        int id = intent.getIntExtra("id", 0);
        double lat = intent.getDoubleExtra("lat", 0.0);
        double lon = intent.getDoubleExtra("lon", 0.0);
        String state = intent.getStringExtra("state");
        String time = intent.getStringExtra("time");

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    lat,
                    lon,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            Log.e("Address not available", "Address not available", ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            Log.e("Address not available", "Address not available", illegalArgumentException);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            if (errorMessage.isEmpty()) {
                Log.e("Address not available", "Address not available");
            }
            deliverResultToReceiver(0, errorMessage, id, state, time);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }

            deliverResultToReceiver(1,
                    TextUtils.join(System.getProperty("line.separator"),
                            addressFragments), id, state, time);
        }

    }

    private void deliverResultToReceiver(int resultCode, String message, int id, String state, String time) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putString("address", message);
        bundle.putString("state", state);
        bundle.putString("time", time);
        mReceiver.send(resultCode, bundle);
    }

}
