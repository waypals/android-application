package com.waypals.services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.feedAdapter.FeedAsyncInterface;
import com.waypals.feedAdapter.FeedMethodEnum;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.request.Pagination;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;

import java.util.ArrayList;
import java.util.List;

public class MessageService //extends AsyncTask<Void, Void, Void>
{
    ProgressDialog dialog;
    ApplicationPreferences appPref;
    Activity context;
    String error;
    String response;
    Pagination pagination;
    List<FeedWrapper> feedContents;
    FeedAsyncInterface asynFinishIterface;
    String tag = "My Message Service";
    String requestFrom;
    Boolean notification;

    public MessageService(ApplicationPreferences appPreferences, Activity context, Pagination pagination, FeedAsyncInterface asynFinishIterface, String requestFrom, Boolean notification) {
        this.appPref = appPreferences;
        this.context = context;
        this.pagination = pagination;
        this.requestFrom = requestFrom;
        this.notification = notification;
        this.feedContents = new ArrayList<FeedWrapper>();
        this.asynFinishIterface = asynFinishIterface;
    }

    public void execute()
    {
        try {
            Gson gson = new Gson();
            System.out.println(gson.toJson(pagination));
            WplHttpClient.callFeedVolleyService(context,
                    CH_Constant.GET_MESSAGE_CONTENT, gson.toJson(pagination), appPref, asynFinishIterface, requestFrom, notification, FeedMethodEnum.MY_MESSAGES);
            Log.d(tag, "Feed Response: " + response);
        } catch (NetworkException ex) {
            Log.d(tag, "Exception found");
            ExceptionHandler.handleException(ex, context);
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
            error = "Error From Server";
        }
    }

}
