package com.waypals;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.waypals.documentcase.DocumentCase;
import com.waypals.exception.ExceptionHandler;
import com.waypals.payment.MakePaymentActivity;
import com.waypals.trip.TripdataActivity;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

public abstract class NavigationDrawer extends Activity {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    Typeface tf;
    String tag = "Drawer Class";
    Activity context = this;
    private String location = "";

    private static ProgressDialog dialog;

    public RightDrawerContent[] setupRightDrawerContent() {


		/* setup drawer list */
        RightDrawerContent fr[] = new RightDrawerContent[]{
                new RightDrawerContent(R.drawable.car_info_icon, getString(R.string.Vehicle_info), Color.WHITE, false, false),
                new RightDrawerContent(R.drawable.tripdata_icon, getString(R.string.trip_data), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.telematic_icon_small, "Telematics", Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.see_me_not, getString(R.string.See_me_not), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.black, getString(R.string.CONNECT), Color.GRAY, false, false),
                new RightDrawerContent(R.drawable.feed_icon, getString(R.string.Feeds), Color.WHITE, false, false),
                new RightDrawerContent(R.drawable.msg_icon, getString(R.string.Messages), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.friend_icon, getString(R.string.Friends), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.share_location_icon, getString(R.string.Share_position), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.waypoint_icon, getString(R.string.WayPoints), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.follow_me_icon, getString(R.string.Follow_me), Color.WHITE, true, false),
               // new RightDrawerContent(R.drawable.black, getString(R.string.GEOFENCES), Color.GRAY, false, false),
               // new RightDrawerContent(R.drawable.add_geofence_icon, getString(R.string.Add_geofence), Color.WHITE, false, false),
               // new RightDrawerContent(R.drawable.showon_mapicon, getString(R.string.View_remove_geofence), Color.WHITE, true, false),
                new RightDrawerContent(R.drawable.menu_seperater_bg, "", Color.GRAY, false, false)};


        return fr;

    }

    public void showProgressBar(Context context)
    {  if ( Looper.myLooper() == Looper.getMainLooper()) {
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        dialog.show();
    }
    }

    public void closeProgressBar()
    {
        if( dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void setupRightDrawerListener(Typeface tf) {

        ListView rightList = (ListView) this.findViewById(R.id.right_drawer);
        RightDrawerAdapter fd = new RightDrawerAdapter(context, R.layout.right_drawer_content, setupRightDrawerContent(), tf);
        rightList.setAdapter(fd);

        rightList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                Intent i;
                switch (position) {
                    case 0:/* Car info action */
                        i = new Intent(context, CarInfo_Screen.class);
                        startActivity(i);
                        break;

                    case 1:/* Car info action */
                        i = new Intent(context, TripdataActivity.class);
                        startActivity(i);
                        break;

                    case 2:/* Telematic */
                        i = new Intent(context, Telematics_Screen.class);
                        startActivity(i);
                        break;

                    case 3:/* See me not */
                        i = new Intent(context, SeeMeNot_Screen.class);
                        startActivity(i);
                        break;

                    case 5:/* Feeds action */
                        i = new Intent(context, MyFeed.class);
                        startActivity(i);
                        break;

                    case 6:/* Messages actions*/
                        i = new Intent(context, MyMessages.class);
                        startActivity(i);
                        break;

                    case 7:/* Friends actions*/
                        i = new Intent(context, Friends_Screen_New.class);
                        startActivity(i);
                        break;

                    case 8:/* Share location*/
                        if (checkPlayServices()) {
                            i = new Intent(context, Sharelocation_Screen.class);
                            startActivity(i);
                        }

                        break;


                    case 9:/*Waypoints*/
                        if (checkPlayServices()) {
                            i = new Intent(context, Waypoint_Screen.class);
                            startActivity(i);
                        }

                        break;

                    case 10:/* Follow me*/
                        //if (checkPlayServices())
                    {
                        i = new Intent(context, FollowMe_Screen.class);
                        startActivity(i);
                    }
                    break;

                    case 12:/*Add geo fence*/
                        if (checkPlayServices()) {
                            i = new Intent(context, Add_Geofence.class);
                            i.putExtra("current_vehicle_latlong", location);
                            startActivity(i);
                        }

                        break;

                    case 13:/*View/Remove geofence*/
                        if (checkPlayServices()) {
                            i = new Intent(context, ShowGeoFenceOnMap.class);
                            startActivity(i);
                        }

                        break;
                }

            }

        });

    }

    public void setupLeftDrawerListener(final ApplicationPreferences appPref, final Typeface tf, final boolean updateTitle) {


        final int totalCars = appPref.getTotalCar();
        String[] vehicaleList = appPref.getVehicalList();
        ListView leftList = (ListView) this.findViewById(R.id.left_drawer);
        LeftDrawerAdapter fd = new LeftDrawerAdapter(context, R.layout.left_drawer_content, setupLeftDrawerContent(vehicaleList, context, totalCars, appPref), tf);
        leftList.setAdapter(fd);
        leftList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view,
                                    int position, long id) {

				/* car overview action */
                if (position == 0) {
                    if (checkPlayServices()) {
                        Intent i = new Intent(context, Car_Overview_Screen.class);
                        startActivity(i);
                    }
                }

				/*setup listers for all the list of cars*/
                if (position >= 2 && position < totalCars + 2) {
                    TextView regText = (TextView) view.findViewById(R.id.leftDrawerRowText);
                    Log.i(tag, "Selected car :" + regText.getText().toString());
                    Utility.setCurrentCarSettings(regText.getText().toString(), appPref, Home_Screen.vehicleInfo);
                    DrawerLayout myDrawer = (DrawerLayout) ((Activity) context)
                            .findViewById(R.id.drawer_layout);
                    setupLeftDrawerListener(appPref, tf, updateTitle);

                    if (updateTitle) {

                        appPref.saveFirstTimeLoggedIn(true);
                        TextView title = (TextView) findViewById(R.id.title_text);
                        title.setText(appPref.getSelectedRegistrationNumber());

                        TextView policyText = (TextView) findViewById(R.id.policy_expiry_title);
                        if(appPref.getCurrentVehiclePolicyDueDate() != null) {
                            policyText.setText(appPref.getCurrentVehiclePolicyDueDate());
                        }else
                        {
                            policyText.setText("No Info");
                        }
                    }

                    setImmobilizer();

                    myDrawer.closeDrawers();
                }

                int index = 3;

				/* alert action */
                if (position == index + totalCars) {
                    Log.i(tag, "Alert action");
                    Intent i = new Intent(context, Alerts_Screen_New.class);
                    startActivity(i);
                }
                index++;
                /* incident action */
                if (position == index + totalCars) {
                    Log.i(tag, "Incident action");
                    Intent i = new Intent(context, Incident_Screen.class);
                    startActivity(i);
                }
                index +=2;
                /* settings action */
                if (position == index + totalCars) {
                    Log.i(tag, "Settings action");
                    Intent i = new Intent(context, Settings_Screen.class);
                    startActivity(i);
                }
                index++;

                  /* Emergency number */
                if (position == index + totalCars) {
                    Log.i(tag, "Emergency action");
                    Intent i = new Intent(context, EmergencyNoActivity.class);
                    startActivity(i);
                }
                index++;
                    /* Make payment action */
                if (position == index + totalCars) {
                    Log.i(tag, "Make payment action");
                    Intent i = new Intent(context, MakePaymentActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                }
                 index++;

                    /* logout action */
                if (position == index + totalCars) {
                    Log.i(tag, "Document case");
                    try {
                        Intent documentCase = new Intent(context, DocumentCase.class);
                        startActivity(documentCase);
                        //  Refactoring code and setting logout immediately once user presses logout button
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }
                index++;

                /* logout action */
                if (position == index + totalCars) {
                    Log.i(tag, "Logout action");
                    try {
                        Intent deRegistrationComplete = new Intent(CH_Constant.DEREGISTRATION_COMPLETE);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(deRegistrationComplete);

                        //  Refactoring code and setting logout immediately once user presses logout button
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }
                index++;



                /* about action */
                if (position == index + totalCars) {
                    Log.i(tag, "About action");
                    try {
                        Intent aboutUs = new Intent(context, AboutUs.class);

                        aboutUs.putExtra(AboutUs.ABOUT_US_TITLE, "About Us");
                        aboutUs.putExtra(AboutUs.ABOUT_US_TEXT, getString(R.string.about_us));

                        startActivity(aboutUs);
                        //  Refactoring code and setting logout immediately once user presses logout button
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            }
        });

    }


    public LeftDrawerContent[] setupLeftDrawerContent(String[] carlist, Context localContext, final int totalCars, ApplicationPreferences appPref) {
        String[] menu_text = {"Car Overview", "Settings", "Log Out", "About"};
        int[] menu_img = {R.drawable.menu_pin_blue, R.drawable.menu_pin_green,
                R.drawable.menu_pin_pink};
        LeftDrawerContent fr[] = new LeftDrawerContent[totalCars + 13];
        try {
            Log.i(tag, "creating list of items for left drawers");

            int index = 0;

            fr[index] = new LeftDrawerContent(R.drawable.menu_car_overview,
                    getString(R.string.VEHICLE_OVERVIEW), Color.WHITE, false, false);
            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.black,
                    getString(R.string.VEHICLE_SELECTION), Color.GRAY, false, false);
            ++index;

			/* setup list of vichels */
            for (int i = 1; i <= totalCars; i++) {
                boolean upper, lower;
                lower = false;
                upper = false;
                switch (i) {
                    case 1:
                        upper = false;
                        lower = false;
                        break;

                    default:
                        upper = true;
                        lower = false;
                        break;
                }
                if (i == totalCars) {
                    upper = true;
                    lower = false;
                }

                if (appPref.getSelectedRegistrationNumber().equals(carlist[i - 1])) {
                    fr[index] = new LeftDrawerContent(R.drawable.menu_car_icon, carlist[i - 1], Color.YELLOW, upper, lower);
                } else {
                    fr[index] = new LeftDrawerContent(R.drawable.menu_car_icon, carlist[i - 1], Color.WHITE, upper, lower);
                }
                ++index;
            }

            fr[index] = new LeftDrawerContent(R.drawable.black,
                    getString(R.string.NOTIFICATIONS), Color.GRAY, false, false);
            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.menu_alert, getString(R.string.Alerts),
                    Color.WHITE, false, false);
            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.menu_incident,
                    getString(R.string.Incidents), Color.WHITE, true, false);
            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.black, getString(R.string.SYSTEM_MANAGER),
                    Color.GRAY, false, false);

            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.menu_setting,
                    getString(R.string.Settings), Color.WHITE, false, false);

            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.emergency_call,
                    getString(R.string.emergency), Color.WHITE, true, false);

            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.payment_icon,
                    getString(R.string.text_make_payment), Color.WHITE, true, false);

            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.document_case_icon,
                    getString(R.string.documents_case), Color.WHITE, true, false);

            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.menu_logout,
                    getString(R.string.Log_Out), Color.WHITE, true, false);

            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.menu_about, getString(R.string.About),
                    Color.WHITE, true, false);
            ++index;
            fr[index] = new LeftDrawerContent(R.drawable.menu_seperater_bg, "",
                    Color.GRAY, false, false);


        } catch (Exception ex) {
            ex.printStackTrace();
            ExceptionHandler.handleException(ex, this);
        }
        return fr;
    }

    public Integer getLayoutHeights() {
        //Determine density
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            //Log.d(tag, "h 40 high large");
            return 40;
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            //Log.d(tag, "h 40 med");
            return 40;
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            //Log.d(tag, "h 30 low");
            return 30;
        } else if (density == DisplayMetrics.DENSITY_DEFAULT) {
            //Log.d(tag, "h 40 default");
            return 40;
        } else if (density == DisplayMetrics.DENSITY_XHIGH) {
            //Log.d(tag,"h size: 80 x");
            return 60;
        } else if (density == DisplayMetrics.DENSITY_XXHIGH) {
            //Log.d(tag,"h size: 80 xx");
            return 80;
        } else if (density == DisplayMetrics.DENSITY_XXXHIGH) {
            //Log.d(tag,"h size: 80 xxx");
            return 80;
        } else {
            //Log.d(tag, "h 40 unkwon");
            return 40;
        }
    }

    public Integer getTextSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        // getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            //Log.d(tag,"text size: 17  high large");
            return 17;
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            //Log.d(tag,"text size: 23 medium");
            return 16;
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            //Log.d(tag,"text size: 17 low");
            return 15;
        } else if (density == DisplayMetrics.DENSITY_DEFAULT) {
            //Log.d(tag,"text size: 17 default");
            return 17;
        } else if (density == DisplayMetrics.DENSITY_XHIGH) {
            //Log.d(tag,"text size: 17 x");
            return 17;
        } else if (density == DisplayMetrics.DENSITY_XXHIGH) {
            //Log.d(tag,"text size: 17 xx");
            return 17;
        } else if (density == DisplayMetrics.DENSITY_XXXHIGH) {
            //Log.d(tag,"text size: 17 xxx");
            return 17;
        } else {
            //Log.d(tag,"text size: 15 unknwn");
            return 15;
        }

    }

    public Integer getDefaultTextSize() {

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density == DisplayMetrics.DENSITY_HIGH) {
            //Log.d(tag, "text size: 23 high");
            return 17;
        } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
            //Log.d(tag, "text size: 23 medum");
            return 16;
        } else if (density == DisplayMetrics.DENSITY_LOW) {
            // Log.d(tag, "text size: 17 low");
            return 15;
        } else if (density == DisplayMetrics.DENSITY_XHIGH) {
            //Log.d(tag, "text size: 18 x");
            return 18;
        } else if (density == DisplayMetrics.DENSITY_XXXHIGH) {
            // Log.d(tag, "text size: 18 xxx");
            return 18;
        } else if (density == DisplayMetrics.DENSITY_XXHIGH) {
            // Log.d(tag,"text size: 18 xx");
            return 18;
        } else {
            // Log.d(tag,"text size: 15 unknwn");
            return 18;
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(tag, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    protected abstract void setImmobilizer();

    class LeftDrawerContent {
        public int icon;
        public String title;
        public int color;
        public boolean upperDivider;
        public boolean lowerDivider;

        public LeftDrawerContent(int icon, String title, int color, boolean upperDivider, boolean lowerDivider) {
            super();
            this.icon = icon;
            this.title = title;
            this.color = color;
            this.upperDivider = upperDivider;
            this.lowerDivider = lowerDivider;

        }
    }

    class RightDrawerContent {
        public int icon;
        public String title;
        public int color;
        public boolean upperDivider;
        public boolean lowerDivider;

        public RightDrawerContent(int icon, String title, int color, boolean upperDivider, boolean lowerDivider) {
            super();
            this.icon = icon;
            this.title = title;
            this.color = color;
            this.upperDivider = upperDivider;
            this.lowerDivider = lowerDivider;

        }
    }

    class LeftDrawerAdapter extends ArrayAdapter<LeftDrawerContent> {
        Context context;
        int layoutResourceId;
        LeftDrawerContent data[] = null;
        Typeface tf;

        public LeftDrawerAdapter(Context context, int resource,
                                 LeftDrawerContent[] objects, Typeface tf) {
            super(context, resource, objects);
            this.layoutResourceId = resource;
            this.context = context;
            this.data = objects;
            this.tf = tf;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            ContentsHolder holder;
            LayoutInflater inflater = ((NavigationDrawer) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);


            holder = new ContentsHolder();
            holder.imgIcon = (ImageView) row
                    .findViewById(R.id.leftDrawerRowIcon);
            holder.txtTitle = (TextView) row
                    .findViewById(R.id.leftDrawerRowText);
            holder.txtTitle.setTypeface(tf);
            holder.checkIcon = (ImageView) row
                    .findViewById(R.id.leftDrawerRowCheckIcon);
            holder.upDivider = (TextView) row.findViewById(R.id.leftDrawerUpperTxtView);

            holder.lowDivider = (TextView) row.findViewById(R.id.leftDrawerLowerTxtView);

            row.setTag(holder);

            LeftDrawerContent drawerContent = data[position];
            holder.txtTitle.setTypeface(tf);
            holder.txtTitle.setText(drawerContent.title);
            holder.imgIcon.setImageResource(drawerContent.icon);

            holder.txtTitle.setTextSize(getDefaultTextSize());

            if (drawerContent.upperDivider) {
                holder.upDivider.setVisibility(View.VISIBLE);
            } else {
                holder.upDivider.setVisibility(View.INVISIBLE);
            }
            if (drawerContent.lowerDivider) {
                holder.lowDivider.setVisibility(View.VISIBLE);
            } else {
                holder.lowDivider.setVisibility(View.INVISIBLE);
            }
            holder.lowDivider.requestLayout();
            holder.upDivider.requestLayout();
            row.setBackgroundColor(getResources().getColor(R.color.gray));
            if (drawerContent.color == Color.WHITE) {
                /*settings for blank header*/
                holder.txtTitle.setTextColor(drawerContent.color);
                row.setBackgroundColor(getResources().getColor(R.color.gray));
                holder.imgIcon.setVisibility(View.VISIBLE);
                holder.checkIcon.setVisibility(View.GONE);
            }
            if (drawerContent.color == Color.GRAY) {
                /*settings for header*/
                row.setBackgroundColor(getResources().getColor(R.color.dark_gray));
                row.getLayoutParams().height = getLayoutHeights();
                row.setPadding(10, 1, 0, 1);
                holder.txtTitle.setTextSize(getTextSize());
                row.requestLayout();
                holder.txtTitle.setTextColor(drawerContent.color);
                holder.txtTitle.setGravity(Gravity.LEFT);
                holder.txtTitle.setTextColor(getResources().getColor(R.color.background_green));
                holder.txtTitle.setTextAppearance(getApplicationContext(), R.style.boldstyle);
                holder.txtTitle.requestLayout();
                holder.txtTitle.setTypeface(tf);

                holder.imgIcon.setVisibility(View.INVISIBLE);
                holder.imgIcon.getLayoutParams().width = 0;
                holder.imgIcon.requestLayout();
                holder.checkIcon.setVisibility(View.GONE);

                if (drawerContent.title.equals("")) {
                    row.setBackgroundColor(getResources().getColor(R.color.gray));
                }

            }
            if (drawerContent.color == Color.YELLOW) {
                /*settings for list of cars*/
                holder.txtTitle.setTextColor(getResources().getColor(R.color.yellow));
                holder.checkIcon.setImageResource(R.drawable.tickicon);
            }

            row.requestLayout();
            return row;

        }

        class ContentsHolder {
            ImageView imgIcon;
            TextView txtTitle;
            int color;
            ImageView checkIcon;
            TextView upDivider;
            TextView lowDivider;
        }
    }

    class RightDrawerAdapter extends ArrayAdapter<RightDrawerContent> {
        Context context;
        int layoutResourceId;
        RightDrawerContent data[] = null;
        Typeface tf;

        public RightDrawerAdapter(Context context, int resource,
                                  RightDrawerContent[] objects, Typeface tf) {
            super(context, resource, objects);
            this.layoutResourceId = resource;
            this.context = context;
            this.data = objects;
            this.tf = tf;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ContentsHolder holder;
            LayoutInflater inflater = ((NavigationDrawer) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);


            holder = new ContentsHolder();
            holder.imgIcon = (ImageView) row
                    .findViewById(R.id.rightDrawerRowIcon);
            holder.txtTitle = (TextView) row
                    .findViewById(R.id.rightDrawerRowText);

            row.setTag(holder);

            holder.upDivider = (TextView) row.findViewById(R.id.rightDrawerUpperTxtView);

            holder.lowDivider = (TextView) row.findViewById(R.id.rightDrawerLowerTxtView);

            holder.txtTitle.setTypeface(tf);

            RightDrawerContent drawerContent = data[position];
            holder.txtTitle.setText(drawerContent.title);

            holder.imgIcon.setImageResource(drawerContent.icon);
            holder.txtTitle.setTextColor(drawerContent.color);

            holder.txtTitle.setTextSize(getDefaultTextSize());

			/*if ((getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK) ==
					Configuration.SCREENLAYOUT_SIZE_LARGE) {
				holder.txtTitle.setTextSize(23);
			}
			else{
				holder.txtTitle.setTextSize(18);
			}*/

            row.setBackgroundColor(getResources().getColor(R.color.gray));

            if (drawerContent.upperDivider) {
                holder.upDivider.setVisibility(View.VISIBLE);
            } else {
                holder.upDivider.setVisibility(View.INVISIBLE);
            }

            if (drawerContent.lowerDivider) {
                holder.lowDivider.setVisibility(View.VISIBLE);
            } else {
                holder.lowDivider.setVisibility(View.INVISIBLE);
            }

            holder.lowDivider.requestLayout();
            holder.upDivider.requestLayout();
            if (drawerContent.color == Color.WHITE) {
                //row.setBackgroundResource(R.drawable.menu_item_bg);
                holder.imgIcon.setVisibility(View.VISIBLE);
            }
            if (drawerContent.color == Color.GRAY) {
                holder.imgIcon.setVisibility(View.INVISIBLE);
                holder.imgIcon.getLayoutParams().width = 0;
                holder.imgIcon.requestLayout();
                row.setBackgroundColor(getResources().getColor(R.color.dark_gray));
                holder.txtTitle.setTextSize(getTextSize());
                row.getLayoutParams().height = getLayoutHeights();
                row.setPadding(0, 1, 0, 1);
                row.requestLayout();
				/*if ((getResources().getConfiguration().screenLayout &
						Configuration.SCREENLAYOUT_SIZE_MASK) ==
						Configuration.SCREENLAYOUT_SIZE_LARGE) {
					holder.txtTitle.setTextSize(21);
					row.getLayoutParams().height=50;
				}
				else{
					row.getLayoutParams().height=50;
					holder.txtTitle.setTextSize(17);
				}*/


                holder.txtTitle.setTypeface(tf);
                holder.txtTitle.setTextColor(getResources().getColor(R.color.background_green));
                holder.txtTitle.setGravity(Gravity.LEFT);

                if (drawerContent.title.equals("")) {
                    row.setBackgroundColor(getResources().getColor(R.color.gray));
                }

                holder.txtTitle.requestLayout();
            }

            row.requestLayout();
            return row;

        }

        class ContentsHolder {
            ImageView imgIcon;
            TextView txtTitle;
            int color;
            TextView upDivider;
            TextView lowDivider;
        }
    }
}