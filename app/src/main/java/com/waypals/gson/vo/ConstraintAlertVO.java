package com.waypals.gson.vo;

public class ConstraintAlertVO {

    private String type;

    private String dateInMilliSec;

    private String message;

    private String response;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateInMilliSec() {
        return dateInMilliSec;
    }

    public void setDateInMilliSec(String dateInMilliSec) {
        this.dateInMilliSec = dateInMilliSec;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


}
