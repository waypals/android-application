package com.waypals.gson.vo;

import java.io.Serializable;

/**
 * Created by surya on 29/10/15.
 */
public class RequestBody implements Serializable{

    String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
