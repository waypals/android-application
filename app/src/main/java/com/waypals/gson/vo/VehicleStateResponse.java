package com.waypals.gson.vo;


public class VehicleStateResponse {

    String response;

    private RideLocation location;

    private String state;

    private boolean isSystemFenceEnabled;

    private String vehicleType;

    public boolean isSystemFenceEnabled() {
        return isSystemFenceEnabled;
    }

    public void setSystemFenceEnabled(boolean isSystemFenceEnabled) {
        this.isSystemFenceEnabled = isSystemFenceEnabled;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public RideLocation getLocation() {
        return location;
    }

    public void setLocation(RideLocation location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("response : " + response);
        builder.append("\nLocation : " + location.toString());
        builder.append("\nState : " + state);
        return builder.toString();
    }

}
