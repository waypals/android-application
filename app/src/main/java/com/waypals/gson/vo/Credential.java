package com.waypals.gson.vo;

import java.io.Serializable;

/**
 * Created by surya on 27/09/16.
 */
public class Credential implements Serializable{

    String login;
    String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
