package com.waypals.gson.vo;

import java.io.Serializable;
import java.util.List;

public class GeogrpahicalArea implements Serializable {

    private static final long serialVersionUID = 248350431883331674L;

    private List<RideLocation> coordinatePoints;

    private RideLocation center;

    private double radius;

    public List<RideLocation> getCoordinatePoints() {
        return coordinatePoints;
    }

    public void setCoordinatePoints(List<RideLocation> coordinatePoints) {
        this.coordinatePoints = coordinatePoints;
    }

    public RideLocation getCenter() {
        return center;
    }

    public void setCenter(RideLocation center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


}
