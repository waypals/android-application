package com.waypals.gson.vo;

import java.io.Serializable;
import java.util.List;

public class SOSServiceResponse implements Serializable {

    private static final long serialVersionUID = -704448725407678068L;
    private String response;
    private List<SOSContacts> sosNumber;
    private String errorCode;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<SOSContacts> getSosNumber() {
        return sosNumber;
    }

    public void setSosNumber(List<SOSContacts> sosNumber) {
        this.sosNumber = sosNumber;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
