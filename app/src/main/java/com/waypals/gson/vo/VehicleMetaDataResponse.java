package com.waypals.gson.vo;

import com.waypals.feedItems.Images;

import java.util.List;

public class VehicleMetaDataResponse {

    private String response;

    private String registrationNumber;

    private String deviceID;

    private String policyNumber;

    private String driver;

    private String carType;

    private List<Images> images;

    public List<Images> getImages() {
        return images;
    }

    public void setImages(List<Images> images) {
        this.images = images;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("registrationNumber " + registrationNumber);
        builder.append("\ncarType " + carType);
        builder.append("\ndriver " + driver);
        builder.append("\npolicyNumber " + policyNumber);
        builder.append("\ndeviceID " + deviceID);

        builder.append("\nresponse " + response);
        return builder.toString();
    }
}
