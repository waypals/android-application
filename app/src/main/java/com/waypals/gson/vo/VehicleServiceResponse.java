package com.waypals.gson.vo;

import java.io.Serializable;
import java.util.Collection;

public class VehicleServiceResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2381877157761142993L;

    String response;
    String error;
    String errorCode;


    Collection<VehicleInformationVO> vehicles;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Collection<VehicleInformationVO> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<VehicleInformationVO> vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (vehicles != null) {
            builder.append("Response : " + response + " vehicles : " + vehicles.toString());
        } else {
            builder.append("Response : " + response);
        }
        return builder.toString();
    }

}
