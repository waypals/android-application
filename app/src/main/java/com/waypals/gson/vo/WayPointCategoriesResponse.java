package com.waypals.gson.vo;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by shantanu on 20/10/14.
 */
public class WayPointCategoriesResponse implements Serializable {
    String response;
    String error;
    String errorCode;
    Collection<WayPointCategoryVO> wayPointsCategoriesResponse;

    public Collection<WayPointCategoryVO> getWayPointsCategoriesResponse() {
        return wayPointsCategoriesResponse;
    }

    public void setWayPointsCategoriesResponse(Collection<WayPointCategoryVO> categoryVOs) {
        this.wayPointsCategoriesResponse = categoryVOs;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (wayPointsCategoriesResponse != null) {
            builder.append("Response : " + response + " Waypoint Categories : " + wayPointsCategoriesResponse.toString());
        } else {
            builder.append("Response : " + response);
        }
        return builder.toString();
    }

}
