package com.waypals.gson.vo;

import java.io.Serializable;

public class VehicleInformationVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5118708877809039158L;

    public RideLocation location;

    public long vehicleId;

    public String state;

    public boolean isSystemFenceEnabled;

    public String registrationNumber;

    public String vehicleType;

    public boolean isImobilizer;

    public String lastServiceDate;
    public String pollutionDueDate;
    public String policyDueDate;
    public String licenceExpDate;


    public String devicePhNo;
    public String userPhNo;

    public long dateTime;

    boolean firstTimeLogin;

    public boolean isFirstTimeLogin() {
        return firstTimeLogin;
    }

    public void setFirstTimeLogin(boolean firstTimeLogin) {
        this.firstTimeLogin = firstTimeLogin;
    }


    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getDevicePhNo() {
        return devicePhNo;
    }

    public void setDevicePhNo(String devicePhNo) {
        this.devicePhNo = devicePhNo;
    }

    public String getUserPhNo() {
        return userPhNo;
    }

    public void setUserPhNo(String userPhNo) {
        this.userPhNo = userPhNo;
    }

    public String getLastServiceDate() {
        return lastServiceDate;
    }

    public void setLastServiceDate(String lastServiceDate) {
        this.lastServiceDate = lastServiceDate;
    }

    public String getPollutionDueDate() {
        return pollutionDueDate;
    }

    public void setPollutionDueDate(String pollutionDueDate) {
        this.pollutionDueDate = pollutionDueDate;
    }

    public String getPolicyDueDate() {
        return policyDueDate;
    }

    public void setPolicyDueDate(String policyDueDate) {
        this.policyDueDate = policyDueDate;
    }

    public String getLicenceExpDate() {
        return licenceExpDate;
    }

    public void setLicenceExpDate(String licenceExpDate) {
        this.licenceExpDate = licenceExpDate;
    }

    public boolean isImobilizer() {
        return isImobilizer;
    }

    public void setIsImobilizer(boolean isImobilizer) {
        this.isImobilizer = isImobilizer;
    }


    public VehicleInformationVO()
    {

    }

    public VehicleInformationVO(RideLocation location, long vehicleId, String state, boolean isSystemFenceEnabled, String registrationNumber, String vehicleType) {
        this.location = location;
        this.vehicleId = vehicleId;
        this.state = state;
        this.isSystemFenceEnabled = isSystemFenceEnabled;
        this.registrationNumber = registrationNumber;
        this.vehicleType = vehicleType;
    }

    public RideLocation getLocation() {
        return location;
    }

    public void setLocation(RideLocation location) {
        this.location = location;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isSystemFenceEnabled() {
        return isSystemFenceEnabled;
    }

    public void setSystemFenceEnabled(boolean isSystemFenceEnabled) {
        this.isSystemFenceEnabled = isSystemFenceEnabled;
    }



    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (location != null)
            builder.append("location " + location.toString());
        builder.append("vehicleId " + vehicleId);
        builder.append("isSystemFenceEnabled " + isSystemFenceEnabled);
        builder.append("state " + state);
        builder.append("Registration Number " + registrationNumber);
        return builder.toString();
    }

}
