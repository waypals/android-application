package com.waypals.gson.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RideLocation implements Serializable {

    public BigDecimal latitude;

    public BigDecimal longitude;

    public RideLocation(BigDecimal latitude, BigDecimal longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return latitude + "/" + longitude;
    }
}
