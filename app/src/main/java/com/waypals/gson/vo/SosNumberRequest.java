package com.waypals.gson.vo;

import com.waypals.objects.EmergencyContact;

import java.io.Serializable;
import java.util.List;

/**
 * Created by surya on 26/10/15.
 */
public class SosNumberRequest implements Serializable{

    private List<SosNumber> sosNumbers;

    public List<SosNumber> getSosNumbers() {
        return sosNumbers;
    }

    public void setSosNumbers(List<SosNumber> sosNumbers) {
        this.sosNumbers = sosNumbers;
    }

}
