package com.waypals.gson.vo;

import java.io.Serializable;

/**
 * Created by surya on 27/09/16.
 */
public class UserCredential implements Serializable {

    public Credential getUserCredential() {
        return userCredential;
    }

    public void setUserCredential(Credential userCredential) {
        this.userCredential = userCredential;
    }

    Credential userCredential;

}
