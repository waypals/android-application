package com.waypals.gson.vo;

public class PhoneNumber {

    private String countryCode;

    private long phoneNo;

    public String getCountryCode() {
        if (countryCode != null && !countryCode.startsWith("+")) {
            countryCode = "+" + countryCode;
        }
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public long getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo) {
        this.phoneNo = phoneNo;
    }

}
