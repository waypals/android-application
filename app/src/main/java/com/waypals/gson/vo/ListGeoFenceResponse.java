package com.waypals.gson.vo;

import java.io.Serializable;
import java.util.List;

public class ListGeoFenceResponse implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String response;

    private List<GeoFenceVO> geoFences;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public List<GeoFenceVO> getGeoFences() {
        return geoFences;
    }

    public void setGeoFences(List<GeoFenceVO> geoFences) {
        this.geoFences = geoFences;
    }
}
