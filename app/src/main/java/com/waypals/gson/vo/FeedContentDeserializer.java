package com.waypals.gson.vo;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.waypals.feedItems.Comments;
import com.waypals.feedItems.Feed;
import com.waypals.feedItems.ImageContent;
import com.waypals.feedItems.JourneyFeedContent;
import com.waypals.feedItems.Owner;

import java.lang.reflect.Type;

/**
 * Created by surya on 25/8/15.
 */
public class FeedContentDeserializer implements JsonDeserializer<Feed> {

    @Override
    public Feed deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Feed feed = new Feed();


        if(json != null && json.isJsonObject())
        {
            JsonObject jsonObject = json.getAsJsonObject();

            JsonElement feedTypeElem = jsonObject.get("type");
            Comments[] commentses = context.deserialize(jsonObject.get("comments"), Comments[].class);
            Owner owner = context.deserialize(jsonObject.get("owner"), Owner.class);
            feed.setTimeStamp(jsonObject.get("timeStamp").getAsString());
            feed.setFeedType(jsonObject.get("feedType").getAsString());
            feed.setComments(commentses);
            feed.setId(jsonObject.get("id").getAsLong());
            feed.setOwner(owner);
            if(feedTypeElem != null)
            {
                String feedType = feedTypeElem.getAsString();
                feed.setType(feedType);
                if("JOURNEY".equalsIgnoreCase(feedType))
                {
                    Log.d("Test Trail", jsonObject.get("content").toString());
                    JourneyFeedContent content = context.deserialize(jsonObject.get("content"), JourneyFeedContent.class);
                    feed.setContent(content);
                }
                else
                {
                    ImageContent content = context.deserialize(jsonObject.get("content"), ImageContent.class);
                    feed.setContent(content);
                }
            }
        }



        return feed;
    }
}
