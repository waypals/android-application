package com.waypals.gson.vo;

public class GeoFenceVO {

    private String name;

    private GeogrpahicalArea geographicalArea;

    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeogrpahicalArea getGeographicalArea() {
        return geographicalArea;
    }

    public void setGeographicalArea(GeogrpahicalArea geographicalArea) {
        this.geographicalArea = geographicalArea;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
