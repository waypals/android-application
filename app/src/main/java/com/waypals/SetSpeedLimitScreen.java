package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.waypals.exception.ExceptionHandler;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

public class SetSpeedLimitScreen extends Activity {

    private Typeface tf;
    private Button cancel, set;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settings_speedlimit);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);

        activity = this;
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(tf);
        title.setText("SET SPEED LIMIT");

        final EditText value = (EditText) findViewById(R.id.speed);
        cancel = (Button) findViewById(R.id.cancel);
        set = (Button) findViewById(R.id.set);

        Float speed = getIntent().getFloatExtra("speed", 0);
        value.setHint(speed + " KMPH");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                setResult(Activity.RESULT_CANCELED, i);
                finish();
            }
        });

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (value.getText() != null && !value.getText().toString().isEmpty() && value.getText().toString().matches("\\d+")) {
                    Intent i = new Intent();
                    i.putExtra("value", value.getText().toString());
                    setResult(Activity.RESULT_OK, i);
                    finish();
                } else {
                    try {
                        Utility.makeNewToast(activity, "Please enter digits only", new Integer(Gravity.BOTTOM));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, activity);
                    }
                }

            }
        });
    }
}
