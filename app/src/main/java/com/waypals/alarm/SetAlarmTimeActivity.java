package com.waypals.alarm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class SetAlarmTimeActivity extends Activity {

    private Button cancel;
    private Button set;
    private Button set_start_time, set_end_time;
    private TextView text_set_start, text_set_end;
    private ApplicationPreferences appPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_set_alarm_time);

        appPref = new ApplicationPreferences(this);

        text_set_start = (TextView) findViewById(R.id.alarm_start_time_text);
        text_set_end = (TextView) findViewById(R.id.end_time);

        set_start_time = (Button) findViewById(R.id.set_start_time);
        set_end_time = (Button) findViewById(R.id.set_end_time);

        set_start_time.setOnClickListener(new DatePickerListener(text_set_start, this));
        set_end_time.setOnClickListener(new DatePickerListener(text_set_end, this));

        cancel = (Button) findViewById(R.id.cancel);
        set = (Button) findViewById(R.id.set);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                setResult(Activity.RESULT_CANCELED, i);
                finish();
            }
        });

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(text_set_start.getText() == null || Utility.isStringNullEmpty(text_set_start.getText().toString())){
                    Utility.makeNewToast(SetAlarmTimeActivity.this, "Please select start time");
                    return;
                }
                else if(text_set_end.getText() == null || Utility.isStringNullEmpty(text_set_end.getText().toString())){
                    Utility.makeNewToast(SetAlarmTimeActivity.this, "Please select till time");
                    return;
                }else {
                    Intent i = new Intent();
                    i.putExtra("start", text_set_start.getText().toString());
                    i.putExtra("end", text_set_end.getText().toString());
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            }
        });

        String start = appPref.getAlarmStartTime();
        String end = appPref.getAlarmEndTime();
        if (start != null && end != null) {
            text_set_start.setText(start);
            text_set_end.setText(end);
            // text_alarm_status_title.setText(start + " till " + end);
        }
    }

    private class DatePickerListener implements View.OnClickListener {

        TextView textView;
        Activity activity;

        public DatePickerListener(TextView textView, Activity activity) {
            this.textView = textView;
            this.activity = activity;
        }


        @Override
        public void onClick(View view) {
            CustomDateTimePicker custom = new CustomDateTimePicker(true, activity, new CustomDateTimePicker.ICustomDateTimeListener() {
                @Override
                public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                  String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                  int hour24, int hour12, int min, int sec, String AM_PM) {

                    SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss a");
                    //df.setTimeZone(TimeZone.getTimeZone("IST"));
                    String set_DateTime = df.format(dateSelected);

                    DatePickerListener.this.textView.setText(set_DateTime);
                }

                @Override
                public void onCancel() {

                }

            });
            custom.set24HourFormat(false);
            /**
             * Pass Directly current data and time to show when it pop up
             */
            //custom.setDate(Calendar.getInstance());
            // custom.setDate(startDate);

            custom.showDialog();
        }
    }
}
