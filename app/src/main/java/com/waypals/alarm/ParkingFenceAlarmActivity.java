package com.waypals.alarm;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.utils.CH_Constant;

public class ParkingFenceAlarmActivity extends Activity {

    MediaPlayer mp;
    int type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_fence_alarm);

        TextView textView = (TextView) findViewById(R.id.text_id);


        Intent intent = getIntent();

        if(intent != null){
           type =  intent.getIntExtra("type", 0);
        }

        if (type == 0){
            textView.setText("Your vehicle has been started/moved.");
        }else if(type == 1){
            textView.setText("Waypals Device has been unplugged from your vehicle.");
        }

        playAlarm();

        Button stopBtn = (Button) findViewById(R.id.stop_alarm_button);
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp != null){
                    mp.stop();
                    landToApp();
                }
            }
        });
    }

    private void landToApp(){
        Intent intentLogin = new Intent(getApplicationContext(), Login_Screen.class);
        intentLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intentLogin);

        finish();
    }

    private void playAlarm(){
        try{
//            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                    + "://" + getPackageName() + "/raw/car_alarm");
            // builder.setSound(sound);
            int alert_id = R.raw.car_alert;
            if (type == 1) {
                alert_id = R.raw.unplug_alert;
            }

            mp = MediaPlayer.create(getApplicationContext(), alert_id);
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
           // mp.setDataSource(getApplicationContext(), sound);
            mp.setLooping(true);
            //mp.prepare();
            mp.start();
        }catch (Exception ex){
            Log.d("Alram", ex.getMessage());
            ExceptionHandler.handlerException(ex, this);
        }
    }
}
