package com.waypals.map;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.waypals.feedItems.RouteCoordinates;
import com.waypals.feedItems.WayPoints;
import com.waypals.utils.Utility;
import com.waypals.R;

import java.util.List;

public class TrailActivity extends FragmentActivity implements OnMapReadyCallback {

    RouteCoordinates[] routeCoordinates;
    WayPoints[] wayPointses;
    ImageView mylocImg;
    int maplayer = 1;
    Marker myPosition;
    private double x;
    private double y;
    private LatLng MY_Position;
    private String pinLoc;
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {

            updateCurrentPosition(location, "net");
            // System.out.println("Network ::" + x + y);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
                updateCurrentPosition(location,"gps");
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    boolean me_position = false;
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    private GoogleMap map;
    private boolean isClicked = false;
    private boolean canSetCenter;

    private void updateCurrentPosition(Location location, String type)
    {
        if(isClicked) {
            x = location.getLatitude();
            y = location.getLongitude();

            if (myPosition != null) {
                myPosition.remove();
            }

            pinLoc = type;
            MY_Position = new LatLng(x, y);
            myPosition = map.addMarker(new MarkerOptions()
                    .position(MY_Position)
                    .title("Me"));
//                    .icon(BitmapDescriptorFactory
  //                          .fromResource(R.drawable.blue_person)));
            updateCamera(location);
        }else {
            if (myPosition != null) {
                myPosition.remove();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trail);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TextView title = (TextView) findViewById(R.id.nav_title_txt);
        ImageView back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mylocImg = (ImageView) findViewById(R.id.img_nav);

        mylocImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isClicked = !isClicked;

                if(isClicked)
                {
                    canSetCenter = true;
                    mylocImg.setImageResource(R.drawable.nav_active_icon);
                }else
                {
                    mylocImg.setImageResource(R.drawable.nav_deactive_icon);
                }
            }
        });

        title.setText("SHARED TRAIL");
        Intent thisIntent = getIntent();
        if (thisIntent != null) {
            Bundle bundle = thisIntent.getExtras();
            Object[] coords = (Object[]) bundle.getSerializable("coordinates");
            int i = 0;
            if (coords != null) {
                routeCoordinates = new RouteCoordinates[coords.length];
                for (Object obj : coords) {
                    if (obj instanceof RouteCoordinates) {
                        routeCoordinates[i] = (RouteCoordinates) obj;
                    }
                    i++;
                }
            }
            Object[] waypoints = (Object[]) bundle.getSerializable("waypoints");
            if (waypoints != null) {
                wayPointses = new WayPoints[waypoints.length];
                i = 0;
                for (Object obj : waypoints) {
                    if (obj instanceof WayPoints) {
                        wayPointses[i] = (WayPoints) obj;
                    }
                    i++;
                }
            }
        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        gps_enabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (gps_enabled)
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0,
                locationListenerGps);
        if (network_enabled)
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0,
                    locationListenerNetwork);
    }

    public boolean isGoogleMapsInstalled() {
        try {
            ApplicationInfo info;
            info = getPackageManager().getApplicationInfo(
                    "com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (routeCoordinates != null && routeCoordinates.length >1) {
            map = googleMap;
            final List<LatLng> lstCoord = Utility.getLatLngFromCoordinates(routeCoordinates);
            Polyline line = googleMap.addPolyline(new PolylineOptions()
                    .addAll(lstCoord)
                    .width(5)
                    .color(Color.BLUE));
            final LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng lt : lstCoord) {
                builder.include(lt);
            }

            if (wayPointses != null) {
                addMarkers(googleMap, wayPointses, lstCoord.get(0), lstCoord.get(lstCoord.size() - 1));
            }

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                }
            });
        }
    }

    private void updateCamera(Location location)
    {
        if(canSetCenter) {
            final List<LatLng> lstCoord = Utility.getLatLngFromCoordinates(routeCoordinates);
            Polyline line = map.addPolyline(new PolylineOptions()
                    .addAll(lstCoord)
                    .width(5)
                    .color(Color.BLUE));
            final LatLngBounds.Builder builder = new LatLngBounds.Builder();

            if (location != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                builder.include(latLng);
            }

            for (LatLng lt : lstCoord) {
                builder.include(lt);
            }

            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                }
            });
        }
        canSetCenter = false;
    }

    private void addMarkers(GoogleMap googleMap, WayPoints[] wayPointses, LatLng start, LatLng end) {
        for (WayPoints wayPoints : wayPointses) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.title(wayPoints.getName());
            markerOptions.snippet(wayPoints.getDescription());
            markerOptions.position(Utility.getLatLngFromCoordinate(wayPoints.getCordinate()));

         if (wayPoints.getWayPointCategory() != null) {
                markerOptions.icon(Utility.getMarkerIcon(wayPoints.getWayPointCategory().getName()));
            }
            googleMap.addMarker(markerOptions);
        }

        MarkerOptions markerOptionsStart = new MarkerOptions();
        markerOptionsStart.position(start);
        markerOptionsStart.icon(BitmapDescriptorFactory.fromResource(R.drawable.a));

        MarkerOptions markerOptionsEnd = new MarkerOptions();
        markerOptionsEnd.position(end);
        markerOptionsEnd.icon(BitmapDescriptorFactory.fromResource(R.drawable.b));

        googleMap.addMarker(markerOptionsStart);
        googleMap.addMarker(markerOptionsEnd);
    }
}
