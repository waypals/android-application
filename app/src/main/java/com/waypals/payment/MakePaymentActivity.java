package com.waypals.payment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.citrus.sdk.Environment;
import com.citrus.sdk.ui.utils.CitrusFlowManager;
import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.response.OrderIdResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

public class MakePaymentActivity extends Activity implements AsyncFinishInterface {

    EditText txtAmount;
    TextView btnPay;
    ApplicationPreferences appPref;
    private ServiceResponse serviceResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment);

        appPref = new ApplicationPreferences(this);

        txtAmount = (EditText) findViewById(R.id.amount);
        btnPay = (TextView) findViewById(R.id.button_payment);

        TextView title = (TextView) findViewById(R.id.nav_title_txt);
        title.setText(getString(R.string.make_payment_text));

        ImageView back_button = (ImageView) findViewById(R.id.left_btn);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid(txtAmount.getText().toString())) {
                    String url = CH_Constant.PAYMENT_REQUEST_URL;
                    PaymentDto paymentDto = new PaymentDto();
                    paymentDto.setAmount(txtAmount.getText().toString());
                    paymentDto.setIsWeb(false);

                    Gson gson = new Gson();
                    String input = gson.toJson(paymentDto);
                    GenericAsyncService getTnxIdService = new GenericAsyncService(appPref, MakePaymentActivity.this, url, "", input,
                                                            "", MakePaymentActivity.this, true);
                    getTnxIdService.execute();
                }else {
                    Utility.makeNewToast(MakePaymentActivity.this, "Please enter amount above Rs. 100");
                }

            }
        });

        initPayment();
    }

    private boolean isValid(String amount)
    {
        try
        {
            if(!Utility.isStringNullEmpty(amount)) {
                double value = Double.parseDouble(amount);
                if (value >= 100) {
                    return true;
                }
            }
        }catch (NumberFormatException ex)
        {
            Utility.makeNewToast(this, "Error!! Please try again");
        }
        return false;
    }

    private void makePayment(String tnxId) {
        if (tnxId != null) {
            String emailId = appPref.getUserName();
            String phoneNum = appPref.getCurrentUserNumber();
            String token = appPref.getUserToken();
            CitrusFlowManager.startShoppingFlow(MakePaymentActivity.this,
                    emailId, phoneNum, txtAmount.getText().toString(), tnxId, token);
        }
    }

    private void initPayment() {
        CitrusFlowManager.initCitrusConfig(CH_Constant.SIGN_UP_ID,
                CH_Constant.SIGN_UP_SECRET_KEY, CH_Constant.SIGN_IN_ID,
                CH_Constant.SIGN_IN_SECREY_KEY,
                getResources().getColor(R.color.white), this,
                Environment.PRODUCTION, CH_Constant.VANITY, CH_Constant.BILL_GENERATOR_URL, CH_Constant.RETURN_URL);
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (!Utility.isStringNullEmpty(response)) {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (serviceResponse != null) {
            if (CH_Constant.SUCCESS.equalsIgnoreCase(serviceResponse.getResponse())) {

                String orderIdResponse = serviceResponse.getOrderId();
                if(orderIdResponse != null) {
                    makePayment(orderIdResponse);
                }
            } else {

            }
        }
    }
}
