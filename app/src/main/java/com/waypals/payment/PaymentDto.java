package com.waypals.payment;

/**
 * Created by surya on 5/1/16.
 */
public class PaymentDto {

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean isWeb() {
        return isWeb;
    }

    public void setIsWeb(boolean isWeb) {
        this.isWeb = isWeb;
    }

    String amount;
    boolean isWeb;


}
