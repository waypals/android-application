package com.waypals.feedItems;

import java.io.Serializable;


public class WayPoints implements Serializable {

    long id;

    WayPointCategory wayPointCategory;
    Cordinate cordinate;
    String description;
    Images[] images;
    String name;
    String type;

    public WayPointCategory getWayPointCategory() {
        return wayPointCategory;
    }

    public void setWayPointCategory(WayPointCategory wayPointCategory) {
        this.wayPointCategory = wayPointCategory;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Cordinate getCordinate() {
        return cordinate;
    }

    public void setCordinate(Cordinate cordinate) {
        this.cordinate = cordinate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Images[] getImages() {
        return images;
    }

    public void setImages(Images[] images) {
        this.images = images;
    }

}
