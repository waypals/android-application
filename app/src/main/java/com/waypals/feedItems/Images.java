package com.waypals.feedItems;

import java.io.Serializable;


public class Images implements Serializable {

    private Long id;
    private String imageLink;
    private String imageName;
    private String imageType;
    private String byteImage;

    public String getByteImage() {
        return byteImage;
    }

    public void setByteImage(String byteImage) {
        this.byteImage = byteImage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
