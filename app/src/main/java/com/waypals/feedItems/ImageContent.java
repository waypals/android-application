package com.waypals.feedItems;

/**
 * Created by surya on 20/8/15.
 */
public class ImageContent extends Content{

    Images images[];

    public Images[] getImages() {
        return images;
    }

    public void setImages(Images[] images) {
        this.images = images;
    }

}
