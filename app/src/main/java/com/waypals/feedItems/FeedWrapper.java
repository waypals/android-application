package com.waypals.feedItems;

import java.io.Serializable;
import java.util.HashMap;

public class FeedWrapper implements Serializable {
    /*Response*/
    Feed feed;
    String feedUpdatedTime;
    String logTime;
    String description;
    FeedUsers feedUsers;

    /*going to be unused in future all in below*/
    HashMap<String, String> imageHexString;
    JourneyFeedContent journeyFeedContent;
    LocationFeedContent locationFeedContent;
    Long[] friendIds;

    public String getFeedUpdatedTime() {
        return feedUpdatedTime;
    }

    public void setFeedUpdatedTime(String feedUpdatedTime) {
        this.feedUpdatedTime = feedUpdatedTime;
    }

    public Long[] getFriendIds() {
        return friendIds;
    }

    public void setFriendIds(Long[] friendIds) {
        this.friendIds = friendIds;
    }

    public JourneyFeedContent getJourneyFeedContent() {
        return journeyFeedContent;
    }

    public void setJourneyFeedContent(JourneyFeedContent journeyFeedContent) {
        this.journeyFeedContent = journeyFeedContent;
    }

    public LocationFeedContent getLocationFeedContent() {
        return locationFeedContent;
    }

    public void setLocationFeedContent(LocationFeedContent locationFeedContent) {
        this.locationFeedContent = locationFeedContent;
    }

    public HashMap<String, String> getImageHexString() {
        return imageHexString;
    }

    public void setImageHexString(HashMap<String, String> imageHexString) {
        this.imageHexString = imageHexString;
    }

    public FeedUsers getFeedUsers() {
        return feedUsers;
    }

    public void setFeedUsers(FeedUsers feedUsers) {
        this.feedUsers = feedUsers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    @Override
    public String toString() {
        return "FeedWrapper{" +
                "feed=" + feed.getContent() +
                ", logTime='" + logTime + '\'' +
                ", description='" + description + '\'' +
                ", feedUsers=" + feedUsers +
                ", imageHexString=" + imageHexString +
                ", journeyFeedContent=" + journeyFeedContent +
                ", locationFeedContent=" + locationFeedContent +
                '}';
    }
}







