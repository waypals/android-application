package com.waypals.feedItems;

import com.waypals.utils.Utility;

import java.io.Serializable;


public class Name implements Serializable {

    Long id;
    String middleName;
    String lastName;
    String firstName;
    String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFullName() {
        return Utility.getInitCaps(firstName) + " " + Utility.getInitCaps(lastName);
    }
}

