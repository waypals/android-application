package com.waypals.feedItems;

import java.io.Serializable;

public class Comments implements Serializable {
    Long id;
    CommentedBy commentedBy;
    String comment;
    String dateTime;

    public CommentedBy getCommentedBy() {
        return commentedBy;
    }

    public void setCommentedBy(CommentedBy commentedBy) {
        this.commentedBy = commentedBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}