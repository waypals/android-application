package com.waypals.feedItems;

import java.io.Serializable;

/**
 * Created by shantanu on 30/10/14.
 */
public class Content implements Serializable {
   public Long id;
   public String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
