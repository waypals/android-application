package com.waypals.feedItems;


public class JourneyFeedContent extends Content {

      WayPoints[] waypoints;
      RouteCoordinates[] routeCoordinates;
      String caption;
//
      Images trailImage;

    public Images getTrailImage() {
        return trailImage;
    }

    public void setTrailImage(Images trailImage) {
        this.trailImage = trailImage;
    }

    public WayPoints[] getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(WayPoints[] waypoints) {
        this.waypoints = waypoints;
    }

    public RouteCoordinates[] getRouteCoordinates() {
        return routeCoordinates;
    }

    public void setRouteCoordinates(RouteCoordinates[] routeCoordinates) {
        this.routeCoordinates = routeCoordinates;
    }
//
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
