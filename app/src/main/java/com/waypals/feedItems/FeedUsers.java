package com.waypals.feedItems;

import java.io.Serializable;


public class FeedUsers implements Serializable {
    Long id;
    Long feedUsersId;
    SharedBy sharedBy;
    SharedWith[] sharedWith;

    public SharedBy getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(SharedBy sharedBy) {
        this.sharedBy = sharedBy;
    }

    public SharedWith[] getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(SharedWith[] sharedWith) {
        this.sharedWith = sharedWith;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFeedUsersId() {
        return feedUsersId;
    }

    public void setFeedUsersId(Long feedUsersId) {
        this.feedUsersId = feedUsersId;
    }
}

