package com.waypals.feedItems;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;


public class Friend implements Parcelable, Serializable {

    public static final Parcelable.Creator<Friend> CREATOR
            = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };
    private String firstName;
    private String lastName;
    private String email;
    private String friendConsumerId;
    private String userId;

    private Friend(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        userId = in.readString();
    }

    public Friend() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFriendConsumerId() {
        return friendConsumerId;
    }

    public void setFriendConsumerId(String friendConsumerId) {
        this.friendConsumerId = friendConsumerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(userId);
    }

}
