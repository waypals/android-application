package com.waypals;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.MyShareLocationRequestsAdapter;
import com.waypals.objects.MyShareLocationResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.List;

/**
 * Created by shantanu on 18/10/14.
 */
public class MyShareLocationRequestsScreen extends Activity implements AsyncFinishInterface {

    TextView title;
    Typeface tf;
    Activity activity;
    Context context;
    ImageView back_btn, right_btn;
    ApplicationPreferences appPref;
    private String tag = "My Share location Requests Screen";
    private View loading;
    private ListView listView;
    private AsyncFinishInterface asyncFinishInterface;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("RELOAD")) {
                GenericAsyncService service = new GenericAsyncService(appPref, activity, CH_Constant.MY_SHARELOCATION_REQUEST, "MY_REQUESTS", "{}", tag, asyncFinishInterface, true);
                service.execute();
            }
            if (action.equals("OPEN_MAP")) {

            }
        }
    };
    private String method;
    private ServiceResponse serviceResponse;
    private MyShareLocationRequestsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysharelocation_requests);
        context = this;
        activity = this;
        appPref = new ApplicationPreferences(context);
        title = (TextView) findViewById(R.id.nav_title_txt);
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        asyncFinishInterface = this;
        Typeface tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);
        title.setTypeface(tf);
        title.setText("SHARE LOCATION");

        listView = (ListView) findViewById(R.id.listView);
        loading = getLoadingView();
        listView.addHeaderView(loading);
        adapter = new MyShareLocationRequestsAdapter(this);
        listView.setAdapter(adapter);


        Log.i(tag, "Load my requests data");
        GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.MY_SHARELOCATION_REQUEST, "MY_REQUESTS", "{}", tag, asyncFinishInterface, true);
        service.execute();

        IntentFilter filter = new IntentFilter();
        filter.addAction("RELOAD");
        filter.addAction("OPEN_MAP");
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private View getLoadingView() {
        View view = LayoutInflater.from(this).inflate(R.layout.loading, null);
        return view;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        if (method.equals("MY_REQUESTS")) {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if (loading != null) {
            listView.removeHeaderView(loading);
        }

        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())))  {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, getString(R.string.server_error));
            }

            return;
        }

        if (method.equals("MY_REQUESTS")) {
            if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                if (serviceResponse.getErrorCode() == null) {
                    adapter.clear();
                    MyShareLocationResponse myShareLocationResponse = new MyShareLocationResponse();
                    myShareLocationResponse.setName("All");
                    adapter.add(myShareLocationResponse);

                    List<MyShareLocationResponse> list = serviceResponse.getMyrequests();
                    for (MyShareLocationResponse m : list) {
                        adapter.add(m);
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    if (serviceResponse.getErrorCode().equals("NO_DATA")) {
                        MyShareLocationResponse m = new MyShareLocationResponse();
                        m.setName("You do not have any requests");
                        adapter.add(m);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }
}
