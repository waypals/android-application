package com.waypals;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.ContactsCompletionView;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Friend;
import com.waypals.request.FollowMeInviteRequest;
import com.waypals.request.TimeRange;
import com.waypals.response.AcceptedFriendResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

public class FollowMeAddRequestFragment extends Fragment implements AsyncFinishInterface, TokenCompleteTextView.TokenListener {

    private static String startime = null, endtime = null;
    private static Date startDate, endDate;
    ContactsCompletionView completionView;
    Friend[] friends;
    ArrayAdapter<Friend> adapter;
    Button back_btn, actionButton;
    TextView title;
    String tag = "Follow me invite screen";
    ArrayList<Friend> selectedFriends = new ArrayList<Friend>();
    EditText message;
    Activity context;
    String method;
    String content;
    ServiceResponse serviceResponse;
    AcceptedFriendResponse friendResponse;
    AsyncFinishInterface asyncFinishInterface;

    private OnFragmentInteractionListener mListener;
    private ApplicationPreferences appPrefs;
    private TextView startTimeTextView, endTimeTextView;
    private CustomDateTimePicker custom;
    private View myview;

    public FollowMeAddRequestFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myview = inflater.inflate(R.layout.fragment_follow_me_add_request, container, false);


        context = getActivity();
        asyncFinishInterface = this;

        actionButton = (Button) myview.findViewById(R.id.postButton);
        back_btn = (Button) myview.findViewById(R.id.left_btn);

        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                CH_Constant.Font_Typface_Path);

        message = (EditText) myview.findViewById(R.id.feedEditText);

        appPrefs = new ApplicationPreferences(getActivity());


        startTimeTextView = (TextView) myview.findViewById(R.id.starttime);
        endTimeTextView = (TextView) myview.findViewById(R.id.endtime);
        try {
            startTimeTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    custom = new CustomDateTimePicker(context, new CustomDateTimePicker.ICustomDateTimeListener() {
                        @Override
                        public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                          String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                          int hour24, int hour12, int min, int sec, String AM_PM) {
                            Log.d("Selected Date:", dateSelected.toString());
                            SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
                            SimpleDateFormat dfTime = new SimpleDateFormat("hh:mm a");
                            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                            String set_DateTime = Utility.convertToString(dateSelected, TimeZone.getTimeZone("UTC"));
                            Log.d("Selected Date:", set_DateTime);
                           // startTimeTextView = (TextView) myview.findViewById(R.id.starttime);

                            System.out.println("Formated Date :: " + set_DateTime);
                            startDate = dateSelected;
                            startime = set_DateTime;
                            String sDateTime = (dfDate.format(dateSelected)+"\n"+dfTime.format(dateSelected)).toUpperCase();
                            startTimeTextView.setText(sDateTime);
                        }

                        @Override
                        public void onCancel() {

                        }


                    });
                    custom.set24HourFormat(false);
                    /**
                     * Pass Directly current data and time to show when it pop up
                     */
                    custom.setDate(Calendar.getInstance());

                    custom.showDialog();
                }
            });


            endTimeTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    custom = new CustomDateTimePicker(context, new CustomDateTimePicker.ICustomDateTimeListener() {
                        @Override
                        public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                          String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                          int hour24, int hour12, int min, int sec, String AM_PM) {

                            SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
                            SimpleDateFormat dfTime = new SimpleDateFormat("hh:mm a");
                            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
                            String set_DateTime = Utility.convertToString(dateSelected, TimeZone.getTimeZone("UTC"));

                            //endTimeTextView = (TextView) myview.findViewById(R.id.endtime);

                            System.out.println("Formatted Date :: " + set_DateTime);

                            endtime = set_DateTime;
                            endDate = dateSelected;
                            String sDateTime = (dfDate.format(dateSelected)+"\n"+dfTime.format(dateSelected)).toUpperCase();
                            endTimeTextView.setText(sDateTime);
                        }

                        @Override
                        public void onCancel() {

                        }


                    });

                    /**
                     * Pass Directly current time format it will return AM and PM if you set
                     * false
                     */
                    custom.set24HourFormat(false);
                    /**
                     * Pass Directly current data and time to show when it pop up
                     */
                    custom.setDate(Calendar.getInstance());

                    custom.showDialog();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        GenericAsyncService service = new GenericAsyncService(appPrefs, getActivity(), CH_Constant.GET_FRIEND_LIST, "listacceptedfriends", "", tag, this, false);
        service.execute();

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                message = (EditText) myview.findViewById(R.id.feedEditText);
                final String text = message.getText().toString();

                if (selectedFriends == null) {
                    selectedFriends = new ArrayList<Friend>();
                }

                try {

                    if (startime == null) {
                        try {
                            Utility.makeNewToast(context, "Please select start time");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                    if (endtime == null) {
                        try {
                            Utility.makeNewToast(context, "Please select end time");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return;
                    } else {
                        Date d = new Date();
                        if (startDate.before(d)) {
                            try {
                                Utility.makeNewToast(context, "Past date/time is not allowed");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return;
                        }
                        if (endDate.before(startDate)) {
                            try {
                                Utility.makeNewToast(context, "End time must be greater than start time");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return;
                        }
                    }


                    if (startDate.after(endDate)) {
                        try {
                            Utility.makeNewToast(context, "Start time must be lesser than end time");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return;
                    }

                    if (selectedFriends == null || selectedFriends.size() < 1) {
                        try {
                            Utility.makeNewToast(context, getString(R.string.inp_select_friend));
                            return;
                        } catch (Exception e) {
                            ExceptionHandler.handlerException(e, context);
                            return;
                        }
                    }

                    if (text.isEmpty()) {
                        try {
                            Utility.makeNewToast(context, getString(R.string.inp_alert_message));
                            return;
                        } catch (Exception e) {
                            ExceptionHandler.handlerException(e, context);
                            return;
                        }
                    }

                    Set<Long> users = new HashSet<Long>();

                    for (Friend f : selectedFriends) {
                        users.add(Long.parseLong(f.getUserId()));
                    }

                    TimeRange range = new TimeRange();
                    range.setStartTime(startime);
                    range.setEndTime(endtime);

                    Log.d("Start Date ::", startime);
                    Log.d("End Date ::",endtime);

                    FollowMeInviteRequest request = new FollowMeInviteRequest();
                    request.setRange(range);
                    request.setFollowWith(users);
                    request.setVehicleId(appPrefs.getVehicle_Selected());

                    Gson gson = new Gson();
                    String input = gson.toJson(request);
                    Log.d("Request Is: ", input);

                    GenericAsyncService service = new GenericAsyncService(appPrefs, context, CH_Constant.FOLLOWME_INVITE, "INVITE", input, tag, asyncFinishInterface, true);
                    service.execute();

                    selectedFriends.clear();
                    message.setText("");
                    completionView.clear();
                } catch (Throwable e) {
                    ExceptionHandler.handleException(e);
                }

            }
        });
        return myview;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void finish(String response, String method) {

        try {
            this.method = method;
            Gson gson = new Gson();
            if (method != null && method.endsWith("listacceptedfriends")) {
                friendResponse = gson.fromJson(response, AcceptedFriendResponse.class);

                if (friendResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    friends = friendResponse.getAcceptedFriendsResult();
                }
                Log.d(tag, friendResponse.toString());
            }
            if (method != null && method.equals("INVITE")) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        } catch (Throwable ex) {
            ExceptionHandler.handleException(ex);
        }

    }

    public void createFriendsList(Friend[] friends) {
        try {
            if (friends != null) {
                adapter = new FilteredArrayAdapter<Friend>(context, R.layout.friend_list_row, friends) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {

                            LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                            convertView = (View) l.inflate(R.layout.friend_list_row, parent, false);
                        }

                        Friend p = getItem(position);
                        ((TextView) convertView.findViewById(R.id.name)).setText(p.getFirstName() + " " + p.getLastName());
                        ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());

                        return convertView;
                    }

                    @Override
                    protected boolean keepObject(Friend obj, String mask) {
                        mask = mask.toLowerCase();
                        if (!selectedFriends.contains(obj)) {
                            return obj.getFirstName().toLowerCase().contains(mask) || obj.getLastName().toLowerCase().contains(mask) || obj.getEmail().toLowerCase().contains(mask);
                        } else
                            return false;
                    }
                };


                completionView = (ContactsCompletionView) myview.findViewById(R.id.contacts);
                completionView.setDuplicateParentStateEnabled(false);
                completionView.setAdapter(adapter);
                completionView.setThreshold(1);
                completionView.setTokenListener(this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            } else {
                try {
                    Utility.makeNewToast(context, getString(R.string.err_no_friends));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, context);
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, context);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        // TODO Auto-generated method stub
        try {
            if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) || (friendResponse != null && "ERROR".equalsIgnoreCase(
                    friendResponse.getResponse()))) {
                String errorMessage = null;
                if(serviceResponse != null) {
                    errorMessage = serviceResponse.getErrorCode();
                }else if(friendResponse != null)
                {
                    errorMessage = friendResponse.getErrorCode();
                }
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(getActivity(),
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(getActivity(), Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(getActivity(), "Server not responding, please try after some time!!");
                }

                return;
            }


            if (method != null && method.endsWith("listacceptedfriends")) {
                if (friendResponse != null && "SUCCESS".equalsIgnoreCase(friendResponse.getResponse())) {
                    createFriendsList(friends);
                } else {
                    try {
                        Utility.makeNewToast(context, CH_Constant.ERROR_Msg(friendResponse.getErrorCode()));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                }

            }
            if (method != null && method.equals("INVITE")) {
                if (serviceResponse != null) {
                    if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                        Utility.makeNewToast(context, getString(R.string.op_alert_followme_invite_success));
                        startTimeTextView.setText("");
                        startime = null;
                        endtime = null;

                        endTimeTextView.setText("");
                    } else {
                        Utility.makeNewToast(context, getString(R.string.op_alert_followme_invite_failed));
                    }
                }
                selectedFriends.clear();

            }
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex, context);
        }
    }

    @Override
    public void onTokenAdded(Object token) {
        if (selectedFriends == null) {
            selectedFriends = new ArrayList<Friend>();
        }

        if (!selectedFriends.contains((Friend) token)) {
            selectedFriends.add((Friend) token);
        }
    }

    @Override
    public void onTokenRemoved(Object token) {
        if (selectedFriends != null && !selectedFriends.isEmpty() && selectedFriends.contains((Friend) token)) {
            selectedFriends.remove((Friend) token);
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
