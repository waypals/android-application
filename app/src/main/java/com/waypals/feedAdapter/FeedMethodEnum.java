package com.waypals.feedAdapter;

public enum FeedMethodEnum {
    NEW_FEEDS, MY_FEEDS, DELETE_FEED, MY_MESSAGES, DELETE_MESSAGE, NEW_MESSAGE
}
