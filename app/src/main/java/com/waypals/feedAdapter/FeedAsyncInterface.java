package com.waypals.feedAdapter;

public interface FeedAsyncInterface {
    public void finish(String response, FeedMethodEnum method, String requestFrom, Boolean showNotification) throws Exception;

    public void onPostExecute(Void Result);
}
