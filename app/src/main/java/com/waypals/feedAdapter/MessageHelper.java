package com.waypals.feedAdapter;

import com.waypals.NavigationDrawer;
import com.waypals.request.Pagination;


public abstract class MessageHelper extends NavigationDrawer {
    public boolean requestExists;

    public abstract void checkNewMessage(boolean overrideShowNotification, String requestFrom);

    public abstract void loadNewMessage();

    public abstract void loadOldMessage();

    public abstract void getMessages();

    public abstract void loadCachedMessage();

    public abstract Pagination getPaginator();

    public abstract void cleanLoad(boolean overrideNotification);
}
