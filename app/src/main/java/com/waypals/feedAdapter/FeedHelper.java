package com.waypals.feedAdapter;

import android.support.v4.app.FragmentActivity;

import com.waypals.request.Pagination;

public abstract class FeedHelper extends FragmentActivity {
    public boolean requestExists;

    public abstract void checkNewFeeds(boolean overrideShowNotification, String requestFrom);

    public abstract void loadNewFeeds();

    public abstract void loadOldFeeds();

    public abstract void getFeeds();

    public abstract void loadCachedFeeds();

    public abstract Pagination getPaginator();

    public abstract void cleanLoad(boolean overrideNotification);
}
