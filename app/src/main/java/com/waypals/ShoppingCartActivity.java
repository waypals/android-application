package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.Json.ObdJson;
import com.waypals.fragments.CartItemFragment;
import com.waypals.fragments.OnlineItemRecyclerViewAdapter;
import com.waypals.fragments.dummy.DummyContent;
import com.waypals.response.Product;
import com.waypals.response.ProductResponse;
import com.waypals.response.ProductWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ShoppingCartActivity extends FragmentActivity implements CartItemFragment.OnListFragmentInteractionListener, AsyncFinishInterface{

    private Button addToCartButton;
    private EditText couponText;
    private TextView couponMessage;
    private TextView subTotalText, couponAmount, totalAmount;
    private List<ProductWrapper> items;
    private ServiceResponse serviceResponse;
    private LinearLayout lytContinue;
    private ApplicationPreferences appPref;
    float discount = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);

        ImageView back = (ImageView) findViewById(R.id.left_btn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView text = (TextView) findViewById(R.id.nav_title_txt);
        text.setText("SHOPPING CART");

        appPref = new ApplicationPreferences(this);

        couponText = (EditText) findViewById(R.id.coupon_text);
        couponMessage = (TextView) findViewById(R.id.coupon_message);
        addToCartButton = (Button) findViewById(R.id.apply_coupon_button);

        subTotalText = (TextView) findViewById(R.id.sub_total_amount);
        couponAmount = (TextView) findViewById(R.id.coupon_amount);
        totalAmount = (TextView) findViewById(R.id.total_amount);

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if("Remove".equals(addToCartButton.getText())) {
                    addToCartButton.setText("Apply");
                    couponText.setText("");
                    couponText.setEnabled(true);
                    discount = 0;
                    updateUI();
                }
                else
                {
                    if (couponText.getText() != null) {
                        if (couponText.getText().toString().isEmpty()) {
                            Utility.makeNewToast(ShoppingCartActivity.this, "Please provide valid coupon code");
                        } else {
                            applyCouponCode(couponText.getText().toString());
                        }
                    } else {
                        Utility.makeNewToast(ShoppingCartActivity.this, "Please provide valid coupon code");
                    }
                }
            }
        });

        lytContinue = (LinearLayout) findViewById(R.id.continue_layout);
        lytContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShoppingCartActivity.this, AddressActivity.class);
                intent.putExtra("Amount", getTotalAmount());

                if (items == null || items.isEmpty()){
                    Utility.makeNewToast(ShoppingCartActivity.this, "Your cart is empty, please add items and proceed");
                    appPref.setNoItemsInCart(0);
                    finish();
                }else {

                    if (items != null && items.size() > 0) {
                        List<String> lstPCodes = new ArrayList<String>();
                        for (ProductWrapper p : items) {
                            for (int i = 0; i < p.getItemInCart(); i++) {
                                lstPCodes.add(p.getProduct().getProductCode());
                            }
                        }

                        String[] pCodes = new String[lstPCodes.size()];
                        lstPCodes.toArray(pCodes);
                        intent.putExtra("pcodes", pCodes);
                        intent.putExtra("couponCode", couponText.getText().toString());
                    }

                    startActivity(intent);
                }
            }
        });

        Intent cart = getIntent();

        if (cart != null) {
            Bundle extras = cart.getExtras();

            if (extras != null) {

                Set<String> strings = extras.keySet();
                Iterator<String> iterator = strings.iterator();

                List<ProductWrapper> lstProducts = new ArrayList<>();

                while (iterator.hasNext()) {
                    String next = iterator.next();
                    Object o = extras.get(next);
                    if (o instanceof ProductWrapper) {
                        lstProducts.add((ProductWrapper) o);
                    }
                }

                updatePrice(lstProducts);
                updateUI();
            }
        }
    }

    private float getTotalAmount() {

        float amount = 0;
        try {
            amount = Float.parseFloat(totalAmount.getText().toString());
        }catch (NumberFormatException ex){

        }finally {
            return amount;
        }
    }

    private void applyCouponCode(String code){
        GenericAsyncService getTnxIdService = new GenericAsyncService(appPref, this, CH_Constant.SERVER+CH_Constant.COUPON_CODE+code, "", "",
                "", this, true);
        getTnxIdService.execute();
    }

    @Override
    public void onListFragmentInteraction(List<ProductWrapper> item) {
       updatePrice(item);
        updateUI();
    }

    private void updatePrice(List<ProductWrapper> items){

        this.items = items;
    }

    private void updateUI(){
        float subTotal = 0.0f;

        if (items == null || items.isEmpty()){
            appPref.setNoItemsInCart(0);
            Utility.makeNewToast(ShoppingCartActivity.this, "Your cart is empty, please add items and proceed");
            finish();
        }else {

            if (items != null) {
                for (ProductWrapper product : items) {
                    subTotal += product.getProduct().getProductMetaData().getPrice() * product.getItemInCart();
                }
            }

            if (subTotalText != null) {
                subTotalText.setText("" + subTotal);
            }

            float dis = 0;
            if (couponAmount != null) {
                dis = (subTotal * discount) / 100;
            }

            if (couponAmount != null) {
                couponAmount.setText("" + dis);
            }

            float total = subTotal - dis;

            if (totalAmount != null) {
                totalAmount.setText("" + total);
            }
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (!Utility.isStringNullEmpty(response)) {
            Gson gson = new Gson();
            Log.d("Raw Response", response);
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (serviceResponse != null) {
            if (CH_Constant.SUCCESS.equalsIgnoreCase(serviceResponse.getResponse())) {
                    this.discount = serviceResponse.getDiscount();
                    couponText.setEnabled(false);
                    addToCartButton.setText("Remove");
                    updateUI();
            } else {

            }
        }
    }
}
