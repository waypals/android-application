package com.waypals.Beans;

import com.waypals.Incident_Detail_Screen;

import java.io.Serializable;

public class IncidentAlerts_Beans implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String Damages;
    String Description;
    String ReportingTime;
    String IncidentTime;
    String Event_type;
    String Event_Id;
    String IncidentId;
    String VehicleId;
    String TicketNumber;
    String VehicleGroup;
    String ReferenceNumber;
    String ReportedLocation;
    String Driver;
    Incident_Detail_Screen.IncidentImages[] incidentImages;
    /*String meta_manufacturingDate;
    String meta_registrationDate;
    String meta_id;
    String meta_color;
    String meta_fuelType;
    String meta_vinNumber;
    String meta_vehicleName;
    String meta_manufacturer;
    String meta_policyNumber;
    String meta_chasisNumber;*/
    String meta_registrationNo;

    String DP_Lat;
    String DP_Long;

    public Incident_Detail_Screen.IncidentImages[] getIncidentImages() {
        return incidentImages;
    }

    public void setIncidentImages(Incident_Detail_Screen.IncidentImages[] incidentImages) {
        this.incidentImages = incidentImages;
    }

    public String getDamages() {
        return Damages;
    }

    public void setDamages(String Damages) {
        this.Damages = Damages;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getReportingTime() {
        return ReportingTime;
    }

    public void setReportingTime(String ReportingTime) {
        this.ReportingTime = ReportingTime;
    }

    public String getDP_Lat() {
        return DP_Lat;
    }

    public void setDP_Lat(String DP_Lat) {
        this.DP_Lat = DP_Lat;
    }

    public String getDP_Long() {
        return DP_Long;
    }

    public void setDP_Long(String DP_Long) {
        this.DP_Long = DP_Long;
    }

    public String getEvent_type() {
        return Event_type;
    }

    public void setEvent_type(String Event_type) {
        this.Event_type = Event_type;
    }

    public String getEvent_Id() {
        return Event_Id;
    }

    public void setEvent_Id(String Event_Id) {
        this.Event_Id = Event_Id;
    }

    public String getIncidentTime() {
        return IncidentTime;
    }

	/*public String getMeta_ManufacturingDate() 
    {
		return meta_manufacturingDate;
	}
	
	public String getMeta_RegistrationDate() 
	{
		return meta_registrationDate;
	}
	
	public String getMeta_id() 
	{
		return meta_id;
	}
	
	public String getMeta_Color() 
	{
		return meta_color;
	}
	
	public String getMeta_FuelType() 
	{
		return meta_fuelType;
	}
	
	public String getMeta_VinNumber() 
	{
		return meta_vinNumber;
	}
	
	public String getMeta_VehicleName() 
	{
		return meta_vehicleName;
	}
	
	public String getMeta_Manufacturer() 
	{
		return meta_manufacturer;
	}
	
	public String getMeta_PolicyNumber() 
	{
		return meta_policyNumber;
	}
	
	public String getMeta_ChasisNumber() 
	{
		return meta_chasisNumber;
	}*/

    public void setIncidentTime(String IncidentTime) {
        this.IncidentTime = IncidentTime;
    }

    public String getVehicleGroup() {
        return VehicleGroup;
    }

    public void setVehicleGroup(String VehicleGroup) {
        this.VehicleGroup = VehicleGroup;
    }

    public String getIncidentId() {
        return IncidentId;
    }

    public void setIncidentId(String IncidentId) {
        this.IncidentId = IncidentId;
    }

    public String getVehicleId() {
        return VehicleId;
    }

    public void setVehicleId(String VehicleId) {
        this.VehicleId = VehicleId;
    }

    public String getTicketNumber() {
        return TicketNumber;
    }

    public void setTicketNumber(String TicketNumber) {
        this.TicketNumber = TicketNumber;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String ReferenceNumber) {
        this.ReferenceNumber = ReferenceNumber;
    }

    public String getReportedLocation() {
        return ReportedLocation;
    }

    public void setReportedLocation(String ReportedLocation) {
        this.ReportedLocation = ReportedLocation;
    }

    public String getDriver() {
        return Driver;
    }

    public void setDriver(String Driver) {
        this.Driver = Driver;
    }

    public String getMeta_RegistrationNo() {
        return meta_registrationNo;
    }

    /*	public void setMeta_ManufacturingDate(String meta_manufacturingDate)
        {
            this.meta_manufacturingDate = meta_manufacturingDate;
        }

        public void setMeta_RegistrationDate(String meta_registrationDate)
        {
            this.meta_registrationDate = meta_registrationDate;
        }

        public void setMeta_id(String meta_id)
        {
            this.meta_id = meta_id;
        }

        public void setMeta_Color(String meta_color)
        {
            this.meta_color = meta_color;
        }

        public void setMeta_FuelType(String meta_fuelType)
        {
            this.meta_fuelType = meta_fuelType;
        }

        public void setMeta_VinNumber(String meta_vinNumber)
        {
            this.meta_vinNumber = meta_vinNumber;
        }

        public void setMeta_VehicleName(String meta_vehicleName)
        {
            this.meta_vehicleName = meta_vehicleName;
        }

        public void setMeta_Manufacturer(String meta_manufacturer)
        {
            this.meta_manufacturer = meta_manufacturer;
        }

        public void setMeta_PolicyNumber(String meta_policyNumber)
        {
            this.meta_policyNumber = meta_policyNumber;
        }

        public void setMeta_ChasisNumber(String meta_chasisNumber)
        {
            this.meta_chasisNumber = meta_chasisNumber;
        }
        */
    public void setMeta_RegistrationNo(String meta_registrationNo) {
        this.meta_registrationNo = meta_registrationNo;
    }


}
