package com.waypals.Beans;

public class GeofenceList_Beans {
    String Geofence_Id;
    String Geofence_Name;
    String AllowedDeviation;

    String GraphArea_Id;
    String GraphArea_LeftTop;
    String GraphArea_RightBottom;
    String GraphArea_Radius;
    String GraphArea_Center_Lat;
    String GraphArea_Center_Long;


    public String getGeofence_Id() {
        return Geofence_Id;
    }

    public void setGeofence_Id(String Geofence_Id) {
        this.Geofence_Id = Geofence_Id;
    }

    public String getGeofence_Name() {
        return Geofence_Name;
    }

    public void setGeofence_Name(String Geofence_Name) {
        this.Geofence_Name = Geofence_Name;
    }

    public String getAllowedDeviation() {
        return AllowedDeviation;
    }

    public void setAllowedDeviation(String AllowedDeviation) {
        this.AllowedDeviation = AllowedDeviation;
    }

    public String getGraphArea_Id() {
        return GraphArea_Id;
    }

    public void setGraphArea_Id(String GraphArea_Id) {
        this.GraphArea_Id = GraphArea_Id;
    }

    public String getGraphArea_LeftTop() {
        return GraphArea_LeftTop;
    }

    public void setGraphArea_LeftTop(String GraphArea_LeftTop) {
        this.GraphArea_LeftTop = GraphArea_LeftTop;
    }

    public String getGraphArea_RightBottom() {
        return GraphArea_RightBottom;
    }

    public void setGraphArea_RightBottom(String GraphArea_RightBottom) {
        this.GraphArea_RightBottom = GraphArea_RightBottom;
    }

    public String getGraphArea_Radius() {
        return GraphArea_Radius;
    }

    public void setGraphArea_Radius(String GraphArea_Radius) {
        this.GraphArea_Radius = GraphArea_Radius;
    }

    public String getGraphArea_Center_Lat() {
        return GraphArea_Center_Lat;
    }

    public void setGraphArea_Center_Lat(String GraphArea_Center_Lat) {
        this.GraphArea_Center_Lat = GraphArea_Center_Lat;
    }

    public String getGraphArea_Center_Long() {
        return GraphArea_Center_Long;
    }

    public void setGraphArea_Center_Long(String GraphArea_Center_Long) {
        this.GraphArea_Center_Long = GraphArea_Center_Long;
    }


}


