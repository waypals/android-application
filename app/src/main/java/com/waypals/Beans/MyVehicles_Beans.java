package com.waypals.Beans;

import java.util.List;

public class MyVehicles_Beans {                                            // ad = Associated Device; meta = metadata
    long vehicleId;
    long vehicleStateId;
    long groupId;
    String groupColor;
    String groupName;
    //	String vehicleMotionState;
    String systemFenceEnabled;
    String systemFenceState;
    String vehicleMotionState;
    String dateTime;
    String gpsSpeed;
    String longitude;
    String latitude;
    String logTime;
    String obd;
    String applicableConstraintRules;
    String meta_manufacturingDate;
    String meta_registrationDate;
    String meta_color;
    String meta_fuelType;
    String meta_vinNumber;
    String meta_vehicleName;
    String meta_manufacturer;
    String meta_policyNumber;
    String meta_chasisNumber;
    String meta_registrationNo;
    String ad_msisdn;
    String ad_imei;
    String ad_serialNumber;
    String ad_simId;
    String ad_memCapacity;
    String ad_macAddress;
    String ad_modemSwVersion;
    String ad_manufacturingDate;
    String ad_registrationDate;
    String ad_modelnumber;
    String ad_manufacturingLocation;
    String ad_manufacturer;
    String ad_firmwareVersion;
    String ad_imsi;
    String ad_hardwareVersion;
    String ad_primaryLanguageSettings;

    Long[] grp_vhcl_Ids;
    List<ConstraintAlerts_Beans> Alerts_list;
    List<IncidentAlerts_Beans> Inci_Alerts_list;

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getVehicleStateId() {
        return vehicleStateId;
    }

    public void setVehicleStateId(long vehicleStateId) {
        this.vehicleStateId = vehicleStateId;
    }

    public String getGroupColor() {
        return groupColor;
    }

    public void setGroupColor(String groupColor) {
        this.groupColor = groupColor;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSystemFenceEnabled() {
        return systemFenceEnabled;
    }

    public void setSystemFenceEnabled(String systemFenceEnabled) {
        this.systemFenceEnabled = systemFenceEnabled;
    }

    public String getSystemFenceState() {
        return systemFenceState;
    }

    public void setSystemFenceState(String systemFenceState) {
        this.systemFenceState = systemFenceState;
    }

    public String getVehicleMotionState() {
        return vehicleMotionState;
    }

    public void setVehicleMotionState(String vehicleMotionState) {
        this.vehicleMotionState = vehicleMotionState;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getGPS_Speed() {
        return gpsSpeed;
    }

    public void setGPS_Speed(String gpsSpeed) {
        this.gpsSpeed = gpsSpeed;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getObd() {
        return obd;
    }

    public void setObd(String obd) {
        this.obd = obd;
    }

    public String getApplicableConstraintRules() {
        return applicableConstraintRules;
    }

    public void setApplicableConstraintRules(String applicableConstraintRules) {
        this.applicableConstraintRules = applicableConstraintRules;
    }

    public String getMeta_ManufacturingDate() {
        return meta_manufacturingDate;
    }

    public void setMeta_ManufacturingDate(String meta_manufacturingDate) {
        this.meta_manufacturingDate = meta_manufacturingDate;
    }

    public String getMeta_RegistrationDate() {
        return meta_registrationDate;
    }

    public void setMeta_RegistrationDate(String meta_registrationDate) {
        this.meta_registrationDate = meta_registrationDate;
    }

    public String getMeta_Color() {
        return meta_color;
    }

    public void setMeta_Color(String meta_color) {
        this.meta_color = meta_color;
    }

    public String getMeta_FuelType() {
        return meta_fuelType;
    }

    public void setMeta_FuelType(String meta_fuelType) {
        this.meta_fuelType = meta_fuelType;
    }

    public String getMeta_VinNumber() {
        return meta_vinNumber;
    }

    public void setMeta_VinNumber(String meta_vinNumber) {
        this.meta_vinNumber = meta_vinNumber;
    }

    public String getMeta_VehicleName() {
        return meta_vehicleName;
    }

    public void setMeta_VehicleName(String meta_vehicleName) {
        this.meta_vehicleName = meta_vehicleName;
    }

    public String getMeta_Manufacturer() {
        return meta_manufacturer;
    }

    public void setMeta_Manufacturer(String meta_manufacturer) {
        this.meta_manufacturer = meta_manufacturer;
    }

    public String getMeta_PolicyNumber() {
        return meta_policyNumber;
    }

    public void setMeta_PolicyNumber(String meta_policyNumber) {
        this.meta_policyNumber = meta_policyNumber;
    }

    public String getMeta_ChasisNumber() {
        return meta_chasisNumber;
    }

    public void setMeta_ChasisNumber(String meta_chasisNumber) {
        this.meta_chasisNumber = meta_chasisNumber;
    }

    public String getMeta_RegistrationNo() {
        return meta_registrationNo;
    }

    public void setMeta_RegistrationNo(String meta_registrationNo) {
        this.meta_registrationNo = meta_registrationNo;
    }

    public String getAd_msisdn() {
        return ad_msisdn;
    }

    public void setAd_msisdn(String ad_msisdn) {
        this.ad_msisdn = ad_msisdn;
    }

    public String getAd_imei() {
        return ad_imei;
    }

    public void setAd_imei(String ad_imei) {
        this.ad_imei = ad_imei;
    }

    public String getAd_serialNumber() {
        return ad_serialNumber;
    }

    public void setAd_serialNumber(String ad_serialNumber) {
        this.ad_serialNumber = ad_serialNumber;
    }

    public String getAd_simId() {
        return ad_simId;
    }

    public void setAd_simId(String ad_simId) {
        this.ad_simId = ad_simId;
    }

    public String getAd_memCapacity() {
        return ad_memCapacity;
    }

    public String getAd_macAddress() {
        return ad_macAddress;
    }

    public void setAd_macAddress(String ad_macAddress) {
        this.ad_macAddress = ad_macAddress;
    }

    public String getAd_modemSwVersion() {
        return ad_modemSwVersion;
    }

    public void setAd_modemSwVersion(String ad_modemSwVersion) {
        this.ad_modemSwVersion = ad_modemSwVersion;
    }

    public String getAd_manufacturingDate() {
        return ad_manufacturingDate;
    }

    public void setAd_manufacturingDate(String ad_manufacturingDate) {
        this.ad_manufacturingDate = ad_manufacturingDate;
    }

    public String getAd_registrationDate() {
        return ad_registrationDate;
    }

    public void setAd_registrationDate(String ad_registrationDate) {
        this.ad_registrationDate = ad_registrationDate;
    }

    public String getAd_modelnumber() {
        return ad_modelnumber;
    }

    public void setAd_modelnumber(String ad_modelnumber) {
        this.ad_modelnumber = ad_modelnumber;
    }

    public String getAd_manufacturingLocation() {
        return ad_manufacturingLocation;
    }

    public void setAd_manufacturingLocation(String ad_manufacturingLocation) {
        this.ad_manufacturingLocation = ad_manufacturingLocation;
    }

    public String getAd_manufacturer() {
        return ad_manufacturer;
    }

    public void setAd_manufacturer(String ad_manufacturer) {
        this.ad_manufacturer = ad_manufacturer;
    }

    public String getAd_firmwareVersion() {
        return ad_firmwareVersion;
    }

    public void setAd_firmwareVersion(String ad_firmwareVersion) {
        this.ad_firmwareVersion = ad_firmwareVersion;
    }

    public String getAd_imsi() {
        return ad_imsi;
    }

    public void setAd_imsi(String ad_imsi) {
        this.ad_imsi = ad_imsi;
    }

    public String getAd_hardwareVersion() {
        return ad_hardwareVersion;
    }

    public void setAd_hardwareVersion(String ad_hardwareVersion) {
        this.ad_hardwareVersion = ad_hardwareVersion;
    }

    public String getAd_primaryLanguageSettings() {
        return ad_primaryLanguageSettings;
    }

    public void setAd_primaryLanguageSettings(String ad_primaryLanguageSettings) {
        this.ad_primaryLanguageSettings = ad_primaryLanguageSettings;
    }

    public Long[] getGrp_Vehicle_Ids() {
        return grp_vhcl_Ids;
    }

    public void setGrp_Vehicle_Ids(Long[] grp_vhcl_Ids) {
        this.grp_vhcl_Ids = grp_vhcl_Ids;
    }

    public List<ConstraintAlerts_Beans> getAlert_List() {
        return Alerts_list;
    }

    public void setAlert_List(List<ConstraintAlerts_Beans> Alerts_list) {
        this.Alerts_list = Alerts_list;
    }

    public List<IncidentAlerts_Beans> getInci_Alerts_list() {
        return Inci_Alerts_list;
    }

    public void setInci_Alerts_list(List<IncidentAlerts_Beans> Inci_Alerts_list) {
        this.Inci_Alerts_list = Inci_Alerts_list;
    }

    public void setAd_MemCapacity(String ad_memCapacity) {
        this.ad_memCapacity = ad_memCapacity;
    }

}
