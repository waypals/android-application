package com.waypals.Beans;

import java.io.Serializable;

public class ConstraintAlerts_Beans implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1207152019077830514L;
    String dataPoint;
    String DateTime;
    String DP_gpsSpeed;
    String DP_Lat;
    String DP_Long;
    String type;
    String ViolatedRule_Id;
    String ViolatedRule_Enabled;
    String ViolatedRule_Constraint_Id;
    String ViolatedRule_Constraint_Name;
    String ViolatedRule_Constraint_AllowedDeviation;
    String ViolatedRule_GraphArea_Id;
    String ViolatedRule_GraphArea_LeftTop;
    String ViolatedRule_GraphArea_RightBottom;
    String ViolatedRule_GraphArea_Radius;
    String ViolatedRule_GraphArea_Center_Lat;
    String ViolatedRule_GraphArea_Center_Long;
    String constraintTransgressionTime;
    String constraintId;
    String Constraint_VehicleId;
    String meta_registrationDate;
    String viewType;
    /*String meta_manufacturingDate;
    String meta_registrationDate;
    String meta_id;
    String meta_color;
    String meta_fuelType;
    String meta_vinNumber;
    String meta_vehicleName;
    String meta_manufacturer;
    String meta_policyNumber;
    String meta_chasisNumber;*/
    String meta_registrationNo;

    public String getDataPoint() {
        return dataPoint;
    }

    public void setDataPoint(String dataPoint) {
        this.dataPoint = dataPoint;
    }

    public String getDPoint_DateTime() {
        return DateTime;
    }

    public void setDPoint_DateTime(String DateTime) {
        this.DateTime = DateTime;
    }

    public String getDP_gpsSpeed() {
        return DP_gpsSpeed;
    }

    public void setDP_gpsSpeed(String DP_gpsSpeed) {
        this.DP_gpsSpeed = DP_gpsSpeed;
    }

    public String getDP_Lat() {
        return DP_Lat;
    }

    public void setDP_Lat(String DP_Lat) {
        this.DP_Lat = DP_Lat;
    }

    public String getDP_Long() {
        return DP_Long;
    }

    public void setDP_Long(String DP_Long) {
        this.DP_Long = DP_Long;
    }

    public String getDP_Type() {
        return type;
    }

    public void setDP_Type(String type) {
        this.type = type;
    }

    public String getViolatedRule_Id() {
        return ViolatedRule_Id;
    }

    public void setViolatedRule_Id(String ViolatedRule_Id) {
        this.ViolatedRule_Id = ViolatedRule_Id;
    }

    public String getViolatedRule_Enabled() {
        return ViolatedRule_Enabled;
    }

    public void setViolatedRule_Enabled(String ViolatedRule_Enabled) {
        this.ViolatedRule_Enabled = ViolatedRule_Enabled;
    }

    public String getViolatedRule_Constraint_Id() {
        return ViolatedRule_Constraint_Id;
    }

    public void setViolatedRule_Constraint_Id(String ViolatedRule_Constraint_Id) {
        this.ViolatedRule_Constraint_Id = ViolatedRule_Constraint_Id;
    }

    public String getViolatedRule_Constraint_Name() {
        return ViolatedRule_Constraint_Name;
    }

    public void setViolatedRule_Constraint_Name(String ViolatedRule_Constraint_Name) {
        this.ViolatedRule_Constraint_Name = ViolatedRule_Constraint_Name;
    }

    public String getViolatedRule_Constraint_AllowedDeviation() {
        return ViolatedRule_Constraint_AllowedDeviation;
    }

    public void setViolatedRule_Constraint_AllowedDeviation(String ViolatedRule_Constraint_AllowedDeviation) {
        this.ViolatedRule_Constraint_AllowedDeviation = ViolatedRule_Constraint_AllowedDeviation;
    }

    public String getViolatedRule_GraphArea_Id() {
        return ViolatedRule_GraphArea_Id;
    }

    public void setViolatedRule_GraphArea_Id(String ViolatedRule_GraphArea_Id) {
        this.ViolatedRule_GraphArea_Id = ViolatedRule_GraphArea_Id;
    }

    public String getViolatedRule_GraphArea_LeftTop() {
        return ViolatedRule_GraphArea_LeftTop;
    }

    public void setViolatedRule_GraphArea_LeftTop(String ViolatedRule_GraphArea_LeftTop) {
        this.ViolatedRule_GraphArea_LeftTop = ViolatedRule_GraphArea_LeftTop;
    }

    public String getViolatedRule_GraphArea_RightBottom() {
        return ViolatedRule_GraphArea_RightBottom;
    }

    public void setViolatedRule_GraphArea_RightBottom(String ViolatedRule_GraphArea_RightBottom) {
        this.ViolatedRule_GraphArea_RightBottom = ViolatedRule_GraphArea_RightBottom;
    }

    public String getViolatedRule_GraphArea_Radius() {
        return ViolatedRule_GraphArea_Radius;
    }

    public void setViolatedRule_GraphArea_Radius(String ViolatedRule_GraphArea_Radius) {
        this.ViolatedRule_GraphArea_Radius = ViolatedRule_GraphArea_Radius;
    }

    public String getViolatedRule_GraphArea_Center_Lat() {
        return ViolatedRule_GraphArea_Center_Lat;
    }

    public void setViolatedRule_GraphArea_Center_Lat(String ViolatedRule_GraphArea_Center_Lat) {
        this.ViolatedRule_GraphArea_Center_Lat = ViolatedRule_GraphArea_Center_Lat;
    }

    public String getViolatedRule_GraphArea_Center_Long() {
        return ViolatedRule_GraphArea_Center_Long;
    }

    public void setViolatedRule_GraphArea_Center_Long(String ViolatedRule_GraphArea_Center_Long) {
        this.ViolatedRule_GraphArea_Center_Long = ViolatedRule_GraphArea_Center_Long;
    }

    public String getConstraintTransgressionTime() {
        return constraintTransgressionTime;
    }

    public void setConstraintTransgressionTime(String constraintTransgressionTime) {
        this.constraintTransgressionTime = constraintTransgressionTime;
    }

    public String getConstraintId() {
        return constraintId;
    }

    public void setConstraintId(String constraintId) {
        this.constraintId = constraintId;
    }

    public String getConstraint_VehicleId() {
        return Constraint_VehicleId;
    }

    public void setConstraint_VehicleId(String Constraint_VehicleId) {
        this.Constraint_VehicleId = Constraint_VehicleId;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    /*public String getMeta_ManufacturingDate()
    {
        return meta_manufacturingDate;
    }

    public String getMeta_RegistrationDate()
    {
        return meta_registrationDate;
    }

    public String getMeta_id()
    {
        return meta_id;
    }

    public String getMeta_Color()
    {
        return meta_color;
    }

    public String getMeta_FuelType()
    {
        return meta_fuelType;
    }

    public String getMeta_VinNumber()
    {
        return meta_vinNumber;
    }

    public String getMeta_VehicleName()
    {
        return meta_vehicleName;
    }

    public String getMeta_Manufacturer()
    {
        return meta_manufacturer;
    }

    public String getMeta_PolicyNumber()
    {
        return meta_policyNumber;
    }

    public String getMeta_ChasisNumber()
    {
        return meta_chasisNumber;
    }
    */
    public String getMeta_RegistrationNo() {
        return meta_registrationNo;
    }

    /*public void setMeta_ManufacturingDate(String meta_manufacturingDate)
    {
        this.meta_manufacturingDate = meta_manufacturingDate;
    }

    public void setMeta_RegistrationDate(String meta_registrationDate)
    {
        this.meta_registrationDate = meta_registrationDate;
    }

    public void setMeta_id(String meta_id)
    {
        this.meta_id = meta_id;
    }

    public void setMeta_Color(String meta_color)
    {
        this.meta_color = meta_color;
    }

    public void setMeta_FuelType(String meta_fuelType)
    {
        this.meta_fuelType = meta_fuelType;
    }

    public void setMeta_VinNumber(String meta_vinNumber)
    {
        this.meta_vinNumber = meta_vinNumber;
    }

    public void setMeta_VehicleName(String meta_vehicleName)
    {
        this.meta_vehicleName = meta_vehicleName;
    }

    public void setMeta_Manufacturer(String meta_manufacturer)
    {
        this.meta_manufacturer = meta_manufacturer;
    }

    public void setMeta_PolicyNumber(String meta_policyNumber)
    {
        this.meta_policyNumber = meta_policyNumber;
    }

    public void setMeta_ChasisNumber(String meta_chasisNumber)
    {
        this.meta_chasisNumber = meta_chasisNumber;
    }
    */
    public void setMeta_RegistrationNo(String meta_registrationNo) {
        this.meta_registrationNo = meta_registrationNo;
    }
    /*
    public void setMeta_RegistrationDate(String meta_registrationDate)
	{
		this.meta_registrationDate = meta_registrationDate;
	}*/
}
