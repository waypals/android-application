package com.waypals;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.VehicleStateResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class Add_Geofence extends FragmentActivity implements OnMarkerDragListener, OnMarkerClickListener, OnMapClickListener, AsyncFinishInterface {
    public static final double RADIUS_OF_EARTH_METERS = 6371009;
    static final int geoAlfa = 80;
    static final int geoColor = 196;
    private static final double DEFAULT_RADIUS = 200;
    public static GoogleMap map;
    public static LatLng selectedCar_latlng;
    public static int connectionTimeOut = 0;
    public double center_lat;
    public double center_lng;
    public double center_radius;
    ImageView save_btn;
    ImageView back_btn;
    TextView navTitle, yellow_ribbon_txt;
    ImageView myloc_img, map_layer_img;
    LinearLayout geo_property_dialog;
    Spinner geo_event_spinr, geo_radius_spinr;
    EditText geo_radius_edit, geo_name_edit, geo_location_edit;
    int maplayer = 1;
    Marker myPosition;
    boolean me_position = false;
    ApplicationPreferences appPref;
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    int fillColor;
    int k = 0;
    DraggableCircle first_circle;
    boolean bool_mapclick = false, isVisible_geo_property;
    VehicleStateResponse vehicleStateResponse;
    String response, errorCode;
    String login_response;
    private List<DraggableCircle> mCircles = new ArrayList<DraggableCircle>(1);
    private LatLng center_circle_latlng;
    private TextWatcher RadiusText_Listener = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
////        	mCircles.remove(first_circle);
            String radiusVal = s.toString();
            //    radiusVal = radiusVal.replace(" m", "");
//        	first_circle = new DraggableCircle(center_circle_latlng, Double.parseDouble(radiusVal));
//	        mCircles.add(first_circle);
            double radius = first_circle.circle.getRadius();
            try {
                radius = Double.parseDouble(radiusVal);
            } catch (NumberFormatException ex) {
            }
            first_circle.circle.setRadius(radius);
        }
    };
    private boolean boundfence;
    private String geo_name = "";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    /**
     * Generate LatLng of radius marker
     */
    private static LatLng toRadiusLatLng(LatLng center, double radius) {
        double radiusAngle = Math.toDegrees(radius / RADIUS_OF_EARTH_METERS) /
                Math.cos(Math.toRadians(center.latitude));
        return new LatLng(center.latitude, center.longitude + radiusAngle);
    }

    private static double toRadiusMeters(LatLng center, LatLng radius) {
        float[] result = new float[1];
        Location.distanceBetween(center.latitude, center.longitude,
                radius.latitude, radius.longitude, result);
        return result[0];
    }

    public static double roundOff(double unrounded, int precision, int roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.car_overview);

            back_btn = (ImageView) findViewById(R.id.left_btn);
            save_btn = (ImageView) findViewById(R.id.right_btn);
            navTitle = (TextView) findViewById(R.id.nav_title_txt);
            map_layer_img = (ImageView) findViewById(R.id.map_layer_img);
            myloc_img = (ImageView) findViewById(R.id.myloc_img);
            yellow_ribbon_txt = (TextView) findViewById(R.id.yellow_ribbon_txt);
//		geo_radius_spinr = (Spinner) findViewById(R.id.geo_radius_spinr);
            geo_radius_edit = (EditText) findViewById(R.id.geo_radius_edit);
            geo_radius_edit.setEnabled(true);
            geo_name_edit = (EditText) findViewById(R.id.geo_name_edit);
            geo_location_edit = (EditText) findViewById(R.id.geo_location_edit);
            geo_event_spinr = (Spinner) findViewById(R.id.geo_event_spinr);
            geo_property_dialog = (LinearLayout) findViewById(R.id.geo_property_dialog);


            EditText editEntryView = (EditText) geo_property_dialog.findViewById(R.id.geo_name_edit);

            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(25);
            editEntryView.setFilters(FilterArray);

            appPref = new ApplicationPreferences(this);

            Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            navTitle.setTypeface(tf);
            navTitle.setText("ADD GEOFENCE");
            yellow_ribbon_txt.setVisibility(View.VISIBLE);
            yellow_ribbon_txt.setText(getString(R.string.create_geofence));

            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    map = googleMap;
                    map.setMapType(maplayer);
                    map.getUiSettings().setCompassEnabled(true);
                    map.setOnMarkerClickListener(Add_Geofence.this);
                    map.setOnMarkerDragListener(Add_Geofence.this);
                    if (ActivityCompat.checkSelfPermission(Add_Geofence.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Add_Geofence.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        return;
                    }
                    map.setMyLocationEnabled(true);

                    map.setOnMarkerDragListener(Add_Geofence.this);
                    map.setOnMapClickListener(Add_Geofence.this);

                    VehicleServices vehicleServices = new VehicleServices();
                    try {
                        vehicleServices.getVehicleState(appPref, Add_Geofence.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            back_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            map_layer_img.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    maplayer++;

                    if (map != null) {
                        map.setMapType(maplayer);

                        if (maplayer == 4) {
                            maplayer = 0;
                        }
                    }

                }
            });

        } catch (Exception e) {
            ExceptionHandler.handleException(e, this);
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void send_addGeofence(String json_req) {
        System.out.println("Add_Geofence_Api :: " + CH_Constant.Create_Geofence_Api);
        //    	("content-type", "application/json");
        //        ("accept-encoding", "gzip");

        try {
            String s = null;
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost postMethod = new HttpPost(CH_Constant.Create_Geofence_Api);

                postMethod.setEntity(new StringEntity(json_req));
                postMethod.setHeader("Content-Type", "application/json");
                postMethod.setHeader("Accept-encoding", "gzip");
                postMethod.setHeader("security-token", appPref.getUserToken());

                HttpResponse httpresponse = httpclient.execute(postMethod);
                HttpEntity responseEntity = httpresponse.getEntity();
                s = EntityUtils.toString(responseEntity);
                Log.e(" Add Geofence response", s);

            } catch (ConnectTimeoutException e) {
                e.printStackTrace();
                connectionTimeOut = 1;
            } catch (HttpHostConnectException e) {
                e.printStackTrace();
                connectionTimeOut = 2;
            }


            JSONObject resObject = new JSONObject(s);

            response = resObject.getString("response");

            if (response.equalsIgnoreCase("SUCCESS")) {
                //				JSONObject resultObj = resObject.getJSONObject("referenceId");
                //				appPref.saveUserToken(resultObj.getString("token"));

            } else
                errorCode = resObject.getString("errorCode");


        } catch (Exception e) {
            e.printStackTrace();
            connectionTimeOut = -1;
        }


    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        onMarkerMoved(marker);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
//    	geo_location_edit.setText(roundOff(marker.getPosition().latitude,7, BigDecimal.ROUND_HALF_UP) + ", " + roundOff(marker.getPosition().longitude, 7, BigDecimal.ROUND_HALF_UP));
        onMarkerMoved(marker);
    }

    @Override
    public void onMarkerDrag(Marker marker) {
//    	geo_location_edit.setText(roundOff(marker.getPosition().latitude,7, BigDecimal.ROUND_HALF_UP) + ", " + roundOff(marker.getPosition().longitude, 7, BigDecimal.ROUND_HALF_UP));
        onMarkerMoved(marker);
    }

    private void onMarkerMoved(Marker marker) {
        for (DraggableCircle draggableCircle : mCircles) {
            if (draggableCircle.onMarkerMoved(marker)) {
                break;
            }
        }
    }

    @Override
    public void onMapClick(LatLng point) {

        if (!bool_mapclick) {
            bool_mapclick = true;
            // We know the center, let's place the outline at a point 3/4 along the view.
            View view = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getView();
            LatLng radiusLatLng = map.getProjection().fromScreenLocation(new Point(
                    view.getHeight() * 3 / 4, view.getWidth() * 3 / 4));

            // ok create it
            //        DraggableCircle circle = new DraggableCircle(point, radiusLatLng);
            //        mCircles.add(circle);

            first_circle = new DraggableCircle(point, DEFAULT_RADIUS);
            mCircles.add(first_circle);

            //save_btn.setText("Save");
            save_btn.setBackgroundResource(R.drawable.save_btn);
//            save_btn.getLayoutParams().height=140;
//            save_btn.getLayoutParams().width=140;
//            save_btn.requestLayout();
            save_btn.setVisibility(View.VISIBLE);
            yellow_ribbon_txt.setText(getString(R.string.set_detail_geofence));

//	        geo_name_edit.setRawInputType(Configuration.KEYBOARD_QWERTY);

            geo_name_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(geo_name_edit.getWindowToken(), 0);


                    return false;
                }
            });

            center_lat = point.latitude;
            center_lng = point.longitude;
            center_radius = DEFAULT_RADIUS;
            geo_radius_edit.setText(roundOff(center_radius, 4, BigDecimal.ROUND_HALF_UP) + "");
            geo_location_edit.setText(roundOff(center_lat, 6, BigDecimal.ROUND_HALF_UP) + ", " + roundOff(center_lng, 6, BigDecimal.ROUND_HALF_UP));

            geo_radius_edit.addTextChangedListener(RadiusText_Listener);

            save_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CH_Constant.isNetworkAvailable(Add_Geofence.this)) {
                        if (geo_name_edit.getText() != null && geo_name_edit.getText().toString().trim().isEmpty())
                            Toast.makeText(Add_Geofence.this, "Please give a name to your geofence.", Toast.LENGTH_SHORT).show();
                        else {

                            geo_name = geo_name_edit.getText().toString();
                            if (geo_event_spinr.getSelectedItemPosition() == 0)
                                boundfence = false;
                            else
                                boundfence = true;
                            new hitAdd_Geofence_Api_Operation().execute("");
                        }
                    } else
                        CH_Constant.showDialogOK(Add_Geofence.this, getString(R.string.app_name), CH_Constant.NO_NETWORK);

                }
            });

        }
        if (!isVisible_geo_property) {
            geo_property_dialog.setVisibility(View.VISIBLE);
        } else {
            geo_property_dialog.setVisibility(View.GONE);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(geo_name_edit.getWindowToken(), 0);
        }

        isVisible_geo_property = !isVisible_geo_property;

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (response != null) {
            Gson gson = new Gson();
            vehicleStateResponse = gson.fromJson(response, VehicleStateResponse.class);
        }

    }

    @Override
    public void onPostExecute(Void Result) {
        if (vehicleStateResponse != null && vehicleStateResponse.getLocation() != null) {
            String location = vehicleStateResponse.getLocation().toString();
            selectedCar_latlng = new LatLng(Double.parseDouble(location.split("/")[0]), Double.parseDouble(location.split("/")[1]));

            map.addMarker(new MarkerOptions().position(selectedCar_latlng)
                    .icon(BitmapDescriptorFactory.fromResource(CH_Constant.getCarCurrentIcon(vehicleStateResponse.getState(), vehicleStateResponse.isSystemFenceEnabled(), vehicleStateResponse.getVehicleType()))));

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedCar_latlng, 18));
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Add_Geofence Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.waypals/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Add_Geofence Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.waypals/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    public class hitAdd_Geofence_Api_Operation extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(Add_Geofence.this);

        @Override
        protected void onPreExecute() {
            this.progressDialog = ProgressDialog.show(Add_Geofence.this, null, "Loading...", true);
        }

        @Override
        protected Void doInBackground(String... params) {

            String reqJson = "{\"vehicleIds\":[" + appPref.getVehicle_Selected() + "],\"name\":" + geo_name + ",\"geographicalArea\" : " +
                    "{\"radius\" :" + center_radius + ",\"center\" : {\"longitude\" : {\"longitude\" : " + center_lng + "}," +
                    "\"latitude\" : {\"latitude\" : " + center_lat + "}}}," + "\"allowedDeviation\":{\"distance\":" + CH_Constant.AddGeofence_allowedDeviation + ",\"unit\":\"METRE\"},\"isOutOfBoundsFence\":" + boundfence + "}";

            System.out.println("Geofence reqJson :: " + reqJson);

            send_addGeofence(reqJson);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (!isFinishing()) {
                if (this.progressDialog != null && this.progressDialog.isShowing())
                    this.progressDialog.dismiss();

                if (connectionTimeOut == 1) {
                    CH_Constant.showDialogOK(Add_Geofence.this, getString(R.string.app_name), "Network failure! Please Try again");
                } else if (connectionTimeOut == 2) {
                    CH_Constant.showDialogOK(Add_Geofence.this, getString(R.string.app_name), "We are experiencing some problem, please try later !");
                } else if (response != null && response.equalsIgnoreCase("SUCCESS")) {
                    Toast toast = Toast.makeText(Add_Geofence.this, "New geofence saved!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    finish();

                } else if (response != null && response.equalsIgnoreCase("ERROR")) {
                    CH_Constant.showDialogOK(Add_Geofence.this, getString(R.string.app_name), CH_Constant.ERROR_Msg(errorCode));

                } else
                //					if(connectionTimeOut == -1)
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Invalid data received from server.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                }


            }

        }

    }

    private class DraggableCircle {
        private final Marker centerMarker;
        private final Marker radiusMarker;
        private final Circle circle;
        private double radius;

        public DraggableCircle(LatLng center, double radius) {
            this.radius = radius;
            System.out.println("radius :: " + radius);
            centerMarker = map.addMarker(new MarkerOptions()
                    .position(center)
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.geo_center_marker)));
            radiusMarker = map.addMarker(new MarkerOptions()
                    .position(toRadiusLatLng(center, radius))
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.geo_radius_marker)));
            circle = map.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radius)
                    .strokeWidth(5)
                    .strokeColor(Color.parseColor("#02BFF0"))
                    .fillColor(Color.HSVToColor(geoAlfa, new float[]{geoColor, 1, 1})));
        }

        public DraggableCircle(LatLng center, LatLng radiusLatLng) {
            this.radius = toRadiusMeters(center, radiusLatLng);
            centerMarker = map.addMarker(new MarkerOptions()
                    .position(center)
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.geo_center_marker)));
            radiusMarker = map.addMarker(new MarkerOptions()
                    .position(radiusLatLng)
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.geo_radius_marker)));
            circle = map.addCircle(new CircleOptions()
                    .center(center)
                    .radius(radius)
                    .strokeWidth(5)
                    .strokeColor(Color.parseColor("#02BFF0"))
                    .fillColor(Color.HSVToColor(geoAlfa, new float[]{geoColor, 1, 1})));
        }

        public boolean onMarkerMoved(Marker marker) {
            if (marker.equals(centerMarker)) {
                center_circle_latlng = marker.getPosition();
                circle.setCenter(marker.getPosition());
                radiusMarker.setPosition(toRadiusLatLng(marker.getPosition(), radius));
                center_lat = marker.getPosition().latitude;
                center_lng = marker.getPosition().longitude;
                geo_location_edit.setText(roundOff(center_lat, 6, BigDecimal.ROUND_HALF_UP) + ", " + roundOff(center_lng, 6, BigDecimal.ROUND_HALF_UP));
                return true;
            }
            if (marker.equals(radiusMarker)) {
                radius = toRadiusMeters(centerMarker.getPosition(), radiusMarker.getPosition());
                circle.setRadius(radius);
                center_radius = radius;
                geo_radius_edit.setText(roundOff(center_radius, 4, BigDecimal.ROUND_HALF_UP) + "");
                return true;
            }
            return false;
        }

        public void onStyleChange() {
            circle.setStrokeWidth(5);
            circle.setFillColor(Color.HSVToColor(geoAlfa, new float[]{geoColor, 1, 1}));
            circle.setStrokeColor(Color.parseColor("#02BFF0"));
        }
    }


}