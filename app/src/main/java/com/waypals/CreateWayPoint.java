package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.util.LruCache;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.imageHelper.ImageResizer;
import com.waypals.objects.MyImageView;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class CreateWayPoint extends NavigationDrawer implements AsyncFinishInterface {
    public static final int CAMERA_PICTURE = 2;
    public static final int GALLERY_PICTURE = 1;
    static String tag = "Create Way Point";
    Activity context;
    File tempFile;
    Typeface tf;
    String tempFileName = "temp.jpg";
    ApplicationPreferences appPref;
    ImageView saveButton, back;
    TextView title;
    EditText waypointName, waypointDesc, browseImage;
    Spinner waypointCategory;
    CheckBox makeMePrivate;
    JSONObject jsonObj;
    AsyncFinishInterface asyncFinishInterface;
    Boolean isImageAvailable = false;
    String fileNamePath = null;
    ArrayList<String> categoryData;
    private ServiceResponse serviceResponse;
    private ProgressDialog waitingBar;
    MyImageView f1, f2, f3, f4, defaultImage;
    GridLayout grid;
    private ImageLoader imageLoader;
    ArrayList<IncidentImages> imagesSet = null;
    ArrayList<MyImageView> gridImags = new ArrayList<MyImageView>();
    HashMap<Integer, IncidentImages> loadedImageObjects;
    HashMap<String, String> currentImages = new HashMap<String, String>();
    HashMap<Integer, ImageView> usedImageViews = new HashMap<Integer, ImageView>();
    private int screenSize = 0;

    private LruCache<String, Bitmap> mMemoryCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.create_waypoint_new);
        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);
        context = this;
        screenSize = Utility.getScreenSize(this);

        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setTypeface(tf);
        title.setText("Waypoint(s)");
        waitingBar = new ProgressDialog(this);

        asyncFinishInterface = this;
        ArrayList location = getIntent().getParcelableArrayListExtra("location");
        Log.d(tag, "location: " + location);
        final JSONObject locationObj = new JSONObject();
        try {
            JSONObject lat = new JSONObject();
            lat.put("latitude", Double.parseDouble(location.get(0).toString()));

            JSONObject lon = new JSONObject();
            lon.put("longitude", Double.parseDouble(location.get(1).toString()));

            locationObj.put("latitude", lat);
            locationObj.put("longitude", lon);
        } catch (Exception ex) {
                ExceptionHandler.handlerException(ex, this);
        }

        final ArrayList category = getIntent().getParcelableArrayListExtra("category");
        Log.d(tag, "category: " + category);


        Spinner categorySpinner = (Spinner) findViewById(R.id.waypointCategory);
        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.create_waypoint_dropdown, createCategoryList(category), tf);
        categorySpinner.setAdapter(adapter);

        context = this;
        appPref = new ApplicationPreferences(context);
        jsonObj = new JSONObject();

        back = (ImageView) findViewById(R.id.left_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        grid = (GridLayout) findViewById(R.id.grid_create_waypoint);
        grid.setColumnCount(2);
        grid.setRowCount(2);

        loadedImageObjects = new HashMap<Integer, IncidentImages>();

        defaultImage = (MyImageView) findViewById(R.id.add_pic_img_waypoint);
        defaultImage.setImageResource(R.drawable.no_image);

        browseImage = (EditText) findViewById(R.id.browseImageText);
        browseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        saveButton = (ImageView) findViewById(R.id.right_btn);
        saveButton.setImageResource(R.drawable.save_btn);
        saveButton.setVisibility(View.VISIBLE);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    jsonObj = new JSONObject();

                    jsonObj.put("cordinate", locationObj);

                    waypointName = (EditText) findViewById(R.id.wayPonitName);
                    if (waypointName.getText().toString().trim().isEmpty()) {
                        Utility.makeNewToast(context, "Enter waypoint name", 60);
                        return;
                    } else {
                        jsonObj.put("name", waypointName.getText().toString().trim());
                    }
                    waypointCategory = (Spinner) findViewById(R.id.waypointCategory);
                    RelativeLayout container = (RelativeLayout) waypointCategory.getSelectedView();
                    TextView textView = (TextView) container.findViewById(R.id.textView);
                    String name = textView.getText().toString().trim();
                    Log.d(tag, "Name:" + name);
                    if (name != null && !name.isEmpty()) {


                        if (name.equalsIgnoreCase("Category")) {
                            Utility.makeNewToast(context, "Select a category", 60);
                            return;
                        }

                        int postion = category.indexOf(name);
                        String other = category.get(postion + 1).toString();
                        long id = Long.parseLong(category.get(postion - 1).toString());
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("id", id);
                        jsonObject.put("name", name);
                        jsonObject.put("others", other);
                        if(name != null) {
                            jsonObject.put("imageName", name.replace(" ","_"));
                        }
                        jsonObj.put("wayPointCategory", jsonObject);
                    } else {
                        Utility.makeNewToast(context, "Select a category", 60);
                        return;
                    }
                    waypointDesc = (EditText) findViewById(R.id.wayPointDesc);
                    if (waypointDesc.getText().toString().trim().isEmpty()) {
                        Utility.makeNewToast(context, "Enter description", 60);
//                        makeErrorToast("enter description");
                        return;
                    } else {
                        jsonObj.put("description", waypointDesc.getText().toString().trim());
                    }
                    makeMePrivate = (CheckBox) findViewById(R.id.makePrivate);
                    if (makeMePrivate.isChecked()) {
                        jsonObj.put("type", "PRIVATE");
                    } else {
                        jsonObj.put("type", "PUBLIC");
                    }
                    if(!waitingBar.isShowing())
                    {
                        waitingBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        waitingBar.setCancelable(false);
                        waitingBar.setTitle("");
                        waitingBar.setMessage("Please wait...");
                       // waitingBar.show();
                    }
                   // if (isImageAvailable) {
                        JSONArray array = new JSONArray();
//                        array.put(Utility.getImageToHex(context, fileNamePath));

                    Iterator iterator = currentImages.entrySet().iterator();
                    while (iterator.hasNext())
                    {
                        Map.Entry<String, String> next = (Map.Entry<String, String>) iterator.next();
                        array.put(next.getValue());
                    }
                    jsonObj.put("imageHexString", array);
//                    } else {
//                        JSONArray array = new JSONArray();
//                        jsonObj.put("imageHexString", array);
//                    }
                    Log.d(tag, "JSON: " + jsonObj.toString());
                    if(array != null) {
                        Log.d(tag, "Images:" + array.length());
                    }
                   // waitingBar.dismiss();

                    GenericAsyncService service = new GenericAsyncService(appPref, context, CH_Constant.SAVE_WAYPOINT, "SAVE_WAYPOINT", jsonObj.toString(), tag, asyncFinishInterface, true);
                    service.execute();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    jsonObj = null;
                }


            }
        });

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getRowBytes()/1024;
            }
        };

    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (method != null && method.equals("SAVE_WAYPOINT")) {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    private void clearImages() {
        grid.removeAllViews();
        gridImags.clear();
        currentImages.clear();
    }

    @Override
    public void onPostExecute(Void Result) {
        try {

            if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, "Server is not responding, please try after some time!!");
                }
                return;
            }

            if (serviceResponse != null && serviceResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                clearImages();
                Utility.makeNewToast(context, context.getString(R.string.op_alert_waypoint_success));
            } else {
                Utility.makeNewToast(context, context.getString(R.string.op_alert_failed_waypoints));
            }
        }catch (Exception ex)
        {
//            if(waitingBar != null && waitingBar.isShowing())
//            {
//                waitingBar.dismiss();
//            }
            ExceptionHandler.handleException(ex);
        }
        finish();
    }

    public String checkImageSize(String fileNamePath) {
        try {
            File f = new File(fileNamePath);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            is.close();

            int kb = size / 1024;

            if (kb > 256) {
                File file = Utility.createFile();
                Bitmap b = ImageResizer.resize(new File(fileNamePath), 256, 256);
                FileOutputStream fout = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fout);

                String path = file.getAbsolutePath();
                fout.flush();
                fout.close();
                b.recycle();
                b = null;
//                fout.close();
                return path;
            } else {
                return fileNamePath;
            }
        } catch (Exception e) {
            Log.e("Error",e.getMessage());
            ExceptionHandler.handlerException(e, context);
            return null;
        }
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }


    private SpinnerContent[] createCategoryList(ArrayList category) {
        SpinnerContent content[] = new SpinnerContent[category.size() / 3 + 1];

        ArrayList<String> list = new ArrayList<String>();
        list.add("Category");
        content[0] = new SpinnerContent("Category");
        if (category != null) {
            for (int i = 0; i < category.size() / 3; i++) {
//                list.add(category.get(3 * i + 1).toString());
                content[i + 1] = new SpinnerContent(category.get(3 * i + 1).toString());
            }
            categoryData = list;
        } else {
            list.add("No Category Found");
            categoryData = null;
        }

        return content;
    }

    private void makeErrorToast(String opt) {
        Toast toast = Toast.makeText(context, "Please " + opt, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/" + timeStamp + "_temp.jpg");

        // Save a file: path for use with ACTION_VIEW intents
        fileNamePath = image.getAbsolutePath();
        return image;
    }

    public void selectImage() {
        if (imagesSet == null) {
            imagesSet = new ArrayList<IncidentImages>();
            loadedImageObjects = new HashMap<Integer, IncidentImages>();
            usedImageViews.put(0, null);
            usedImageViews.put(1, null);
            usedImageViews.put(2, null);
            usedImageViews.put(3, null);
        }
        if (grid.getChildCount() >= 4) {
            try {
                Utility.makeNewToast(context, "You can not add more than 4 images");
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, context);
            } finally {
                return;
            }
        }
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateWayPoint.this);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                fileNamePath = null;
                Log.d(tag, "image selection canceled");
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("resultCode", String.valueOf(resultCode));
        if (requestCode == CAMERA_PICTURE && resultCode == RESULT_OK)// capture image form camera
        {
            onPhotoTaken();
        } else if (requestCode == GALLERY_PICTURE) // pick picture from gallary
        {
            onPhotoTakenFromGallery(data);
        }
    }

    public void onPhotoTaken() {
        try {
            defaultImage = (MyImageView) findViewById(R.id.add_pic_img_waypoint);
            defaultImage.setVisibility(View.GONE);
            if (fileNamePath != null) {
                fileNamePath = checkImageSize(fileNamePath);
                {
                    String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                    String type = name.substring(name.indexOf(".") + 1);
                    String imageStr = Utility.getImageToHex(context, fileNamePath);
                    final IncidentImages images = new IncidentImages();
                    images.setImageName(name);
                    images.setImageStr(imageStr);
                    if (type.equalsIgnoreCase(IncidentImages.ImageType.JPEG.name())) {
                        images.setImageType(IncidentImages.ImageType.JPEG);
                    } else if (type.equalsIgnoreCase(IncidentImages.ImageType.PNG.name())) {
                        images.setImageType(IncidentImages.ImageType.PNG);
                    } else if (type.equalsIgnoreCase(IncidentImages.ImageType.JPG.name())) {
                        images.setImageType(IncidentImages.ImageType.JPG);
                    } else {
                        Utility.makeNewToast(context, "Image format not supported");
                        return;
                    }
                    loadedImageObjects.put(images.hashCode(), images);
                    currentImages.put(""+images.hashCode(), imageStr);
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final MyImageView imageView = new MyImageView(context);
                    imageView.setLayoutParams(params);
                    imageView.setPadding(15, 5, 15, 15);
                    imageView.setName(""+images.hashCode());
                    imageView.getLayoutParams().height = screenSize / 2 - 20;
                    imageView.getLayoutParams().width = screenSize / 2 - 20;
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    //imageView.setImageURI(Uri.fromFile(new File(fileNamePath)));
                    imageView.setImageBitmap(Utility.decodeSampledBitmapFromPath(fileNamePath, 256, 256));
                    imageView.setOnLongClickListener(new ImageClickListener(imageView));
                    grid.addView(imageView);
                    gridImags.add(imageView);
                }

            }
            Log.d("camera imagePath", fileNamePath);
            Log.d(tag, "Total Image files: " + imagesSet);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("camera imagePath", "onactivity result");
        }

    }



    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            defaultImage = (MyImageView) findViewById(R.id.add_pic_img_waypoint);
            defaultImage.setVisibility(View.GONE);

            if(data != null)
            {
            Uri _uri = data.getData();
            if (_uri != null) {
                fileNamePath = getPath(_uri);
                Log.d(tag, "gallary image path: " + fileNamePath);
                if (fileNamePath != null) {
                    fileNamePath = checkImageSize(fileNamePath);
                    {
                        String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                        String type = name.substring(name.indexOf(".") + 1);
                        String imageStr = Utility.getImageToHex(context, fileNamePath);
                        final IncidentImages images = new IncidentImages();
                        images.setImageName(name);
                        images.setImageStr(imageStr);

                        if (type.equalsIgnoreCase(IncidentImages.ImageType.JPEG.name())) {
                            images.setImageType(IncidentImages.ImageType.JPEG);
                        } else if (type.equalsIgnoreCase(IncidentImages.ImageType.PNG.name())) {
                            images.setImageType(IncidentImages.ImageType.PNG);
                        } else if (type.equalsIgnoreCase(IncidentImages.ImageType.JPG.name())) {
                            images.setImageType(IncidentImages.ImageType.JPG);
                        } else {
                            Utility.makeNewToast(context, "Image format not supported");
                            return;
                        }

                        loadedImageObjects.put(images.hashCode(), images);
                        currentImages.put("" + images.hashCode(), imageStr);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        final MyImageView imageView = new MyImageView(context);
                        imageView.setLayoutParams(params);
                        imageView.setPadding(15, 5, 15, 15);
                        imageView.setName("" + images.hashCode());
                        imageView.getLayoutParams().height = screenSize / 2 - 20;
                        imageView.getLayoutParams().width = screenSize / 2 - 20;
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        imageView.setImageURI(Uri.fromFile(new File(fileNamePath)));
                        imageView.setOnLongClickListener(new ImageClickListener(imageView));
                        grid.addView(imageView);
                        gridImags.add(imageView);
                    }
                }
                Log.d(tag, "images list : " + imagesSet);
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(tag, "onactivity result");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void setImmobilizer() {

    }


    class SpinnerContent {
        public String title;

        public SpinnerContent(String title) {
            super();
            this.title = title;
        }
    }


    class SpinnerAdapter extends ArrayAdapter<SpinnerContent> {
        Context context;
        int layoutResourceId;
        SpinnerContent data[] = null;
        Typeface tf;

        public SpinnerAdapter(Context context, int resource,
                              SpinnerContent[] objects, Typeface tf) {
            super(context, resource, objects);
            this.layoutResourceId = resource;
            this.context = context;
            this.data = objects;
            this.tf = tf;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            ContentsHolder holder;
            LayoutInflater inflater = ((NavigationDrawer) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ContentsHolder();

            holder.txtTitle = (TextView) row.findViewById(R.id.textView);


            SpinnerContent drawerContent = data[position];
            holder.txtTitle.setTypeface(tf);
            holder.txtTitle.setText(drawerContent.title);

            row.setTag(holder);
            row.requestLayout();
            return row;

        }

        class ContentsHolder {

            TextView txtTitle;

        }
    }

    private class ImageClickListener implements View.OnLongClickListener
    {

        private MyImageView imageView;

        public ImageClickListener(MyImageView view)
        {
            this.imageView = view;
        }

        @Override
        public boolean onLongClick(View view) {
            try {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("Delete Image");
                dialog.setMessage("Do you want to delete this image ?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(tag, "deleting image");
                        try {
                            grid.removeView(imageView);
                            gridImags.remove(imageView);
                            currentImages.remove(imageView.getName());

                        } catch (Exception ex) {
                            ExceptionHandler.handleException(ex, context);
                        }
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });
                dialog.setCancelable(false);
                dialog.create();
                dialog.show();
            } catch (Throwable e) {
                ExceptionHandler.handleException(e);
            }
            return false;
        }
    }

    public static class IncidentImages implements Serializable {

        enum ImageType {
            PNG, JPG, JPEG;
        }

        private String imageStr;
        private String imageName;
        private ImageType imageType;
        private String imageLink;

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }

        public String getImageStr() {
            return imageStr;
        }

        public void setImageStr(String imageStr) {
            this.imageStr = imageStr;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public ImageType getImageType() {
            return imageType;
        }

        public void setImageType(ImageType imageType) {
            this.imageType = imageType;
        }

        @Override
        public String toString() {
            return "IncidentImages{" +
                    "imageStr='" + imageStr + '\'' +
                    ", imageName='" + imageName + '\'' +
                    ", imageType='" + imageType + '\'' +
                    '}';
        }
    }
}
