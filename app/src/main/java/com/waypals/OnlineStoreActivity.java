package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.fragments.OnlineItemFragment;
import com.waypals.fragments.dummy.DummyContent;
import com.waypals.response.Product;
import com.waypals.response.ProductWrapper;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.Utility;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class OnlineStoreActivity extends FragmentActivity implements OnlineItemFragment.OnListFragmentInteractionListener {


    private Map<String, ProductWrapper> mapItemPerchased;

    TextView itemCount, itemTotalPrice;

    private ApplicationPreferences appPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_store);

        appPref = new ApplicationPreferences(this);
        ImageView back = (ImageView) findViewById(R.id.left_btn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView text = (TextView) findViewById(R.id.nav_title_txt);
        text.setText("Waypals Store");

        itemCount = (TextView) findViewById(R.id.item_count);
        itemTotalPrice = (TextView) findViewById(R.id.item_total_price);

        mapItemPerchased = new HashMap<>();

        if (savedInstanceState != null) {
            Set<String> strings = savedInstanceState.keySet();

            Iterator<String> iterator = strings.iterator();

            while (iterator.hasNext()) {
                String next = iterator.next();
                if (savedInstanceState.get(next) instanceof ProductWrapper) {
                    mapItemPerchased.put(next, (ProductWrapper) savedInstanceState.get(next));
                }
            }
        }

        Button button = (Button) findViewById(R.id.add_to_cart_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnlineStoreActivity.this, ShoppingCartActivity.class);

                if (mapItemPerchased.isEmpty()){
                    Utility.makeNewToast(OnlineStoreActivity.this, "Your cart is empty please add items and proceed");
                }else {
                    Set<Map.Entry<String, ProductWrapper>> entries = mapItemPerchased.entrySet();
                    Iterator<Map.Entry<String, ProductWrapper>> iterator = entries.iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<String, ProductWrapper> next = iterator.next();
                        intent.putExtra(next.getKey(), next.getValue());
                    }
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        Set<Map.Entry<String, ProductWrapper>> entries = mapItemPerchased.entrySet();
        Iterator<Map.Entry<String, ProductWrapper>> iterator = entries.iterator();
        while (iterator.hasNext()){
            Map.Entry<String, ProductWrapper> next = iterator.next();
            outState.putSerializable(next.getKey(), next.getValue());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (appPref.getNoItemsInCart() == 0){
            if(mapItemPerchased != null){
                mapItemPerchased.clear();
                itemCount.setText("0");
                itemTotalPrice.setText("INR 0.0");

            }
        }

    }

    @Override
    public void onAddToCartTap(ProductWrapper item) {
        ProductWrapper noOfItems = mapItemPerchased.get(item.getProduct().getProductCode());

        if (noOfItems == null) {
            noOfItems = item;
        }

            Log.d("Quantity", "" + item.getStockQty());
            if (noOfItems.isValid()) {
                noOfItems.addToCart();
                mapItemPerchased.put(item.getProduct().getProductCode(), noOfItems);

                int itemTotal = 0;
                float price = 0;

                Set<String> strings = mapItemPerchased.keySet();
                Iterator<String> iterator = strings.iterator();
                while (iterator.hasNext()){
                   String key = iterator.next();
                    ProductWrapper productWrapper = mapItemPerchased.get(key);
                    itemTotal += productWrapper.getItemInCart();
                    price += productWrapper.getItemInCart() * productWrapper.getProduct().getProductMetaData().getPrice();
                }

                itemCount.setText(""+itemTotal);
                itemTotalPrice.setText("INR "+price);

                Utility.makeNewToast(this, "Item added to cart");
            } else {
                Utility.makeNewToast(this, "No more items available for this product");
            }

    }
}
