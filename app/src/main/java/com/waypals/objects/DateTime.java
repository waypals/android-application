package com.waypals.objects;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTime implements Comparable<DateTime>, Serializable {
    public transient String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss z";
    public transient SimpleDateFormat SDF = new SimpleDateFormat(DATE_FORMAT);
    private static final long serialVersionUID = -6923631408774638302L;
    private String date;
    private transient Date dateTemp;
  //  private long dateInMillis;

    protected DateTime() {
        date = new String();
        //this.dateInMillis = date.getTime();
    }

    /**
     * @param date in format dd/MM/yyyy HH:mm:ss z
     */
    public DateTime(final String date) {
        try {
            this.date = date;
            SDF.setTimeZone(TimeZone.getTimeZone("GMT"));
            this.dateTemp = SDF.parse(date);
           // this.dateInMillis = this.date.getTime();
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "Invalid date. Required format: " + DATE_FORMAT);
        }
    }

    public  DateTime (final Date date, SimpleDateFormat sdf) {
        //final DateTime dt = new DateTime();
        //this.SDF = sdf;
        this.date = sdf.format(date);
        this.dateTemp = date;

    }

    public static DateTime newInstance() {
        return new DateTime();
    }

    @Override
    public String toString() {
        return date;
    }

    public String toString(final TimeZone timeZone) {
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
        format.setTimeZone(timeZone);
        return format.format(this.date);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (this == obj) {
            return true;
        } else if (obj instanceof DateTime) {
            DateTime that = (DateTime) obj;
            return this.date.equals(that.date);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return date.hashCode();
    }

    @Override
    public int compareTo(final DateTime o) {
        if (o == null) {
            return +1;
        }
        return this.dateTemp.compareTo(o.dateTemp);
    }

    public final Date toDate() {
        return this.dateTemp;
    }

    /**
     * Returns the difference in millis between this DateTime
     * and the provided one.
     * ( this - date )
     *
     * @param date
     * @return
     */
    public long differenceInMillis(final DateTime date) {
        return this.dateTemp.getTime() - date.dateTemp.getTime();
    }

    /**
     * Returns the difference in millis between this DateTime
     * and the provided java.util.Date.
     * ( this - date )
     *
     * @param date
     * @return
     */
    public long differenceInMillis(final Date date) {
        return this.dateTemp.getTime() - dateTemp.getTime();
    }

    public long differenceInMillisWithCurrentTime() {
        return this.dateTemp.getTime() - System.currentTimeMillis();
    }

    public long dateInMillis() {
        return this.dateTemp.getTime();
    }

    public boolean isGreaterThan(final DateTime logTime) {
        return this.dateTemp.getTime() > logTime.toDate().getTime();
    }

    public boolean isGreaterThanEqual(final DateTime logTime) {
        return this.dateTemp.getTime() >= logTime.toDate().getTime();
    }

    public boolean isLessThan(final DateTime logTime) {
        return this.dateTemp.getTime() < logTime.toDate().getTime();
    }

    public boolean isLessThanEqual(final DateTime logTime) {
        return this.dateTemp.getTime() <= logTime.toDate().getTime();
    }

    public void setDateFormat(SimpleDateFormat dateFormat) {
        this.SDF = dateFormat;
    }
}