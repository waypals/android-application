package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by shantanu on 6/2/15.
 */
public class IncidentType implements Serializable {
    private String eventType;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
