package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by shantanu on 2/2/15.
 */
public class Obd implements Serializable {
    private Long vehicleId;
    private Speed vehicleSpeed;
    private Integer engineState;
    private Rpm rpm;//0 	16,383.75 	rpm
    private String obdVin;
    private Float obdThrottle;
    private Float obdCoolantTemperature;//-40 	215 	°C
    private Float obdIntakeTemperature;
    private Float obdFuelLevel;
    private Float obdAirFlowRate;
    private Float obdEngineLoad; //0 	100 	 %
    private Float obdSleepVoltage;
    private Float obdRunningVoltage;

    private Integer obdProtocol;
    private Integer obdManifoldPressure;
    private Double obdShortTermFuelTrim1;//2 %
    private Double obdShortTermFuelTrim2;//2 %
    private Integer obdDistanceWithMil;
    private Integer obdMil;
    private String obdPendingDtc;
    private String obdCurrentDtc;
    private String obdPermanentDtc;

    public Float getObdSleepVoltage() {
        return obdSleepVoltage;
    }

    public void setObdSleepVoltage(Float obdSleepVoltage) {
        this.obdSleepVoltage = obdSleepVoltage;
    }

    public Float getObdRunningVoltage() {
        return obdRunningVoltage;
    }

    public void setObdRunningVoltage(Float obdRunningVoltage) {
        this.obdRunningVoltage = obdRunningVoltage;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Speed getVehicleSpeed() {
        return vehicleSpeed;
    }

    public void setVehicleSpeed(Speed vehicleSpeed) {
        this.vehicleSpeed = vehicleSpeed;
    }

    public Integer getEngineState() {
        return engineState;
    }

    public void setEngineState(Integer engineState) {
        this.engineState = engineState;
    }

    public Rpm getRpm() {
        return rpm;
    }

    public void setRpm(Rpm rpm) {
        this.rpm = rpm;
    }

    public String getObdVin() {
        return obdVin;
    }

    public void setObdVin(String obdVin) {
        this.obdVin = obdVin;
    }

    public Float getObdThrottle() {
        return obdThrottle;
    }

    public void setObdThrottle(Float obdThrottle) {
        this.obdThrottle = obdThrottle;
    }

    public Float getObdCoolantTemperature() {
        return obdCoolantTemperature;
    }

    public void setObdCoolantTemperature(Float obdCoolantTemperature) {
        this.obdCoolantTemperature = obdCoolantTemperature;
    }

    public Float getObdIntakeTemperature() {
        return obdIntakeTemperature;
    }

    public void setObdIntakeTemperature(Float obdIntakeTemperature) {
        this.obdIntakeTemperature = obdIntakeTemperature;
    }

    public Float getObdFuelLevel() {
        return obdFuelLevel;
    }

    public void setObdFuelLevel(Float obdFuelLevel) {
        this.obdFuelLevel = obdFuelLevel;
    }

    public Float getObdAirFlowRate() {
        return obdAirFlowRate;
    }

    public void setObdAirFlowRate(Float obdAirFlowRate) {
        this.obdAirFlowRate = obdAirFlowRate;
    }

    public Float getObdEngineLoad() {
        return obdEngineLoad;
    }

    public void setObdEngineLoad(Float obdEngineLoad) {
        this.obdEngineLoad = obdEngineLoad;
    }

    public Integer getObdProtocol() {
        return obdProtocol;
    }

    public void setObdProtocol(Integer obdProtocol) {
        this.obdProtocol = obdProtocol;
    }

    public Integer getObdManifoldPressure() {
        return obdManifoldPressure;
    }

    public void setObdManifoldPressure(Integer obdManifoldPressure) {
        this.obdManifoldPressure = obdManifoldPressure;
    }

    public Double getObdShortTermFuelTrim1() {
        return obdShortTermFuelTrim1;
    }

    public void setObdShortTermFuelTrim1(Double obdShortTermFuelTrim1) {
        this.obdShortTermFuelTrim1 = obdShortTermFuelTrim1;
    }

    public Double getObdShortTermFuelTrim2() {
        return obdShortTermFuelTrim2;
    }

    public void setObdShortTermFuelTrim2(Double obdShortTermFuelTrim2) {
        this.obdShortTermFuelTrim2 = obdShortTermFuelTrim2;
    }

    public Integer getObdDistanceWithMil() {
        return obdDistanceWithMil;
    }

    public void setObdDistanceWithMil(Integer obdDistanceWithMil) {
        this.obdDistanceWithMil = obdDistanceWithMil;
    }

    public Integer getObdMil() {
        return obdMil;
    }

    public void setObdMil(Integer obdMil) {
        this.obdMil = obdMil;
    }

    public String getObdPendingDtc() {
        return obdPendingDtc;
    }

    public void setObdPendingDtc(String obdPendingDtc) {
        this.obdPendingDtc = obdPendingDtc;
    }

    public String getObdCurrentDtc() {
        return obdCurrentDtc;
    }

    public void setObdCurrentDtc(String obdCurrentDtc) {
        this.obdCurrentDtc = obdCurrentDtc;
    }

    public String getObdPermanentDtc() {
        return obdPermanentDtc;
    }

    public void setObdPermanentDtc(String obdPermanentDtc) {
        this.obdPermanentDtc = obdPermanentDtc;
    }
}
