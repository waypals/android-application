package com.waypals.objects;

/**
 * Created by shantanu on 23/1/15.
 */
public class Gps {
    private GeoLocation geoLocation;

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }
}
