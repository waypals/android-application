package com.waypals.objects;

/**
 * Created by surya on 17/2/16.
 */
public class UserGps {

    private Gps gps;
    private String userName;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Gps getGps() {
        return gps;
    }

    public void setGps(Gps gps) {
        this.gps = gps;
    }
}
