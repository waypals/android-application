package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by shantanu on 5/2/15.
 */
public class IncidentAlerts implements Serializable {
    private Long id;
    private IncidentEvents event;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IncidentEvents getEvent() {
        return event;
    }

    public void setEvent(IncidentEvents event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "IncidentAlerts{" +
                "id=" + id +
                ", event=" + event +
                '}';
    }
}
