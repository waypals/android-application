package com.waypals.objects;

/**
 * Created by shantanu on 2/2/15.
 */
public enum MotionState {
    OUT_OF_NETWORK_WITH_PRIOR_IGNITION_ON_STATE,
    /**
     * Vehicle was in ignition OFF state and now not able to contact with the
     * Application.
     */
    OUT_OF_NETWORK_WITH_PRIOR_IGNITION_OFF_STATE,
    /**
     * Vehicle is in ignition ON state but not moving. <br>
     * i.e. vehicle is stopped at red-light, etc.
     */
    STOPPED,
    /**
     * Vehicle is moving.
     */
    MOVING,
    /**
     * Vehicle is in ignition OFF state.
     */
    PARKED,
    /**
     * Vehicle is in ignition OFF state and moving.
     */
    TOWED;
}
