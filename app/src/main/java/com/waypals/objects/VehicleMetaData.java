package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by shantanu on 6/2/15.
 */
public class VehicleMetaData implements Serializable {
    private String registrationNo;

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    @Override
    public String toString() {
        return "VehicleMetaData{" +
                "registrationNo='" + registrationNo + '\'' +
                '}';
    }
}
