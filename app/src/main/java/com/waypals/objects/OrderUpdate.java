package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by surya on 26/10/16.
 */
public class OrderUpdate implements Serializable {

    private String orderId;
    private Integer paymentStatus;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Integer paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
