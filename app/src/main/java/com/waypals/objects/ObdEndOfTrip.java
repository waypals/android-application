package com.waypals.objects;

import com.waypals.response.ScoreResponse;

import java.io.Serializable;

/**
 * Created by surya on 10/5/16.
 */
public class ObdEndOfTrip implements Serializable {

    private Distance distance;

    private Speed maxSpeed;
    private float totalFuelCons;
    private int idleTime;
    private int totalTime;
    private String logTime;
    private long vehicleId;
    private boolean hidden;
    private String startTime;
    private ScoreResponse score;

    public ScoreResponse getScore() {
        return score;
    }

    public void setScore(ScoreResponse score) {
        this.score = score;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Speed getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Speed maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getTotalFuelCons() {
        return totalFuelCons;
    }

    public void setTotalFuelCons(float totalFuelCons) {
        this.totalFuelCons = totalFuelCons;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(int idleTime) {
        this.idleTime = idleTime;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
