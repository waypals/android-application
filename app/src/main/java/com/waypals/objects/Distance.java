package com.waypals.objects;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by shantanu on 2/2/15.
 */
public class Distance implements Serializable {

    private BigDecimal distance;
    private String unit;

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
