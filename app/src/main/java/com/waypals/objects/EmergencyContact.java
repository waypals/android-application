package com.waypals.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.waypals.gson.vo.SOSContacts;

import java.io.Serializable;

/**
 * Created by surya on 23/10/15.
 */
public class EmergencyContact implements Serializable {

    private String countryCode;
    private long phoneNumber;
    private String name;
    private String relation;

    private int id;

    public EmergencyContact(SOSContacts sosContacts) {

        if(sosContacts != null) {
            if(sosContacts.getEmergencyPhoneNumber() != null) {
                this.phoneNumber = sosContacts.getEmergencyPhoneNumber().getPhoneNo();
                this.countryCode = sosContacts.getEmergencyPhoneNumber().getCountryCode();
            }
            this.name = sosContacts.getName();
            this.relation = sosContacts.getRelation();
        }
    }

    public EmergencyContact()
    {

    }


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
