package com.waypals.objects;

import com.waypals.feedItems.RouteCoordinates;

import java.io.Serializable;

/**
 * Created by surya on 11/5/16.
 */
public class VehicleTripOverview implements Serializable {

    RouteCoordinates location;
    String dateTime;

    public RouteCoordinates getLocation() {
        return location;
    }

    public void setLocation(RouteCoordinates location) {
        this.location = location;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
