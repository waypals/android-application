package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by surya on 14/06/2017.
 */
public class CountryDataWrapper implements Serializable {

    private static final long serialVersionUID = 822131550588656706L;

    private String isdCode;
    private String country;

    public String getIsdCode() {
        return isdCode;
    }

    public void setIsdCode(final String isdCode) {
        this.isdCode = isdCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

}
