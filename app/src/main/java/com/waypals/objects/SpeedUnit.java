package com.waypals.objects;

/**
 * Created by shantanu on 2/2/15.
 */
public enum SpeedUnit {
    KMPH("km/h"), MPH("mph"), MS("m/s");

    private String notation;

    private SpeedUnit(final String notation) {
        this.notation = notation;
    }

    public String getNotation() {
        return notation;
    }

    @Override
    public String toString() {
        return notation;
    }
}
