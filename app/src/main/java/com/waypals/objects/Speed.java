package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by shantanu on 2/2/15.
 */
public class Speed implements Serializable {
    private Float speed;
    private SpeedUnit unit = SpeedUnit.KMPH;

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public SpeedUnit getUnit() {
        return unit;
    }

    public void setUnit(SpeedUnit unit) {
        this.unit = unit;
    }
}
