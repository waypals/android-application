package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by shantanu on 2/2/15.
 */
public class Rpm implements Serializable {
    private Float rpm;

    public Float getRpm() {
        return rpm;
    }

    public void setRpm(Float rpm) {
        this.rpm = rpm;
    }
}
