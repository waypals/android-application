package com.waypals.objects;

/**
 * Created by jasbeer on 31/5/15.
 */
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class MyImageView extends ImageView
{

    private String hashCode;

    public MyImageView(Activity activity)
    {
        super(activity);
    }

    /**
     * @param context
     * @param attrs
     */
    public MyImageView(Context context, AttributeSet     attrs)
    {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public MyImageView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    public String getName() {
        return hashCode;
    }

    public void setName(String hashCode) {
        this.hashCode = hashCode;
    }
}
