package com.waypals.objects;

import com.waypals.feedItems.Images;
import com.waypals.feedItems.RouteCoordinates;

import java.io.Serializable;

/**
 * Created by shantanu on 6/2/15.
 */
public class IncidentEvents implements Serializable {
    private Long id;
    private IncidentType type;
    private String incidentTime;
    private String reportedLocation;
    private String referenceNumber;
    private String driver;
    private String damages;
    private String ticketNumber;
    private Long vehicleId;
    private String description;
    private String reportingTime;
    private VehicleMetaData vehicleMetadata;
    private Images[] incidentImages;
    private RouteCoordinates incidentLocation;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IncidentType getType() {
        return type;
    }

    public void setType(IncidentType type) {
        this.type = type;
    }

    public String getReportedLocation() {
        return reportedLocation;
    }

    public void setReportedLocation(String reportedLocation) {
        this.reportedLocation = reportedLocation;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDamages() {
        return damages;
    }

    public void setDamages(String damages) {
        this.damages = damages;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIncidentTime() {
        return incidentTime;
    }

    public void setIncidentTime(String incidentTime) {
        this.incidentTime = incidentTime;
    }

    public String getReportingTime() {
        return reportingTime;
    }

    public void setReportingTime(String reportingTime) {
        this.reportingTime = reportingTime;
    }

    public VehicleMetaData getVehicleMetadata() {
        return vehicleMetadata;
    }

    public void setVehicleMetadata(VehicleMetaData vehicleMetadata) {
        this.vehicleMetadata = vehicleMetadata;
    }

    public Images[] getIncidentImages() {
        return incidentImages;
    }

    public void setIncidentImages(Images[] incidentImages) {
        this.incidentImages = incidentImages;
    }

    public RouteCoordinates getIncidentLocation() {
        return incidentLocation;
    }

    public void setIncidentLocation(RouteCoordinates incidentLocation) {
        this.incidentLocation = incidentLocation;
    }

    @Override
    public String toString() {
        return "IncidentEvents{" +
                "id=" + id +
                ", type=" + type +
                ", incidentTime=" + incidentTime +
                ", reportedLocation='" + reportedLocation + '\'' +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", driver='" + driver + '\'' +
                ", damages='" + damages + '\'' +
                ", vehicleId=" + vehicleId +
                ", description='" + description + '\'' +
                ", reportingTime=" + reportingTime +
                ", vehicleMetadata=" + vehicleMetadata +
                ", incidentImages=" + incidentImages +
                ", incidentLocation='" + incidentLocation + '\'' +
                '}';
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
}