package com.waypals.objects;

/**
 * Created by shantanu on 4/2/15.
 */
public class VehicleAlerts {
    String type;
    long date;
    String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
