package com.waypals.objects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by surya on 25/10/16.
 */
public class OrderRequest implements Serializable{

    private String name;
    private Address address;
    private String email;
    private Phone phoneNumber;
    private List<String> productCodes;
    private Integer deviceOrderStatus;
    private Float totalAmout;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Phone getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Phone phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(List<String> productCodes) {
        this.productCodes = productCodes;
    }

    public Integer getDeviceOrderStatus() {
        return deviceOrderStatus;
    }

    public void setDeviceOrderStatus(Integer deviceOrderStatus) {
        this.deviceOrderStatus = deviceOrderStatus;
    }

    public Float getTotalAmout() {
        return totalAmout;
    }

    public void setTotalAmout(Float totalAmout) {
        this.totalAmout = totalAmout;
    }
}
