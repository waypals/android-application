package com.waypals.objects;

/**
 * Created by shantanu on 2/2/15.
 */
public enum LengthUnit {

    MILLIMETRE("mm") {
        @Override
        public double toMillimetres(final double length) {
            return length;
        }

        @Override
        public double toCentimetres(final double length) {
            return length / (C_CM / C_MM);
        }

        @Override
        public double toInches(final double length) {
            return length / (C_INCH / C_MM);
        }

        @Override
        public double toFeets(double length) {
            return length / (C_FT / C_MM);
        }

        @Override
        public double toYards(double length) {
            return length / (C_YD / C_MM);
        }

        @Override
        public double toMetres(double length) {
            return length / (C_M / C_MM);
        }

        @Override
        public double toKilometres(double length) {
            return length / (C_KM / C_MM);
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_MM);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_MM);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toMillimetres(sourceLength);
        }
    },
    CENTIMETRE("cm") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_CM / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return length;
        }

        @Override
        public double toInches(double length) {
            return length / (C_INCH / C_CM);
        }

        @Override
        public double toFeets(double length) {
            return length / (C_FT / C_CM);
        }

        @Override
        public double toYards(double length) {
            return length / (C_YD / C_CM);
        }

        @Override
        public double toMetres(double length) {
            return length / (C_M / C_CM);
        }

        @Override
        public double toKilometres(double length) {
            return length / (C_KM / C_CM);
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_CM);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_CM);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toCentimetres(sourceLength);
        }
    },
    INCHE("inch") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_INCH / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_INCH / C_CM);
        }

        @Override
        public double toInches(double length) {
            return length;
        }

        @Override
        public double toFeets(double length) {
            return length / (C_FT / C_INCH);
        }

        @Override
        public double toYards(double length) {
            return length / (C_YD / C_INCH);
        }

        @Override
        public double toMetres(double length) {
            return length / (C_M / C_INCH);
        }

        @Override
        public double toKilometres(double length) {
            return length / (C_KM / C_INCH);
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_INCH);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_INCH);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toInches(sourceLength);
        }
    },
    FEET("ft") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_FT / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_FT / C_CM);
        }

        @Override
        public double toInches(double length) {
            return x(length, C_FT / C_INCH);
        }

        @Override
        public double toFeets(double length) {
            return length;
        }

        @Override
        public double toYards(double length) {
            return length / (C_YD / C_FT);
        }

        @Override
        public double toMetres(double length) {
            return length / (C_M / C_FT);
        }

        @Override
        public double toKilometres(double length) {
            return length / (C_KM / C_FT);
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_FT);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_FT);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toFeets(sourceLength);
        }
    },
    YARD("yd") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_YD / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_YD / C_CM);
        }

        @Override
        public double toInches(double length) {
            return x(length, C_YD / C_INCH);
        }

        @Override
        public double toFeets(double length) {
            return x(length, C_YD / C_FT);
        }

        @Override
        public double toYards(double length) {
            return length;
        }

        @Override
        public double toMetres(double length) {
            return length / (C_M / C_YD);
        }

        @Override
        public double toKilometres(double length) {
            return length / (C_KM / C_YD);
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_YD);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_YD);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toYards(sourceLength);
        }
    },
    METRE("m") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_M / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_M / C_CM);
        }

        @Override
        public double toInches(double length) {
            return x(length, C_M / C_INCH);
        }

        @Override
        public double toFeets(double length) {
            return x(length, C_M / C_FT);
        }

        @Override
        public double toYards(double length) {
            return x(length, C_M / C_YD);
        }

        @Override
        public double toMetres(double length) {
            return length;
        }

        @Override
        public double toKilometres(double length) {
            return length / (C_KM / C_M);
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_M);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_M);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toMetres(sourceLength);
        }
    },
    KILOMETRE("km") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_KM / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_KM / C_CM);
        }

        @Override
        public double toInches(double length) {
            return x(length, C_KM / C_INCH);
        }

        @Override
        public double toFeets(double length) {
            return x(length, C_KM / C_FT);
        }

        @Override
        public double toYards(double length) {
            return x(length, C_KM / C_YD);
        }

        @Override
        public double toMetres(double length) {
            return x(length, C_KM / C_M);
        }

        @Override
        public double toKilometres(double length) {
            return length;
        }

        @Override
        public double toMiles(double length) {
            return length / (C_MILE / C_KM);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_KM);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toKilometres(sourceLength);
        }
    },
    MILE("mile") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_MILE / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_MILE / C_CM);
        }

        @Override
        public double toInches(double length) {
            return x(length, C_MILE / C_INCH);
        }

        @Override
        public double toFeets(double length) {
            return x(length, C_MILE / C_FT);
        }

        @Override
        public double toYards(double length) {
            return x(length, C_MILE / C_YD);
        }

        @Override
        public double toMetres(double length) {
            return x(length, C_MILE / C_M);
        }

        @Override
        public double toKilometres(double length) {
            return x(length, C_MILE / C_KM);
        }

        @Override
        public double toMiles(double length) {
            return length;
        }

        @Override
        public double toNauticalmiles(double length) {
            return length / (C_NMI / C_MILE);
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toMiles(sourceLength);
        }
    },
    NAUTICAL_MILE("nmi") {
        @Override
        public double toMillimetres(double length) {
            return x(length, C_NMI / C_MM);
        }

        @Override
        public double toCentimetres(double length) {
            return x(length, C_NMI / C_CM);
        }

        @Override
        public double toInches(double length) {
            return x(length, C_NMI / C_INCH);
        }

        @Override
        public double toFeets(double length) {
            return x(length, C_NMI / C_FT);
        }

        @Override
        public double toYards(double length) {
            return x(length, C_NMI / C_YD);
        }

        @Override
        public double toMetres(double length) {
            return x(length, C_NMI / C_M);
        }

        @Override
        public double toKilometres(double length) {
            return x(length, C_NMI / C_KM);
        }

        @Override
        public double toMiles(double length) {
            return x(length, C_NMI / C_MILE);
        }

        @Override
        public double toNauticalmiles(double length) {
            return length;
        }

        @Override
        public double convert(double sourceLength, LengthUnit sourceUnit) {
            return sourceUnit.toNauticalmiles(sourceLength);
        }
    };

    private static final double C_MM = 1.0;
    private static final double C_CM = C_MM * 10.0;
    private static final double C_INCH = C_CM * 2.54;
    private static final double C_FT = C_CM * 30.0;
    private static final double C_YD = C_FT * 3.0;
    private static final double C_M = C_CM * 100.0;
    private static final double C_KM = C_M * 1000.0;
    private static final double C_MILE = C_KM * 1.60934;
    private static final double C_NMI = C_MILE * 72913.38583;
    private String notation;

    private LengthUnit(String notation) {
        this.notation = notation;
    }

    /**
     * Scale d by m, checking for overflow. This has a short name to make above
     * code more readable.
     */
    private static double x(final double d, final double m) {
        double over = Double.MAX_VALUE / m;
        if (d > over)
            return Double.MAX_VALUE;
        if (d < -over)
            return Double.MIN_VALUE;
        return d * m;
    }

    public String getNotation() {
        return notation;
    }

    @Override
    public String toString() {
        return notation;
    }

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     * @see #convert
     */
    public abstract double toMillimetres(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toCentimetres(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toInches(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toFeets(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toYards(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toMetres(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toKilometres(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toMiles(final double length);

    /**
     * @param length the length
     * @return the converted length, or <tt>Double.MIN_VALUE</tt> if conversion
     * would negatively overflow, or <tt>Double.MAX_VALUE</tt> if it
     * would positively overflow.
     */
    public abstract double toNauticalmiles(final double length);

    /**
     * Convert the given length in the given unit to this unit. Conversions from
     * coarser to finer granularities with arguments that would numerically
     * overflow saturate to <tt>Long.MIN_VALUE</tt> if negative or
     * <tt>Long.MAX_VALUE</tt> if positive.
     * <p/>
     * <p/>
     * For example, to convert 10 metres to millimetres, use:
     * <tt>LengthUnit.MILLIMETRE.convert(10.0, LengthUnit.METRE)</tt>
     *
     * @param sourceLength the length in the given <tt>sourceUnit</tt>
     * @param sourceUnit   the unit of the <tt>sourceLength</tt> argument
     * @return the converted length in this unit, or <tt>Long.MIN_VALUE</tt> if
     * conversion would negatively overflow, or <tt>Long.MAX_VALUE</tt>
     * if it would positively overflow.
     */
    public abstract double convert(final double sourceLength,
                                   final LengthUnit sourceUnit);
}