package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by surya on 25/10/16.
 */
public class Phone implements Serializable{

    String countryCode;

    String phoneNo;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {
        return countryCode+phoneNo;
    }
}
