package com.waypals.objects;

import com.waypals.R;

import java.io.Serializable;

/**
 * Created by surya on 14/02/2017.
 */
public class GreenDriveDetail implements Serializable {

    GreenDriveData HARSH_CORNERING;
    GreenDriveData HARSH_BRAKING;
    GreenDriveData SPEEDING;
    GreenDriveData HARSH_ACCELERATION;
    GreenDriveData IDLING;

    public GreenDriveData getHARSH_CORNERING() {
        if (this.HARSH_CORNERING != null) {
            this.HARSH_CORNERING.setTitle("Cornering");
            this.HARSH_CORNERING.setType(GreenDriveData.TYPE.CORNERING);
        }
        return HARSH_CORNERING;
    }

    public void setHARSH_CORNERING(GreenDriveData HARSH_CORNERING) {
        this.HARSH_CORNERING = HARSH_CORNERING;
    }

    public GreenDriveData getHARSH_BRAKING() {
        if (this.HARSH_BRAKING != null) {
            this.HARSH_BRAKING.setTitle("Breaking");
            this.HARSH_BRAKING.setType(GreenDriveData.TYPE.BREAKING);
        }
        return HARSH_BRAKING;
    }

    public void setHARSH_BRAKING(GreenDriveData HARSH_BRAKING) {
        this.HARSH_BRAKING = HARSH_BRAKING;
    }

    public GreenDriveData getSPEEDING() {
        if (this.SPEEDING != null) {
            this.SPEEDING.setTitle("Speeding");
            this.SPEEDING.setType(GreenDriveData.TYPE.SPEED);
        }
        return SPEEDING;
    }

    public void setSPEEDING(GreenDriveData SPEEDING) {
        this.SPEEDING = SPEEDING;
    }

    public GreenDriveData getHARSH_ACCELERATION() {
        if (this.HARSH_ACCELERATION != null) {
            this.HARSH_ACCELERATION.setTitle("Acceleration");
            this.HARSH_ACCELERATION.setType(GreenDriveData.TYPE.ACCEL);
        }
        return HARSH_ACCELERATION;
    }

    public void setHARSH_ACCELERATION(GreenDriveData HARSH_ACCELERATION) {
        this.HARSH_ACCELERATION = HARSH_ACCELERATION;
    }

    public GreenDriveData getIDLING() {
        if (this.IDLING != null) {
            this.IDLING.setTitle("Idling");
            this.IDLING.setType(GreenDriveData.TYPE.IDLING);
        }
        return IDLING;
    }

    public void setIDLING(GreenDriveData IDLING) {
        this.IDLING = IDLING;
    }
}
