package com.waypals.objects;

import java.io.Serializable;

/**
 * Created by surya on 14/02/2017.
 */
public class GreenDriveData implements Serializable {

    String id;
    String points;
    String allowedInTrip;
    String title;
    TYPE type;
    public enum TYPE{
        CORNERING, ACCEL, BREAKING, IDLING, SPEED
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getAllowedInTrip() {
        return allowedInTrip;
    }

    public void setAllowedInTrip(String allowedInTrip) {
        this.allowedInTrip = allowedInTrip;
    }
}
