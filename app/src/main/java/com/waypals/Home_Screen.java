package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.analytics.ScoreActivity;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.MobileDeRegistration;
import com.waypals.rest.service.VehicleServices;
import com.waypals.timer.SOSSMSTimer;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.waypals.utils.WplHttpClient;

import java.util.Collection;
import java.util.Iterator;
import java.util.Timer;

public class Home_Screen extends NavigationDrawer implements AsyncFinishInterface {
    public static Context context;
    public static Collection<VehicleInformationVO> vehicleInfo;
    public static Handler refresh_handler = new Handler();
    static String tag = "Home Screen";
    static DrawerLayout myDrawer;
    static int totalCars = 0;
    static String[] vehicaleList;
    static TextView home_app_title;
    static VehicleServices vs = new VehicleServices();
    boolean[] isParkFenceOn;
    ImageView homebtnSlide_left;
    ImageView homebtnSlide_right;
    ApplicationPreferences appPref;
    AsyncFinishInterface asynFinishIterface;
    String location = "";
    Typeface tf;
    String errorMessage;
    VehicleServiceResponse vehicleServiceResponse;
    SOSSMSTimer sosSMSTimer;
    Timer timer;
    private Activity activity;
    private LocationManager locationManager;
    private LocationListener locationListener;

    private BroadcastReceiver fragmentHelper = null;

    public static void Alert_Dialog(final Context con) {

        final Dialog dialog = new Dialog(con,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.break_down_call);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            context = Home_Screen.this;

            fragmentHelper = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();

                    switch (action) {
                        case CH_Constant.DEREGISTRATION_COMPLETE:
                            Log.d(Home_Screen.tag, "Calling De-registration");
                            try {
                                closeProgressBar();
                                MobileDeRegistration mr = new MobileDeRegistration(appPref, Home_Screen.this, appPref.getDeviceRegistrationId(), null);
                                mr.execute();

                            } catch (Exception e) {
                                ExceptionHandler.handleException(e);
                            }
                            break;

                        case CH_Constant.NO_EMERGENCY_CONTACT:
                            onEmptyEmergencyContactList();
                            break;
                    }
                }
            };


            IntentFilter filter = new IntentFilter();
            filter.addAction(CH_Constant.DEREGISTRATION_COMPLETE);
            filter.addAction(CH_Constant.NO_EMERGENCY_CONTACT);
            LocalBroadcastManager.getInstance(this).registerReceiver(fragmentHelper, filter);

            tf = Typeface.createFromAsset(getAssets(),
                    CH_Constant.Font_Typface_Path);

            appPref = new ApplicationPreferences(context);
            // CH_Constant.setTypeface((ViewGroup)this.findViewById(android.R.id.content).getRootView(),tf);

            setContentView(R.layout.home_screen);

			/* setup left and right menu button listener for drawer */
            setupMenuButtonListeners();


            setUpLandingPageContent();
			/* setup main content list */
            // setupListContent();

			/* setup content */
            setupRightDrawerListener(tf);

			/* setup contents */
            getGpsLocation();

            Log.i(tag, "Load vehicle service data");

            if (CH_Constant.isNetworkAvailable(Home_Screen.this)) {
                vs.getVehiclesData(appPref, this, true);
            } else {
                createLeftPane(appPref.getVehicleResponse());
                setUpVehicleData();
                Utility.makeNewToast(this, CH_Constant.NO_NETWORK);
            }

            activity = this;

        } catch (Exception ex) {
            ex.printStackTrace();
            ExceptionHandler.handleException(ex, this);
            finish();
        }

    }

    private void onEmptyEmergencyContactList() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(Home_Screen.this, android.R.style.Theme_Holo_Light_Dialog));
        dialog.setTitle("No emergency contact available");
        dialog.setMessage("Add emergency contact now?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(Home_Screen.this, EmergencyNoActivity.class);
                startActivity(intent);
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dialog.setCancelable(true);
        dialog.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        TextView policyText = (TextView) findViewById(R.id.policy_expiry_title);
        if (appPref.getCurrentVehiclePolicyDueDate() != null) {
            policyText.setText(appPref.getCurrentVehiclePolicyDueDate());
        } else {
            policyText.setText(R.string.no_info);
        }
        checkImmobilization();
        super.onResume();
    }


    public void setupMenuButtonListeners() {

        myDrawer = (DrawerLayout) ((Activity) context)
                .findViewById(R.id.drawer_layout);
        Log.i(tag, "setup left menu listener");
        homebtnSlide_left = (ImageView) findViewById(R.id.homebtnSlide_left);

        homebtnSlide_left.setImageResource(R.drawable.left_slide_icon);
        homebtnSlide_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                myDrawer.openDrawer(Gravity.LEFT);
            }
        });
        Log.i(tag, "setup right menu listener");
        homebtnSlide_right = (ImageView) findViewById(R.id.homebtnSlide_right);

        homebtnSlide_right.setImageResource(R.drawable.right_slide_icon);
        homebtnSlide_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                myDrawer.openDrawer(Gravity.RIGHT);
            }
        });

    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fragmentHelper);
        super.onDestroy();
    }

    private void setUpLandingPageContent() {
        ImageView imgLocation = (ImageView) findViewById(R.id.img_vehicle_location);
        imgLocation.setClickable(true);
        imgLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home_Screen.this,
                        Map_Screen.class);
                startActivityForResult(i, 103);
            }
        });

        ImageView imgViewSOSCall = (ImageView) findViewById(R.id.img_sos_call);
        imgViewSOSCall.setClickable(true);
        imgViewSOSCall.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new
                        Intent(Home_Screen.this, SOS_Popup.class);
                startActivity(i);
            }
        });

        final ImageView imgViewSoSMsg = (ImageView) findViewById(R.id.img_sos_msg);
        imgViewSoSMsg.setClickable(true);
        imgViewSoSMsg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String sosSend = appPref.getSOSSend();
                if (sosSend == null || "false".equals(sosSend)
                        || sosSend.length() == 0) {
                    sosSMSTimer = new SOSSMSTimer(Home_Screen.this,
                            appPref, context);
                    timer = new Timer();
                    timer.schedule(sosSMSTimer, 1000, 30000);
                    appPref.saveSOSSend("true");
                    // TODO: change to cross
                    imgViewSoSMsg.setImageResource(R.drawable.stop_message_icon);

                } else {
                    stopSOSMessage();
                }
                return true;
            }
        });

        imgViewSoSMsg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String sosSend = appPref.getSOSSend();
                if (sosSend == null || "false".equals(sosSend)
                        || sosSend.length() == 0) {
                    Utility.makeNewToast(Home_Screen.this, "Long press to send sos message");
                }else
                {
                    stopSOSMessage();
                }
            }
        });

        LinearLayout lytInfoCalls = (LinearLayout) findViewById(R.id.layout_information_calls);
        lytInfoCalls.setClickable(true);
        lytInfoCalls.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home_Screen.this,
                        BreakDown_Popup.class);
                i.putExtra("action", "2");
                startActivity(i);
            }
        });

        LinearLayout lytImmob = (LinearLayout) findViewById(R.id.layout_immobilizer_on_off);
        lytImmob.setClickable(true);
        lytImmob.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home_Screen.this,
                        Engine_Screen.class);
                startActivity(i);
            }
        });

        LinearLayout lytTelematics = (LinearLayout) findViewById(R.id.layout_telematics);
        lytTelematics.setClickable(true);
        lytTelematics.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home_Screen.this,
                        ScoreActivity.class);
                i.putExtra(IntentConstants.VEHICLE_SERVICE_RESPONSE,
                        vehicleServiceResponse);
                startActivity(i);
            }
        });

        LinearLayout lytBreakDown = (LinearLayout) findViewById(R.id.layout_breakdown_call);
        lytBreakDown.setClickable(true);
        lytBreakDown.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home_Screen.this,
                        BreakDown_Popup.class);
                i.putExtra("action", "1");
                startActivity(i);
            }
        });

        LinearLayout lytAddIncident = (LinearLayout) findViewById(R.id.layout_addIncident);
        lytAddIncident.setClickable(true);
        lytAddIncident.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home_Screen.this,
                        Add_Incident.class);
                i.putExtra("action", "5");
                startActivity(i);
            }
        });

        LinearLayout lytGeofence = (LinearLayout) findViewById(R.id.layout_geofence);
        lytGeofence.setClickable(true);
        lytGeofence.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home_Screen.this, GeofenceActivity.class);
                i.putExtra("action", "6");
                startActivity(i);
            }
        });
    }

    private void stopSOSMessage() {
        try {
            appPref.saveSOSSend("false");
            if (timer != null) {
                timer.cancel();
                timer = null;
            }

            ImageView imgViewSoSMsg = (ImageView) findViewById(R.id.img_sos_msg);
            if (imgViewSoSMsg != null) {
                imgViewSoSMsg
                        .setImageResource(R.drawable.sos_msg_icon);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            ExceptionHandler.handlerException(e, this);
        }
    }

    @Override
    public void finish(String response, String method) {
        Log.i(tag, "method RES: " + method);
        Gson gson = new Gson();
        try {
            if (method != null && method.contains("vehiclesdata")) {
                Log.i(tag, "getVehiclesData RES: " + response);
                vehicleServiceResponse = gson.fromJson(response,
                        VehicleServiceResponse.class);

                if (vehicleServiceResponse != null && "SUCCESS"
                        .equalsIgnoreCase(vehicleServiceResponse.getResponse())) {
                    appPref.saveVehicleServiceResponse(response);
                    createLeftPane(vehicleServiceResponse);
                } else if (vehicleServiceResponse != null && "ERROR".equalsIgnoreCase(
                        vehicleServiceResponse.getResponse())) {
                    errorMessage = vehicleServiceResponse.getErrorCode();

                    Utility.makeNewToast(activity, "Server not responding, please try after some time!!");
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, this);
        }

    }

    private void createLeftPane(VehicleServiceResponse vehicleServiceResponse) {

        vehicleInfo = vehicleServiceResponse.getVehicles();

        int totalVehicle = vehicleInfo.size();
        vehicaleList = new String[totalVehicle];
        isParkFenceOn = new boolean[totalVehicle];
        totalCars = totalVehicle;
        appPref.saveTotalCar(totalVehicle);
        Log.i(tag, "Data RES: total vehicale " + totalVehicle);

        Iterator<VehicleInformationVO> v1 = vehicleInfo.iterator();
        int count = 0;
        while (v1.hasNext()) {
            VehicleInformationVO vehicleInformation = v1.next();
            Log.i(tag,
                    "Data RES: REG "
                            + vehicleInformation.getRegistrationNumber());
            Log.i(tag, "Data RES: REG " + vehicleInformation.getState());
            Log.i(tag, "Data RES: REG " + vehicleInformation.getVehicleId());
            Log.i(tag,
                    "Data RES: REG "
                            + vehicleInformation.isSystemFenceEnabled());
            vehicaleList[count] = vehicleInformation.getRegistrationNumber();
            isParkFenceOn[count] = vehicleInformation.isSystemFenceEnabled();
            if (count == 0) {
                appPref.saveVehicle_Selected(vehicleInformation.getVehicleId());
                appPref.saveCar1_Parkfence(vehicleInformation
                        .isSystemFenceEnabled());
                appPref.saveSelectedRegistrationNumber(vehicleInformation
                        .getRegistrationNumber());
                appPref.saveCurrentDeviceIsImmobilizer(vehicleInformation.isImobilizer());
                appPref.saveCurrentDeviceNumber(vehicleInformation.getDevicePhNo());
                appPref.saveCurrentUserNumber(vehicleInformation.getUserPhNo());

//                if(vehicleInformation.getPolicyDueDate() != null) {
//                    appPref.saveCurrentVehiclePolicyDueDate(Utility.parseServerDate(vehicleInformation.getPolicyDueDate().toString()));
//                }
                Utility.saveExpiryDates(appPref, vehicleInformation);
            }
            count++;
        }
        appPref.saveVehicalList(vehicaleList);

    }

    @Override
    public void onPostExecute(Void Result) {

        try {
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(context,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                finish();
                Intent i = new Intent(Home_Screen.this, Login_Screen.class);
                startActivity(i);
                return;
            }

            setUpVehicleData();

            VehicleServiceResponse vehicleResponse = appPref.getVehicleResponse();
            if (vehicleResponse != null){

                Collection<VehicleInformationVO> vehicles = vehicleResponse.getVehicles();

                if(vehicles != null){
                    Iterator<VehicleInformationVO> iterator = vehicles.iterator();
                    while (iterator.hasNext()){
                        VehicleInformationVO next = iterator.next();
                        if (next != null){
                            if (next.isFirstTimeLogin()){
                                Intent i = new Intent(Home_Screen.this, ChangePasswordActivity.class);
                                i.putExtra("firstTime", true);
                                startActivity(i);
                            }
                        }
                        break;
                    }
                }


            }


        } catch (Exception e) {
            Log.e("Home Screen", e.getMessage());
            ExceptionHandler.handlerException(e, this);
        }

    }

    private void setUpVehicleData() {
        if (vehicaleList != null) {
            setupLeftDrawerListener(appPref, tf, true);
            home_app_title = (TextView) findViewById(R.id.title_text);
            home_app_title.setText(appPref.getSelectedRegistrationNumber());
            home_app_title.setTypeface(tf);

            TextView txtUserName = (TextView) findViewById(R.id.text_user_name);
            if (appPref.getUserName() != null) {
                txtUserName.setText(appPref.getUserName().toLowerCase());
            }

            TextView policyText = (TextView) findViewById(R.id.policy_expiry_title);
            if (appPref.getCurrentVehiclePolicyDueDate() != null) {
                policyText.setText(appPref.getCurrentVehiclePolicyDueDate());
            } else {
                policyText.setText("No Info");
            }

            setImmobilizer();
            getSOSNumbers();

        } else if (appPref.getVehicleResponse() != null) {
            createLeftPane(appPref.getVehicleResponse());
        } else {
            Toast toast = Toast.makeText(context, "No Vehicle Data",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 60);
            toast.show();
            finish();
        }
    }

    private void getSOSNumbers() {
        new Thread() {
            public void run() {
                try {
                    WplHttpClient.callVolleyService(context,
                            CH_Constant.GET_SOS_NUMBERS,
                            VehicleServices.getVehicleIdRequest(appPref),
                            appPref, new AsyncFinishInterface() {
                                @Override
                                public void finish(String response, String method) throws Exception {
                                    appPref.saveSOSNumbers(response);
                                }

                                @Override
                                public void onPostExecute(Void Result) {

                                }
                            }, null, false);

                } catch (NetworkException e) {
                    Log.e("Home Screen ", "SOS Number...");
                    e.printStackTrace();
                }
            }

            ;
        }.start();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 103) {
            if (resultCode == RESULT_OK) {
                /* lets refresh content of left drawer and main title */
                setupLeftDrawerListener(appPref, tf, true);
                home_app_title = (TextView) findViewById(R.id.title_text);
                home_app_title.setText(appPref.getSelectedRegistrationNumber());
                home_app_title.setTypeface(tf);

                setImmobilizer();
            }
            if (resultCode == RESULT_CANCELED) {
                // Write your code if there's no result
            }
        }
    }


    @Override
    protected void setImmobilizer() {
        boolean isImmob = appPref.getCurrentDeviceIsImmobilizer();
        TextView immobText = (TextView) findViewById(R.id.text_immobilizer_onOff);
        ImageView immob = (ImageView) findViewById(R.id.img_immobilizer);
        float alpha = 1f;
        boolean isEnabled = isImmob;
        if (!isImmob) {
            immob.setImageResource(R.drawable.immobilizer_off_icon);
            immobText.setText(R.string.immobilizer_not_supported);
            alpha = 0.4f;
        } else {

            String currentSimNum = Utility.getCurrentPhoneNum(this);
            String currentUserRegNum = appPref.getCurrentUserNumber();
            String currentDeviceNum = appPref.getCurrentDeviceNumber();

            //Log.d("CurrentNum", currentSimNum);
            //Log.d("CurrentRegNum", currentUserRegNum);
            //Log.d("CurrentDeviceNum", currentDeviceNum);

//            if(!Utility.isStringNullEmpty(currentSimNum)  && !Utility.isStringNullEmpty(currentUserRegNum)
//                    && currentSimNum.equals(currentUserRegNum)) {
            if (!Utility.isStringNullEmpty(currentUserRegNum)) {
                if (!Utility.isStringNullEmpty(currentDeviceNum)) {
                    alpha = 1f;
                    isEnabled = true;
                    checkImmobilization();
                } else {
                    isEnabled = false;
                }
            } else {
                isEnabled = false;
                if (appPref.isFirstTimeLoggedIn()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Update registered number");
                    alert.setMessage("Phone number mismatched with the registered number,\n please update or use the registered number to access immobilizer feature");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, close
                            // current activity
                            dialog.cancel();
                        }
                    })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alert.create();

                    // show it
                    alertDialog.show();
                }

                appPref.saveFirstTimeLoggedIn(false);

                immob.setImageResource(R.drawable.immobilizer_off_icon);
                immobText.setText(R.string.immobilizer_not_supported);
                alpha = 0.4f;
            }
        }
        LinearLayout lytImmob = (LinearLayout) findViewById(R.id.layout_immobilizer_on_off);
        lytImmob.setEnabled(isEnabled);

        lytImmob.setAlpha(alpha);
    }

    private void checkImmobilization(){
        TextView immobText = (TextView) findViewById(R.id.text_immobilizer_onOff);
        ImageView immob = (ImageView) findViewById(R.id.img_immobilizer);

        boolean bEngineState = appPref.getEngineState();
        if (bEngineState) {
            immob.setImageResource(R.drawable.immobilizer_on_icon);
            immobText.setText(R.string.immobilizer_on);
        } else {
            immob.setImageResource(R.drawable.immobilizer_off_icon);
            immobText.setText(R.string.immobilizer_off);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getGpsLocation() {

        try {
            if (!Utility.checkLocationServiceEnabled(context)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Your dialog code.
                        if (ActivityCompat.checkSelfPermission(Home_Screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ActivityCompat.checkSelfPermission(Home_Screen.this,
                                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            return;
                        }
                        //Utility.showLocationDisabledAlert(context);
                    }
                });
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    appPref.saveCurrentLocation(location.getLatitude() + "," + location.getLongitude());
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, locationListener);
        } catch (Exception ex) {
            Log.e("Home Screen ", ex.getMessage());
            ExceptionHandler.handleException(ex);
        }
    }

    class Content {
        public int icon;
        public String title;
        boolean sms;
        boolean call;

        public Content(int icon, String title, boolean isSMS, boolean isCall) {
            super();
            this.icon = icon;
            this.title = title;
            this.sms = isSMS;
            this.call = isCall;
        }

        public Content(String title) {
            super();
            this.title = title;
        }
    }

    class ContentAdapter extends ArrayAdapter<Content> {
        Context context;
        int layoutResourceId;
        Content data[] = null;

        public ContentAdapter(Context context, int resource, Content[] objects) {
            super(context, resource, objects);
            this.layoutResourceId = resource;
            this.context = context;
            this.data = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            // ContentsHolder holder;
            LayoutInflater inflater = ((Home_Screen) context)
                    .getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            final ContentsHolder holder = new ContentsHolder();
            holder.imgIcon = (ImageView) row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
            holder.txtTitle.setTypeface(tf);
            holder.imgIcon1 = (ImageView) row.findViewById(R.id.imgIcon1);
            holder.imgIconSMS = (ImageView) row.findViewById(R.id.imgIconMsg);
            row.setTag(holder);
            Content drawerContent = data[position];
            holder.txtTitle.setText(drawerContent.title);
            holder.imgIcon.setImageResource(drawerContent.icon);
            if (position == 0) {
                holder.imgIcon1.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Home_Screen.this, SOS_Popup.class);
                        startActivity(i);

                    }
                });
                holder.imgIconSMS.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        try {
                            String sosSend = appPref.getSOSSend();
                            if (sosSend == null || sosSend.equals("false")
                                    || sosSend.length() == 0) {
                                sosSMSTimer = new SOSSMSTimer(Home_Screen.this,
                                        appPref, context);
                                timer = new Timer();
                                timer.schedule(sosSMSTimer, 1000, 30000);
                                appPref.saveSOSSend("true");
                                // TODO: change to cross
                                holder.imgIconSMS
                                        .setImageResource(R.drawable.stop_message_icon);

                            } else {
                                appPref.saveSOSSend("false");
                                if (timer != null) {
                                    timer.cancel();
                                    timer = null;
                                }
                                holder.imgIconSMS
                                        .setImageResource(R.drawable.message_icon);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.e("Home Screen", e.getMessage());
                        }
                    }
                });

            }

            if (!drawerContent.call) {
                holder.imgIcon1.setVisibility(View.INVISIBLE);
                // holder.txtTitle.setGravity(Gravity.LEFT);
            }
            if (!drawerContent.sms) {
                holder.imgIconSMS.setVisibility(View.INVISIBLE);
                holder.imgIconSMS.getLayoutParams().width = 0;
                holder.imgIconSMS.getLayoutParams().height = 0;
                RelativeLayout.LayoutParams m = (RelativeLayout.LayoutParams) (holder.imgIconSMS.getLayoutParams());
                m.setMargins(0, 0, 0, 0);
                holder.imgIconSMS.setLayoutParams(m);
                holder.imgIconSMS.requestLayout();
            }
            return row;

        }

        class ContentsHolder {
            ImageView imgIcon;
            TextView txtTitle;
            ImageView imgIconSMS;
            ImageView imgIcon1;
        }
    }

}
