package com.waypals.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Cordinate;
import com.waypals.feedItems.RouteCoordinates;
import com.waypals.gson.vo.SosNumber;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.R;
import com.waypals.objects.DateTime;
import com.waypals.objects.EmergencyContact;
import com.waypals.request.TimeRange;

import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    private static AlertDialog alert;
    private static String tag = "waypals Util";

    private static final String EMAIL_REGEX = "^[A-Za-z0-9_\\-\\.]+(\\.[A-Za-z0-9\\-]+)*(\\_[A-Za-z0-9]+)*@[A-Za-z0-9_\\-]+(\\.([A-Za-z0-9_\\-]{2,4})+)*(\\.[A-Za-z]{2,4})$";

    public static boolean isValidEmailId(String email) {

        if(isStringNullEmpty(email))
        {
            return false;
        }

        Pattern r = Pattern.compile(EMAIL_REGEX);

        Matcher matcher = r.matcher(email);

        return matcher.find();

    }

    public static boolean isValidPhone(String phone) {
        return Patterns.PHONE.matcher(phone).matches() && Pattern.compile("(^\\d{10}$)").matcher(phone).matches();

    }

    public static boolean isNumber(String deviceId) {
        try
        {
            if( !Utility.isStringNullEmpty(deviceId)) {
                Long number = Long.parseLong(deviceId);
                return true;
            }
        }
        catch (NumberFormatException ex)
        {}
        return false;
    }

    public static boolean isValidPinCode(String s) {

        if(isNumber(s)){
            if(s.length() == 6){
                return true;
            }else{
                return false;
            }
        }

        return false;
    }

    public static String[] getLast12Months() {
        String data[] = new String[12];
        SimpleDateFormat sdf = new SimpleDateFormat("MMM, yyyy");

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        data[0] = sdf.format(c.getTime());
        for(int i = 1; i < 12; i++)
        {
            c.add(Calendar.MONTH, -1);
            data[i] = sdf.format(c.getTime());
        }

        return data;
    }

    public static TimeRange getTimeRange(String date){
        TimeRange time = null;

        try {
        if(!isStringNullEmpty(date)){
            time = new TimeRange();
            SimpleDateFormat sdf = new SimpleDateFormat("MMM, yyyy");
            Date parse = sdf.parse(date);

            Calendar c = Calendar.getInstance();
            c.setTime(parse);

            String startDate = "1/" + (c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR)+" 00:00:00";
            String endDate = ""+ c.getActualMaximum(Calendar.DATE) +"/" + (c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR)+" 00:00:00";
            SimpleDateFormat new_sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date startD = new_sdf.parse(startDate);
            Date endD = new_sdf.parse(endDate);


            String startServerDate = Utility.convertToString(startD, TimeZone.getTimeZone("UTC"));
            String endServerDate = Utility.convertToString(endD, TimeZone.getTimeZone("UTC"));

            time.setStartTime(startServerDate);
            time.setEndTime(endServerDate);


        }}catch (Exception ex){
            ExceptionHandler.handleException(ex);
        }

        return time;
    }

    public enum WayPointType{Entertainment("Entertainment"), Hospital("Hospital"), Restaurant("Restaurant"), PetrolPump("Petrol Pump"), Start("Start"), End("End");

        String value;
        WayPointType(String s) {
            this.value = s;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED
                || connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTING) {
            return true;
        } else if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTING) {
            // TODO Implement dialog asking user to continue in GPRS mode or not
            return true;
        } else
            return false;
    }

    private static void showDialog(String message, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
        builder.setCancelable(false).setTitle("")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        alert.dismiss();
                    }
                });
        builder.setMessage(message);
        alert = builder.create();
        alert.show();
    }

    public static Bitmap decodeFile(File f) {
        Bitmap bm = null;
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 120;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            o2.inPurgeable = true;
            bm = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bm;
    }

    public static Bitmap makeImageBitmap(Context context, String imageString) {
        Bitmap imageDefault = BitmapFactory.decodeResource(context.getResources(), R.drawable.picture_holder);
        Bitmap image = null;
        if (imageString != null && imageString != "") {
            try {
//				byte[] decodeByte=Base64.decode(imageString, Base64.URL_SAFE);
//				byte[] decodeByte=Base64.decode(imageString, Base64.URL_SAFE);
//				image=BitmapFactory.decodeByteArray(decodeByte, 0, decodeByte.length);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                Log.e("Utility.class-Exception", String.valueOf(e.getMessage()));
                image = imageDefault;
            } catch (NullPointerException e) {
                e.printStackTrace();
                Log.e("Utility.class-Exception", String.valueOf(e.getMessage()));
                image = imageDefault;
            }
        } else {
            image = imageDefault;
        }
        return image;
    }

    public static DateTime convertToDateTime(String date)
    {
        SimpleDateFormat oldFormat = new SimpleDateFormat("MMM d, yyyy");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-dd-MM");

        if(date != null)
        {
            try {
                Date oldDate = oldFormat.parse(date);
                Date newDate = newFormat.parse(newFormat.format(oldDate));
                DateTime d =  new DateTime(newDate, newFormat);
                return d;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        return null;
    }

    public static void main(String[] args)
    {
        System.out.println(""+getDate(0));
        System.out.println(formatDate("Mar 15, 2017"));
        System.out.println(new DateTime(formatDate("Mar 15, 2017")));

    }

    public static Bitmap getResizedBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        int scaleWidth = (int) ((width / 2));
        int scaleHeight = (int) ((height / 2));

        // Create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // Resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // Recreate the new Bitmap

        return bm.createScaledBitmap(bm, scaleWidth, scaleHeight, false);

    }

    public static String getImageToHex(Activity activity, String filepath) {
        if (filepath != null && !filepath.isEmpty()) {


            try {
                byte[] data = null;
                File src = new File(filepath);
                DataInputStream dis = new DataInputStream(new FileInputStream(src));
                Log.d(tag, "Image size: " + dis.available());
                data = new byte[dis.available()];
                dis.readFully(data);
                dis.close();

                System.gc();

                char[] toBe = Hex.encodeHex(data);
                return new String(toBe);

            } catch (Exception ex) {
                ex.printStackTrace();
                ExceptionHandler.handleException(ex);
                return "";
            }
        } else {
            return "";
        }

    }

    public static String getImageToHex(Bitmap bitmap)
    {
        if(bitmap != null)
        {
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                char[] toBe = Hex.encodeHex(byteArray);
                return new String(toBe);
            }catch (Exception ex)
            {
                Log.e("Get Image To Hex", ex.getMessage());
                ExceptionHandler.handleException(ex);
            }
        }
        return null;
    }

    public static String getCurrentUtcTime() {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = sdf.format(d);
        return date;
    }

    public static Date convertToDate(String str) {

        try {
            str = parseDate(str);
            SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
            istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            return istFormat.parse(str);
        }catch (Exception ex){

        }

        return null;
    }

    public static String getSecMinHourDayFormat(int time){

        String result = "";

        if(time <60){
            result = ""+time+" (sec)";
        }else if(time >=60 && time <3600){
            result = ""+time/60 + " m "+time%60 + " s";
        }else if(time >=3600 && time <3600*24){
            result = ""+time/3600 + " h ";

            int rem = time % 3600;
            if(rem <60){
                result = result+" "+rem+" s";
            }else if(rem >=60 && rem <3600){
                result = result+" "+rem/60 + " m "+rem%60 + " s";
            }
        }else if(time >= 3600*24){
            result = ""+time/24*3600+" d";

            int r = time%24*3600;
            if(r >=3600 && r <3600*24){
                result = ""+r/3600 + " h ";

                int rem = r % 3600;
                if(rem <60){
                    result = result+" "+rem+" s";
                }else if(rem >=60 && rem <3600){
                    result = result+" "+rem/60 + " m "+rem%60 + " s";
                }
            }
        }

        return result;
    }

    public static List<SosNumber> convertToSosContacts(List<EmergencyContact> lstContacts)
    {
        List<SosNumber> lstSosContacts = null;

        if(lstContacts != null)
        {
            lstSosContacts = new ArrayList<>();
            for(EmergencyContact contact : lstContacts)
            {
                SosNumber sosContacts = new SosNumber();

                sosContacts.setCountryCode(contact.getCountryCode());
                sosContacts.setEmergencyPhoneNumber(String.valueOf(contact.getPhoneNumber()));
                sosContacts.setName(contact.getName());
                sosContacts.setRelation(contact.getRelation());

                lstSosContacts.add(sosContacts);
            }
        }

        return lstSosContacts;
    }

    public static boolean isSameDay(String startDate, String endDate){

        boolean result = true;
        try {
            if (startDate != null && endDate != null) {

                String[] split1 = startDate.split("\\s");
                String[] split2 = endDate.split("\\s");
                if (split1 != null && split1.length > 1 && split2 != null && split2.length > 1) {
                    if ("PM".equalsIgnoreCase(split1[1]) && "AM".equalsIgnoreCase(split2[1])) {
                        result = false;
                    } else {
                        int hrStart = parseDateFor(startDate, 0);
                        int mmStart = parseDateFor(startDate, 1);
                        int ssStart = parseDateFor(startDate, 2);

                        int hrEnd = parseDateFor(endDate, 0);
                        int mmEnd = parseDateFor(endDate, 1);
                        int ssEnd = parseDateFor(endDate, 2);

                        // 10:10:00 9:59:59 10:00:10

                        if(hrStart > hrEnd){
                             result = false;
                        }else if (hrStart == hrEnd){
                            if( mmStart > mmEnd){
                                result = false;
                            }else if( mmStart == mmEnd){
                                if(ssStart > ssEnd){
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }
        }catch (Exception ex) {
            ExceptionHandler.handleException(ex);
        }
        return result;
    }

    public static int parseDateFor(String date, int c){

        int code = 0;
        if (date != null){

            String[] split = date.split("\\s");
            String[] times = split[0].split("\\:");
            switch (c){
                case 0: code = Integer.parseInt(times[0]); break;
                case 1: code = Integer.parseInt(times[1]); break;
                case 2: code = Integer.parseInt(times[2]); break;
            }

            if ("AM".equals(split[1])){
                if(Integer.parseInt(times[0]) == 12){
                    if(Integer.parseInt(times[1]) == 0){
                        if(Integer.parseInt(times[2]) == 0){
                            if(c == 0){
                                code = 0;
                            }
                        }
                    }
                }
            }else  if ("PM".equals(split[1])){
                if(Integer.parseInt(times[0]) == 12){
                    if(Integer.parseInt(times[1]) == 0){
                        if(Integer.parseInt(times[2]) == 0){
                            if(c == 0){
                                code = code + 12;
                            }
                        }
                    }
                }else if(Integer.parseInt(times[0]) >= 1 && Integer.parseInt(times[0]) < 12){
                    code += 12;
                }
            }
        }

        return code;
    }

    public static boolean isBeforeCurrentDate(String date)
    {
        try {
        SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date deviceDate = null;
        if (date != null && date.contains("UTC")) {
            SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            deviceDate = utcFormat.parse(date);


            istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            date = istFormat.format(deviceDate);

            long parse = istFormat.parse(date).getTime();
            Date d = new Date();
            long time = d.getTime();

            return time > parse;

        } else if (date != null && date.contains("IST")) {
            SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z", Locale.ENGLISH);
            date = date.replace(" IST", " GMT+0530");
            utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

            deviceDate = utcFormat.parse(date.trim());

            istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            date = istFormat.format(deviceDate);

            long parse = istFormat.parse(date).getTime();
            Date d = new Date();
            long time = d.getTime();
            return time > parse;
        }
        else if (date != null && date.contains("CEST")) {
            SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
            date = date.replace(" CEST", " GMT+02:00");
            utcFormat.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
            deviceDate = utcFormat.parse(date);

            istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            date = istFormat.format(deviceDate);

            long parse = istFormat.parse(date).getTime();
            Date d = new Date();
            long time = d.getTime();
            return time > parse;
        }
        return true;
    } catch (Exception ex) {
        ex.printStackTrace();
        return true;
    }

    }

    public static String toUTC(String date){
        try{
            SimpleDateFormat gmtFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
            TimeZone gmtTime = TimeZone.getTimeZone("GMT");
            gmtFormat.setTimeZone(gmtTime);

            SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
            istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            gmtFormat.setTimeZone(gmtTime);

            return (gmtFormat.format(istFormat.parse(date)));

        }catch(Exception ex){

        }

        return null;
    }


    public static String getUTCtoIST(String date) {
        try {
            SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date deviceDate = null;
            if (date != null && date.contains("UTC")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                deviceDate = utcFormat.parse(date);


                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date.toString();

            } else if (date != null && date.contains("IST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z", Locale.ENGLISH);
                date = date.replace(" IST", " GMT+0530");
                utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

                deviceDate = utcFormat.parse(date.trim());

                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date.toString();
            }
            else if (date != null && date.contains("CEST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                date = date.replace(" CEST", " GMT+02:00");
                utcFormat.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
                deviceDate = utcFormat.parse(date);

                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date.toString();
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String formatDate(String date)
    {

        SimpleDateFormat oldFormat = new SimpleDateFormat("MMM d, yyyy");
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");

        if(date != null)
        {
            try {
                Date oldDate = oldFormat.parse(date);
                return newFormat.format(oldDate);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        return null;
    }

    public static String parseServerDate(String date) {
        try {
            SimpleDateFormat istFormat = new SimpleDateFormat("MMM d, yyyy");
            Date deviceDate = null;
            if (date != null && date.contains("UTC")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                deviceDate = utcFormat.parse(date);

                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date.toString();

            } else if (date != null && date.contains("IST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z", Locale.ENGLISH);
                date = date.replace(" IST", " GMT+0530");
                utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

                deviceDate = utcFormat.parse(date.trim());

                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date.toString();
            }
            else if (date != null && date.contains("CEST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                date = date.replace(" CEST", " GMT+02:00");
                utcFormat.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
                deviceDate = utcFormat.parse(date);

                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date.toString();
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String parseDate(String date) {
        try {
            Date deviceDate = null;
            if (date != null && date.contains("UTC")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                deviceDate = utcFormat.parse(date);

                SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date;

            } else if (date != null && date.contains("IST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z", Locale.ENGLISH);
                date = date.replace(" IST", " GMT+0530");
                utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

                deviceDate = utcFormat.parse(date.trim());

                SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date;
            }
            else if (date != null && date.contains("CEST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                date = date.replace(" CEST", " GMT+02:00");
                utcFormat.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
                deviceDate = utcFormat.parse(date);

                SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String parseDateForTrip(String date) {
        try {
            Date deviceDate = null;
            if (date != null && date.contains("UTC")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                deviceDate = utcFormat.parse(date);

                SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date;

            } else if (date != null && date.contains("IST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z", Locale.ENGLISH);
                date = date.replace(" IST", " GMT+0530");
                utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

                deviceDate = utcFormat.parse(date.trim());

                SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date;
            }
            else if (date != null && date.contains("CEST")) {
                SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                date = date.replace(" CEST", " GMT+02:00");
                utcFormat.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
                deviceDate = utcFormat.parse(date);

                SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                date = istFormat.format(deviceDate);

                return date;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public static Long getDateToLong(String date) {
        try {
            Date deviceDate = null;
            if (date != null) {
                if (date.contains("UTC")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    deviceDate = sdf.parse(date);

                    sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
                    date = sdf.format(deviceDate);

                    Date values = sdf.parse(date);
                    return values.getTime();
                }


                else if (date.contains("CEST")) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                    sdf.setTimeZone(TimeZone.getTimeZone("CEST"));
                    deviceDate = sdf.parse(date);

                    sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                    date = sdf.format(deviceDate);

                    Date values = sdf.parse(date);
                    return values.getTime();
                }
                else if (date != null && date.contains("IST")) {
                    SimpleDateFormat utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z", Locale.ENGLISH);
                    date = date.replace(" IST", " GMT+0530");
                    utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

                    deviceDate = utcFormat.parse(date.trim());

                    SimpleDateFormat istFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
                    istFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                    date = istFormat.format(deviceDate);

                    Date values = istFormat.parse(date);
                    return values.getTime();
                }
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String convertToString(Date date, TimeZone timeZone){

        if(date != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
            df.setTimeZone(timeZone);
            return df.format(date);
        }
        return "";
    }

    public static long toLocalTime(long time, TimeZone to) {
        return time + to.getOffset(time);
    }

    public static long toUTC(long time, TimeZone from) {
        return time - from.getOffset(time);
    }


    public static Date getDate(int days){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("IST"));
        calendar.add(Calendar.DATE, days);

        return new Date(toUTC(calendar.getTime().getTime(), calendar.getTimeZone()));
    }

    public static String getInitCaps(String str) {
        str = str.toLowerCase();
        str = (str.charAt(0) + "").toUpperCase() + str.substring(1, str.length());
        return str;
    }

    /*this will change all the settings for selected car in appPref, using car registration number*/
    public static void setCurrentCarSettings(String carRegNum, ApplicationPreferences appPref, Collection<VehicleInformationVO> vehicleInfoVO) {
        try {
            // = Home_Screen.vehicleInfo;

            if (vehicleInfoVO != null && vehicleInfoVO.size() > 0) {
                Iterator<VehicleInformationVO> v1 = vehicleInfoVO.iterator();

                while (v1.hasNext()) {
                    VehicleInformationVO vehicleInformation = v1.next();

                    if (vehicleInformation != null && vehicleInformation.getRegistrationNumber() != null &&
                            vehicleInformation.getRegistrationNumber().equals(carRegNum)) {

                        appPref.saveSelectedRegistrationNumber(vehicleInformation.getRegistrationNumber());
                        appPref.saveCar1_Parkfence(vehicleInformation.isSystemFenceEnabled());

                        if(vehicleInformation.getLocation() != null) {
                            appPref.saveSelectedCarLatLng(vehicleInformation.getLocation().toString());
                        }else {
                            appPref.saveSelectedCarLatLng(null);
                        }

                        appPref.saveCurrentDeviceNumber(vehicleInformation.getDevicePhNo());
                        appPref.saveVehicle_Selected(vehicleInformation.getVehicleId());
                        appPref.saveCurrentDeviceIsImmobilizer(vehicleInformation.isImobilizer());

                        saveExpiryDates(appPref, vehicleInformation);
                        Log.i(tag, "New Selected Car : " + vehicleInformation.getRegistrationNumber());
                        Log.i(tag, "New Selected Car " + vehicleInformation.getState());
                        Log.i(tag, "New Selected Car " + vehicleInformation.getVehicleId());
                        Log.i(tag, "New Selected Car " + vehicleInformation.isSystemFenceEnabled());
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public static void saveExpiryDates(ApplicationPreferences appPref, VehicleInformationVO vehicleInformation)
    {
        if(appPref != null && vehicleInformation != null) {

            if(vehicleInformation.getLastServiceDate() != null) {
                appPref.saveCurrentVehicleLastServiceDate(Utility.parseServerDate(vehicleInformation.getLastServiceDate().toString()));
            }else
            {
                appPref.saveCurrentVehicleLastServiceDate(CH_Constant.NO_INFO);
            }

            if(vehicleInformation.getPolicyDueDate() != null) {
                appPref.saveCurrentVehiclePolicyDueDate(Utility.parseServerDate(vehicleInformation.getPolicyDueDate().toString()));
            }
            else {
                appPref.saveCurrentVehiclePolicyDueDate(CH_Constant.NO_INFO);
            }

            if(vehicleInformation.getPollutionDueDate() != null) {
                appPref.saveCurrentVehiclePollutionDate(Utility.parseServerDate(vehicleInformation.getPollutionDueDate().toString()));
            } else {
                appPref.saveCurrentVehiclePollutionDate(CH_Constant.NO_INFO);
            }

            if(vehicleInformation.getLicenceExpDate() != null) {
                appPref.saveCurrentVehicleLicenceDate(Utility.parseServerDate(vehicleInformation.getLicenceExpDate().toString()));
            }else {
                appPref.saveCurrentVehicleLicenceDate(CH_Constant.NO_INFO);
            }
        }
    }


    public static File getHexToImage(String hexString, String fileId) {
        try {
            String dir = Environment.getExternalStorageDirectory().getAbsolutePath();
            String path = "/waypals/Feed/Cache";
            File destinationDir = new File(dir + path);

            if (destinationDir.isDirectory()) {
                destinationDir.mkdirs();
                Log.d(tag, "feed cached file dir created");
            }

            destinationDir = new File(dir + path + fileId);
            if (destinationDir.isFile()) {
                // File already exists return this one
            } else {
                // File not exists create this one
                char[] data = new char[hexString.length()];
                hexString.getChars(0, hexString.length(), data, 0);
                byte[] img = Hex.decodeHex(data);
                DataOutputStream dos = new DataOutputStream(new FileOutputStream(destinationDir));
                dos.write(img);
                dos.close();
            }

            return destinationDir;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public enum PushMessageType {
        IN_NETWORK, TOWED,
        TRIP_END,
        IMMOB_RES,
        DEVICE_UNPLUGED, DEVICE_CRASHED, HARSH_ACCELERATION, HARSH_BRAKING, HARSH_CORNERING,
        SYSTEM_FENCE_VIOLATED, GEO_FENCE_VIOLATED, OUT_OF_BOUND_FENCE_VIOLATED, FOLLOW_ME, FRIEND_REQUEST,
        SPEED_LIMIT_VIOLATED, INITIATE_CHANGE_PASSWORD , SHARE_LOCATION , FUEL_LIMIT_VIOLATED,BATTERY_DISCHARGED,BATTERY_CHARGING_FAULT,VEHICLE_POLICY_ALERT,VEHICLE_POLLUTION_ALERT,VEHICLE_SERVICE_ALERT, DEFAULT
    }

    public static String getPushAlertName(String type) {
        if (type != null) {
            if (type.equalsIgnoreCase(PushMessageType.SYSTEM_FENCE_VIOLATED.toString()) ) {
                return "Secure Park alert";
            } else if (type.equalsIgnoreCase(PushMessageType.IN_NETWORK.toString())) {
                return "In network alert";
            } else if (type.equalsIgnoreCase(PushMessageType.TOWED.toString())) {
                return "Towed alert";
            } else if (type.equalsIgnoreCase(PushMessageType.GEO_FENCE_VIOLATED.toString()) || type.equalsIgnoreCase(PushMessageType.OUT_OF_BOUND_FENCE_VIOLATED.toString())) {
                return "Geo fence alert";
            } else if (type.equalsIgnoreCase(PushMessageType.SPEED_LIMIT_VIOLATED.toString())) {
                return "Speed alert";
            } else if (type.equalsIgnoreCase(PushMessageType.SHARE_LOCATION.toString())) {
                return "Share location";
            } else if(type.equalsIgnoreCase(PushMessageType.FUEL_LIMIT_VIOLATED.toString()))
            {
                return "Fuel Limit Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.BATTERY_DISCHARGED.toString()))
            {
                return  "Battery Discharged Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.BATTERY_CHARGING_FAULT.toString()))
            {
                return "Battery Charging Fault";
            }
            else if(type.equalsIgnoreCase(PushMessageType.VEHICLE_POLICY_ALERT.toString()))
            {
                return  "Vehicle Policy Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.VEHICLE_POLLUTION_ALERT.toString()))
            {
                return  "Vehicle Pollution Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.VEHICLE_SERVICE_ALERT.toString()))
            {
                return  "Vehicle Service Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.INITIATE_CHANGE_PASSWORD.toString()))
            {
                return  "Password Changed Alert";
            }
            else if(PushMessageType.FOLLOW_ME.toString().equalsIgnoreCase(type))
            {
                return "Follow me Request";
            }
            else if(PushMessageType.FRIEND_REQUEST.toString().equalsIgnoreCase(type))
            {
                return "Friend Request";
            }
            else if(type.equalsIgnoreCase(PushMessageType.DEVICE_UNPLUGED.toString()))
            {
                return "Device Unplugged Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.DEVICE_CRASHED.toString()))
            {
                return "Device Crashed Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.HARSH_ACCELERATION.toString()))
            {
                return "Harsh Acceleration Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.HARSH_BRAKING.toString()))
            {
                return "Harsh Braking Alert";
            }
            else if(type.equalsIgnoreCase(PushMessageType.HARSH_CORNERING.toString()))
            {
                return "Harsh Cornering Alert";
            }else if(type.equalsIgnoreCase(PushMessageType.TRIP_END.toString()))
            {
                return "Trip Score";
            }
            else {
                return "Alerts";
            }
        } else {
            return "ALERT";
        }

    }

    public static ArrayList<Boolean> createConfirmationAlert(Context context, String message, String title) {
        try {
            final ArrayList<Boolean> result = new ArrayList<Boolean>();
            AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
            dialog.setTitle(title);
            dialog.setMessage(message);
            dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    result.add(true);
                }
            });
            dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    result.add(false);
                }
            });
            dialog.setCancelable(false);
            dialog.create();

            return result;
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
            return null;
        }
    }

    public static void makeToastBottom(Context context, String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 30);
        toast.show();
    }

    public static void makeToast(Context context, String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 30);
        toast.show();
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = Utility.calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    public static String getCurrentPhoneNum(Context context)
    {
        String mobNum = null;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            mobNum = tm.getLine1Number();
//            Log.d("Sim Num", mobNum);

        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, (Activity) context);
        }
        return mobNum;
    }

    public static boolean isStringNullEmpty(String value)
    {
        if(value != null && !value.trim().isEmpty())
        {
            return false;
        }

        return true;
    }

    public static void makeNewToast(Activity context, String text) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast, null, false);
        TextView t = (TextView) layout.findViewById(R.id.textView);
        t.setText(text);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.setGravity(Gravity.TOP, 0, 200);
        toast.show();
    }

    public static void makeNewToast(Activity context, String text, Integer gravity) throws Exception {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast, null, false);
        TextView t = (TextView) layout.findViewById(R.id.textView);
        t.setText(text);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.setGravity(gravity, 0, 200);
        toast.show();
    }

    public static void makeNewToast(Activity context, String text, int top) throws Exception {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast, null, false);
        TextView t = (TextView) layout.findViewById(R.id.textView);
        t.setText(text);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.setGravity(Gravity.TOP, 0, top);
        toast.show();
    }

    public static void browseImageSource(final Activity activity, final int CAMERA_PICTURE, final int GALLERY_PICTURE) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            activity.startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    activity.startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.show();
    }

    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/" + "temp.jpg");
        return image;
    }


    public static void openUrl(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static Intent openGoogleMap(Context context, Float latitude, Float longitude) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f", latitude, longitude, latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public static void onUserChanged(ApplicationPreferences appPrefs) {
        try {
            appPrefs.removeFeeds();
            appPrefs.removeMessages();
            appPrefs.clear();
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }
    }

    public static File createFile() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();

            File dir = new File(storageDir + "/_cachedImages");
            if (!dir.isDirectory()) {
                dir.mkdir();
            }

            File image = new File(dir.getAbsolutePath() + "/" + timeStamp + "_temp.jpg");

            return image;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static File createFileWithPath(String path) {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();

            File dir = new File(storageDir + "/_cachedImages");
            if (!dir.isDirectory()) {
                dir.mkdir();
            }

            File image = new File(path + "/" + timeStamp + "_temp.jpg");

            return image;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static File createTempImageFile(Bitmap bitmap) {
        try {

            if(bitmap == null)
            {
                return null;
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();

            File dir = new File(storageDir + "/_cachedImages");
            if (!dir.isDirectory()) {
                dir.mkdir();
            }

            File image = new File(dir.getAbsolutePath() + "/" + timeStamp + "_temp.jpg");
            FileOutputStream ostream = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
            ostream.close();
            return image;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void clearTempImgDir() {
        try {
            String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();

            File dir = new File(storageDir + "/_cachedImages");
            if (dir.isDirectory()) {
                dir.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SendSMS(final String number, final String text, Context context) {
        Log.i(tag, "send sms to " + number + " | message: " + text);
        final SmsManager smsManager = SmsManager.getDefault();

        final String SENT = "SMS_SENT";

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        final Activity activity = (Activity) context;
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(activity, "SMS Sent.", Toast.LENGTH_LONG)
                                .show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(activity, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(activity, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(activity, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(activity, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT));

        smsManager.sendTextMessage(number, null, text, null, null);
    }

    public static void showLocationDisabledAlert(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
        builder
                .setTitle("Enable GPS")
                .setMessage("Please enable GPS to use current location")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(viewIntent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static Boolean checkLocationServiceEnabled(Context activity) {
        LocationManager lm = (LocationManager) activity.getSystemService(activity.LOCATION_SERVICE);
        return (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    public static String getLongToDate(Long time) {
        Date d = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("'on' dd/MM/yyyy 'at' hh:mm aa");
        String value = sdf.format(d);
        return value;
    }

    public static String parseServerDate(Long time) {
        Date d = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
        String value = sdf.format(d);
        return value;
    }

    public static String getLongToDateOnly(Long time) {
        Date d = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("'on' dd/MM/yyyy");
        String value = sdf.format(d);
        return value;
    }

    public static int getScreenSize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        return display.getWidth();
    }

    public static List<LatLng> getLatLngFromCoordinates(RouteCoordinates[] data)
    {
        List<LatLng> lstCoordinates = null;

        if(data != null)
        {
            lstCoordinates = new ArrayList<LatLng>();
            for(RouteCoordinates coord : data)
            {
                double lat = Double.parseDouble(coord.getLatitude());
                double lon = Double.parseDouble(coord.getLongitude());
                LatLng latLng = new LatLng(lat, lon);
                lstCoordinates.add(latLng);
            }
        }

        return lstCoordinates;
    }


    public static LatLng getLatLngFromCoordinate(Cordinate data)
    {
        LatLng lstCoordinates = null;

        if(data != null)
        {
                double lat = Double.parseDouble(data.getLatitude());
                double lon = Double.parseDouble(data.getLongitude());
                lstCoordinates = new LatLng(lat, lon);
        }

        return lstCoordinates;
    }

    public static BitmapDescriptor getMarkerIcon(String type)
    {
        if(WayPointType.Entertainment.toString().equalsIgnoreCase(type))
        {
            return BitmapDescriptorFactory.fromResource(R.drawable.entertainment);
        }
        else if(WayPointType.Hospital.toString().equalsIgnoreCase(type))
        {
            return BitmapDescriptorFactory.fromResource(R.drawable.hospital);
        }
        else if(WayPointType.Restaurant.toString().equalsIgnoreCase(type))
        {
            return BitmapDescriptorFactory.fromResource(R.drawable.restaurant);
        }
        else if(WayPointType.PetrolPump.toString().equalsIgnoreCase(type))
        {
            return BitmapDescriptorFactory.fromResource(R.drawable.petrol_pump);
        }
        else
        {
            return BitmapDescriptorFactory.fromResource(R.drawable.circle_pointer);
        }

    }

    public static final Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getDrawable(context, id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

}
