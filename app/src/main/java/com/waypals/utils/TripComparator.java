package com.waypals.utils;

import com.waypals.objects.ObdEndOfTrip;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by surya on 12/5/16.
 */
public class TripComparator implements Comparator<ObdEndOfTrip> {

    @Override
    public int compare(ObdEndOfTrip lhs, ObdEndOfTrip rhs) {

        int result = 0;
        if (lhs != null && rhs != null) {
            if (lhs.getStartTime() != null && rhs.getStartTime() != null) {

                try {
                    Date leftDate = Utility.convertToDate(lhs.getStartTime());
                    Date rightDate = Utility.convertToDate(rhs.getStartTime());
                    if (leftDate != null && rightDate != null) {
                        result = leftDate.before(rightDate) ? 1 : -1;
                    }
                } catch (Exception ex) {
                }
            }
        }
        return result;
    }
}
