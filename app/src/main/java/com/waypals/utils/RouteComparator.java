package com.waypals.utils;

import com.waypals.objects.VehicleTripOverview;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by surya on 12/5/16.
 */
public class RouteComparator implements Comparator<VehicleTripOverview> {

    @Override
    public int compare(VehicleTripOverview lhs, VehicleTripOverview rhs) {

        if (lhs != null && rhs != null) {
            if (lhs.getDateTime() != null && rhs.getDateTime() != null) {

                try {
                    Date leftDate = Utility.convertToDate(lhs.getDateTime());
                    Date rightDate = Utility.convertToDate(rhs.getDateTime());
                    if (leftDate != null && rightDate != null) {
                        return leftDate.before(rightDate) ? -1 : 1;
                    }
                } catch (Exception ex) {
                }
            }
        }

        return 0;
    }
}
