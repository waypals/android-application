package com.waypals.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.gson.vo.PhoneNumber;
import com.waypals.gson.vo.SOSContacts;
import com.waypals.gson.vo.SOSServiceResponse;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.wareninja.opensource.common.ObjectSerializer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class ApplicationPreferences {
    private static final String APP_SHARED_PREFS = "com.lst.android.chleon"; // Name
    private static final String VEHICLE_REG_NO_ARRAY_KEY = "VEHICLE_REG_NO_ARRAY_KEY";
    private static final String SELECTED_REG_NUM = "SELECTED_REG_NUM";
    private static final String VEHICLES_DATA = "vehiclesData";
    private static final String SOS_NUMBERS = "sosNumbers";
    // of
    // the
    // file
    // -.xml
    private SharedPreferences appSharedPrefs;
    private Editor prefsEditor;

    public ApplicationPreferences(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
                Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public void clear() {
        this.prefsEditor.clear().commit();
    }

    public boolean getChleonLog() {
        return appSharedPrefs.getBoolean("isChleon_on", false);
    }

    public void saveChleonLog(Boolean text) {
        prefsEditor.putBoolean("isChleon_on", text);
        prefsEditor.commit();
    }

    public boolean getLogout() {
        return appSharedPrefs.getBoolean("isLoggedOut", true);
    }

    public void saveLogout(Boolean text) {
        prefsEditor.putBoolean("isLoggedOut", text);
        prefsEditor.commit();
    }

    public void setEngineState(boolean onOff)
    {
        prefsEditor.putBoolean("engineState", onOff);
        prefsEditor.commit();
    }

    public boolean getEngineState()
    {
        return appSharedPrefs.getBoolean("engineState",true);
    }


    public boolean getLoadingFeedForFirstTime() {
        return appSharedPrefs.getBoolean("loadingFeedForFirstTime", true);
    }

    public boolean getLoadingMessagesForFirstTime() {
        return appSharedPrefs.getBoolean("loadingMessagesForFirstTime", true);
    }

    public void saveLoadingMessagesForFirstTime(Boolean text) {
        prefsEditor.putBoolean("loadingMessagesForFirstTime", text);
        prefsEditor.commit();
    }

    public void saveLoadingFeedForFirstTime(Boolean text) {
        prefsEditor.putBoolean("loadingFeedForFirstTime", text);
        prefsEditor.commit();
    }

    public boolean getIsLostFeedAvailable() {
        return appSharedPrefs.getBoolean("lostFeedAvailable", false);
    }

    public void saveIsLostFeedAvailable(Boolean text) {
        prefsEditor.putBoolean("lostFeedAvailable", text);
        prefsEditor.commit();
    }

    public boolean getIsLostMessageAvailable() {
        return appSharedPrefs.getBoolean("lostMsgAvailable", false);
    }

    public void saveIsLostMessageAvailable(Boolean text) {
        prefsEditor.putBoolean("lostMsgAvailable", text);
        prefsEditor.commit();
    }

    public boolean getFirstLaunch() {
        return appSharedPrefs.getBoolean("launch", true);
    }

    public void saveFirstLaunch(Boolean text) {
        prefsEditor.putBoolean("launch", text);
        prefsEditor.commit();
    }

    public boolean getDeviceReg() {
        return appSharedPrefs.getBoolean("deviceRegisteredForPushService", false);
    }

    public int getCurrentFeedPage() {
        return appSharedPrefs.getInt("currentPage", 1);
    }

    public void saveCurrentFeedPage(int page) {
        prefsEditor.putInt("currentPage", page);
        prefsEditor.commit();
    }

    public int getAvailableMessageCount() {
        return appSharedPrefs.getInt("availableMsgCount", 0);
    }

    public void saveAvailableMessageCount(int page) {
        prefsEditor.putInt("availableMsgCount", page);
        prefsEditor.commit();
    }

    public int getAvailableFeedsCount() {
        return appSharedPrefs.getInt("availableFeedCount", 0);
    }

    public void saveAvailableFeedsCount(int page) {
        prefsEditor.putInt("availableFeedCount", page);
        prefsEditor.commit();
    }

    public int getLoadedFeeds() {
        return appSharedPrefs.getInt("loadedFeeds", 0);
    }

    public void saveLoadedFeeds(int count) {
        prefsEditor.putInt("loadedFeeds", count);
        prefsEditor.commit();
    }


    public int getLoadedMessages() {
        return appSharedPrefs.getInt("loadedMessages", 0);
    }


    public void saveLoadedMessages(int count) {
        prefsEditor.putInt("loadedMessages", count);
        prefsEditor.commit();
    }


    public void setDeviceReq(Boolean text) {
        prefsEditor.putBoolean("deviceRegisteredForPushService", text);
        prefsEditor.commit();
    }

    public void saveFeeds(HashMap<Long, FeedWrapper> feedFormats) {
        prefsEditor.putString("feedList", ObjectSerializer.serialize(feedFormats));
        prefsEditor.commit();

    }

    public void saveMessages(HashMap<Long, FeedWrapper> feedFormats) {
        prefsEditor.putString("msgList", ObjectSerializer.serialize(feedFormats));
        prefsEditor.commit();

    }

    public void saveLastFeedUpdateTime(Long time) {
        prefsEditor.putLong("lastfeedupdatetime", time);
        prefsEditor.commit();
    }

    public Long getLastFeedUpdateTime() {
        return appSharedPrefs.getLong("lastfeedupdatetime", 0);
    }

    public void saveLastMessagedUpdateTime(Long time) {
        prefsEditor.putLong("lastMessageupdatetime", time);
        prefsEditor.commit();
    }

    public Long getLastMessaggeUpdateTime() {
        return appSharedPrefs.getLong("lastMessageupdatetime", 0);
    }


    public void removeFeeds() {
        prefsEditor.remove("feedList");
        prefsEditor.commit();
    }

    public void removeMessages() {
        prefsEditor.remove("msgList");
        prefsEditor.commit();
    }

    public void clearFeeds() {
        String feedlist = appSharedPrefs.getString("feedList", null);
        if (feedlist != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(feedlist);

            if (map != null) {
                map.clear();
                saveFeeds(map);
                Log.d("clearFeeds", "Map is cleared");
            }
        }
    }


    public void clearMessages() {
        String msgList = appSharedPrefs.getString("msgList", null);
        if (msgList != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(msgList);

            if (map != null) {
                map.clear();
                saveMessages(map);
                Log.d("clearMessages", "Map is cleared");
            }
        }
    }

    public void addFeed(FeedWrapper feed) {
        String feedlist = appSharedPrefs.getString("feedList", null);
        if (feedlist != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(feedlist);
            if (map != null) {
                if (map.size() <= 10) {
                    map.put(feed.getFeed().getId(), feed);
                    saveFeeds(map);
                } else {
                    Log.d("addFeed", "default cached size is full");
                    return;
                }
            }

        } else {
            HashMap<Long, FeedWrapper> map = new HashMap<Long, FeedWrapper>();
            map.put(feed.getFeed().getId(), feed);
            saveFeeds(map);
        }
    }

    public void addMessage(FeedWrapper feed) {
        String msgList = appSharedPrefs.getString("msgList", null);
        if (msgList != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(msgList);
            if (map != null) {
                if (map.size() <= 10) {
                    map.put(feed.getFeed().getId(), feed);
                    saveMessages(map);
                } else {
                    Log.d("addMessage", "default cached size is full");
                    return;
                }
            }
        } else {
            HashMap<Long, FeedWrapper> map = new HashMap<Long, FeedWrapper>();
            map.put(feed.getFeed().getId(), feed);
            saveMessages(map);
        }
    }

    public ArrayList<FeedWrapper> getFeeds() {
        String feedlist = appSharedPrefs.getString("feedList", null);
        if (feedlist != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(feedlist);
            if (map != null && map.size() > 0) {
                ArrayList<FeedWrapper> list = new ArrayList<FeedWrapper>(map.values());
                return list;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public ArrayList<FeedWrapper> getMessages() {
        String msgList = appSharedPrefs.getString("msgList", null);
        if (msgList != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(msgList);
            if (map != null && map.size() > 0) {
                ArrayList<FeedWrapper> list = new ArrayList<FeedWrapper>(map.values());
                return list;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public HashMap<Long, FeedWrapper> getFeedsMap() {
        String feedlist = appSharedPrefs.getString("feedList", null);
        if (feedlist != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(feedlist);
            return map;
        } else {
            return null;
        }
    }

    public HashMap<Long, FeedWrapper> getMessageMap() {
        String msgList = appSharedPrefs.getString("msgList", null);
        if (msgList != null) {
            HashMap<Long, FeedWrapper> map = (HashMap<Long, FeedWrapper>) ObjectSerializer.deserialize(msgList);
            return map;
        } else {
            return null;
        }
    }

    public boolean removeFeedFromCache(FeedWrapper feedWrapper) {
        HashMap<Long, FeedWrapper> map = getFeedsMap();

        if (map != null) {
            Log.d("Prefs", "Input Size" + map.size());
            map.remove(feedWrapper.getFeed().getId());
            saveFeeds(map);
            return true;
        } else {
            return false;
        }
    }

    public boolean removeMessageFromCache(FeedWrapper feedWrapper) {
        HashMap<Long, FeedWrapper> map = getMessageMap();

        if (map != null) {
            Log.d("Prefs", "Input Size" + map.size());
            map.remove(feedWrapper.getFeed().getId());
            saveMessages(map);
            return true;
        } else {
            return false;
        }
    }

    // public String getSyncTime() {
    // return appSharedPrefs.getString("sync_time", "");
    // }
    //
    // public void saveSyncTime(String text) {
    // prefsEditor.putString("sync_time", text);
    // prefsEditor.commit();
    // }

    public String getDeviceRegistrationId() {
        return appSharedPrefs.getString("registration_id", null);
    }

    public void saveDeviceRegistrationId(String text) {
        prefsEditor.putString("registration_id", text);
        prefsEditor.commit();
    }

    public String getUserName() {
        return appSharedPrefs.getString("username", "");
    }

    public void saveUserName(String text) {
        prefsEditor.putString("username", text);
        prefsEditor.commit();
    }

    public void saveSOSSend(String text) {
        prefsEditor.putString("sos", text);
        prefsEditor.commit();
    }

    public String getSOSSend() {
        return appSharedPrefs.getString("sos", "");
    }

    public String getUserPassword() {
        try {
            return appSharedPrefs.getString("user_password", "");
           // byte[] data = Base64.decode(base64, Base64.DEFAULT);
           // String text = new String(data, "UTF-8");
           // return text;
        }catch (Exception ex){

        }
        return "";
    }

    public void saveUserPassword(String text) {
        try {
          //  byte[] data = text.getBytes("UTF-8");
          //  String base64 = Base64.encodeToString(data, Base64.DEFAULT);

            prefsEditor.putString("user_password", text);
            prefsEditor.commit();
        }catch (Exception ex){

        }
    }

    public String getUserToken() {
        return appSharedPrefs.getString("user_token", "");
    }

    public void saveUserToken(String text) {
        prefsEditor.putString("user_token", text);
        prefsEditor.commit();
    }

    //
    // public String getLast_Alert_ReportTime() {
    // return appSharedPrefs.getString("last_alert_reportTime", "");
    // }

    // public void saveLast_Alert_ReportTime(String text) {
    // prefsEditor.putString("last_alert_reportTime", text);
    // prefsEditor.commit();
    // }

    // public String getLast_Incident_ReportTime() {
    // return appSharedPrefs.getString("last_inci_reportTime", "");
    // }

    // public void saveLast_Incident_ReportTime(String text) {
    // prefsEditor.putString("last_inci_reportTime", text);
    // prefsEditor.commit();
    // }

    public String getDeviceId() {
        return appSharedPrefs.getString("device_id", "");
    }

    public void saveDeviceId(String text) {
        prefsEditor.putString("device_id", text);
        prefsEditor.commit();
    }

    public long getVehicle_Selected() {
        return appSharedPrefs.getLong("vehicle_selected", 0);
    }

    public void saveVehicle_Selected(long no) {
        prefsEditor.putLong("vehicle_selected", no);
        prefsEditor.commit();
    }

    public boolean getCar1_Parkfence() {
        return appSharedPrefs.getBoolean("car1_prkfnce", false);
    }

    public void saveCar1_Parkfence(Boolean text) {
        prefsEditor.putBoolean("car1_prkfnce", text);
        prefsEditor.commit();
    }

    public void saveTotalCar(int no) {
        prefsEditor.putInt("total_cars", no);
        prefsEditor.commit();
    }

    public void saveVehicalList(String list[]) {
        String data = "";
        if (list.length > 0) {
            for (int i = 0; i < list.length; i++) {
                if (i == 0) {
                    data = list[i];
                } else {
                    data += "," + list[i];
                }
            }
        }
        prefsEditor.putString("vehical_list", data);
        prefsEditor.commit();
    }

    public String[] getVehicalList() {
        String data = appSharedPrefs.getString("vehical_list", "");
        if (data.length() > 0) {
            return data.split(",");
        } else {
            return null;
        }
    }

    public int getTotalCar() {
        return appSharedPrefs.getInt("total_cars", 0);
    }

    public void saveSelectedCarLatLng(String value) {
        prefsEditor.putString("lat_long", value);
        prefsEditor.commit();
    }

    public String getSelectedCarLatLng() {
        return appSharedPrefs.getString("lat_long", "");
    }

    public boolean saveSelectedRegistrationNumber(String value) {
        prefsEditor.putString(SELECTED_REG_NUM, value);
        return prefsEditor.commit();
    }

    public String getSelectedRegistrationNumber() {
        return appSharedPrefs.getString(SELECTED_REG_NUM, "");
    }

    public boolean saveVehicleServiceResponse(String vehicleServiceResponse) {
        prefsEditor.putString(VEHICLES_DATA, vehicleServiceResponse);
        return prefsEditor.commit();
    }

    public VehicleServiceResponse getVehicleResponse() {
        VehicleServiceResponse vehicleServiceResponse = null;
        try {
            Gson gson = new Gson();
            String response = appSharedPrefs.getString(VEHICLES_DATA, "");
            if (response != null) {
                vehicleServiceResponse = gson.fromJson(response,
                        VehicleServiceResponse.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return vehicleServiceResponse;
    }

    public boolean saveSOSNumbers(String sosNumbers) {
        prefsEditor.putString(SOS_NUMBERS, sosNumbers);
        return prefsEditor.commit();
    }

    public List<SOSContacts> getSOSServiceResponse(boolean forCall) {
        String response = appSharedPrefs.getString(SOS_NUMBERS, "");

        if (response != null) {
            Gson gson = new Gson();
            SOSServiceResponse sosServiceResponse = gson.fromJson(response,
                    SOSServiceResponse.class);
            if (sosServiceResponse != null
                    && sosServiceResponse.getResponse() != null
                    && sosServiceResponse.getResponse().equals(
                    CH_Constant.SUCCESS)) {

                List<SOSContacts> sosNumber = sosServiceResponse.getSosNumber();

                if(forCall){
                    SOSContacts contacts = new SOSContacts();
                    contacts.setName("Emergency Number");
                    PhoneNumber phoneNumber = new PhoneNumber();
                    phoneNumber.setCountryCode("");
                    phoneNumber.setPhoneNo(112);
                    contacts.setEmergencyPhoneNumber(phoneNumber);
                    contacts.setGender("");
                    contacts.setRelation("");

                    if(sosNumber != null){
                        sosNumber.add(0, contacts);
                    }else{
                        sosNumber = new ArrayList<>();
                        sosNumber.add(contacts);
                    }
                }

                return sosNumber;

            }
        }

        return null;
    }


    public void saveCurrentLocation(String location) {
        prefsEditor.putString("CURRENT_LOCATION", location);
        prefsEditor.commit();
    }

    public LatLng getCurrentLocation() {
        String response = appSharedPrefs.getString("CURRENT_LOCATION", null);

        if (response != null) {
            String[] values = response.split(",");
            LatLng l = new LatLng(Double.parseDouble(values[0]), Double.parseDouble(values[1]));
            return l;
        } else {
            return null;
        }
    }

    public void saveCurrentDeviceIsImmobilizer(boolean imobilizer) {
        prefsEditor.putBoolean("CURRENT_DEVICE_IMMOBILIZER", imobilizer);
        prefsEditor.commit();
    }

    public boolean getCurrentDeviceIsImmobilizer()
    {
        return appSharedPrefs.getBoolean("CURRENT_DEVICE_IMMOBILIZER", false);
    }

    public void saveFirstTimeLoggedIn(boolean loginRelogin) {
        prefsEditor.putBoolean("IS_FIRST_TIME_LOGGED_IN", loginRelogin);
        prefsEditor.commit();
    }

    public boolean isFirstTimeLoggedIn()
    {
        return appSharedPrefs.getBoolean("IS_FIRST_TIME_LOGGED_IN", true);
    }

    public void saveCurrentDeviceNumber(String deviceNum) {
        prefsEditor.putString("CURRENT_DEVICE_NUMBER", deviceNum);
        prefsEditor.commit();
    }

    public String getCurrentDeviceNumber()
    {
        return appSharedPrefs.getString("CURRENT_DEVICE_NUMBER", null);
    }

    public void saveCurrentUserNumber(String deviceNum) {
        prefsEditor.putString("CURRENT_USER_NUMBER", deviceNum);
        prefsEditor.commit();
    }

    public String getCurrentUserNumber()
    {
        return appSharedPrefs.getString("CURRENT_USER_NUMBER", null);
    }

    public void saveCurrentVehiclePolicyDueDate(String utCtoIST) {
        prefsEditor.putString("CURRENT_VEHICLE_LAST_POLICY_DATE", utCtoIST);
        prefsEditor.commit();
    }

    public String getCurrentVehiclePolicyDueDate()
    {
        return appSharedPrefs.getString("CURRENT_VEHICLE_LAST_POLICY_DATE", null);
    }

    public void saveCurrentVehicleLastServiceDate(String utCtoIST) {
        prefsEditor.putString("CURRENT_VEHICLE_LAST_SERVICE_DATE", utCtoIST);
        prefsEditor.commit();
    }

    public String getCurrentVehicleLastServiceDate()
    {
        return appSharedPrefs.getString("CURRENT_VEHICLE_LAST_SERVICE_DATE", null);
    }

    public void saveCurrentVehiclePollutionDate(String utCtoIST) {
        prefsEditor.putString("CURRENT_VEHICLE_POLLUTION_DATE", utCtoIST);
        prefsEditor.commit();
    }

    public String getCurrentVehiclePollutionDate()
    {
        return appSharedPrefs.getString("CURRENT_VEHICLE_POLLUTION_DATE", null);
    }

    public void saveCurrentVehicleLicenceDate(String utCtoIST) {
        prefsEditor.putString("CURRENT_VEHICLE_LICENCE_DATE", utCtoIST);
        prefsEditor.commit();
    }

    public String getCurrentVehicleLicenceDate()
    {
        return appSharedPrefs.getString("CURRENT_VEHICLE_LICENCE_DATE", null);
    }

    public void setNoItemsInCart(int i) {
        prefsEditor.putInt("ITEMS_IN_CART", i);
        prefsEditor.commit();
    }

    public int getNoItemsInCart(){
        return appSharedPrefs.getInt("ITEMS_IN_CART",0);
    }

    public void saveAlarmStartTime(String start) {
        prefsEditor.putString("ALARM_START_TIME", start);
        prefsEditor.commit();
    }

    public void saveAlarmEndTime(String end) {
        prefsEditor.putString("ALARM_END_TIME", end);
        prefsEditor.commit();
    }
    public void setAlarm(boolean value) {
        prefsEditor.putBoolean("SET_ALARM", value);
        prefsEditor.commit();
    }

    public boolean isAlarm(){
        return appSharedPrefs.getBoolean("SET_ALARM", false);
    }

    public String getAlarmStartTime()
    {
        return appSharedPrefs.getString("ALARM_START_TIME", null);
    }

    public String getAlarmEndTime()
    {
        return appSharedPrefs.getString("ALARM_END_TIME", null);
    }

    public boolean isInBetweenAlarmTime(){
        if (isAlarm()){
            try {
            SimpleDateFormat smf = new SimpleDateFormat("hh:mm:ss a");
                //smf.setTimeZone(TimeZone.getTimeZone("IST"));
            String start = getAlarmStartTime();
            String end = getAlarmEndTime();

            if (start == null || end == null){
                return false;
            }

                Date now = new Date();
                String nowDateStr = smf.format(now);
                Calendar timeCal = Calendar.getInstance();
                //timeCal.setTime(now);
                timeCal.set(2017, 1, 1, Utility.parseDateFor(nowDateStr, 0), Utility.parseDateFor(nowDateStr, 1), Utility.parseDateFor(nowDateStr, 2));

                Date nowDate = timeCal.getTime();

                Calendar startCal = Calendar.getInstance();
                //startCal.setTime(startTime);
                startCal.set(2017, 1, 1, Utility.parseDateFor(start, 0), Utility.parseDateFor(start, 1), Utility.parseDateFor(start, 2));

                Calendar endCal = Calendar.getInstance();
                endCal.set(2017, 1, 1, Utility.parseDateFor(end, 0), Utility.parseDateFor(end, 1), Utility.parseDateFor(end, 2));

                boolean isSameDay = Utility.isSameDay(start, end);
                if (!isSameDay) {
                    endCal.add(Calendar.DATE, 1);
                }



                Log.d("NowTime", nowDate.toString());
                Log.d("StartTime", startCal.getTime().toString());
                Log.d("EndTime", endCal.getTime().toString());

                if(nowDate.after(startCal.getTime()) && nowDate.before(endCal.getTime())){
                    return true;
                }


            }catch (Exception ex){

            }

            return false;
        }else {
            return false;
        }
    }

    public void clearAll() {
        prefsEditor.clear();
        prefsEditor.commit();
    }
}
