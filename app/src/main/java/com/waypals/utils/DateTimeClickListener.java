package com.waypals.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by surya on 2/11/15.
 */
public class DateTimeClickListener implements View.OnClickListener
{

    public interface DateSelectedCallback
    {
        void onSet(Date date);
    }

    private TextView textView;
    private Activity activity;
    private Date selectedDate;
    private DateSelectedCallback handler;

    public DateTimeClickListener(TextView textView, Activity activity)
    {
        this.textView = textView;
        this.activity = activity;

    }

    public DateTimeClickListener(TextView textView, Activity activity, DateSelectedCallback handler)
    {
        this.textView = textView;
        this.activity = activity;
        this.handler = handler;
    }




    @Override
    public void onClick(View view) {
        CustomDateTimePicker custom = new CustomDateTimePicker(activity, true, new CustomDateTimePicker.ICustomDateTimeListener() {
            @Override
            public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                              String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                              int hour24, int hour12, int min, int sec, String AM_PM) {

                SimpleDateFormat df = new SimpleDateFormat("MMM d, yyyy");
                String set_DateTime = df.format(dateSelected);
                selectedDate = dateSelected;
                DateTimeClickListener.this.textView.setText(set_DateTime);

                if(handler != null)
                {
                    handler.onSet(selectedDate);
                }
            }

            @Override
            public void onCancel() {

            }
        });
        custom.set24HourFormat(false);
        /**
         * Pass Directly current data and time to show when it pop up
         */
        //custom.setDate(Calendar.getInstance());
        // custom.setDate(startDate);
        if(selectedDate != null){
            custom.setDate(selectedDate);
        }

        custom.showDialog();
    }


    public Date getSelectedDate()
    {
        return selectedDate;
    }
}