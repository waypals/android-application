package com.waypals.utils;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waypals.Alerts_Screen;
import com.waypals.Beans.ConstraintAlerts_Beans;
import com.waypals.Beans.IncidentAlerts_Beans;
import com.waypals.R;

import java.util.List;

public class Data_ListAdapter extends BaseAdapter {
    Context contxt;
    int alrt_inci_val;
    Resources res;
    int width, height;
    List<ConstraintAlerts_Beans> Alerts_list;
    List<IncidentAlerts_Beans> Inci_Alerts_list;
    String[] inci_eventType = {"ACCIDENT", "TRAFFIC_VIOLATION", "DEVICE_BREAKDOWN", "THEFT"};
    String[] alert_eventType = {"SYSTEM_FENCE", "OUT_BOUND_GEO_FENCE", "IN_BOUND_GEO_FENCE", "SPEED_LIMIT"};
    //Sunny changes
    int[] inci_images = {R.drawable.accident, R.drawable.traffic_violation, R.drawable.break_down, R.drawable.theft};

    //	int[] inci_images = {R.drawable.inci_accident, R.drawable.inci_traffic, R.drawable.inci_breakdown, R.drawable.inci_theft};
    int[] alert_images = {R.drawable.parkingfence_icon, R.drawable.alert_gf_enter, R.drawable.alert_gf_exit, R.drawable.alert_speed, R.drawable.alert_speed};
    ViewHolder holder;
    private LayoutInflater inflater = null;
    @SuppressWarnings("unused")
    private int selectedPos = -1;

    //    public Data_ListAdapter(Activity a, int title_val, List<Media_Beans> media_ListBeans, List<Discussion_Beans> mDiscussion_Beans, ArrayList<String> array_sort)
    public Data_ListAdapter(Context contxt, int alrt_inci_val, List<ConstraintAlerts_Beans> Alerts_list, List<IncidentAlerts_Beans> Inci_Alerts_list) {
        this.contxt = contxt;
        this.alrt_inci_val = alrt_inci_val;
        if (Alerts_list != null)
            this.Alerts_list = Alerts_list;
        if (Inci_Alerts_list != null)
            this.Inci_Alerts_list = Inci_Alerts_list;
        inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        if (alrt_inci_val == CH_Constant.Alert_Val)
            return Alerts_list.size();
        else
            return Inci_Alerts_list.size();
    }

    public void setSelectedPosition(int pos) {
        selectedPos = pos;
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        holder = null;

        if (convertView == null) {
            switch (alrt_inci_val) {
                case CH_Constant.Alert_Val:
                    vi = inflater.inflate(R.layout.alert_list_row, parent, false);
                    holder = new ViewHolder();
                    holder.img = (ImageView) vi.findViewById(R.id.inci_image);
                    holder.car_no_plate_alrt = (TextView) vi.findViewById(R.id.car_no_plate_alrt);
                    holder.date_time_alert = (TextView) vi.findViewById(R.id.date_time_alert);
                    holder.alert_type = (TextView) vi.findViewById(R.id.alert_type);

                    holder.car_no_plate_alrt.setTypeface(Alerts_Screen.tf);
                    holder.date_time_alert.setTypeface(Alerts_Screen.tf);
                    holder.alert_type.setTypeface(Alerts_Screen.tf);

                    vi.setTag(holder);
                    break;
                //
                case CH_Constant.Incident_Val:
                    vi = inflater.inflate(R.layout.alert_list_row, parent, false);
                    holder = new ViewHolder();
                    holder.img = (ImageView) vi.findViewById(R.id.inci_image);
                    holder.car_no_plate_alrt = (TextView) vi.findViewById(R.id.car_no_plate_alrt);
                    holder.date_time_alert = (TextView) vi.findViewById(R.id.date_time_alert);
                    holder.alert_type = (TextView) vi.findViewById(R.id.alert_type);

                    holder.car_no_plate_alrt.setTypeface(Alerts_Screen.tf);
                    holder.date_time_alert.setTypeface(Alerts_Screen.tf);
                    holder.alert_type.setTypeface(Alerts_Screen.tf);

                    vi.setTag(holder);
                    break;

                default:
                    break;
            }

        } else
            holder = (ViewHolder) vi.getTag();

        if (position >= getCount()) {
        } else {
            switch (alrt_inci_val) {
                case CH_Constant.Alert_Val:
                    if (Alerts_list != null && Alerts_list.size() > 0) {
                        holder.car_no_plate_alrt.setText(Alerts_list.get(position).getMeta_RegistrationNo());
                        holder.date_time_alert.setText(Alerts_list.get(position).getDPoint_DateTime());
                        holder.alert_type.setText(Alerts_list.get(position).getViewType());
//					holder.img.setImageResource(alert_images[position]);
                        if (Alerts_list.get(position).getDP_Type().equalsIgnoreCase(alert_eventType[0]))
                            holder.img.setImageResource(alert_images[0]);
                        else if (Alerts_list.get(position).getDP_Type().equalsIgnoreCase(alert_eventType[1]))
                            holder.img.setImageResource(alert_images[1]);
                        else if (Alerts_list.get(position).getDP_Type().equalsIgnoreCase(alert_eventType[2]))
                            holder.img.setImageResource(alert_images[2]);
                        else if (Alerts_list.get(position).getDP_Type().equalsIgnoreCase(alert_eventType[3]))
                            holder.img.setImageResource(alert_images[3]);
//					else if(Alerts_list.get(position).getDP_Type().equalsIgnoreCase(alert_eventType[3]))
//						holder.img.setImageResource(inci_images[3]);
                    }
                    break;

                case CH_Constant.Incident_Val:
                    if (Inci_Alerts_list != null && Inci_Alerts_list.size() > 0) {
                        holder.car_no_plate_alrt.setText(Inci_Alerts_list.get(position).getMeta_RegistrationNo());
                        holder.date_time_alert.setText(Inci_Alerts_list.get(position).getIncidentTime());
                        holder.alert_type.setText(Inci_Alerts_list.get(position).getDescription());
                        if (Inci_Alerts_list.get(position).getEvent_type().equalsIgnoreCase(inci_eventType[0]))
                            holder.img.setImageResource(inci_images[0]);
                        else if (Inci_Alerts_list.get(position).getEvent_type().equalsIgnoreCase(inci_eventType[1]))
                            holder.img.setImageResource(inci_images[1]);
                        else if (Inci_Alerts_list.get(position).getEvent_type().equalsIgnoreCase(inci_eventType[2]))
                            holder.img.setImageResource(inci_images[2]);
                        else if (Inci_Alerts_list.get(position).getEvent_type().equalsIgnoreCase(inci_eventType[3]))
                            holder.img.setImageResource(inci_images[3]);
                    }
                    break;


                default:
                    break;
            }


        }

        return vi;
    }

    public class ViewHolder {
        public TextView car_no_plate_alrt;
        public TextView date_time_alert;
        public TextView alert_type;
        public ImageView img;
        public ImageView overlay;
        public ImageView arrow;
    }


}
