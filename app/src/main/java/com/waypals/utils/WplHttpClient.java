package com.waypals.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Looper;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.waypals.exception.ExceptionHandler;
import com.waypals.exception.NetworkException;
import com.waypals.feedAdapter.FeedAsyncInterface;
import com.waypals.feedAdapter.FeedMethodEnum;
import com.waypals.rest.service.AsyncFinishInterface;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WplHttpClient {

    private static ProgressDialog dialog;


    public static void callVolleyService(final Context context, String url, String bodyRequest, final ApplicationPreferences appPrefs, final AsyncFinishInterface asyncFinishInterface, final String method, final boolean showDialog) throws NetworkException {
        try {

            if (CH_Constant.isNetworkAvailable(context)) {

                RequestQueue queue = Volley.newRequestQueue(context);

                JSONObject requestBody = null;

                showDialog(showDialog, context);
                if (bodyRequest != null && !bodyRequest.isEmpty()) {
                    requestBody = new JSONObject(bodyRequest);
                }
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, requestBody, new Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        try {
                            hideProgress();
                            if (jsonObject != null) {
                                String s = jsonObject.toString();
                                asyncFinishInterface.finish(s, method);
                                asyncFinishInterface.onPostExecute(null);
                            }
                        } catch (Exception e) {
                            if (e != null && e.getMessage() != null) {
                                Log.e("Error:", e.getMessage());
                            }
                            hideProgress();
                            ExceptionHandler.handlerException(e, (Activity) context);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error.getMessage() != null) {
                            Log.e("Error", error.getMessage());
                        }
                        hideProgress();
                        ExceptionHandler.handlerException(error, (Activity) context);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("Accept-encoding", "gzip");

                        if (appPrefs != null && appPrefs.getUserToken() != null) {
                            params.put("security-token", appPrefs.getUserToken());
                        }
                        return params;
                    }
                };
                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsonObjectRequest);
            } else {
                Utility.makeNewToast((Activity) context, CH_Constant.NO_NETWORK);
            }

        } catch (Exception ex) {
            if (ex != null && ex.getMessage() != null) {
                Log.e("Error:", ex.getMessage());
            }
            hideProgress();
            ExceptionHandler.handlerException(ex, (Activity) context);
        }
    }

    public static void showDialog(boolean showDialog, final Context context) {
        if (Looper.myLooper() == Looper.getMainLooper() && showDialog) {
            if (dialog == null) {
                dialog = new ProgressDialog(context);
                dialog.setCancelable(false);
                dialog.setMessage("Please wait...");
            }
            if (!dialog.isShowing()) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!((Activity) context).isFinishing()) {
                            dialog.show();
                        }
                    }
                });

            }
        }
    }

    public static void hideProgress() {
        if (dialog != null) {
            if (dialog.isShowing()) { //check if dialog is showing.

                //get the Context object that was used to great the dialog
                Context context = ((ContextWrapper) dialog.getContext()).getBaseContext();

                //if the Context used here was an activity AND it hasn't been finished or destroyed
                //then dismiss it
                if (context instanceof Activity) {
                    if (!((Activity) context).isFinishing())
                        dialog.dismiss();
                } else //if the Context used wasnt an Activity, then dismiss it too
                    dialog.dismiss();
            }
            dialog = null;
        }
    }

    public static void callVolleyGetService(final Context context, String url, final AsyncFinishInterface asyncFinishInterface, boolean showDialog) throws NetworkException {
        try {
            if (CH_Constant.isNetworkAvailable(context)) {
                RequestQueue queue = Volley.newRequestQueue(context);
                JSONObject requestBody = null;

                showDialog(showDialog, context);


                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, requestBody, new Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        try {
                            hideProgress();
                            if (jsonObject != null) {
                                String s = jsonObject.toString();
                                asyncFinishInterface.finish(s, null);
                                asyncFinishInterface.onPostExecute(null);
                            }
                        } catch (Exception e) {
                            if (e != null && e.getMessage() != null) {
                                Log.e("Error:", e.getMessage());
                            }
                            hideProgress();
                            ExceptionHandler.handlerException(e, (Activity) context);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error.getMessage() != null) {
                            Log.e("Error", error.getMessage());
                        }
                        hideProgress();
                        ExceptionHandler.handlerException(error, (Activity) context);
                    }
                });
                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsonObjectRequest);
            } else {
                Utility.makeNewToast((Activity) context, CH_Constant.NO_NETWORK);
            }
        } catch (Exception ex) {
            if (ex != null && ex.getMessage() != null) {
                Log.e("Error:", ex.getMessage());
            }
            hideProgress();
            ExceptionHandler.handlerException(ex, (Activity) context);
        }
    }


    public static void callFeedVolleyService(final Context context, String url, String bodyRequest, final ApplicationPreferences appPrefs, final FeedAsyncInterface asyncFinishInterface, final String requestFrom, final Boolean notification, final FeedMethodEnum method) throws NetworkException {
        try {
            if (CH_Constant.isNetworkAvailable(context)) {
                RequestQueue queue = Volley.newRequestQueue(context);
//            dialog = new ProgressDialog(context);
//            if (!dialog.isShowing()) {
//                dialog.setCancelable(false);
//                dialog.setMessage("Please wait...");
//                dialog.show();
//            }
                JSONObject requestBody = null;
                if (bodyRequest != null && !bodyRequest.isEmpty()) {
                    requestBody = new JSONObject(bodyRequest);
                }
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, requestBody, new Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        try {
                            if (jsonObject != null) {
                                String s = jsonObject.toString();
                                asyncFinishInterface.finish(s, method, requestFrom, notification);
                                asyncFinishInterface.onPostExecute(null);
//                            if(dialog != null) {
//                                dialog.dismiss();
//                            }
                            }
                        } catch (Exception e) {
                            if (e != null && e.getMessage() != null) {
                                Log.e("Error:", e.getMessage());
                            }
                            ExceptionHandler.handlerException(e, (Activity) context);
//                        if(dialog != null) {
//                            dialog.dismiss();
//                        }
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error.getMessage() != null) {
                            Log.e("Error", error.getMessage());
                        }
                        ExceptionHandler.handlerException(error, (Activity) context);
//                    if(dialog != null) {
//                        dialog.dismiss();
//                    }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("Accept-encoding", "gzip");
                        params.put("security-token", appPrefs.getUserToken());
                        return params;
                    }
                };
                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(jsonObjectRequest);
            } else {
                Utility.makeNewToast((Activity) context, CH_Constant.NO_NETWORK);
            }
        } catch (Exception ex) {
            if (ex != null && ex.getMessage() != null) {
                Log.e("Error:", ex.getMessage());
            }
            ExceptionHandler.handlerException(ex, (Activity) context);
//            if(dialog != null) {
//                dialog.dismiss();
//            }
        }
    }

}
