package com.waypals.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.waypals.MyFeed;
import com.waypals.exception.ExceptionHandler;
import com.waypals.rest.service.AsyncFinishInterface;

import org.json.JSONObject;

public class MyFeedAsyncTask implements AsyncFinishInterface {

    private ApplicationPreferences appPreferences;
    private AsyncFinishInterface asynFinishIterface;
    private Context context;
    private JSONObject response;
    private String error;
    private String serviceName;

    public MyFeedAsyncTask(String serviceName, ApplicationPreferences appPref, Context context) {

        this.serviceName = serviceName;
        this.appPreferences = appPref;
        this.context = (MyFeed) context;
		asynFinishIterface = (MyFeed) context;
    }

    public void execute() {

        try {

            JSONObject json = new JSONObject();
            json.put("pageNumber", "1");
            json.put("pageSize", "10");
            json.put("feedType", "SELF");

            WplHttpClient.callVolleyService(context, serviceName, json.toString(), appPreferences, this, null, true);

            Log.d("MyFeedAsyncTask", response.toString());


        } catch (Throwable e) {
            error = "Error From Server";
            ExceptionHandler.handleException(e);
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.response = new JSONObject(response);
        asynFinishIterface.finish(this.response.toString(), serviceName);
    }

    @Override
    public void onPostExecute(Void result) {
        try {
            asynFinishIterface.onPostExecute(result);
        } catch (Throwable e) {
            ExceptionHandler.handleException(e, (Activity) context);
        }
    }


}
