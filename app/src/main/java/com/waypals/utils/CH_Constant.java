package com.waypals.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.waypals.BuildConfig;
import com.waypals.Home_Screen;
import com.waypals.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CH_Constant {

    public static final int AddGeofence_allowedDeviation = 10;
    public static final int Alert_Val = 100;
    public static final int Incident_Val = 200;
    public static final int No = -400;
    public static final int Pending = -500;

    public static final int Profile = -600;
    public static final int Status = -700;
    //	public static int Box_Alerts_Type;
    //	public static int Box_InciAlert_Type;
    public static final int Box_Default = 00;
    public static final int Box_Alerts = 10;
    public static final int Box_EnterGeofence = 20;    //	Out Bound
    public static final int Box_LeaveGeofence = 30;    //	In Bound
    public static final int Box_OutSystemfence = 40;
    public static final int Box_Accident = 50;
    public static final int Box_Theft = 60;
    public static final int Box_Traffice = 70;
    public static final int Box_Breakdown = 80;
    public static final String broadcast_fragment_view_map = "com.waypals.followme.open.map";
    public static final String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE";
    public static final String DEREGISTRATION_COMPLETE = "DEREGISTRATION_COMPLETE";
    public static final String NO_INFO = "No Info";
    public static final String EDIT_EMERGENCY_NO = "Edit";
    public static final String DELETE_EMERGENCY_NO = "Delete";

    // Citrus payment registraion details
    public static final String SIGN_UP_ID = "rrtspjgapq-signup";
    public static final String SIGN_UP_SECRET_KEY = "c8ff67eb930a9cb005d0e362b422c317";
    public static final String SIGN_IN_ID = "rrtspjgapq-signin";
    public static final String SIGN_IN_SECREY_KEY =  "1e6d2f986476897e9dbf187d6436d2d2";
    public static final String VANITY = "waypals";
    public static final String NO_EMERGENCY_CONTACT = "NO_EMERGENCY_CONTACT";

    public static String SERVER = BuildConfig.API_HOST;

    public static String PRODUCTS_IN_STORE = "/store/productsinstock";
    public static String COUPON_CODE = "/store/verifycoupon/";
    public static String ITEM_ORDER_STORE = "/store/itemorderresquest";
    public static String UPDATE_ITEM_ORDER = "/store/updateitemorder";
    public static final String PAYMENT_SERVER = BuildConfig.PAYMENT_HOST;
   // public static final String PAYMENT_SERVER = "http://52.76.228.27:9080/core-server/";
    public static final String BILL_GENERATOR_URL = PAYMENT_SERVER + "MobilePaymentRequest.jsp";
    public static final String RETURN_URL = PAYMENT_SERVER + "returnData.jsp";
    public static final String PAYMENT_REQUEST_URL = SERVER + "/user/paymentrequest";
    public static final String PAYMENT_RESPONSE_URL = SERVER + "/user/updatepaymentrequest";
    //updatepaymentrequest   (String tranRef,int paymentStatus)

    public static String IMMOBILIZE_SERVICE = SERVER + "/vehicle/immobilisevehicle";

    public static String Login_Api = SERVER + "/user/login";
    public static String Logout_Api = SERVER + "/user/logout";
    public static String FORGET_PASSWORD = SERVER + "/user/forgotpassword";
    public static String VERIFY_EMAIL = SERVER + "/user/verifyemail/";
    //public static String MyVehicles_Api = SERVER + "/vehicle/myvehicles";
    //public static String VehicleInformation_Api = SERVER + "/vehicle/vehicleinformation";
    public static String EnablingParkfence_Api = SERVER + "/vehicle/enableparkingfence";
    public static String DisablingParkfence_Api = SERVER + "/vehicle/disableparkingfence";
    // public static String Alert_Acknowledge_Api = SERVER + "/user/acknowledgealert";
    public static String IS_VALID_DEVICE_ID = SERVER + "/user/isvaliddevice";
    public static String IS_VALID_EMAIL_ID = SERVER + "/user/isvalidemailid";
    public static String IS_VALID_REGISTRATION_NUM = SERVER + "/user/isvalidvehicleregno";
    public static String GET_VEHICLE_TYPE = SERVER + "/user/getvehicletype";

    public static String GET_COUNTRIES = SERVER + "/user/getcountries";
    public static String GET_STATES = SERVER + "/user/getstates";
    public static String GET_CITIES = SERVER + "/user/getcities";

    public static String GET_VEHICLE_MANU_FROM_TYPE = SERVER + "/user/getvehiclemanufacturersbyvehicletype";
    public static String GET_VEHICLE_MODELS_FROM_MANU = SERVER + "/user/getvehiclemodelsbymanufacturer";
    public static String REGISTER_USER = SERVER + "/user/registeruser";
    public static String CHANGE_PASSWORD = SERVER  + "/user/changepassword";
    public static String DELETE_ACCOUNT = SERVER  + "/user/deleteaccount";

    public static String VEHICLE_DOCUMENT_UPLOAD = SERVER + "/vehicle/uploaddocument";
    public static String GET_VEHICLE_DOCUMENT = SERVER + "/vehicle/getdocuments/";
    public static String DELETE_DOCUMENTS = SERVER + "/vehicle/deletedocument/";

    public static String Add_Incident_Api = SERVER + "/user/addincident";
    public static String Create_Geofence_Api = SERVER + "/user/creategeofence";
    public static String List_Geofence_Api = SERVER + "/user/listgeofences";
    public static String Remove_Geofence_Api = SERVER + "/user/deletegeofence";
    public static String GET_INCIDENT_ALERT = SERVER + "/user/incidentalerts/";
    public static String GET_ALERTS = SERVER + "/user/rideconstraintalerts";
    public static String GET_VEHICLE_DATA_ON_LOGIN_SUCCESS = SERVER + "/vehicle/vehiclesdata";
    public static String GET_VEHICLE_STATE_ON_MAP_LOAD = SERVER + "/vehicle/vehiclestate";
    public static String GET_CAR_INFO = SERVER + "/vehicle/vehicleinfo";
    public static String GET_TELEMATICS_DATA = SERVER + "/vehicle/obd";

    public static String GET_TRIP_DATA = SERVER + "/vehicle/trip";
    public static String GET_TRIP_OVERVIEW = SERVER + "/vehicle/tripoverview";

    public static String GET_LATEST_TRIP_SCORE = SERVER + "/vehicle/latesttripscore";
    public static String GET_OVERALL_TRIP_SCORE = SERVER + "/vehicle/overallscore";

    public static String GET_SOS_NUMBERS = SERVER + "/user/getSOSNumber";
    public static String SAVE_UPDATE_SOS = SERVER + "/user/saveorupdateSOSNumber";
    public static String GET_FRIEND_LIST = SERVER + "/friend/listacceptedfriends";
    public static String FRIEND_REQUEST_ACCEPT = SERVER + "/friend/acceptfriend";
    public static String FRIEND_REQUEST_REJECT = SERVER + "/friend/rejectfriend";
    public static String GET_FRIEND_PENDING_LIST = SERVER + "/friend/listpendingfriends";
    public static String GET_FRIEND_REQUEST_SENT_LIST = SERVER + "/friend/listfriendsrequestsentbyme";
    public static String GET_WAYPOINT_CATEGORIES = SERVER + "/trail/getWayPointsCategories";
    public static String GET_FEED_CONTENT = SERVER + "/feed/myfeeds";
    public static String GET_MESSAGE_CONTENT = SERVER + "/feed/myfeedmessage";
    public static String DELETE_FEED = SERVER + "/feed/deletefeed";
    public static String DELETE_MESSAGE = SERVER + "/feed/deletemessage";
    public static String CREATE_NEW_FEED = SERVER + "/feed/newpost";
    public static String CREATE_NEW_MESSAGE = SERVER + "/feed/newfeedmessage";
    public static String COMMENT_ON_FEED = SERVER + "/feed/commentonfeed";
    public static String SHARE_FEED = SERVER + "/feed/sharefeed";
    public static String SHARE_MESSAGE = SERVER + "/feed/sharefeedmessage";
    public static String SAVE_DEVICE_TOKEN = SERVER + "/mobileapp/register";
    public static String REMOVE_DEVICE_TOKEN = SERVER + "/mobileapp/unregister";
    public static String SAVE_WAYPOINT = SERVER + "/trail/newwaypoint";
    public static String SHARE_LOCATION = SERVER + "/user/shareposition";
    public static String MY_SHARELOCATION_REQUEST = SERVER + "/user/mysharepositionrequests";
    public static String DELETE_SHARELOCATION_REQUEST = SERVER + "/user/deletemysharepositionrequests/";
    public static String GET_SETTINGS = SERVER + "/user/getsettings";
    public static String UPDATE_SETTINGS = SERVER + "/user/settingsupdate";
    public static String SAVE_SEE_ME_NOT = SERVER + "/vehicle/seemenot";
    public static String FOLLOWME_INVITE = SERVER + "/followMe/registerForFollowMe";
    public static String FOLLOWME_STOP = SERVER + "/followMe/stopFollow";
    public static String FOLLOWME_STOP_FOLLOWME = SERVER + "/followMe/stopUserToFollowMyVehicle";
    public static String FOLLOWME_GET_VEHICLESTATE = SERVER + "/followMe/getVehicleState";
    public static String FOLLOWME_GETALL_VEHICLESTATE = SERVER + "/followMe/getAllVehicleState";
    public static String FOLLOWME_GET_FOLLOWERS = SERVER + "/followMe/getUsersFollowingMe";
    public static String FOLLOWME_GET_REQUESTS = SERVER + "/followMe/getRequestsOfFollowMe";
    public static String FOLLOWME_UPDATE_REQUEST = SERVER + "/followMe/updateFollowMeRequest";
    public static String NEW_FRIEND = SERVER + "/friend/newfriend/";
    public static String INVITE_FRIEND = SERVER + "/friend/invitefriend/";
    public static String LIST_ACCEPTED_FRIEND = SERVER + "/friend/listacceptedfriends";
    public static String LIST_REJECTED_FRIEND = SERVER + "friend/listrejectedfriends";
    public static String LIST_PENDING_FRIEND = SERVER + "/friend/listpendingfriends";
    public static String LIST_FREIND_REQUEST_SENT_BY_ME = SERVER + "/friend/listfriendsrequestsentbyme";
    public static String LIST_PENDING_REQUEST_COUNT = SERVER + "/friend/pendingfriendcount";
    public static String UNFRIEND = SERVER + "/friend/unfriend";
    public static String UPLOAD_VEHICLE_IMAGE = SERVER + "/vehicle/newvehicleimage";


    public static String SUCCESS = "SUCCESS";
    public static String ERROR = "ERROR";
    public static String NO_NETWORK = "There is currently no connection.Please check network connectivity.";
    public static String FILL_DETAILS_FIRST = "Hold up eager beaver, please first fill in the details.";
    public static String SomeProblem = "We are experiencing some problem, please try later !";
    public static String ConnectionError = "Network error !";
    public static String SYSTEM_ERROR = "System error occurred. Please try again later !";
    public static String AUTHENTICATION_FAILED = "Please login to continue";
    public static String INVALID_CREDENTIALS = "User name or password is incorrect";
    public static String AUTHORIZATION_FAILED = "The user has unauthorized operation.";
    public static String VEHICLE_NOT_FOUND = "The requested vehicle is not found in the system or is not associated with the user.";
    public static String DEVICE_NOT_FOUND = "The requested device is not found in the system or is not associated with the user.";
    //	public static String Font_Typface_Path = "fonts/Roboto-Light.ttf";
    public static String Font_Typface_Path = "fonts/helvetica-normal.ttf";
    public static String Parkfence_Checkbox_Text = "\t";
    public static String incidentAlert = "INCIDENTALERT";
    public static String constraintAlert = "CONSTRAINTALERT";
    public static String Traffic_Violation = "TRAFFIC_VIOLATION";
    public static String Accident = "ACCIDENT";
    public static String Device_Breakdown = "DEVICE_BREAKDOWN";
    public static String Theft = "THEFT";
    public static String Outbound_Geofence = "OUT_BOUND_GEO_FENCE";
    public static String Inbound_Geofence = "IN_BOUND_GEO_FENCE";
    public static String System_Fence = "SYSTEM_FENCE";
    public static String SPEED_LIMIT = "SPEED_LIMIT";
    public static String SELECTED_CAR_VEHICLE_ID = "selectedCarVehicleId";
    public static String tag = "CH_Constant";
    public static String errorCode;
    public static String NETWORK_ERROR = "Network Error!!";
    static String response, login_response;
    static int error_count = 0;
    public static final String REGISTRATION_FAILED = "registration_failed";
    public static final String TRIP_OVERVIEW = "TRIP_OVERVIEW";

    public static String getCurrent_DateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss zzz");
        String current_DateTime = dateFormat.format(new Date());

        return current_DateTime;
    }

    public static int getSingleAlertType(String alert_type, String type_val) {
        if (alert_type.equalsIgnoreCase(incidentAlert)) {
            if (type_val.equalsIgnoreCase(Traffic_Violation)) {
                return Box_Traffice;
            } else if (type_val.equalsIgnoreCase(Accident)) {
                return Box_Accident;
            } else if (type_val.equalsIgnoreCase(Device_Breakdown)) {
                return Box_Breakdown;
            } else //	if(type_val.equalsIgnoreCase(Theft))
            {
                return Box_Theft;
            }
        } else
        //	if(alert_type.equalsIgnoreCase(constraintAlert))
        {
            if (type_val.equalsIgnoreCase(Inbound_Geofence)) {
                return Box_LeaveGeofence;
            } else if (type_val.equalsIgnoreCase(Outbound_Geofence)) {
                return Box_EnterGeofence;
            } else //	if(type_val.equalsIgnoreCase(System_Fence))
            {
                return Box_OutSystemfence;
            }
        }
    }

    public static String ERROR_Msg(String errorCode) {
        String error_msg = errorCode;

        if (errorCode.equalsIgnoreCase("SYSTEM_ERROR")) {
            error_msg = SYSTEM_ERROR;
        } else if (errorCode.equalsIgnoreCase("AUTHENTICATION_FAILED")) {
            error_msg = INVALID_CREDENTIALS;
        } else if (errorCode.equalsIgnoreCase("AUTHORIZATION_FAILED")) {
            error_msg = AUTHORIZATION_FAILED;
        } else if (errorCode.equalsIgnoreCase("VEHICLE_NOT_FOUND")) {
            error_msg = VEHICLE_NOT_FOUND;
        } else if (errorCode.equalsIgnoreCase("DEVICE_NOT_FOUND")) {
            error_msg = DEVICE_NOT_FOUND;
        }


        return error_msg;

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED
                || connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTING) {
            return true;
        } else if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTING) {
            return true;
        } else
            return false;

        //		   	if (connMgr.getActiveNetworkInfo() != null && connMgr.getActiveNetworkInfo().isAvailable() && connMgr.getActiveNetworkInfo().isConnected())
        //		   		return true;
        //		   	 else
        //		   	 {
        //		   		 Log.v("PIPA", "Internet Connection Not Present");
        //		   		 return false;
        //		   	 }
    }

    public static void showDialogOK(Context context, String title, String message) {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(context);
        alt_bld.setMessage(message).setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alt_bld.create();
        alert.setTitle(title);
        //alert.setIcon(AlertDialog.BUTTON_NEGATIVE);
        alert.show();
    }

    public static void buildAlertMessageNoGps(final Context contxt, Typeface tf) {
        TextView message = new TextView(contxt);
        message.setTypeface(tf);

        if ((Home_Screen.context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) {
            message.setTextSize(23);
        }
        message.setTextSize(new Home_Screen().getTextSize());
        message.setText("Your GPS seems to be disabled, do you want to enable it?");
        final AlertDialog.Builder builder = new AlertDialog.Builder(contxt);
//		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
        builder.setView(message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {

                        contxt.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static int getCarCurrentIcon(String state, boolean parkFence, String vehicleType) {
        int res_id = 0;
        if (vehicleType == null || vehicleType.equals("CAR") || !vehicleType.equals("BIKE")) {
            if (state.toUpperCase().contains("PARKED") && parkFence) {
                res_id = R.drawable.parkingfence_parked_icon;
            } else if
                    (state.toUpperCase().contains("PARKED")) {
                res_id = R.drawable.parking_icon;
            } else if (state.toUpperCase().contains("MOVING")) {
                res_id = R.drawable.car_moving_icon;
            } else if (state.toUpperCase().contains("STOP")) {
                res_id = R.drawable.car_stop_icon;
            } else if ((state.toUpperCase().contains("OUT_OF_NETWORK_WITH_PRIOR_IGNITION_OFF_STATE") || state.toUpperCase().contains("OUT_OF_NETWORK_WITH_PRIOR_IGNITION_OFF_STATE")) && parkFence ) {
                res_id = R.drawable.car_no_network_fence_icon;
            } else if ((state.toUpperCase().contains("OUT_OF_NETWORK_WITH_PRIOR_IGNITION_OFF_STATE") || state.toUpperCase().contains("OUT_OF_NETWORK_WITH_PRIOR_IGNITION_OFF_STATE")) ) {
                res_id = R.drawable.car_no_network_icon;
            } else {
                res_id = R.drawable.car_stop_icon;
            }
        } else if (vehicleType.equals("BIKE")) {
            if (state.toUpperCase().contains("PARKED") && parkFence) {
                res_id = R.drawable.parkingfence_parked_icon_bike;
            } else if
                    (state.toUpperCase().contains("PARKED")) {
                res_id = R.drawable.parking_icon;
            } else if (state.toUpperCase().contains("MOVING")) {
                res_id = R.drawable.bike_moving_icon;
            } else if (state.toUpperCase().contains("STOP")) {
                res_id = R.drawable.bike_stop_icon;
            } else {
                res_id = R.drawable.bike_no_network_icon;
            }
        }
        return res_id;
    }



    // recursively apply typeface to our textviews
    public static final void setTypeface(ViewGroup viewGroup, Typeface typeface) {
        if (viewGroup == null) return;

        int children = viewGroup.getChildCount();
        for (int i = 0; i < children; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup) setTypeface((ViewGroup) view, typeface);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setTypeface(typeface);
            }
        }
    }
}
