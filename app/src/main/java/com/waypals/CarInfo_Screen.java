package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayout;
import android.text.InputType;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Images;
import com.waypals.gson.vo.VehicleMetaDataResponse;
import com.waypals.imageHelper.ImageResizer;
import com.waypals.objects.MyImageView;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CarInfo_Screen extends NavigationDrawer implements AsyncFinishInterface {
    public static final int CAMERA_PICTURE = 2;
    public static final int GALLERY_PICTURE = 1;
    static int clickCount = 0;
    ImageView back_btn, right_btn;
    TextView carInfo_title_txt;
    HashMap<String, String> loadedImageObjects = new HashMap<String, String>();
    HashMap<Integer, ImageView> usedImageViews = new HashMap<Integer, ImageView>();
    HashMap<String, String> currentImages = new HashMap<String, String>();
    EditText plateNo_et, driverName_et, policy_et, deviceId_et, carType_et, picture;
    ArrayList<IncidentImages> imagesSet = null;
    ArrayList<MyImageView> gridImags = new ArrayList<MyImageView>();
    ApplicationPreferences appPrefs;
    String fileNamePath;
    VehicleMetaDataResponse vehicleMetaDataResponse;
    Typeface tf;
    MyImageView f1, f2, f3, f4, defaultImage;
    int screenSize = 0;
    Activity activity;
    AsyncFinishInterface asyncFinishInterface;
    ServiceResponse serviceResponse;

    GridLayout grid;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            clickCount = 0;
            activity = this;
            asyncFinishInterface = this;
            screenSize = Utility.getScreenSize(this);
            setContentView(R.layout.car_info_screen);
            tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            CH_Constant.setTypeface((ViewGroup) findViewById(R.id.detail_root_layout), tf);
            back_btn = (ImageView) findViewById(R.id.left_btn);
            //   back_btn.setTypeface(tf);
            // option_btn = (Button) findViewById(R.id.right_btn);
            carInfo_title_txt = (TextView) findViewById(R.id.nav_title_txt);
            plateNo_et = (EditText) findViewById(R.id.carinfo_plateNo_edit);
            driverName_et = (EditText) findViewById(R.id.carinfo_driver_edit);
            policy_et = (EditText) findViewById(R.id.carinfo_policynumber_edit);
            deviceId_et = (EditText) findViewById(R.id.carinfo_deviceId_edit);
            carType_et = (EditText) findViewById(R.id.carinfo_carType_edit);

            Typeface tf = Typeface.createFromAsset(getAssets(),
                    CH_Constant.Font_Typface_Path);
            carInfo_title_txt.setTypeface(tf);
            carInfo_title_txt.setText("VEHICLE INFO");


            appPrefs = new ApplicationPreferences(this);

            plateNo_et.setInputType(InputType.TYPE_NULL);

            VehicleServices vehicleService = new VehicleServices();
            vehicleService.getCarInfo(appPrefs, this);

            back_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickCount = 0;
                    finish();
                }
            });

            grid = (GridLayout) findViewById(R.id.grid);
            grid.setColumnCount(2);
            grid.setRowCount(2);

            setupLeftDrawerListener(appPrefs, tf, false);
            setupRightDrawerListener(tf);


            defaultImage = (MyImageView) findViewById(R.id.add_pic_img);
            defaultImage.setImageResource(R.drawable.no_image);

            picture = (EditText) findViewById(R.id.picture);
            picture.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectImage();
                }
            });
            picture.setEnabled(false);

            right_btn = (ImageView) findViewById(R.id.right_btn);
            right_btn.setImageResource(R.drawable.edit_btn);
            right_btn.setVisibility(View.VISIBLE);
            right_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickCount++;
                    if (clickCount == 1) {
                        picture.setEnabled(true);
                        right_btn.setImageResource(R.drawable.done_btn);
                        setupImageViewListenrs();
                    }
                    if (clickCount > 1 && loadedImageObjects != null) {
                        try {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                            dialog.setTitle("Save");
                            dialog.setMessage("Do you want to update details?");
                            dialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.d(tag, "deleting feed");
                                    try {
                                        if (loadedImageObjects != null) {
                                            imagesSet = new ArrayList<IncidentImages>();
                                            HashMap<String, String> images = new HashMap<String, String>();
//                                            for (IncidentImages img : loadedImageObjects.values()) {
//                                                images.put(img.getImageName(), img.getImageStr());
//                                            }
                                            Iterator iterator = loadedImageObjects.entrySet().iterator();
                                            while (iterator.hasNext())
                                            {
                                                Map.Entry<String, String> next = (Map.Entry<String, String>) iterator.next();
                                                images.put(next.getKey(), next.getValue());
                                            }
                                            Gson gson = new Gson();
                                            RequestObject input = new RequestObject(appPrefs.getVehicle_Selected(), images);

                                            GenericAsyncService service = new GenericAsyncService(appPrefs, activity, CH_Constant.UPLOAD_VEHICLE_IMAGE, "UPLOAD_IMAGE", gson.toJson(input).toString(), tag, asyncFinishInterface, true);
                                            service.execute();
                                        }

                                    } catch (Exception ex) {
                                        ExceptionHandler.handleException(ex, activity);
                                    }
                                }
                            });
                            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    return;
                                }
                            });
                            dialog.setCancelable(false);
                            dialog.create();
                            dialog.show();
                        } catch (Throwable e) {
                            ExceptionHandler.handleException(e);
                        }
                    }

                }
            });
        } catch (Exception e) {
            ExceptionHandler.handleException(e, this);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clickCount = 0;
        finish();
    }

    @Override
    public void finish(String response, String method) {
        Gson gson = new Gson();
        if (method != null && method.equals("UPLOAD_IMAGE")) {
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        } else {
            if (response != null) {

                vehicleMetaDataResponse = gson.fromJson(response,
                        VehicleMetaDataResponse.class);
            }

        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())) || (vehicleMetaDataResponse != null && "ERROR".equalsIgnoreCase(
                vehicleMetaDataResponse.getResponse()))) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(this, Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, "Server not responding, please try after some time!!");
            }

            return;
        }

        if (serviceResponse != null) {
            if (serviceResponse.getResponse().equals("SUCCESS")) {
                Utility.makeNewToast(activity, "Information updated");
                finish();
            } else {
                Utility.makeNewToast(activity, "Could not update information");
            }
        }

        if (vehicleMetaDataResponse != null) {
            plateNo_et.setText(vehicleMetaDataResponse.getRegistrationNumber());
            driverName_et.setText(vehicleMetaDataResponse.getDriver());
            deviceId_et.setText(vehicleMetaDataResponse.getDeviceID());
            // deviceId_et.setText(MyVehicles_Json.get_MyVehicles_list().get(i).getAd_imei());
            carType_et.setText(vehicleMetaDataResponse.getCarType());
            policy_et.setText(vehicleMetaDataResponse.getPolicyNumber());

            clearImages();
            if (vehicleMetaDataResponse.getImages() != null && vehicleMetaDataResponse.getImages().size() > 0) {
                defaultImage = (MyImageView) findViewById(R.id.add_pic_img);
                defaultImage.setVisibility(View.GONE);
                List<Images> images = vehicleMetaDataResponse.getImages();
                for (final Images i : images) {
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final MyImageView imageView = new MyImageView(activity);
                    imageView.setLayoutParams(params);
                    imageView.setPadding(15, 5, 15, 15);
                    imageView.getLayoutParams().height = screenSize / 2 - 20;
                    imageView.getLayoutParams().width = screenSize / 2 - 20;
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageView.setName(i.getImageName());
                    Log.d("Image URL", i.getImageLink());

                    final RequestCreator creator = Picasso.with(activity).load(i.getImageLink());
                    creator.into(imageView);
                    creator.placeholder(R.drawable.no_image);
                    //grid.addView(imageView);
                    //gridImags.add(imageView);
                    creator.fetch(new Callback() {
                        @Override
                        public void onSuccess() {
                            imageView.setOnLongClickListener(new ImageClickListener(imageView));
                            grid.addView(imageView);
                            gridImags.add(imageView);
                            try {
                                imageView.setDrawingCacheEnabled(true);
                                currentImages.put(i.getImageName(), i.getImageLink());
                                loadedImageObjects.put(i.getImageName(), i.getImageLink());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError() {

                        }
                    });

                }
            }
        }
    }

    private void clearImages() {
        grid.removeAllViews();
        gridImags.clear();
        currentImages.clear();
        loadedImageObjects.clear();
    }


    private void setupImageViewListenrs() {
        if (gridImags != null && gridImags.size() > 0) {
            for (final MyImageView imageView : gridImags) {
                imageView.setOnLongClickListener(new ImageClickListener(imageView));
            }
        }
    }

    @Override
    protected void setImmobilizer() {

    }

    private class ImageClickListener implements View.OnLongClickListener
    {

        private MyImageView imageView;

        public ImageClickListener(MyImageView view)
        {
            this.imageView = view;
        }

        @Override
        public boolean onLongClick(View view) {
            try {
                AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                dialog.setTitle("Delete Image");
                dialog.setMessage("Do you want to delete this image ?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(tag, "deleting image");
                        try {
                            grid.removeView(imageView);
                            gridImags.remove(imageView);
                            currentImages.remove(imageView.getName());
                            loadedImageObjects.remove(imageView.getName());

                        } catch (Exception ex) {
                            ExceptionHandler.handleException(ex, activity);
                        }
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });
                dialog.setCancelable(false);
                dialog.create();
                dialog.show();
            } catch (Throwable e) {
                ExceptionHandler.handleException(e);
            }
            return false;
        }
    }


    public void selectImage() {
        if (imagesSet == null) {
            imagesSet = new ArrayList<IncidentImages>();
            usedImageViews.put(0, null);
            usedImageViews.put(1, null);
            usedImageViews.put(2, null);
            usedImageViews.put(3, null);
        }
        if (grid.getChildCount() >= 4) {
            try {
                Utility.makeNewToast(context, "You can not add more than 4 images");
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, context);
            } finally {
                return;
            }
        }
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CarInfo_Screen.this);
        builder.setTitle("Add Photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, CAMERA_PICTURE);
                        }
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, GALLERY_PICTURE);
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                fileNamePath = null;
                Log.d(tag, "image selection canceled");
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {

        }

        Log.i("resultCode", String.valueOf(resultCode));
        if (requestCode == CAMERA_PICTURE && resultCode == RESULT_OK)// capture image form camera
        {

            onPhotoTaken();
        } else if (requestCode == GALLERY_PICTURE && resultCode == RESULT_OK) // pick picture from gallary
        {
            onPhotoTakenFromGallery(data);
        }
    }

    public void onPhotoTaken() {
        try {
            defaultImage = (MyImageView) findViewById(R.id.add_pic_img);
            defaultImage.setVisibility(View.GONE);
            if (fileNamePath != null) {
                fileNamePath = checkImageSize(fileNamePath);
                {
                    String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                    String type = name.substring(name.indexOf(".") + 1);
                    String imageStr = Utility.getImageToHex(activity, fileNamePath);
                    final IncidentImages images = new IncidentImages();
                    images.setImageName(name);
                    images.setImageStr(imageStr);
                    if (type.equalsIgnoreCase(ImageType.JPEG.name())) {
                        images.setImageType(ImageType.JPEG);
                    } else if (type.equalsIgnoreCase(ImageType.PNG.name())) {
                        images.setImageType(ImageType.PNG);
                    } else if (type.equalsIgnoreCase(ImageType.JPG.name())) {
                        images.setImageType(ImageType.JPG);
                    } else {
                        Utility.makeNewToast(activity, "Image format not supported");
                        return;
                    }
                    loadedImageObjects.put(""+images.hashCode(), imageStr);
                    currentImages.put(""+images.hashCode(), imageStr);
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final MyImageView imageView = new MyImageView(activity);
                    imageView.setLayoutParams(params);
                    imageView.setPadding(15, 5, 15, 15);
                    imageView.setName(""+images.hashCode());
                    imageView.getLayoutParams().height = screenSize / 2 - 20;
                    imageView.getLayoutParams().width = screenSize / 2 - 20;
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageView.setImageURI(Uri.fromFile(new File(fileNamePath)));
                    imageView.setOnLongClickListener(new ImageClickListener(imageView));
                    grid.addView(imageView);
                    gridImags.add(imageView);
                }

            }
            Log.d("camera imagePath", fileNamePath);
            Log.d(tag, "Total Image files: " + imagesSet);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("camera imagePath", "onactivity result");
        }

    }

    private String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public void onPhotoTakenFromGallery(Intent data) {
        try {
            defaultImage = (MyImageView) findViewById(R.id.add_pic_img);
            defaultImage.setVisibility(View.GONE);

            Uri _uri = data.getData();
            if (_uri != null) {
                fileNamePath = getPath(_uri);
                Log.d(tag, "gallary image path: " + fileNamePath);
                if (fileNamePath != null) {
                    fileNamePath = checkImageSize(fileNamePath);
                    {
                        String name = fileNamePath.substring(fileNamePath.lastIndexOf("/") + 1, fileNamePath.length());
                        String type = name.substring(name.indexOf(".") + 1);
                        String imageStr = Utility.getImageToHex(activity, fileNamePath);
                        final IncidentImages images = new IncidentImages();
                        images.setImageName(name);
                        images.setImageStr(imageStr);

                        if (type.equalsIgnoreCase(ImageType.JPEG.name())) {
                            images.setImageType(ImageType.JPEG);
                        } else if (type.equalsIgnoreCase(ImageType.PNG.name())) {
                            images.setImageType(ImageType.PNG);
                        } else if (type.equalsIgnoreCase(ImageType.JPG.name())) {
                            images.setImageType(ImageType.JPG);
                        } else {
                            Utility.makeNewToast(activity, "Image format not supported");
                            return;
                        }

                        loadedImageObjects.put(""+images.hashCode(), imageStr);
                        currentImages.put(""+images.hashCode(), imageStr);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        final MyImageView imageView = new MyImageView(activity);
                        imageView.setLayoutParams(params);
                        imageView.setPadding(15, 5, 15, 15);
                        imageView.setName(""+images.hashCode());
                        imageView.getLayoutParams().height = screenSize / 2 - 20;
                        imageView.getLayoutParams().width = screenSize / 2 - 20;
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        imageView.setImageURI(Uri.fromFile(new File(fileNamePath)));
                        imageView.setOnLongClickListener(new ImageClickListener(imageView));
                        grid.addView(imageView);
                        gridImags.add(imageView);
                    }
                }
                Log.d(tag, "images list : " + imagesSet);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(tag, "onactivity result");
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File image = new File(storageDir + "/" + timeStamp + "_temp.jpg");

        // Save a file: path for use with ACTION_VIEW intents
        fileNamePath = image.getAbsolutePath();
        return image;
    }

    public String checkImageSize(String fileNamePath) {
        try {
            File f = new File(fileNamePath);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            is.close();

            int kb = size / 1024;

            if (kb > 256) {
                File file = Utility.createFile();
                Bitmap b = ImageResizer.resize(new File(fileNamePath), 256, 256);
                FileOutputStream fout = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                fout.flush();
                b.recycle();
                b = null;
                fout.close();
                return file.getAbsolutePath();
            } else {
                return fileNamePath;
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, activity);
            return null;
        }
    }

    private Integer getAvailbleImagePosition() {
        int ret = 0;

        Set<Integer> pos = usedImageViews.keySet();

        for (int i = 0; i < usedImageViews.size(); i++) {
            ImageView img = usedImageViews.get(i);
            if (img == null) {
                ret = i;
                break;
            }
        }
        return ret;
    }

    enum ImageType {
        PNG, JPG, JPEG;
    }

    public static class IncidentImages implements Serializable {

        private String imageStr;
        private String imageName;
        private ImageType imageType;
        private String imageLink;

        public String getImageLink() {
            return imageLink;
        }

        public void setImageLink(String imageLink) {
            this.imageLink = imageLink;
        }

        public String getImageStr() {
            return imageStr;
        }

        public void setImageStr(String imageStr) {
            this.imageStr = imageStr;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public ImageType getImageType() {
            return imageType;
        }

        public void setImageType(ImageType imageType) {
            this.imageType = imageType;
        }

        @Override
        public String toString() {
            return "IncidentImages{" +
                    "imageStr='" + imageStr + '\'' +
                    ", imageName='" + imageName + '\'' +
                    ", imageType='" + imageType + '\'' +
                    '}';
        }
    }

    private class RequestObject {
        private Long vehicleId;
        private HashMap<String, String> imageHexString;

        private RequestObject(Long vehicleId, HashMap<String, String> imageHexString) {
            this.vehicleId = vehicleId;
            this.imageHexString = imageHexString;
        }

        public Long getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(Long vehicleId) {
            this.vehicleId = vehicleId;
        }

        public HashMap<String, String> getImageHexString() {
            return imageHexString;
        }

        public void setImageHexString(HashMap<String, String> imageHexString) {
            this.imageHexString = imageHexString;
        }
    }
}
