package com.waypals;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.waypals.gson.vo.VehicleInformationVO;
import com.waypals.gson.vo.VehicleServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by shantanu on 18/10/14.
 */
public class Sharelocation_Screen extends FragmentActivity implements
        AsyncFinishInterface {

    static String tag = "Share location Screen";
    static Marker currentMarker = null;
    static Collection<VehicleInformationVO> vehicleInfoVO;
    static VehicleServices vs = new VehicleServices();
    static Long carSelected;
    static Marker selCar_mrker;
    static int totalCars = 0;
    TextView title;
    Typeface tf;
    Context context;
    ImageView back_btn, right_btn;
    ApplicationPreferences appPref;
    GoogleMap map = null;
    LocationManager lm = null;
    LocationListener ll = null;
    LatLng selectedCar_latlng;
    String[] vehicaleList;
    boolean[] isParkFenceOn;
    NetworkInfo currentNetworkInfo;
    String dialogMesage;
    private BroadcastReceiver Network_Receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            boolean noConnectivity = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent
                    .getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_IS_FAILOVER, false);

            currentNetworkInfo = (NetworkInfo) intent
                    .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent
                    .getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);

            if (currentNetworkInfo.isConnected()) {

                if (CH_Constant.isNetworkAvailable(Sharelocation_Screen.this)) {

					/* setup contents */
                    try {
                        Log.i(tag, "Load vehicle service data");
                        // /vs.getVehiclesData(appPref, asynFinishIterface);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            } else {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No Network Connected", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 80);
                toast.show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogMesage = getResources().getString(R.string.share_location_default);
        setContentView(R.layout.sharelocation_screen);
        context = this;
        appPref = new ApplicationPreferences(context);
        title = (TextView) findViewById(R.id.nav_title_txt);
        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        right_btn = (ImageView) findViewById(R.id.right_btn);
        right_btn.setImageResource(R.drawable.friend_list_active_icon);
        right_btn.setVisibility(View.VISIBLE);
        right_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, MyShareLocationRequestsScreen.class);
                startActivity(i);
            }
        });


        Typeface tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);
        title.setTypeface(tf);
        title.setText("SHARE LOCATION");

        setUpMap(true, false);

    }

    public void myLocation() {
        Log.d(tag, "register location manager");
        if (lm == null) {
            lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        }
        if (ll == null) {
            ll = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    setCurrentMarker(location, map);
                    Log.d(tag, "got new location");
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, ll);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMap(false, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            return;
        }
        lm.removeUpdates(ll);
        Log.d(tag, "remove ll");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermission
            return;
        }
        lm.removeUpdates(ll);
    }

    public void setCurrentMarker(final Location location, final GoogleMap mMap) {
        try {
            final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions opt = new MarkerOptions().title("Your location").icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_green));
            opt.position(latLng);
            opt.visible(true);
            if (currentMarker != null) {
                currentMarker.remove();
                currentMarker = mMap.addMarker(opt);
            } else {
                currentMarker = mMap.addMarker(opt);
            }
            currentMarker.showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isGpsEnabled() {
        Boolean ok = false;
        LocationManager lm = null;
        boolean gps_enabled = false, network_enabled = false;
        if (lm == null)
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                final AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                dialog.setMessage("Your GPS seems to be disabled, do you want to enable it?");
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(myIntent);
                    }
                });
                dialog.show();

            }else {
                Location lastKnownLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastKnownLocation != null) {
                    setCurrentMarker(lastKnownLocation, map);
                }
            }


        } else {
            ok = true;
            myLocation();
        }
        return ok;
    }

    public void setUpMap(final boolean isInitial, final boolean updateIfNeeded) {
        if (map == null) {
            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;
                    map.setMapType(1);
                    map.getUiSettings().setCompassEnabled(true);

                    myLocation();

                    if(isInitial) {
                        updateMap();
                    }

                    if(updateIfNeeded){
                        updateMapIfNeeded();
                    }
                }
            });

        }
    }

    private void updateMap(){
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                String idis = marker.getId();
                if (idis.equals("m0")) {
                    dialogMesage = getResources().getString(R.string.share_car_location);
                } else if (idis.equals("m1")) {
                    dialogMesage = getResources().getString(R.string.share_my_location);
                }
                final AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
                dialog.setMessage(dialogMesage);
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                dialog.setPositiveButton("Share", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent i = new Intent(context, Share_Message_Screen.class);
                        i.putExtra("screen", "sharelocation");
                        i.putExtra("lat", Double.toString(marker.getPosition().latitude));
                        i.putExtra("long", Double.toString(marker.getPosition().longitude));
                        startActivity(i);
                        finish();
                    }
                });
                dialog.show();


                return false;
            }
        });
        if (isGpsEnabled()) {
            myLocation();
        }

        Log.i(tag, "Load vehicle service data");
        try {
            vs.getVehiclesData(appPref, this, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {

        Log.i(tag, "method RES: " + method);
        Gson gson = new Gson();
        if (method.contains("vehiclesdata")) {
            Log.i(tag, "getVehiclesData RES: " + response);
            VehicleServiceResponse vehicleServiceResponse = gson.fromJson(
                    response, VehicleServiceResponse.class);


            if (vehicleServiceResponse.getResponse()
                    .equalsIgnoreCase("SUCCESS")) {
                vehicleInfoVO = vehicleServiceResponse.getVehicles();

                int totalVehicle = vehicleInfoVO.size();
                vehicaleList = new String[totalVehicle];
                isParkFenceOn = new boolean[totalVehicle];
                totalCars = totalVehicle;
                Log.i(tag, "Data RES: total vehicale " + totalVehicle);

                Iterator<VehicleInformationVO> v1 = vehicleInfoVO.iterator();
                int count = 0;
                while (v1.hasNext()) {
                    VehicleInformationVO v2 = v1.next();
                    Log.i(tag, "Data RES: REG " + v2.getRegistrationNumber());
                    Log.i(tag, "Data RES: REG " + v2.getState());
                    Log.i(tag, "Data RES: REG " + v2.getVehicleId());
                    Log.i(tag, "Data RES: REG " + v2.isSystemFenceEnabled());
                    vehicaleList[count] = v2.getRegistrationNumber();
                    isParkFenceOn[count] = v2.isSystemFenceEnabled();
                    count++;
                }
            }
        }
        if (method.contains("contacts")) {


        }
    }

    @Override
    public void onPostExecute(Void Result) {
        setUpMapIfNeeded();


    }

    public boolean isGoogleMapsInstalled() {
        try {
            Log.i(tag, "check Google Map installed");
            ApplicationInfo info;
            info = getPackageManager().getApplicationInfo(
                    "com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;

        }
    }

    private void setUpMapIfNeeded() {
        setUpMap(false, true);
    }

    private void updateMapIfNeeded(){
        if (isGoogleMapsInstalled()) {
            if (map != null) {

                Iterator<VehicleInformationVO> vehicleInfoItr = vehicleInfoVO
                        .iterator();
                if (vehicleInfoItr != null) {
                    if (selCar_mrker != null) {
                        selCar_mrker.remove();
                    }
                    int i = 0;
                    while (vehicleInfoItr.hasNext()) {
                        VehicleInformationVO vehicleInfo = vehicleInfoItr
                                .next();
                        if (vehicleInfo.getLocation() != null) {
                            String location = vehicleInfo.getLocation()
                                    .toString();
                            Log.i(tag, location);
                            LatLng latlng = new LatLng(
                                    Double.parseDouble(location.split("/")[0]),
                                    Double.parseDouble(location.split("/")[1]));

                            if (appPref.getVehicle_Selected() == vehicleInfo
                                    .getVehicleId()) {
                                int res_icon = CH_Constant.getCarCurrentIcon(
                                        vehicleInfo.getState(),
                                        vehicleInfo.isSystemFenceEnabled, vehicleInfo.getVehicleType());
                                selCar_mrker = map
                                        .addMarker(new MarkerOptions()
                                                .position(latlng)
                                                .icon(BitmapDescriptorFactory
                                                        .fromResource(R.drawable.pin_car_waypoint)));
                                selCar_mrker.setVisible(true);
                                selCar_mrker.setTitle("Vehicle location");
                                selCar_mrker.showInfoWindow();

                                selectedCar_latlng = latlng;
                                appPref.saveSelectedCarLatLng(location);
                            }
                        }
                        i++;
                    }
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(selectedCar_latlng).zoom(11).build();
                    map.moveCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));


                }

            }

        }
    }


}
