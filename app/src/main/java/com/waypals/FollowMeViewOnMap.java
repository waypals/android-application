package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;
import com.waypals.adapters.ContactsCompletionView;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Friend;
import com.waypals.objects.GeoLocation;
import com.waypals.objects.UserGps;
import com.waypals.response.FollowmeUserWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class FollowMeViewOnMap extends Fragment implements AsyncFinishInterface, TokenCompleteTextView.TokenListener {

    static FragmentManager fm;
    ArrayList<Friend> selectedFriends = new ArrayList<Friend>();
    private AsyncFinishInterface async;
    private GoogleMap map;
    private String to;
    private String userId;
    private String token;
    private String tag = "Follow me map fragment";
    private OnFragmentInteractionListener mListener;
    private ServiceResponse serviceresponse;
    private String method;
    private HashMap<String, Marker> markerMap = new HashMap<>();
    private TimerTask timerTask;
    private Timer timer;
    private FollowmeUserWrapper[] wrapper;
    private String[] listOfTokens;
    private ApplicationPreferences prefs;
    private View view;
    private String userName;
    private boolean isShown = false;
    private ContactsCompletionView completionView;
    private ArrayAdapter<Friend> adapter;
    private int numPoints;

    public FollowMeViewOnMap() {

    }

    public static FollowMeViewOnMap getInstance(FragmentManager manager, String to, String userid, String token, String userName) {
        fm = manager;
        FollowMeViewOnMap fragment = new FollowMeViewOnMap();
        Bundle b = new Bundle();
        b.putString("to", to);
        b.putString("userId", userid);
        b.putString("token", token);
        b.putString("userName", userName);

        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = new ApplicationPreferences(getActivity());
        if (getArguments() != null) {
            to = getArguments().getString("to");

            if (to != null && !to.equals("ALL")) {
                userId = getArguments().getString("userId");
                token = getArguments().getString("token");
                userName = getArguments().getString("userName");
                Log.d(tag, "TO: " + to + "|userid : " + userId + "| token:" + token);
            } else {
                try {
                    JSONObject input = new JSONObject();
                    input.put("vehicleId", prefs.getVehicle_Selected());
                    GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_REQUESTS, "GET_FOLLOWING", input.toString(), tag, this, false);
                    service.execute();
                } catch (Exception e) {
                    ExceptionHandler.handleException(e);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_follow_me_view_on_map, container, false);
        async = this;
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                if (map != null) {
                    Log.d(tag, "Map is not null");
                    if (to != null && !to.equals("ALL")) {
                        try {
                            final JSONObject input = new JSONObject();
                            input.put("token", token);
                            Log.d(tag, "request : " + input);


                            timerTask = new TimerTask() {
                                @Override
                                public void run() {
                                    try {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (view != null && view.getVisibility() == View.VISIBLE) {
                                                    GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GET_VEHICLESTATE, "GET_LOCATION", input.toString(), tag, async, false);
                                                    service.execute();
                                                } else {
                                                    cancel();
                                                }
                                            }
                                        });

                                    } catch (Exception e) {
                                        ExceptionHandler.handleException(e);
                                    }
                                }
                            };
                            timer = new Timer(true);


                        } catch (Exception e) {
                            ExceptionHandler.handleException(e);
                        }
                    } else {
                        Log.d(tag, "Wrapper size: " + listOfTokens);
                        RelativeLayout lyt = (RelativeLayout) view.findViewById(R.id.search_followers_layout);
                        lyt.setVisibility(View.VISIBLE);

                        timerTask = new TimerTask() {
                            @Override
                            public void run() {
                                try {
                                    JSONArray array = new JSONArray();
                                    for (String x : listOfTokens) {
                                        array.put(x);
                                    }
                                    final JSONObject input = new JSONObject();
                                    input.put("token", array);

                                    Handler handler = new Handler(Looper.getMainLooper());
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            GenericAsyncService service = new GenericAsyncService(prefs, getActivity(), CH_Constant.FOLLOWME_GETALL_VEHICLESTATE, "GETALL_LOCATION", input.toString(), tag, async, false);
                                            service.execute();
                                        }
                                    });

                                } catch (Exception e) {
                                    ExceptionHandler.handleException(e);
                                }
                            }
                        };
                        timer = new Timer(true);
                        timer.scheduleAtFixedRate(timerTask, 1000, 5 * 1000);
                    }
                } else {
                    Log.d(tag, "Map is null");
                }
            }
        });




        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        return true;
                    }
                    Log.d("Map", "Back Pressed");
                    onBackPressed();
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    public void createFriendsList(Friend[] friends) {
        try {
            if (friends != null) {
                adapter = new FilteredArrayAdapter<Friend>(getActivity(), R.layout.friend_list_row, friends) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {

                            LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                            convertView = (View) l.inflate(R.layout.friend_list_row, parent, false);
                        }

                        Friend p = getItem(position);

                        if(p != null) {
                            ((TextView) convertView.findViewById(R.id.name)).setText(p.getFirstName());
                            ((TextView) convertView.findViewById(R.id.email)).setText("");
                        }
                        return convertView;
                    }

                    @Override
                    protected boolean keepObject(Friend obj, String mask) {
                        mask = mask.toLowerCase();
                        if (!selectedFriends.contains(obj)) {
                            return obj.getFirstName().toLowerCase().contains(mask);
                        } else
                            return false;
                    }
                };

                completionView = (ContactsCompletionView) view.findViewById(R.id.contacts);
                completionView.setDuplicateParentStateEnabled(false);
                completionView.setAdapter(adapter);
                completionView.setThreshold(1);
                completionView.setSingleLine();
                completionView.setTokenLimit(1);
                completionView.setTokenListener(this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            } else {
                try {
                    Utility.makeNewToast(getActivity(), getString(R.string.err_no_friends));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, getActivity());
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, getActivity());
        }
    }

    @Override
    public void finish(String response, String method) throws Exception {
        this.method = method;
        Log.d("FollowResponse", response);
        if (method != null && method.equals("GET_LOCATION")) {
            if (response != null) {
                Gson gson = new Gson();
                serviceresponse = gson.fromJson(response, ServiceResponse.class);
            }
            Log.d(tag, "Service response: " + serviceresponse.toString());
        }
        if (method != null && method.equals("GETALL_LOCATION")) {
            if (response != null) {
                Gson gson = new Gson();
                serviceresponse = gson.fromJson(response, ServiceResponse.class);
            }
            Log.d(tag, "Service response: " + serviceresponse.toString());
        }
        if (method != null && method.equals("GET_FOLLOWING")) {
            if (response != null) {
                Gson gson = new Gson();
                serviceresponse = gson.fromJson(response, ServiceResponse.class);
            }

        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (serviceresponse != null && "ERROR".equalsIgnoreCase(
                serviceresponse.getResponse())) {
            String errorMessage = serviceresponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(getActivity(),
                        CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
                Intent i = new Intent(getActivity(), Login_Screen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {
                //Utility.makeNewToast(getActivity(), getString(R.string.server_error_expired));
            }

            return;
        }

        if (method != null && method.equals("GET_LOCATION")) {
            if (serviceresponse != null) {
                if (serviceresponse.getGps() != null) {
                    GeoLocation gps = serviceresponse.getGps().getGeoLocation();
                    Log.d(tag, "Service response: " + serviceresponse.toString());

                    if(gps != null) {
                        Double lat = gps.getLatitude();
                        Double lon = gps.getLongitude();
                        final LatLng latlng = new LatLng(lat, lon);

                        MarkerOptions mp = new MarkerOptions();
                        mp.title(serviceresponse.getUserName() + "'s location");
                        mp.position(latlng);
                        if (!markerMap.containsKey(token)) {
                            Marker m = map.addMarker(mp);
                            m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_map_pin_icon));
                            markerMap.put(token, m);
                        } else {
                            Marker m = markerMap.get(token);
                            if (m != null) {
                                m.remove();
                                m = map.addMarker(mp);
                                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_map_pin_icon));
                                markerMap.put(token, m);
                            }
                        }
                        if (!to.equals("ALL")) {
                            final LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            builder.include(latlng);
                            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                                @Override
                                public void onMapLoaded() {
                                    if (!isShown) {
                                        isShown = true;
                                        if (latlng != null) {
                                            map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
        if (method != null && method.equals("GETALL_LOCATION")) {
            if (serviceresponse != null && serviceresponse.getGpsData() != null) {
                LatLng fistOne = null;
                HashMap<String, UserGps> data = serviceresponse.getGpsData();

                Set<String> tokens = data.keySet();
                final LatLngBounds.Builder builder = new LatLngBounds.Builder();
                numPoints = 0;

                int index = 0;
                List<Friend> lstFriends = new ArrayList<>();

                for (String t : tokens) {
                    if (data != null && data.get(t) != null) {
                        GeoLocation gps = data.get(t).getGps() != null ? data.get(t).getGps().getGeoLocation() : null;
                        if (gps != null) {
                            Log.d(tag, "Service response: " + serviceresponse.toString());
                            Double lat = gps.getLatitude();
                            Double lon = gps.getLongitude();
                            LatLng latlng = new LatLng(lat, lon);

                            if (fistOne == null) {
                                fistOne = latlng;
                            }

                            builder.include(latlng);
                            numPoints++;
                            MarkerOptions mp = new MarkerOptions();
                            mp.title(data.get(t).getUserName() + "'s location");
                            mp.position(latlng);

                            Friend friend = new Friend();
                            friend.setFirstName(data.get(t).getUserName());
                            friend.setUserId(t);
                            lstFriends.add(friend);

                            if (!markerMap.containsKey(t)) {
                                Marker m = map.addMarker(mp);
                                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_map_pin_icon));
                                markerMap.put(t, m);
                            } else {
                                Marker m = markerMap.get(t);
                                if (m != null) {
                                    m.remove();
                                    m = map.addMarker(mp);
                                    m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_map_pin_icon));
                                    markerMap.put(t, m);
                                }
                            }
                        }
                    }
                }

                Friend[] frndItems = null;
                if(lstFriends != null && !lstFriends.isEmpty())
                {
                    frndItems = new Friend[lstFriends.size()];
                    createFriendsList(lstFriends.toArray(frndItems));
                }

//                if (map.getCameraPosition().zoom < 5) {
//
//                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(fistOne, 11));
//                }
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (!isShown) {
                            isShown = true;
                            if(numPoints >0) {
                                map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));
                            }
                        }
                    }
                });
                // builder.include(fistOne);

                //     map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));

            }
        }
        if (method.equals("GET_FOLLOWING")) {
            if (serviceresponse != null) {
                List<FollowmeUserWrapper> listFollowMeUserWrapper = serviceresponse.getListFollowMeUserWrapper();
                if (listFollowMeUserWrapper != null) {
                    try {
                        listOfTokens = new String[listFollowMeUserWrapper.size()];
                        for (int i = 0; i < listFollowMeUserWrapper.size(); i++) {
                            if (listFollowMeUserWrapper.get(i).getToken() != null) {
                                listOfTokens[i] = listFollowMeUserWrapper.get(i).getToken();
                            }
                        }

                        if(listOfTokens != null && listOfTokens.length >0) {
                            timer.scheduleAtFixedRate(timerTask, 1000, 5 * 1000);
                        }
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e);
                    }
                }
            }

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (timer != null) {
            timer.cancel();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onTokenAdded(Object token) {
        if (selectedFriends == null) {
            selectedFriends = new ArrayList<Friend>();
        }

        if (!selectedFriends.contains((Friend) token)) {
            selectedFriends.add((Friend) token);
        }

        Friend friend = (Friend)token;
        if (friend != null && markerMap.containsKey(friend.getUserId())) {

            resetIconColor();

            Marker m = markerMap.get(friend.getUserId());
            if( m!= null) {
                m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.blue_map_pin_icon));
                float zoom = map.getCameraPosition().zoom;
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(m.getPosition())
                        .zoom(zoom).build();

                map.moveCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        }
    }

    private void resetIconColor()
    {
        if(markerMap != null)
        {
            Set<String> strings = markerMap.keySet();
            if(strings != null)
            {
                for(String str : strings)
                {
                    Marker marker = markerMap.get(str);
                    if(marker != null)
                    {
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_map_pin_icon));
                    }
                }
            }
        }
    }

    @Override
    public void onTokenRemoved(Object token) {
        if (selectedFriends != null && !selectedFriends.isEmpty() && selectedFriends.contains((Friend) token)) {
            selectedFriends.remove((Friend) token);
        }

        resetIconColor();
    }

    public void onBackPressed() {

        if (view != null && view.getVisibility() == View.VISIBLE) {
            Intent i = new Intent();
            i.setAction("BACK");
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
        } else {
            getActivity().finish();
        }

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


}
