package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.Beans.ConstraintAlerts_Beans;
import com.waypals.Beans.IncidentAlerts_Beans;
import com.waypals.exception.ExceptionHandler;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.WplHttpClient;
import com.waypals.utils.Data_ListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Alerts_Screen extends Activity {
    public static ArrayList<ConstraintAlerts_Beans> Alerts_list;
    public static ArrayList<IncidentAlerts_Beans> Inci_Alerts_list;
    public static Typeface tf;
    ImageView back_btn;
    TextView nav_title_txt;
    String tag = "Alert Screen";
    ListView alert_list;
    Data_ListAdapter customAdapter;
    int alrt_inci_val;
    ApplicationPreferences appPref;
    JSONObject response;
    Calendar localCal = new GregorianCalendar(TimeZone.getDefault());
    SimpleDateFormat formatIDT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
    SimpleDateFormat formatUTC = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
    SimpleDateFormat formatIST = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alerts_list);

        back_btn = (ImageView) findViewById(R.id.left_btn);
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        alert_list = (ListView) findViewById(R.id.alert_list);

        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);


        appPref = new ApplicationPreferences(this);

        alrt_inci_val = getIntent().getExtras().getInt("alrt_inci_val");

        Alerts_list = new ArrayList<ConstraintAlerts_Beans>();
        Inci_Alerts_list = new ArrayList<IncidentAlerts_Beans>();


        back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alert_list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                try {
                    if (alrt_inci_val == CH_Constant.Alert_Val) {
                        Intent i = new Intent(Alerts_Screen.this, Alert_Detail_Screen.class);
                        i.putExtra("alert_index", pos);
                        i.putExtra("alert_list", Alerts_list);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(Alerts_Screen.this, Incident_Detail_Screen.class);
                        i.putExtra("type", "set");
                        i.putExtra("alert_index", pos);
                        i.putExtra("alert_list", Inci_Alerts_list);
                        startActivity(i);
                    }
                } catch (Exception e) {
                    ExceptionHandler.handleException(e, Alerts_Screen.this);
                }

            }
        });

        NetworkCall networkCall = new NetworkCall(this);
        networkCall.execute();
        if (alrt_inci_val == CH_Constant.Alert_Val) {
            Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            nav_title_txt.setTypeface(tf);
            nav_title_txt.setText("ALERTS");
        } else {
            Typeface tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
            nav_title_txt.setTypeface(tf);
            nav_title_txt.setText("INCIDENTS");
        }

    }

    public void done() throws JSONException {
        if (alrt_inci_val == CH_Constant.Alert_Val) {
            JSONArray constraintListArray = response.optJSONArray("constraintAlert");
            if (constraintListArray != null && constraintListArray.length() > 0) {
                try {
                    setConstraintAlertList(constraintListArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new JSONException("Response is not parsable");
                }
                customAdapter = new Data_ListAdapter(Alerts_Screen.this, alrt_inci_val, Alerts_list, null);
                alert_list.setAdapter(customAdapter);
            } else {
                Toast toast = Toast.makeText(Alerts_Screen.this, "No Alerts", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            JSONArray incidenceAlertArray = response.optJSONArray("incidentAlert");
            if (incidenceAlertArray != null && incidenceAlertArray.length() > 0) {
                try {
                    setIncidenceAlerts(incidenceAlertArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new JSONException("Response is not parsable");
                }
                customAdapter = new Data_ListAdapter(this, alrt_inci_val, null, Inci_Alerts_list);
                alert_list.setAdapter(customAdapter);
            } else {
                Toast toast = Toast.makeText(Alerts_Screen.this, "No Incidents", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    private void setIncidenceAlerts(JSONArray incidentAlerts_Array) throws JSONException {
        if (incidentAlerts_Array.length() > 0) {
            int length = incidentAlerts_Array.length();
            for (int j = 0; j < length; j++) {
                IncidentAlerts_Beans inci_alerts_Data = new IncidentAlerts_Beans();
                JSONObject alertsObj = incidentAlerts_Array.getJSONObject(j);

                inci_alerts_Data.setIncidentId(alertsObj.getString("id"));

                JSONObject event_obj = alertsObj.getJSONObject("event");

                inci_alerts_Data.setEvent_Id(event_obj.getString("id"));

                String reportingTime = event_obj.getString("reportingTime");


                if (reportingTime != null && reportingTime.contains("IDT")) {
                    try {
                        formatIDT.setTimeZone(TimeZone.getTimeZone("IDT"));
                        Date date = formatIDT.parse(reportingTime);
                        localCal.setTime(date);
                        reportingTime = formatIST.format(localCal.getTime());
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else if (reportingTime != null && reportingTime.contains("UTC")) {
                    try {
                        Date date = formatIST.parse(reportingTime);
                        localCal.setTime(date);
                        reportingTime = formatIST.format(localCal.getTime());
                        Log.d(tag, "reporting time:" + reportingTime);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                String incidentTime = event_obj.getString("incidentTime");
                if (incidentTime != null && incidentTime.contains("UTC")) {
                    try {
                        Date date = formatIST.parse(incidentTime);
                        localCal.setTime(date);
                        incidentTime = formatIST.format(localCal.getTime());
                        Log.d(tag, "incident time:" + incidentTime);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else if (incidentTime != null && incidentTime.contains("IDT")) {
                    try {
                        formatIDT.setTimeZone(TimeZone.getTimeZone("IDT"));
                        Date date = formatIDT.parse(incidentTime);
                        localCal.setTime(date);
                        incidentTime = formatIDT.format(localCal.getTime());
                        Log.d(tag, "incident time:" + incidentTime);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                inci_alerts_Data.setReportingTime(reportingTime);
                inci_alerts_Data.setDamages(event_obj.getString("damages"));
                inci_alerts_Data.setIncidentTime(incidentTime);
                inci_alerts_Data.setVehicleId(event_obj.getString("vehicleId"));
                try {
                    inci_alerts_Data.setTicketNumber(event_obj.optString("ticketNumber"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                inci_alerts_Data.setVehicleGroup(event_obj.getString("vehicleGroup"));
                inci_alerts_Data.setDescription(event_obj.getString("description"));
                inci_alerts_Data.setReferenceNumber(event_obj.getString("referenceNumber"));
                inci_alerts_Data.setReportedLocation(event_obj.getString("reportedLocation"));
                inci_alerts_Data.setDriver(event_obj.getString("driver"));

                JSONObject type_obj = event_obj.getJSONObject("type");
                inci_alerts_Data.setEvent_type(type_obj.getString("eventType"));

                JSONObject vehiclemetadata_obj = event_obj.getJSONObject("vehicleMetadata");
                /*inci_alerts_Data.setMeta_ManufacturingDate(vehiclemetadata_obj.getString("manufacturingDate"));
                inci_alerts_Data.setMeta_RegistrationDate(vehiclemetadata_obj.getString("registrationDate"));
				inci_alerts_Data.setMeta_id(vehiclemetadata_obj.getString("id"));
				inci_alerts_Data.setMeta_Color(vehiclemetadata_obj.getString("color"));
				inci_alerts_Data.setMeta_FuelType(vehiclemetadata_obj.getString("fuelType"));
				inci_alerts_Data.setMeta_VinNumber(vehiclemetadata_obj.getString("vinNumber"));
				inci_alerts_Data.setMeta_VehicleName(vehiclemetadata_obj.getString("vehicleName"));
				inci_alerts_Data.setMeta_Manufacturer(vehiclemetadata_obj.getString("manufacturer"));
				inci_alerts_Data.setMeta_PolicyNumber(vehiclemetadata_obj.getString("policyNumber"));
				inci_alerts_Data.setMeta_ChasisNumber(vehiclemetadata_obj.getString("chasisNumber"));*/
                inci_alerts_Data.setMeta_RegistrationNo(vehiclemetadata_obj.getString("registrationNo"));

                Inci_Alerts_list.add(inci_alerts_Data);

            }
        }
    }

    private String getViewTye(String type, String name) {

        String viewType = type;
        if (type != null) {
            if (type.equals(CH_Constant.Inbound_Geofence)) {
                viewType = "Vehicle violated " + name + " geofence";
            } else if (type.equals(CH_Constant.Outbound_Geofence)) {
                viewType = "Vehicle violated " + name + " geofence";
            } else if (type.equals(CH_Constant.System_Fence)) {
                viewType = "Vehicle violated parking fence";
            } else if (type.equals(CH_Constant.SPEED_LIMIT)) {
                viewType = "Vehicle violated speed limit";
            }


        }
        return viewType;
    }

    private void setConstraintAlertList(JSONArray array) throws JSONException {
        int length = array.length();
        for (int j = 0; j < length; j++) {
            ConstraintAlerts_Beans alerts_Data = new ConstraintAlerts_Beans();
            JSONObject alertsObj = array.getJSONObject(j);

            alerts_Data.setConstraintTransgressionTime(alertsObj.getString("constraintTransgressionTime"));
            alerts_Data.setConstraintId(alertsObj.getString("id"));
            alerts_Data.setConstraint_VehicleId(alertsObj.getString("vehicleId"));
            alerts_Data.setDP_Type(alertsObj.getString("type"));

            JSONObject dataPoint_obj = alertsObj.getJSONObject("dataPoint");
            alerts_Data.setDPoint_DateTime(dataPoint_obj.getString("dateTime"));

            JSONObject dp_geoSpeed_obj = dataPoint_obj.getJSONObject("gpsSpeed");
            alerts_Data.setDP_gpsSpeed(dp_geoSpeed_obj.getString("speed") + dp_geoSpeed_obj.getString("unit"));

            JSONObject dataPoint_geoLoc_obj = dataPoint_obj.getJSONObject("geoLocation");
            alerts_Data.setDP_Long(dataPoint_geoLoc_obj.getString("longitude"));
            alerts_Data.setDP_Lat(dataPoint_geoLoc_obj.getString("latitude"));

            JSONObject violatedRule_obj = alertsObj.getJSONObject("violatedRule");
            alerts_Data.setViolatedRule_Id(violatedRule_obj.getString("id"));
            alerts_Data.setViolatedRule_Enabled(violatedRule_obj.getString("enabled"));

            JSONObject vr_constraint_obj = violatedRule_obj.getJSONObject("constraint");
            alerts_Data.setViolatedRule_Constraint_Id(vr_constraint_obj.optString("id"));
            alerts_Data.setViolatedRule_Constraint_Name(vr_constraint_obj.optString("name"));
            try {
//					JSONObject allowedDeviation_obj = vr_constraint_obj.getJSONObject("allowedDeviation");
//					alerts_Data.setViolatedRule_Constraint_AllowedDeviation(allowedDeviation_obj.getString("distance") + allowedDeviation_obj.getString("unit"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            JSONObject geographArea_obj = vr_constraint_obj.optJSONObject("geographicalArea");
            //alerts_Data.setViolatedRule_GraphArea_Id(geographArea_obj.getString("id"));
            //alerts_Data.setViolatedRule_GraphArea_LeftTop(geographArea_obj.getString("leftTop"));
            if (geographArea_obj != null)
                alerts_Data.setViolatedRule_GraphArea_Radius(geographArea_obj.optString("radius"));
//				alerts_Data.setViolatedRule_GraphArea_RightBottom(geographArea_obj.getString("rightBottom"));
            try {
                JSONObject geographArea_center_obj = geographArea_obj.optJSONObject("center");
                if (geographArea_center_obj != null) {
                    alerts_Data.setViolatedRule_GraphArea_Center_Lat(geographArea_center_obj.getString("latitude"));
                    alerts_Data.setViolatedRule_GraphArea_Center_Long(geographArea_center_obj.getString("longitude"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String name = vr_constraint_obj.optString("name");
                if (name == null) {
                    name = "";
                }
                alerts_Data.setViewType(getViewTye(alertsObj.getString("type"), name));
                JSONObject cnstrnt_vehiclemetadata_obj = alertsObj.getJSONObject("vehicleMetaData");
                    /*alerts_Data.setMeta_ManufacturingDate(cnstrnt_vehiclemetadata_obj.getString("manufacturingDate"));
                    alerts_Data.setMeta_RegistrationDate(cnstrnt_vehiclemetadata_obj.getString("registrationDate"));
					alerts_Data.setMeta_id(cnstrnt_vehiclemetadata_obj.getString("id"));
					alerts_Data.setMeta_Color(cnstrnt_vehiclemetadata_obj.getString("color"));
					alerts_Data.setMeta_FuelType(cnstrnt_vehiclemetadata_obj.getString("fuelType"));
					alerts_Data.setMeta_VinNumber(cnstrnt_vehiclemetadata_obj.getString("vinNumber"));
					alerts_Data.setMeta_VehicleName(cnstrnt_vehiclemetadata_obj.getString("vehicleName"));
					alerts_Data.setMeta_Manufacturer(cnstrnt_vehiclemetadata_obj.getString("manufacturer"));
					alerts_Data.setMeta_PolicyNumber(cnstrnt_vehiclemetadata_obj.getString("policyNumber"));
					alerts_Data.setMeta_ChasisNumber(cnstrnt_vehiclemetadata_obj.getString("chasisNumber"));*/
                alerts_Data.setMeta_RegistrationNo(cnstrnt_vehiclemetadata_obj.getString("registrationNo"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Alerts_list.add(alerts_Data);
        }

    }


    private class NetworkCall implements AsyncFinishInterface { //extends AsyncTask<String, Void, Void> {
        Alerts_Screen alertScreen;

        public NetworkCall(Alerts_Screen alertScreen) {
            this.alertScreen = alertScreen;
        }

        public void execute() {
            try {
                if (alrt_inci_val == CH_Constant.Alert_Val) {
                    //String apiPath, String bodyRequest,CH_AppPreferences appPrefs
                    WplHttpClient.callVolleyService(alertScreen, CH_Constant.GET_ALERTS, "", appPref, this, null, true);
                } else {
                    WplHttpClient.callVolleyService(alertScreen, CH_Constant.GET_INCIDENT_ALERT + appPref.getVehicle_Selected(), "", appPref, this, null, true);
                }
            } catch (Exception e) {
                ExceptionHandler.handlerException(e, alertScreen);
            }
        }

        @Override
        public void finish(String resp, String method) throws Exception {
            if (alrt_inci_val == CH_Constant.Alert_Val) {
                response = new JSONObject(resp);
            }else {
                Gson gson = new Gson();
                response = new JSONObject(resp);
                ServiceResponse response1 = gson.fromJson(response.toString(), ServiceResponse.class);
                Log.d(tag, "response for incidents 1 :" + response1);
                Log.d(tag, "response for incidents " + response);
            }
        }

        @Override
        public void onPostExecute(Void result) {

            try {
                alertScreen.done();
            } catch (JSONException e) {
                Toast toast = Toast.makeText(Alerts_Screen.this, "Parsing Error", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } catch (Exception e) {
                Toast toast = Toast.makeText(Alerts_Screen.this, "Please try again!!", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }

    }
}







