package com.waypals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Name;
import com.waypals.request.ChangePassword;
import com.waypals.response.FollowmeUserWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import org.json.JSONObject;

public class ChangePasswordActivity extends Activity implements AsyncFinishInterface{

    private ImageView back_button;
    private TextView title;
    private EditText oldText, newPass;
    private Button btnChange;
    private ApplicationPreferences pref;
    private ServiceResponse serviceResponse;
    private String method;
    private boolean isFirstTime = false;
    private EditText newPassConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Intent intent = getIntent();
        isFirstTime = intent.getBooleanExtra("firstTime", false);

        pref = new ApplicationPreferences(this);

        back_button = (ImageView) findViewById(R.id.left_btn);

        back_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if(!isFirstTime) {
                    finish();
                }else{
                    showFirstTimeMessage();
                }
            }
        });

        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setText("Change Password");

        oldText = (EditText) findViewById(R.id.old_password);
        newPass = (EditText) findViewById(R.id.new_password);
        newPassConfirm = (EditText) findViewById(R.id.new_password_confirm);

        btnChange = (Button) findViewById(R.id.change_button);

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()){
                    changePassword(oldText.getText().toString(), newPass.getText().toString());
                }else{
                    showFirstTimeMessage();
                }
            }
        });
    }

    public void showFirstTimeMessage(){
        Utility.makeNewToast(this, "Please change your password");
    }

    @Override
    public void onBackPressed() {
        if(!isFirstTime) {
            super.onBackPressed();
        }else{
            showFirstTimeMessage();
        }
    }

    void changePassword(String oldPassStr, String newPassStr){
        try {
            //Log.d(tag, "broad cast received to stop user from following");
            //Long userid = intent.getLongExtra("userId", -1);
            //String token = intent.getStringExtra("token");

            ChangePassword changePassword = new ChangePassword();
            changePassword.setOldPassword(oldPassStr);
            changePassword.setNewPassword(newPassStr);

            Gson gson = new Gson();
            String input = gson.toJson(changePassword);

            GenericAsyncService service = new GenericAsyncService(pref, this, CH_Constant.CHANGE_PASSWORD, "CHANGE_PASSWORD", input, "", this, true);
            service.execute();

        } catch (Exception e) {
            ExceptionHandler.handleException(e);
        }
    }

    boolean isValid(){

        if(Utility.isStringNullEmpty(oldText.getText().toString())){
            Utility.makeNewToast(this, "Please provide old password");
            return false;
        }

        if(Utility.isStringNullEmpty(newPass.getText().toString())){
            Utility.makeNewToast(this, "Please provide new password");
            return false;
        }

        if(!newPass.getText().toString().equals(newPassConfirm.getText().toString())){
            Utility.makeNewToast(this, "Password did not matched");
            return false;
        }

        return true;
    }

    @Override
    public void finish(String response, String method) throws Exception {

        this.method = method;
        if ("CHANGE_PASSWORD".equals(method)) {
            Gson gson = new Gson();
            if (response != null) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }
    }

    @Override

    public void onPostExecute(Void Result) {
        if ("CHANGE_PASSWORD".equals(method) && serviceResponse != null && serviceResponse.getResponse().equals("SUCCESS")) {
            pref.saveUserPassword(newPass.getText().toString());
            Utility.makeNewToast(this, "Password has been changed successfully");
            finish();

        }   else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                serviceResponse.getResponse())) {
            String errorMessage = serviceResponse.getErrorCode();
            if (errorMessage != null
                    && errorMessage.equals("AUTHENTICATION_FAILED")) {
                Toast toast = Toast.makeText(this,
                        "Incorrect Password/Authentication Failure", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 60);
                toast.show();
//                Intent i = new Intent(this, Login_Screen.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(i);
            }
            else {
                Utility.makeNewToast(this, "Server not responding, please try after some time!!");
            }
        }
    }
}
