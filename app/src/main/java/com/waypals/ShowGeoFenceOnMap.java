package com.waypals;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.GeoFenceVO;
import com.waypals.gson.vo.GeogrpahicalArea;
import com.waypals.gson.vo.ListGeoFenceResponse;
import com.waypals.gson.vo.RideLocation;
import com.waypals.gson.vo.VehicleStateResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ShowGeoFenceOnMap extends FragmentActivity implements
        AsyncFinishInterface {
    static final int geoAlfa = 80;
    static final int geoColor = 196;
    public static GoogleMap map;
    public static LatLng selectedCar_latlng;
    // static Collection<VehicleInformationVO> vehicleInfo;
    static String state = "";
    static String vehicleType = "";
    static boolean isSystemFenceEnabled;
    static String tag = "List GeoFence";
    VehicleServices vs = new VehicleServices();
    ImageView back_btn, save_btn;
    TextView navTitle, yellow_ribbon_txt;
    ImageView myloc_img, map_layer_img;
    LinearLayout geo_property_dialog;
    Spinner geo_event_spinr, geo_radius_spinr;
    EditText geo_radius_edit, geo_name_edit, geo_location_edit;
    int maplayer = 1;
    Marker myPosition;
    boolean me_position = false;
    ApplicationPreferences appPref;
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    // ArrayList<Circle> circle;
    // ArrayList<Marker> geoMarker;
    int fillColor;
    int k = 0;
    boolean bool_mapclick = false, isVisible_geo_property;
    List<GeoFenceVO> geoFences;
    HashMap<LatLng, String[]> geoFenceIdsMap;
    private Marker selected_to_remove = null;
    private long id = -1;

    public static double roundOff(double unrounded, int precision,
                                  int roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.car_overview);

            back_btn = (ImageView) findViewById(R.id.left_btn);
            save_btn = (ImageView) findViewById(R.id.right_btn);
            navTitle = (TextView) findViewById(R.id.nav_title_txt);
            map_layer_img = (ImageView) findViewById(R.id.map_layer_img);
            myloc_img = (ImageView) findViewById(R.id.myloc_img);
            yellow_ribbon_txt = (TextView) findViewById(R.id.yellow_ribbon_txt);
            // geo_radius_spinr = (Spinner) findViewById(R.id.geo_radius_spinr);
            geo_radius_edit = (EditText) findViewById(R.id.geo_radius_edit);
            geo_name_edit = (EditText) findViewById(R.id.geo_name_edit);
            geo_location_edit = (EditText) findViewById(R.id.geo_location_edit);
            geo_event_spinr = (Spinner) findViewById(R.id.geo_event_spinr);
            geo_property_dialog = (LinearLayout) findViewById(R.id.geo_property_dialog);

            appPref = new ApplicationPreferences(this);

            Intent intent = getIntent();
            id = intent.getLongExtra("ID", -1);

            Typeface tf = Typeface.createFromAsset(getAssets(),
                    CH_Constant.Font_Typface_Path);
            navTitle.setTypeface(tf);
            navTitle.setText("GEO FENCES");
//			yellow_ribbon_txt.setVisibility(View.VISIBLE);
//			yellow_ribbon_txt.setText(getString(R.string.create_geofence));

			/*
             * String location = appPref.getSelectedCarLatLng();
			 * selectedCar_latlng = new LatLng(Double.parseDouble(location
			 * .split("/")[0]), Double.parseDouble(location.split("/")[1]));
			 */

            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    map = googleMap;
                    map.setMapType(maplayer);
                    map.getUiSettings().setCompassEnabled(true);

                    if (ActivityCompat.checkSelfPermission(ShowGeoFenceOnMap.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ShowGeoFenceOnMap.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        return;
                    }
                    map.setMyLocationEnabled(false);

                    try {
                        vs.getVehicleState(appPref, ShowGeoFenceOnMap.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            back_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            map_layer_img.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    maplayer++;
                    map.setMapType(maplayer);

                    if (maplayer == 4) {
                        maplayer = 0;
                    }

                }
            });

            geoFenceIdsMap = new HashMap<LatLng, String[]>();

        } catch (Exception e1) {
            ExceptionHandler.handleException(e1, this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putLong("ID", id);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        id = savedInstanceState.getLong("ID");
    }

    @Override
    public void finish(String response, String method) throws Exception {
        Log.i(tag, "method RES: " + method);
        Gson gson = new Gson();
        if (method.contains("vehiclestate")) {
            Log.d(tag, "getVehiclesData RES: " + response);
            VehicleStateResponse vehicleServiceResponse = gson.fromJson(
                    response, VehicleStateResponse.class);

            if (vehicleServiceResponse != null
                    && vehicleServiceResponse.getLocation() != null) {
                String location = vehicleServiceResponse.getLocation()
                        .toString();
                selectedCar_latlng = new LatLng(Double.parseDouble(location
                        .split("/")[0]),
                        Double.parseDouble(location.split("/")[1]));
                state = vehicleServiceResponse.getState();
                vehicleType = vehicleServiceResponse.getVehicleType();
                isSystemFenceEnabled = vehicleServiceResponse.isSystemFenceEnabled();
            } else {

                if(!Utility.isStringNullEmpty(appPref.getSelectedCarLatLng())) {
                    String location = appPref.getSelectedCarLatLng().toString();
                    selectedCar_latlng = new LatLng(Double.parseDouble(location
                            .split("/")[0]),
                            Double.parseDouble(location.split("/")[1]));
                }else {
                    selectedCar_latlng = null;
                }
            }

            vs.getListOfGeoFence(appPref, this, new AsyncFinishInterface() {
                @Override
                public void finish(String response, String method) throws Exception {
                    String listOfGeoFences = response;
                    if (listOfGeoFences != null) {
                        Gson gson = new Gson();
                        ListGeoFenceResponse listOfGeofence = gson.fromJson(
                                listOfGeoFences, ListGeoFenceResponse.class);
                        if (listOfGeofence != null
                                && listOfGeofence.getResponse().equals("SUCCESS")) {

                            if (id == -1) {
                                geoFences = listOfGeofence.getGeoFences();
                            }else{

                                geoFences = new ArrayList<GeoFenceVO>();

                                for(GeoFenceVO geoFenceVO: listOfGeofence.getGeoFences()){
                                    if (geoFenceVO.getId() == id){
                                        geoFences.add(geoFenceVO);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onPostExecute(Void Result) {
                    cameraUpdate();
                }
            });
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        // TODO Auto-generated method stub

        if(selectedCar_latlng != null) {
            map.addMarker(new MarkerOptions().position(selectedCar_latlng).icon(
                    BitmapDescriptorFactory.fromResource(CH_Constant
                            .getCarCurrentIcon(state, isSystemFenceEnabled, vehicleType))));
        }

       // map.moveCamera(CameraUpdateFactory
       //         .newLatLngZoom(selectedCar_latlng, 13));

        map.setOnMarkerClickListener(new OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {

                if (marker.getTitle() != null) {
                    selected_to_remove = marker;

                    LatLng latLng = marker.getPosition();

                    Intent i = new Intent(getApplicationContext(), RemoveGeofenceConfirm.class);

                    i.putExtra("data", geoFenceIdsMap.get(latLng));

                    startActivityForResult(i, 100);

                    return false;
                } else {
                    return false;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }

    private void cameraUpdate() {

        if (geoFences != null) {
            List<LatLng> lstCoord = new ArrayList<LatLng>();
            for (GeoFenceVO geoFenceVO : geoFences) {
                GeogrpahicalArea geographicalArea = geoFenceVO
                        .getGeographicalArea();
                if (geographicalArea.getCenter() != null) {
                    String location = geographicalArea.getCenter().toString();
                    LatLng latLng = new LatLng(Double.parseDouble(location
                            .split("/")[0]),
                            Double.parseDouble(location.split("/")[1]));
                    Circle circle = map.addCircle(new CircleOptions()
                                    .center(latLng)
                                    .radius(geographicalArea.getRadius()).strokeColor(Color.BLUE)
                    );

                    String data[] = new String[]{geoFenceVO.getName(), String.valueOf(geoFenceVO.getId())};

                    geoFenceIdsMap.put(latLng, data);
                    Log.i(tag, "Added one geofence into list : " + latLng.toString() + ", name " + geoFenceVO.getName().toLowerCase() + ",id : " + geoFenceVO.getId());
                    map.addMarker(new MarkerOptions().position(latLng).icon(
                            BitmapDescriptorFactory.fromResource(R.drawable.enter_geofence_icon)).title(geoFenceVO.getName()));
                    lstCoord.add(latLng);

//					 //circle is the google.maps.Circle-instance
//			        com.google.maps.event.addListener(circle,"mouseover",function(){
                    //this.getMap().getDiv().setAttribute("title",this.get(geoFenceVO.getName()));});

//			        google.maps.event.addListener(circle,"mouseout",function(){
//			             this.getMap().getDiv().removeAttribute("title");});
                } else if (geographicalArea.getCoordinatePoints() != null && geographicalArea.getCoordinatePoints().size() > 0) {
                    int size = geographicalArea.getCoordinatePoints().size();
                    LatLng[] array = new LatLng[size];
                    int i = 0;
                    List<RideLocation> locationList = geographicalArea.getCoordinatePoints();

                    for (RideLocation locationPoint : locationList) {
                        String location = locationPoint.toString();
                        array[i] = new LatLng(Double.parseDouble(location
                                .split("/")[0]),
                                Double.parseDouble(location.split("/")[1]));
                        i++;
                    }

                    map.addMarker(new MarkerOptions().position(array[0]).icon(
                            BitmapDescriptorFactory.fromResource(R.drawable.enter_geofence_icon)).title(geoFenceVO.getName()));
                    map.addPolygon(new PolygonOptions()
                            .add(array)
                            .fillColor(Color.LTGRAY).strokeColor(Color.BLUE));
                    lstCoord.add(array[0]);
                }
            }

            final LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for(LatLng lt : lstCoord) {
                builder.include(lt);
            }

          //  map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));

            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));
                }
            });
        }
    }

}
