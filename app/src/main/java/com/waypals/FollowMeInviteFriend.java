package com.waypals;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.adapters.ContactsCompletionView;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.Friend;
import com.waypals.request.FollowMeInviteRequest;
import com.waypals.request.TimeRange;
import com.waypals.response.AcceptedFriendResponse;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.CustomDateTimePicker;
import com.waypals.utils.Utility;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class FollowMeInviteFriend extends Activity implements TokenCompleteTextView.TokenListener, AsyncFinishInterface {

    private static String startime = null, endtime = null;
    private static Date startDate, endDate;
    ContactsCompletionView completionView;
    Friend[] friends;
    ArrayAdapter<Friend> adapter;
    Button back_btn, actionButton;
    TextView title;
    String tag = "Follow me invite screen";
    ArrayList<Friend> selectedFriends = new ArrayList<Friend>();
    EditText message;
    Activity context;
    String method;
    String content;
    ServiceResponse serviceResponse;
    AcceptedFriendResponse friendResponse;
    AsyncFinishInterface asyncFinishInterface;
    private ApplicationPreferences appPrefs;
    private TextView startTimeTextView, endTimeTextView;
    private CustomDateTimePicker custom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.followme_create_request);
        context = this;
        asyncFinishInterface = this;

        actionButton = (Button) findViewById(R.id.postButton);
        back_btn = (Button) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Typeface tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        message = (EditText) findViewById(R.id.feedEditText);

        appPrefs = new ApplicationPreferences(this);


        startTimeTextView = (TextView) findViewById(R.id.starttime);
        endTimeTextView = (TextView) findViewById(R.id.endtime);
        try {
            startTimeTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    custom = new CustomDateTimePicker(context, new CustomDateTimePicker.ICustomDateTimeListener() {
                        @Override
                        public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                          String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                          int hour24, int hour12, int min, int sec, String AM_PM) {

                            String set_DateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz").format(dateSelected);

                            startTimeTextView = (TextView) findViewById(R.id.starttime);

                            System.out.println("Formated Date :: " + set_DateTime);
                            startDate = dateSelected;
                            startime = set_DateTime;

                            startTimeTextView.setText((new SimpleDateFormat("HH:mm aa").format(dateSelected)).toUpperCase());
                        }

                        @Override
                        public void onCancel() {

                        }


                    });

                    /**
                     * Pass Directly current time format it will return AM and PM if you set
                     * false
                     */
                    custom.set24HourFormat(false);
                    /**
                     * Pass Directly current data and time to show when it pop up
                     */
                    custom.setDate(Calendar.getInstance());

                    custom.showDialog();
                }
            });


            endTimeTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    custom = new CustomDateTimePicker(context, new CustomDateTimePicker.ICustomDateTimeListener() {
                        @Override
                        public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName,
                                          String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName,
                                          int hour24, int hour12, int min, int sec, String AM_PM) {

                            String set_DateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz").format(dateSelected);

                            endTimeTextView = (TextView) findViewById(R.id.endtime);

                            System.out.println("Formatted Date :: " + set_DateTime);

                            endtime = set_DateTime;
                            endDate = dateSelected;
                            endTimeTextView.setText((new SimpleDateFormat("HH:mm aa").format(dateSelected)).toUpperCase());
                        }

                        @Override
                        public void onCancel() {

                        }


                    });

                    /**
                     * Pass Directly current time format it will return AM and PM if you set
                     * false
                     */
                    custom.set24HourFormat(false);
                    /**
                     * Pass Directly current data and time to show when it pop up
                     */
                    custom.setDate(Calendar.getInstance());

                    custom.showDialog();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        /*call for list of friends*/
        GenericAsyncService service = new GenericAsyncService(appPrefs, this, CH_Constant.GET_FRIEND_LIST, "listacceptedfriends", "", tag, this, false);
        service.execute();

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                message = (EditText) findViewById(R.id.feedEditText);
                String text = message.getText().toString();

                if (selectedFriends == null) {
                    selectedFriends = new ArrayList<Friend>();
                }
                if (text.isEmpty()) {
                    try {
                        Utility.makeNewToast(context, getString(R.string.inp_alert_message));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                } else {
                    if (selectedFriends != null && selectedFriends.size() < 1) {
                        try {
                            Utility.makeNewToast(context, getString(R.string.inp_select_friend));
                        } catch (Exception e) {
                            ExceptionHandler.handlerException(e, context);
                        }
                    } else {
                        try {

                            Set<Long> users = new HashSet<Long>();

                            for (Friend f : selectedFriends) {
                                users.add(Long.parseLong(f.getUserId()));
                            }

                            TimeRange range = new TimeRange();
                            range.setStartTime(startime);
                            range.setEndTime(endtime);

                            FollowMeInviteRequest request = new FollowMeInviteRequest();
                            request.setRange(range);
                            request.setFollowWith(users);
                            request.setVehicleId(appPrefs.getVehicle_Selected());

                            Gson gson = new Gson();
                            String input = gson.toJson(request);

                            GenericAsyncService service = new GenericAsyncService(appPrefs, context, CH_Constant.FOLLOWME_INVITE, "INVITE", input, tag, asyncFinishInterface, true);
                            service.execute();


                            selectedFriends.clear();
                            message.setText("");
                            completionView.clear();
                        } catch (Throwable e) {
                            ExceptionHandler.handleException(e);
                        }
                    }

                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish(String response, String method) {

        try {
            this.method = method;
            Gson gson = new Gson();
            if (method.equals("listacceptedfriends")) {
                friendResponse = gson.fromJson(response, AcceptedFriendResponse.class);

                if (friendResponse.getResponse().equalsIgnoreCase("SUCCESS")) {
                    friends = friendResponse.getAcceptedFriendsResult();
                }
                Log.d(tag, friendResponse.toString());
            }
            if (method.equals("INVITE")) {
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        } catch (Throwable ex) {
            ExceptionHandler.handleException(ex);
        }

    }


    public void createFriendsList(Friend[] friends) {
        try {
            if (friends != null) {
                adapter = new FilteredArrayAdapter<Friend>(this, R.layout.friend_list_row, friends) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {

                            LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                            convertView = (View) l.inflate(R.layout.friend_list_row, parent, false);
                        }

                        Friend p = getItem(position);
                        ((TextView) convertView.findViewById(R.id.name)).setText(p.getFirstName() + " " + p.getLastName());
                        ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());

                        return convertView;
                    }

                    @Override
                    protected boolean keepObject(Friend obj, String mask) {
                        mask = mask.toLowerCase();
                        if (!selectedFriends.contains(obj)) {
                            return obj.getFirstName().toLowerCase().startsWith(mask) || obj.getLastName().toLowerCase().startsWith(mask) || obj.getEmail().toLowerCase().startsWith(mask);
                        } else
                            return false;
                    }
                };


                completionView = (ContactsCompletionView) findViewById(R.id.contacts);
                completionView.setDuplicateParentStateEnabled(false);
                completionView.setAdapter(adapter);
                completionView.setTokenListener(this);
                completionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
            } else {
                try {
                    Utility.makeNewToast(context, getString(R.string.err_no_friends));
                } catch (Exception e) {
                    ExceptionHandler.handlerException(e, context);
                }
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, context);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        try {

            if ((serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) || (friendResponse != null && "ERROR".equalsIgnoreCase(
                    friendResponse.getResponse()))) {
                String errorMessage = null;
                if(serviceResponse != null) {
                    errorMessage = serviceResponse.getErrorCode();
                }else if(friendResponse != null)
                {
                    errorMessage = friendResponse.getErrorCode();
                }
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }

                return;
            }

            if (method != null && method.endsWith("listacceptedfriends")) {
                if (friendResponse != null && "SUCCESS".equalsIgnoreCase(friendResponse.getResponse())) {
                    createFriendsList(friends);
                } else {
                    try {
                        Utility.makeNewToast(context, CH_Constant.ERROR_Msg(friendResponse.getErrorCode()));
                    } catch (Exception e) {
                        ExceptionHandler.handlerException(e, context);
                    }
                }

            }
            if (method != null && method.equals("INVITE")) {
                if (serviceResponse != null) {
                    if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                        Utility.makeNewToast(this, getString(R.string.op_alert_followme_invite_success));
                    } else {
                        Utility.makeNewToast(this, getString(R.string.op_alert_followme_invite_failed));
                    }
                }
                selectedFriends.clear();

            }
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex, context);
        }
    }

    @Override
    public void onTokenAdded(Object token) {
        if (selectedFriends == null) {
            selectedFriends = new ArrayList<Friend>();
        }

        if (!selectedFriends.contains((Friend) token)) {
            selectedFriends.add((Friend) token);
        }
    }

    @Override
    public void onTokenRemoved(Object token) {
        if (selectedFriends != null && !selectedFriends.isEmpty() && selectedFriends.contains((Friend) token)) {
            selectedFriends.remove((Friend) token);
        }

    }

}
