package com.waypals.feedImpl;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.waypals.feedItems.ImageContent;
import com.waypals.R;
import com.waypals.feedItems.FeedWrapper;
import com.waypals.feedItems.Images;
import com.waypals.utils.ApplicationPreferences;

import java.util.List;

public class FeedListAdapter extends BaseAdapter {
    ImageLoader imageLoader = null;
    String tag = "Feed List Adapter";
    ApplicationPreferences appPref;
    FeedListAdapter fd;
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedWrapper> feedItems;

    public FeedListAdapter(Activity activity, List<FeedWrapper> feedItems, ApplicationPreferences appPref) {
        this.activity = activity;
        this.feedItems = feedItems;
        this.appPref = appPref;
        this.fd = this;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.feed_screen, null);

        if (imageLoader == null) {
            imageLoader = AppController.getInstance().getImageLoader();
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) convertView
                .findViewById(R.id.feedText);
        TextView url = (TextView) convertView.findViewById(R.id.txtUrl);

        FeedImageView feedImageView = (FeedImageView) convertView
                .findViewById(R.id.feedImage1);

        final FeedWrapper item = feedItems.get(position);

        name.setText(item.getFeed().getOwner().getName().getFirstName() + " " + item.getFeed().getOwner().getName().getLastName());

        TableRow deleteRow = (TableRow) convertView.findViewById(R.id.deleteRow);

//        if(deleteRow!=null)
//        {
//            deleteRow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    try
//                    {
//                        final ArrayList<Boolean> result = new ArrayList<Boolean>();
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
//                        dialog.setTitle("Delete Feed");
//                        dialog.setMessage( "Do you want to delete this feed ?");
//                        dialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                Log.d(tag, "deleting feed");
//                                try{
//                                    JSONObject input = new JSONObject();
//                                    input.put("feedId", item.getFeed().getId());
//                                    input.put("sharedBy", item.getFeed().getFeedUsers()[0].getSharedBy().getUserId());
//                                    DeleteFeedService deleteFeedService = new DeleteFeedService(activity,appPref,input, fd, item);
//                                    deleteFeedService.execute();
//                                }
//                                catch (Throwable ex)
//                                {
//                                    ExceptionHandler.handleException(ex);
//                                }
//                            }
//                        });
//                        dialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                return;
//                            }
//                        });
//                        dialog.setCancelable(false);
//                        dialog.create();
//                        dialog.show();
//                    }
//                    catch (Throwable e)
//                    {
//                        ExceptionHandler.handleException(e);
//                    }
//
//                }
//            });
//        }
        // Converting timestamp into x ago format
//		CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
//				Long.parseLong(item.getTimeStamp()),
//				System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        timestamp.setText(item.getFeed().getTimeStamp());

        // Chcek for empty status message
        if (!TextUtils.isEmpty(item.getDescription())) {
            statusMsg.setText(item.getDescription());
            statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }

//		// Checking for null feed url
//		if (item.getUrl() != null) {
//			url.setText(Html.fromHtml("<a href=\"" + item.getUrl() + "\">"
//					+ item.getUrl() + "</a> "));
//
//			// Making url clickable
//			url.setMovementMethod(LinkMovementMethod.getInstance());
//			url.setVisibility(View.VISIBLE);
//		} else {
//			// url is null, remove from the view
//			url.setVisibility(View.GONE);
//		}

        // user profile pic

        // Feed image setting only one for now
        ImageContent imageContent = (ImageContent) item.getFeed().getContent();
        final Images[] images = imageContent.getImages();
        if (images != null && images.length > 0) {

            if (images[0].getImageName() != null) {
                feedImageView.setImageUrl(images[0].getImageName(), imageLoader);
                feedImageView.setVisibility(View.VISIBLE);
                feedImageView
                        .setResponseObserver(new FeedImageView.ResponseObserver() {
                            @Override
                            public void onError() {
                            }

                            @Override
                            public void onSuccess() {
                            }
                        });
            } else {
                feedImageView.setVisibility(View.GONE);
            }
        } else {
            feedImageView.setVisibility(View.GONE);
        }


        return convertView;
    }

}
