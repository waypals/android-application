/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waypals.pushNotification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.waypals.Alerts_Screen_New;
import com.waypals.Engine_Screen;
import com.waypals.FollowMe_Screen;
import com.waypals.Friends_Screen_New;
import com.waypals.Login_Screen;
import com.waypals.alarm.ParkingFenceAlarmActivity;
import com.waypals.analytics.ScoreActivity;
import com.waypals.exception.ExceptionHandler;
import com.waypals.R;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.Utility;

import java.util.Date;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService{
    public static final int NOTIFICATION_ID = 1;
    public static final String TAG = "GCM Demo";
    NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    String alertType;
    String message;
    String sender;
    String latitude;
    String longitude;
    ApplicationPreferences appPref;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        appPref = new ApplicationPreferences(this);

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (extras != null && !extras.isEmpty()) {  // has effect of unparcelling Bundle

            alertType = extras.getString("type");
            message = extras.getString("message");

            if (alertType!=null && alertType.equalsIgnoreCase("SHARE_LOCATION")) {
                sender = extras.getString("sender");
                latitude = extras.getString("latitude");
                longitude = extras.getString("longitude");
            }
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification();
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification();
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification();
                if(extras != null) {
                    Log.i(TAG, "Received: " + extras.toString());
                }
            }
            else
            {
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification();
                if(extras != null) {
                    Log.i(TAG, "Received: " + extras.toString());
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification() {

        try {
            Intent i = null;

            if (alertType != null && alertType.equalsIgnoreCase("SHARE_LOCATION")) {
                i = Utility.openGoogleMap(getApplicationContext(), Float.parseFloat(latitude), Float.parseFloat(longitude));
                message = Utility.getInitCaps(sender) + " has shared a location with you";
            } else if (alertType != null && ("SYSTEM_FENCE_VIOLATED".equalsIgnoreCase(alertType) || ("OUT_OF_BOUND_FENCE_VIOLATED".equalsIgnoreCase(alertType))
                    || ("GEO_FENCE_VIOLATED".equalsIgnoreCase(alertType)) || ("SPEED_LIMIT_VIOLATED".equalsIgnoreCase(alertType))
                    || Utility.PushMessageType.BATTERY_DISCHARGED.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.BATTERY_CHARGING_FAULT.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.VEHICLE_POLLUTION_ALERT.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.VEHICLE_SERVICE_ALERT.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.VEHICLE_POLICY_ALERT.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.INITIATE_CHANGE_PASSWORD.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.DEVICE_UNPLUGED.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.DEVICE_CRASHED.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.IN_NETWORK.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.TOWED.toString().equalsIgnoreCase(alertType)
                    || Utility.PushMessageType.FUEL_LIMIT_VIOLATED.toString().equalsIgnoreCase(alertType))) {
                if (alertType != null) {
                    i = new Intent(getApplicationContext(), Alerts_Screen_New.class);
                    //        i.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Log.d("Alert Type:", alertType);
                }
            } else if (alertType != null && Utility.PushMessageType.FOLLOW_ME.toString().equalsIgnoreCase(alertType)) {
                i = new Intent(getApplicationContext(), FollowMe_Screen.class);
                i.putExtra("follow_me_view", true);
                //     i.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                Log.d("Follow Me Alert", alertType);
            } else if (alertType != null && Utility.PushMessageType.FRIEND_REQUEST.toString().equalsIgnoreCase(alertType)) {
                i = new Intent(getApplicationContext(), Friends_Screen_New.class);
                i.putExtra("friend_request_notification", true);
                //       i.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                Log.d("Follow Me Alert", alertType);
            }else if (alertType != null && Utility.PushMessageType.TRIP_END.toString().equalsIgnoreCase(alertType)) {
                i = new Intent(getApplicationContext(), ScoreActivity.class);
                i.putExtra("trip_end", true);
                Log.d("Trip End Alert", alertType);
            }else if (alertType != null && Utility.PushMessageType.IMMOB_RES.toString().equalsIgnoreCase(alertType)) {
                i = new Intent(getApplicationContext(), Engine_Screen.class);
               // i.putExtra("trip_end", true);
                if(message != null){
                    if(message.contains(" ON ")){
                        appPref.setEngineState(true);
                    }else if(message.contains(" OFF ")){
                        appPref.setEngineState(false);
                    }

                    if(Engine_Screen.isVisible){
                        Intent gcm_rec = new Intent("engine_state_changed");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(gcm_rec);
                    }

                }
                Log.d("Vehicle Immobilise", alertType);
            }

            if (i != null) {

                Intent[] activities = new Intent[2];

                Intent intentLogin = new Intent(getApplicationContext(), Login_Screen.class);
                intentLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activities[0] = intentLogin;
                activities[1] = i;

                PendingIntent viewPendingIntent = PendingIntent.getActivities(getApplicationContext(), 0, activities, PendingIntent.FLAG_UPDATE_CURRENT);
                builder =
                        new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.push_alert_icon)
                                .setContentTitle(Utility.getPushAlertName(alertType))
                                .setContentText(message)
                                .setTicker(message)
                                .setAutoCancel(true)
                                .setDefaults(NotificationCompat.DEFAULT_ALL)
                                .setContentIntent(viewPendingIntent);

                NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(getBaseContext());
                mNotificationManager.notify((int) (new Date().getTime()), builder.build());
                playAlarmAudio(alertType);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            ExceptionHandler.handleException(e);
        }
    }

    private void playAlarmAudio(String alertType){
        if ("SYSTEM_FENCE_VIOLATED".equalsIgnoreCase(alertType) || Utility.PushMessageType.TOWED.toString().equalsIgnoreCase(alertType)){
            try {
                if (appPref != null && appPref.isInBetweenAlarmTime()) {
                    Intent intent = new Intent(getApplicationContext(), ParkingFenceAlarmActivity.class);
                    intent.putExtra("type", 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }catch (Exception ex){
                ExceptionHandler.handlerException(ex, null);
            }
        }

        if (Utility.PushMessageType.DEVICE_UNPLUGED.toString().equalsIgnoreCase(alertType)){
            try {
                    Intent intent = new Intent(getApplicationContext(), ParkingFenceAlarmActivity.class);
                    intent.putExtra("type", 1);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

            }catch (Exception ex){
                ExceptionHandler.handlerException(ex, null);
            }
        }
    }

}
