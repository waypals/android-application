package com.waypals.response;


import com.waypals.documentcase.Document;
import com.waypals.objects.CountryDataWrapper;
import com.waypals.objects.GreenDriveDetail;
import com.waypals.objects.IncidentAlerts;
import com.waypals.objects.MyShareLocationResponse;
import com.waypals.objects.ObdEndOfTrip;
import com.waypals.objects.UserGps;
import com.waypals.objects.VehicleAlerts;
import com.waypals.objects.VehicleTripOverview;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ServiceResponse implements Serializable {
    String response;
    String errorCode;

    String code;
    boolean isValid;
    String[] NotAUser = null;
    String friendRequestStatus = null;
    List<FollowmeUserWrapper> listFollowMeUserWrapper = null;
    FollowMeGetVehicleResponse gps = null;
    HashMap<String, UserGps> gpsData = null;
    List<FriendInfo> acceptedFriendsResult = null;
    List<FriendInfo> pendingFriendsResult = null;
    List<FriendInfo> friendsListRequestSentByMeResult = null;
    List<MyShareLocationResponse> myrequests = null;
    List<VehicleAlerts> alertsResponseList = null;
    Collection<IncidentAlerts> incidentAlert = null;
    ScoreResponse score;

    public List<CountryDataWrapper> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryDataWrapper> countries) {
        this.countries = countries;
    }

    public List<String> getState() {
        return state;
    }

    public void setState(List<String> state) {
        this.state = state;
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    List<CountryDataWrapper> countries;
    List<String> state;
    List<String> city;

    public List<Product> getProductWrappers() {
        return productWrappers;
    }

    public void setProductWrappers(List<Product> productWrappers) {
        this.productWrappers = productWrappers;
    }

    List<Product> productWrappers;

    List<String> vehicleType;
    List<String> manufactuerurs;
    List<String> models;
    String refId;

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    float discount;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    List<ObdEndOfTrip> trips;

    List<VehicleTripOverview> tripOverviews;

    public List<VehicleTripOverview> getTripOverviews() {
        return tripOverviews;
    }

    public void setTripOverviews(List<VehicleTripOverview> tripOverviews) {
        this.tripOverviews = tripOverviews;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    String userName;

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    List<Document> documents;

    String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<String> getManufactuerurs() {
        return manufactuerurs;
    }

    public void setManufactuerurs(List<String> manufactuerurs) {
        this.manufactuerurs = manufactuerurs;
    }

    public List<String> getModels() {
        return models;
    }

    public void setModels(List<String> models) {
        this.models = models;
    }

    public List<String> getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(List<String> vehicleType) {
        this.vehicleType = vehicleType;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    public Collection<IncidentAlerts> getIncidentAlert() {
        return incidentAlert;
    }

    public void setIncidentAlert(Collection<IncidentAlerts> incidentAlert) {
        this.incidentAlert = incidentAlert;
    }

    public List<VehicleAlerts> getAlertsResponseList() {
        return alertsResponseList;
    }

    public void setAlertsResponseList(List<VehicleAlerts> alertsResponseList) {
        this.alertsResponseList = alertsResponseList;
    }

    public List<MyShareLocationResponse> getMyrequests() {
        return myrequests;
    }

    public void setMyrequests(List<MyShareLocationResponse> myrequests) {
        this.myrequests = myrequests;
    }

    public List<FriendInfo> getFriendsListRequestSentByMeResult() {
        return friendsListRequestSentByMeResult;
    }

    public void setFriendsListRequestSentByMeResult(List<FriendInfo> friendsListRequestSentByMeResult) {
        this.friendsListRequestSentByMeResult = friendsListRequestSentByMeResult;
    }

    public List<FriendInfo> getPendingFriendsResult() {
        return pendingFriendsResult;
    }

    public void setPendingFriendsResult(List<FriendInfo> pendingFriendsResult) {
        this.pendingFriendsResult = pendingFriendsResult;
    }

    public String getFriendRequestStatus() {
        return friendRequestStatus;
    }

    public void setFriendRequestStatus(String friendRequestStatus) {
        this.friendRequestStatus = friendRequestStatus;
    }

    public List<FriendInfo> getAcceptedFriendsResult() {
        return acceptedFriendsResult;
    }

    public void setAcceptedFriendsResult(List<FriendInfo> acceptedFriendsResult) {
        this.acceptedFriendsResult = acceptedFriendsResult;
    }

    public HashMap<String, UserGps> getGpsData() {
        return gpsData;
    }

    public void setGpsData(HashMap<String, UserGps> gpsData) {
        this.gpsData = gpsData;
    }


    public List<FollowmeUserWrapper> getListFollowMeUserWrapper() {
        return listFollowMeUserWrapper;
    }

    public void setListFollowMeUserWrapper(List<FollowmeUserWrapper> listFollowMeUserWrapper) {
        this.listFollowMeUserWrapper = listFollowMeUserWrapper;
    }

    public List<ObdEndOfTrip> getTrips() {
        return trips;
    }

    public void setTrips(List<ObdEndOfTrip> trips) {
        this.trips = trips;
    }

    public String[] getNotAUser() {
        return NotAUser;
    }

    public void setNotAUser(String[] notAUser) {
        NotAUser = notAUser;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public FollowMeGetVehicleResponse getGps() {
        return gps;
    }

    public void setGps(FollowMeGetVehicleResponse gps) {
        this.gps = gps;
    }

    @Override
    public String toString() {
        return "ServiceResponse{" +
                "response='" + response + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", NotAUser=" + Arrays.toString(NotAUser) +
                ", friendRequestStatus='" + friendRequestStatus + '\'' +
                ", listFollowMeUserWrapper=" + listFollowMeUserWrapper +
                ", gps=" + gps +
                ", gpsData=" + gpsData +
                ", acceptedFriendsResult=" + acceptedFriendsResult +
                ", pendingFriendsResult=" + pendingFriendsResult +
                ", friendsListRequestSentByMeResult=" + friendsListRequestSentByMeResult +
                ", myrequests=" + myrequests +
                ", alertsResponseList=" + alertsResponseList +
                ", incidentAlert=" + incidentAlert +
                '}';
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ScoreResponse getScore() {
        return score;
    }

    public void setScore(ScoreResponse score) {
        this.score = score;
    }
}
