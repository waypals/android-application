package com.waypals.response;

/**
 * Created by surya on 5/1/16.
 */
public class OrderIdResponse {

    String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}
