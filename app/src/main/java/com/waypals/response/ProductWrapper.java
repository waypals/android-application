package com.waypals.response;

import java.io.Serializable;

/**
 * Created by surya on 21/10/16.
 */
public class ProductWrapper implements Serializable {


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    Product product;

    public int getStockQty() {
        return stockQty;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;
    }

    public boolean isValid(){
        return (itemsInCart < stockQty) ? true: false;
    }

    public void addToCart(){
        itemsInCart++;
    }

    int stockQty;
    int itemsInCart;


    public void setItemsInCart(int itemsInCart){
        this.itemsInCart = itemsInCart;
    }

    public int getItemInCart() {
        return itemsInCart;
    }
}
