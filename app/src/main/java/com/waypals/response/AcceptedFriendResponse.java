package com.waypals.response;

import com.waypals.feedItems.Friend;

import java.util.Arrays;

public class AcceptedFriendResponse {

    String response;

    Friend[] acceptedFriendsResult;

    String errorCode;
    Friend[] friend;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Friend[] getAcceptedFriendsResult() {
        return acceptedFriendsResult;
    }

    public void setAcceptedFriendsResult(Friend[] acceptedFriendsResult) {
        this.acceptedFriendsResult = acceptedFriendsResult;
    }

    public Friend[] getFriend() {
        return friend;
    }

    public void setFriend(Friend[] friend) {
        this.friend = friend;
    }

    @Override
    public String toString() {
        return "AcceptedFriendResponse{" +
                "response='" + response + '\'' +
                ", acceptedFriendsResult=" + Arrays.toString(acceptedFriendsResult) +
                ", friend=" + Arrays.toString(friend) +
                '}';
    }
}
