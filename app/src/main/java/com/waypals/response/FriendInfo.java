package com.waypals.response;

import java.io.Serializable;

/**
 * Created by shantanu on 27/1/15.
 */
public class FriendInfo implements Serializable {

    private String firstName;
    private String lastName;
    private String email;
    private Long userId;
    private Long friendConsumerId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFriendConsumerId() {
        return friendConsumerId;
    }

    public void setFriendConsumerId(Long friendConsumerId) {
        this.friendConsumerId = friendConsumerId;
    }
}
