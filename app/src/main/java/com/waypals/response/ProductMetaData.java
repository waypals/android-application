package com.waypals.response;

import com.waypals.feedItems.Images;

import java.io.Serializable;
import java.util.List;

/**
 * Created by surya on 21/10/16.
 */
public class ProductMetaData implements Serializable{

        boolean immobilizer;
        float price;
        String description;
        String name;
        List<Images> images;
        String companyName;
        String productType;

        public boolean isImmobilizer() {
                return immobilizer;
        }

        public void setImmobilizer(boolean immobilizer) {
                this.immobilizer = immobilizer;
        }

        public float getPrice() {
                return price;
        }

        public void setPrice(float price) {
                this.price = price;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public List<Images> getImages() {
                return images;
        }

        public void setImages(List<Images> images) {
                this.images = images;
        }

        public String getCompanyName() {
                return companyName;
        }

        public void setCompanyName(String companyName) {
                this.companyName = companyName;
        }

        public String getProductType() {
                return productType;
        }

        public void setProductType(String productType) {
                this.productType = productType;
        }
}
