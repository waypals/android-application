package com.waypals.response;

import com.waypals.feedItems.FeedWrapper;


public class FeedContentResponse {
    String response;
    FeedWrapper[] feedResponseList;
    String error;
    String errorCode;
    int unreadFeedCount;
    int totalNumberOfRecords;


    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public int getUnreadFeedCount() {
        return unreadFeedCount;
    }

    public void setUnreadFeedCount(int unreadFeedCount) {
        this.unreadFeedCount = unreadFeedCount;
    }

    public int getTotalNumberOfRecords() {
        return totalNumberOfRecords;
    }

    public void setTotalNumberOfRecords(int totalNumberOfRecords) {
        this.totalNumberOfRecords = totalNumberOfRecords;
    }

    public FeedWrapper[] getFeedResponseList() {
        return feedResponseList;
    }

    public void setFeedResponseList(FeedWrapper[] feedResponseList) {
        this.feedResponseList = feedResponseList;
    }

//
//    @Override
//    public String toString() {
//        StringBuilder builder = new StringBuilder();
//        if(feedResponseList!=null){
//            builder.append("Response : " + response + "Feed list : " + feedResponseList.toString());
//        }else{
//            builder.append("Response : " + response );
//        }
//        return builder.toString();
//    }
}
