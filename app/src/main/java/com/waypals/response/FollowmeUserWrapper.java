package com.waypals.response;

import com.waypals.feedItems.Name;
import com.waypals.request.TimeRange;

import java.io.Serializable;

/**
 * Created by shantanu on 14/1/15.
 */
public class FollowmeUserWrapper implements Serializable {
    private Long userId;
    private Name followerUserName;
    private String token;
    private TimeRange range;
    private String followingVehicle;

    public TimeRange getRange() {
        return range;
    }

    public void setRange(TimeRange range) {
        this.range = range;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Name getFollowerUserName() {
        return followerUserName;
    }

    public void setFollowerUserName(Name followerUserName) {
        this.followerUserName = followerUserName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFollowingVehicle() {
        return followingVehicle;
    }

    public void setFollowingVehicle(String followingVehicle) {
        this.followingVehicle = followingVehicle;
    }
}
