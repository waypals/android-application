package com.waypals.response;

import com.waypals.objects.GreenDriveDetail;

import java.io.Serializable;

/**
 * Created by surya on 14/02/2017.
 */
public class ScoreResponse implements Serializable {

    String id;
    int score;

    GreenDriveDetail greenDriveDetail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public GreenDriveDetail getGreenDriveDetail() {
        return greenDriveDetail;
    }

    public void setGreenDriveDetail(GreenDriveDetail greenDriveDetail) {
        this.greenDriveDetail = greenDriveDetail;
    }
}
