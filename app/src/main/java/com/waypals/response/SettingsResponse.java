package com.waypals.response;

import com.waypals.request.Notification;
import com.waypals.request.SpeedLimit;

import java.io.Serializable;

public class SettingsResponse implements Serializable {

    private String response;
    private SpeedLimit speedLimit;
    private Boolean parkingFenceEnabled;
    private Notification notificationType;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    String errorCode;



    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public SpeedLimit getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(SpeedLimit speedLimit) {
        this.speedLimit = speedLimit;
    }

    public Boolean getParkingFenceEnabled() {
        return parkingFenceEnabled;
    }

    public void setParkingFenceEnabled(Boolean parkingFenceEnabled) {
        this.parkingFenceEnabled = parkingFenceEnabled;
    }

    public Notification getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Notification notificationType) {
        this.notificationType = notificationType;
    }


}
