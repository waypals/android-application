package com.waypals.response;

import com.waypals.objects.MotionState;
import com.waypals.objects.Obd;

import java.io.Serializable;

/**
 * Created by shantanu on 2/2/15.
 */
public class ObdResponse implements Serializable {

    private String response;
    private String errorCode;
    private Obd obd;
    //private ObdEndOfTrip obdEot;
    private MotionState state;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Obd getObd() {
        return obd;
    }

    public void setObd(Obd obd) {
        this.obd = obd;
    }

//    public ObdEndOfTrip getObdEot() {
//        return obdEot;
//    }
//
//    public void setObdEot(ObdEndOfTrip obdEot) {
//        this.obdEot = obdEot;
//    }

    public MotionState getState() {
        return state;
    }

    public void setState(MotionState state) {
        this.state = state;
    }
}
