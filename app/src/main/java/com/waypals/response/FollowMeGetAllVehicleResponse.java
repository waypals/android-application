package com.waypals.response;

import com.waypals.objects.Gps;

import java.util.HashMap;

/**
 * Created by shantanu on 23/1/15.
 */
public class FollowMeGetAllVehicleResponse {

    private HashMap<String, Gps> gpsData;

    public HashMap<String, Gps> getGpsData() {
        return gpsData;
    }

    public void setGpsData(HashMap<String, Gps> gpsData) {
        this.gpsData = gpsData;
    }
}
