package com.waypals.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by surya on 21/10/16.
 */
public class Product implements Serializable{

    long id;
    String productCode;
    boolean enable;
    ProductMetaData productMetaData;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public ProductMetaData getProductMetaData() {
        return productMetaData;
    }

    public void setProductMetaData(ProductMetaData productMetaData) {
        this.productMetaData = productMetaData;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

}
