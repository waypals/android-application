package com.waypals.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by surya on 21/10/16.
 */
public class ProductResponse implements Serializable {

    String response;
    List<ProductWrapper> productWrappers;

    public List<ProductWrapper> getProductWrappers() {
        return productWrappers;
    }

    public void setProductWrappers(List<ProductWrapper> productWrappers) {
        this.productWrappers = productWrappers;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
