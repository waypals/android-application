package com.waypals.response;

import com.waypals.objects.GeoLocation;

/**
 * Created by shantanu on 23/1/15.
 */
public class FollowMeGetVehicleResponse {

    private GeoLocation geoLocation;

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

}
