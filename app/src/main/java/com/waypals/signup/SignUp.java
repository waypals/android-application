package com.waypals.signup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.Timer;
import java.util.TimerTask;

public class SignUp extends Activity implements AsyncFinishInterface{

    private static boolean isTimerRunning = false;
    private static int elapsedTime = 60;
    private EditText txtDeviceId;
    private EditText txtEmail;
    private EditText textCode;
    private ApplicationPreferences appPref;
    private ServiceResponse serviceResponse;
    private String deviceId;
    private String email;
    private String method;
    private String code;
    private TextView btnSendCode;

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            btnSendCode.setText("Resend in " + msg.what + " s"); //
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        appPref = new ApplicationPreferences(this);

        txtDeviceId = (EditText) findViewById(R.id.device_id);

        txtEmail = (EditText) findViewById(R.id.text_email);

        textCode = (EditText) findViewById(R.id.text_send_code);

        TextView button = (TextView) findViewById(R.id.button_next);

        btnSendCode = (TextView) findViewById(R.id.button_send_code);

        btnSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isTimerRunning) {
                    if (isValidEmailId()) {
                        sendVerificationCode(txtEmail.getText().toString());
                    }
                }else{
                    Utility.makeNewToast(SignUp.this, "Please check your email or send code again after some time");
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deviceId = txtDeviceId.getText().toString();
                validateDeviceId(deviceId);
              //    showNextPage(deviceId);
            }
        });

    }

    protected void startTimer() {
        isTimerRunning = true;
        //btnSendCode.setEnabled(false);
        //TaskScheduler timer = new TaskScheduler();

        final Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                elapsedTime -= 1; //decreases every sec
                mHandler.obtainMessage(elapsedTime).sendToTarget();
                if (elapsedTime == 0){
                    timer.cancel();
                    isTimerRunning = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //btnSendCode.setEnabled(true);
                            btnSendCode.setText("Send Code");
                        }
                    });
                    elapsedTime = 60;

                }
            }
        }, 0, 1000);
    }


    private void sendVerificationCode(String email){

        String input = "{\"email\":"+email+"}";

        GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.VERIFY_EMAIL, "VERIFY_EMAIL",input,"", this, true);
        service.execute();

    }

    private boolean isValidEmailId(){

        email = txtEmail.getText().toString();
        if (Utility.isStringNullEmpty(email) || !Utility.isValidEmailId(email)) {
            Utility.makeNewToast(this, "Please enter valid email");
            return false;
        }

        return true;
    }

    private void validateDeviceId(String deviceId)
    {
        try {

            if(Utility.isStringNullEmpty(deviceId) || !Utility.isNumber(deviceId))
            {
                Utility.makeNewToast(this, "Please enter valid device number");
                return;
            }

            if(!isValidEmailId()) {
                return;
            }

            if(Utility.isStringNullEmpty(textCode.getText().toString())){
                Utility.makeNewToast(this, "Please enter verification code/check your mailbox");
                return;
            }

            if(email != null && email.equals(txtEmail.getText().toString())){

                if( code != null && code.equals(textCode.getText().toString())){
                    GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.IS_VALID_DEVICE_ID + "/" + deviceId, "VALID_DEVICE_ID", "", "", this, true);
                    service.execute();
                }else{
                    Utility.makeNewToast(this, "Please enter correct verification code/check your mailbox");
                    return;
                }

            }else{
                Utility.makeNewToast(this, "Please enter correct verification code/verify email id.");
                return;
            }




        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void showNextPage(String deviceId)
    {
        Intent page2 = new Intent(this, SignUpPage2.class);
        page2.putExtra(Constants.DEVICE_ID, deviceId);
        page2.putExtra("emailId",txtEmail.getText().toString());
        startActivity(page2);
    }

    @Override
    public void finish(String response, String method) throws Exception {
        try {
            if (response != null) {
               // Log.d("Device", response);
                Gson gson = new Gson();
                this.method = method;
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }

        } catch (Exception e) {
            ExceptionHandler.handlerException(e, this);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("email", email);
        outState.putString("code", code);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        email = savedInstanceState.getString("email");
        code = savedInstanceState.getString("code");
    }

    @Override
    public void onPostExecute(Void Result) {
        if(serviceResponse!=null)
        {
            if("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse()))
            {
                if("VERIFY_EMAIL".equals(method))
                {
                    email = txtEmail.getText().toString();
                    code = serviceResponse.getCode();
                    Utility.makeNewToast(this, "Please check your mail for verification code");
                    startTimer();
                }else {
                    if (serviceResponse.isValid()) {
                        showNextPage(deviceId);
                    } else {
                        Utility.makeNewToast(this, "This '" + deviceId + "' Id doesn't exists in system, please enter correct device Id");
                    }
                }
            }
            else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();

                if("INVAILD_EMAILID".equals(errorMessage))
                {
                    Utility.makeNewToast(this, "This email id is invalid/already registered");
                }else{
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }
            }
        }else
        {
            Utility.makeNewToast(this, getString(R.string.server_error));
        }
    }
}
