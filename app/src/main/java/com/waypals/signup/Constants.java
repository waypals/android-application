package com.waypals.signup;

/**
 * Created by surya on 2/11/15.
 */
public class Constants {

    public static final String DEVICE_ID = "deviceId";

    public static final String NAME = "name";

    public static final String EMAIL_ID = "emailId";

    public static final String GENDER = "gender";

    public static final String DOB = "dob";

    public static final String CODE = "code";

    public static final String NUMBER = "number";

    public static final String COUNTRY = "country";
    public static final String STATE = "state";
    public static final String CITY = "city";
    public static final String ADDRESS_LINE = "address_line";
}
