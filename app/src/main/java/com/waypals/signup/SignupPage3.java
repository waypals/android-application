package com.waypals.signup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.Login_Screen;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.objects.Address;
import com.waypals.objects.UserRegistration;
import com.waypals.objects.VehicleData;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.DateTimeClickListener;
import com.waypals.utils.Utility;

import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SignupPage3 extends Activity implements AsyncFinishInterface{

    private static final String VALIDATE_REGISTRATION_NUM = "VALID_REGISTRATION";
    private static final String REGISTER = "REGISTER";
    private final String SELECT_VEHICLE_TYPE = "Vehicle Type";
    private final String SELECT_MANUFACTURER = "Manufacturer";
    private final String SELECT_MODEL = "Model";

    private ApplicationPreferences appPref;

    private ServiceResponse serviceResponse;
    private String method;


    private TextView textVehicle, textManuf;

    private SpinnerAdapter<String> adpVehicleType, adpManufType, adpModelType;

    private Spinner spinnerVehicleType, spinnerManufType, spinnerModelType;
    private RelativeLayout vehicleTypeLyt, manufTypeLyt, modelTypeLty;
    private EditText textRegNum, txtPolicyNum, txtPolicyStartDate, txtPolicyEndDate, policyName;
    private TextView txtBack, txtNext, textModel;

    private UserRegistration userRegistration;
    private String startDate;
    private Date dateStart;
    private Date dateEnd;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_page3);

        appPref = new ApplicationPreferences(this);


        vehicleTypeLyt = (RelativeLayout) findViewById(R.id.spinner_vehicle);
        manufTypeLyt = (RelativeLayout) findViewById(R.id.spinner_manufacturer);
        modelTypeLty = (RelativeLayout) findViewById(R.id.spinner_model);

        spinnerVehicleType = (Spinner) vehicleTypeLyt.findViewById(R.id.spinner_drop_down);
        spinnerManufType = (Spinner) manufTypeLyt.findViewById(R.id.spinner_drop_down);
        spinnerModelType = (Spinner) modelTypeLty.findViewById(R.id.spinner_drop_down);

        textVehicle = (TextView) vehicleTypeLyt.findViewById(R.id.text_spinner);
        textManuf = (TextView) manufTypeLyt.findViewById(R.id.text_spinner);
        textModel = (TextView) modelTypeLty.findViewById(R.id.text_spinner);

        spinnerVehicleType.setPrompt("Vehicle Type");
        spinnerManufType.setPrompt("Manufacturer");
        spinnerModelType.setPrompt("Model");

        textVehicle.setHint("Vehicle Type");
        textManuf.setHint("Manufacturer");
        textModel.setHint("Model");

        txtBack = (TextView) findViewById(R.id.btn_back);
        txtNext = (TextView) findViewById(R.id.btn_next);

        txtNext.setText("Register");

        policyName = (EditText) findViewById(R.id.text_policy_name);
        textRegNum = (EditText) findViewById(R.id.text_vehicle_reg_no);
        txtPolicyNum = (EditText) findViewById(R.id.text_policy_num);

        txtPolicyStartDate = (EditText) findViewById(R.id.text_policyStartDate);
        txtPolicyEndDate = (EditText) findViewById(R.id.text_policyEndDate);

        adpVehicleType = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, SELECT_VEHICLE_TYPE);
        adpModelType = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, SELECT_MODEL);
        adpManufType = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, SELECT_MANUFACTURER);

        spinnerVehicleType.setAdapter(adpVehicleType);
        spinnerManufType.setAdapter(adpManufType);
        spinnerModelType.setAdapter(adpModelType);

        initEvents();

        setDefaults();
        getVehicleType();

        userRegistration = new UserRegistration();

        Intent lastIntent = getIntent();

        deviceId = lastIntent.getStringExtra(Constants.DEVICE_ID);
        userRegistration.setUserName(lastIntent.getStringExtra(Constants.NAME));
        userRegistration.setEmail(lastIntent.getStringExtra(Constants.EMAIL_ID));
        userRegistration.setCounryCode(lastIntent.getStringExtra(Constants.CODE));
        userRegistration.setPhoneNumber(lastIntent.getStringExtra(Constants.NUMBER));
        userRegistration.setGender(lastIntent.getStringExtra(Constants.GENDER));
        userRegistration.setDateOfBirth(Utility.convertToDateTime(lastIntent.getStringExtra(Constants.DOB)));
        //userRegistration.setDateOfBirth(Utility.formatDate(lastIntent.getStringExtra(Constants.DOB)));

        Address address = new Address();

        address.setAddressLine(lastIntent.getStringExtra(Constants.ADDRESS_LINE));
        address.setCountry(lastIntent.getStringExtra(Constants.COUNTRY));
        address.setProvince(lastIntent.getStringExtra(Constants.STATE));
        address.setCity(lastIntent.getStringExtra(Constants.CITY));

        userRegistration.setAddress(address);

        System.out.println();
    }

    private void initEvents()
    {
        textVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerVehicleType.performClick();
            }
        });

        textManuf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerManufType.performClick();
            }
        });

        textModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerModelType.performClick();
            }
        });

        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidData())
                {
                    validateRegistrationNumber(textRegNum.getText().toString());
                }
            }
        });

        txtPolicyStartDate.setOnClickListener(new DateTimeClickListener(txtPolicyStartDate, this, new DateTimeClickListener.DateSelectedCallback() {
            @Override
            public void onSet(Date date) {
                    dateStart = date;

                    if( !isBeforeDate(dateStart, dateEnd))
                    {
                        Utility.makeNewToast(SignupPage3.this, "Start Date must be prior to end date");
                    }
            }
        }));
        txtPolicyEndDate.setOnClickListener(new DateTimeClickListener(txtPolicyEndDate, this, new DateTimeClickListener.DateSelectedCallback() {
            @Override
            public void onSet(Date date) {
                    dateEnd = date;

                     if( !isBeforeDate(dateStart, dateEnd))
                    {
                     Utility.makeNewToast(SignupPage3.this, "End date must be after the start date");
                    }
            }
        }));
    }

    private boolean isBeforeDate(Date first, Date second)
    {
        if(first != null)
        {
            if(second != null)
            {
                Calendar cal1 = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal1.setTime(first);
                cal2.setTime(second);
                boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
                return first.before(second) && !sameDay;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if(second != null)
            {
                return true;
            }
        }
        return false;
    }


    private VehicleData getVehicleData()
    {
        VehicleData vehicleData = new VehicleData();

        vehicleData.setPolicyName(policyName.getText().toString());
        vehicleData.setDeviceId(deviceId);
        vehicleData.setRegNo(textRegNum.getText().toString());
        vehicleData.setManufacturer(textManuf.getText().toString());
        vehicleData.setVehicleType(textVehicle.getText().toString());
        vehicleData.setModelName(textModel.getText().toString());
        vehicleData.setPolicyNo(txtPolicyNum.getText().toString());

        if(txtPolicyStartDate.getText() != null) {
            if(Utility.formatDate(txtPolicyStartDate.getText().toString()) != null) {
                vehicleData.setPolicyStartDate(Utility.formatDate(txtPolicyStartDate.getText().toString()));
            }
        }

        if(txtPolicyEndDate.getText() != null) {
            if(Utility.formatDate(txtPolicyEndDate.getText().toString()) != null) {
                vehicleData.setPolicyEndDate(Utility.formatDate(txtPolicyEndDate.getText().toString()));
            }
        }

        return vehicleData;
    }

    private boolean isValidData()
    {
        boolean isValid = false;
        String regNum = textRegNum.getText().toString();

        if(Utility.isStringNullEmpty(regNum))
        {
            Utility.makeNewToast(this, "Please enter registration number");
            return isValid;
        }

        String policyNum = txtPolicyNum.getText().toString();

        if(Utility.isStringNullEmpty(policyNum))
        {
            Utility.makeNewToast(this, "Please enter policy number");
            return isValid;
        }

        String policyN = policyName.getText().toString();

        if(Utility.isStringNullEmpty(policyN))
        {
            Utility.makeNewToast(this, "Please enter company insurance name");
            return isValid;
        }


        String vehicleType = textVehicle.getText().toString();

        if(Utility.isStringNullEmpty(vehicleType))
        {
            Utility.makeNewToast(this, "Please select vehicle type");
            return isValid;
        }

        String manufacturer = textManuf.getText().toString();

        if(Utility.isStringNullEmpty(manufacturer))
        {
            Utility.makeNewToast(this, "Please select manufacturer");
            return isValid;
        }

        String model = textModel.getText().toString();
        if(Utility.isStringNullEmpty(model))
        {
            Utility.makeNewToast(this, "Please select model");
            return isValid;
        }

        String policyStart = txtPolicyStartDate.getText().toString();
        if(Utility.isStringNullEmpty(policyStart))
        {
            Utility.makeNewToast(this, "Please select policy start date");
            return isValid;
        }

        String policyEnd = txtPolicyEndDate.getText().toString();
        if(Utility.isStringNullEmpty(policyEnd))
        {
            Utility.makeNewToast(this, "Please select policy end date");
            return isValid;
        }

        if(!isBeforeDate(dateStart, dateEnd))
        {
            return isValid;
        }

        return true;
    }

    private void registerUser(UserRegistration userRegistration) {
        try {
            if(userRegistration != null) {

                Gson gson = new Gson();
                String s = gson.toJson(userRegistration);

                Log.d("UserRequest", s);

                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.REGISTER_USER, REGISTER, s, "", this, true);
                service.execute();
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }

    }

    private void validateRegistrationNumber(String regNum)
    {
        try {
            GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.IS_VALID_REGISTRATION_NUM + "/" + URLEncoder.encode(regNum, "UTF-8"), VALIDATE_REGISTRATION_NUM, "", "", this, true);

            service.execute();
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void setDefaults()
    {
        clearVehicleType();
        clearManufacturers();
        clearModels();
    }

    private void clearVehicleType() {
        List<String> vehicleType = new ArrayList<>();
        adpVehicleType.clear();
        textVehicle.setText("");
        adpVehicleType.notifyDataSetChanged();
    }

    private void clearManufacturers() {

        List<String> vehicleType = new ArrayList<>();
        adpManufType.clear();
        textManuf.setText("");
        adpManufType.notifyDataSetChanged();
    }

    private void getManufacturersByVehicle(String vehicleType)
    {
        try {
            clearModels();
            clearManufacturers();

            if(validateData()) {
                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_VEHICLE_MANU_FROM_TYPE+"/"+URLEncoder.encode(vehicleType, "UTF-8"), SELECT_MANUFACTURER, "", "", this, true);
                service.execute();
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void getModelByManufacturers(String manufacturers)
    {
        try {
            if(validateData()) {
                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_VEHICLE_MODELS_FROM_MANU+"/"+URLEncoder.encode(manufacturers, "UTF-8"), SELECT_MODEL, "", "", this, true);
                service.execute();
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void getVehicleType()
    {
        try {
            if(validateData()) {
                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_VEHICLE_TYPE, SELECT_VEHICLE_TYPE, "", "", this, true);
                service.execute();
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private boolean validateData()
    {
        return true;
    }

    @Override
    public void finish(String response, String method) throws Exception {

        this.method = method;
        serviceResponse = null;

        Log.d("UserResponse", response);

        try {
            if (response != null)
            {
                Gson gson = new Gson();
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }

    }

    @Override
    public void onPostExecute(Void Result) {

        if(serviceResponse != null)
        {
            if("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse()))
            {
                if(SELECT_VEHICLE_TYPE.equals(method))
                {
                    List<String> vehicleType = serviceResponse.getVehicleType();
                    adpVehicleType.clear();
                    if(vehicleType != null) {
                        for (String type : vehicleType) {
                            adpVehicleType.add(type);
                        }
                    }

                    adpVehicleType.notifyDataSetChanged();

                }
                else if(SELECT_MANUFACTURER.equals(method))
                {
                    List<String> vehicleType = serviceResponse.getManufactuerurs();
                    adpManufType.clear();
                    if(vehicleType != null) {
                        for (String type : vehicleType) {
                            adpManufType.add(type);
                        }
                    }
                    adpManufType.notifyDataSetChanged();
                }
                else if(SELECT_MODEL.equals(method))
                {
                    List<String> vehicleType = serviceResponse.getModels();
                    adpModelType.clear();
                    if(vehicleType != null) {
                        for (String type : vehicleType) {
                            adpModelType.add(type);
                        }
                    }
                    adpModelType.notifyDataSetChanged();
                }else if(VALIDATE_REGISTRATION_NUM.equals(method))
                {
                    boolean isValid = serviceResponse.isValid();

                    if(isValid)
                    {
                        userRegistration.setVehicleData(getVehicleData());
                        registerUser(userRegistration);
                    }
                    else
                    {
                        Utility.makeNewToast(this, "Registration Number is already exists in the system, please enter another registration number");
                    }
                }
                else if(REGISTER.equals(method))
                {
                     if("SUCCESS".equals(serviceResponse.getResponse()))
                     {
                         performPostRegistration();
                     }
                }
            }
            else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                {
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }
            }
        }else
        {
            Utility.makeNewToast(this, getString(R.string.server_error));
        }

    }

    private void performPostRegistration() {

        Utility.makeNewToast(this, "You have been successfully registered! Please check your registered mail for username/password.");

        //Redirecting to Login Page
        Intent i = new Intent(this, Login_Screen.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);

    }

    private void clearModels()
    {
        textModel.setText("");
        adpModelType.clear();
        adpModelType.notifyDataSetChanged();
    }

    private class SpinnerAdapter<T> extends ArrayAdapter<T>
    {
        private Activity activity;
        private String type;

        public SpinnerAdapter(Context context, int resource, String type) {
            super(context, resource);

            this.type = type;
            this.activity = (Activity) context;
        }


        @Override
        public View getDropDownView(final int position, View convertView, final ViewGroup parent) {

            if(convertView == null) {
                convertView = LayoutInflater.from(activity).inflate(R.layout.spinner_row_layout, null);
            }

                T item = getItem(position);
                Log.d("Item", item.toString());
                // convertView.setVisibility(View.GONE);

            final TextView textView = (TextView) convertView.findViewById(R.id.text_content);
            textView.setText(item.toString());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (SELECT_VEHICLE_TYPE.equals(type)) {
                        hideSpinnerDropDown(spinnerVehicleType);
                        clearManufacturers();
                        clearModels();
                        textVehicle.setText(textView.getText().toString());
                        getManufacturersByVehicle(textView.getText().toString());
                    }else if(SELECT_MANUFACTURER.equals(type))
                    {
                        hideSpinnerDropDown(spinnerManufType);
                        clearModels();
                        textManuf.setText(textView.getText().toString());
                        getModelByManufacturers(textView.getText().toString());
                    }else if(SELECT_MODEL.equals(type))
                    {
                        hideSpinnerDropDown(spinnerModelType);
                        textModel.setText(textView.getText().toString());
                    }

                }
            });

            return convertView;
        }
    }


    public static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
