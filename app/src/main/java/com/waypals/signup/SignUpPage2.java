package com.waypals.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.exception.ExceptionHandler;
import com.waypals.objects.CountryDataWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.DateTimeClickListener;
import com.waypals.utils.Utility;

import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SignUpPage2 extends Activity implements AsyncFinishInterface {

    private EditText txtFirstName, txtLastName, txtAddress;
    private EditText txtDob;
    private Spinner spinnerGender;
    private TextView txtCode;
    private EditText txtNumber;
    private ApplicationPreferences appPref;
    private ServiceResponse serviceResponse;
    private String email;

    private RelativeLayout lytGenderSpinner;
    private TextView textGender;

    private String deviceId;
    private com.waypals.adapters.SpinnerAdapter<String> spinnerGenderAdapter;
    private Date dateDOB;

    private RelativeLayout lytCode;
    private Spinner spinnerCode;

    private RelativeLayout lytCountry;
    private Spinner spinnerCountry;
    private TextView txtCountry;

    private RelativeLayout lytState;
    private Spinner spinnerState;
    private TextView txtState;

    private RelativeLayout lytCity;
    private Spinner spinnerCity;
    private TextView txtCity;
    private String SELECT_COUNTRY = "Select Country";
    private String SELECT_STATE = "Select State";
    private String SELECT_CITY = "Select City";
    private SpinnerAdapter<String> adpCountries, adpStates, adpCities;
    private String method;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_page2);

        appPref = new ApplicationPreferences(this);

        TextView btnBack = (TextView) findViewById(R.id.button_back);
        TextView btnNxt = (TextView) findViewById(R.id.button_next);

        txtFirstName = (EditText) findViewById(R.id.text_user_first_name);
        txtLastName = (EditText) findViewById(R.id.text_user_last_name);

        txtDob = (EditText) findViewById(R.id.text_dob);

        txtAddress = (EditText) findViewById(R.id.text_address);
        txtAddress.setHint("Street Address");
      //  txtDob.setEnabled(false);

        lytCode = (RelativeLayout) findViewById(R.id.custom_spinner);
        spinnerCode = (Spinner) lytCode.findViewById(R.id.spinner_drop_down);
        txtCode = (TextView) lytCode.findViewById(R.id.text_spinner);
        txtCode.setHint("Code");

        spinnerCode.setPrompt("Select Code");
        String[] codes = {"+91", "+1"};
        com.waypals.adapters.SpinnerAdapter<String> spinnerCodeAdapter = new com.waypals.adapters.SpinnerAdapter<String>(this, R.layout.spinner_text_layout, codes, txtCode, spinnerCode);

        spinnerCode.setAdapter(spinnerCodeAdapter);


        txtNumber = (EditText) findViewById(R.id.text_number);

        lytGenderSpinner = (RelativeLayout) findViewById(R.id.layout_gender_spinner);

        spinnerGender = (Spinner) lytGenderSpinner.findViewById(R.id.spinner_drop_down);
        spinnerGender.setPrompt("Select Gender");

        String[] gender = {"Male", "Female", "Other"};
        textGender = (TextView) lytGenderSpinner.findViewById(R.id.text_spinner);
        textGender.setHint("Select Gender");
        spinnerGenderAdapter = new com.waypals.adapters.SpinnerAdapter<String>(this, R.layout.spinner_text_layout, gender, textGender, spinnerGender);

        spinnerGender.setAdapter(spinnerGenderAdapter);


        lytCountry = (RelativeLayout) findViewById(R.id.drop_down_country);
        spinnerCountry = (Spinner) lytCountry.findViewById(R.id.spinner_drop_down);
        txtCountry = (TextView) lytCountry.findViewById(R.id.text_spinner);
        txtCountry.setHint("Country");
        spinnerCountry.setPrompt("Select Country");

        lytState = (RelativeLayout) findViewById(R.id.drop_down_state);
        spinnerState = (Spinner) lytState.findViewById(R.id.spinner_drop_down);
        txtState = (TextView) lytState.findViewById(R.id.text_spinner);
        txtState.setHint("State");
        spinnerState.setPrompt("Select State");

        lytCity = (RelativeLayout) findViewById(R.id.drop_down_city);
        spinnerCity = (Spinner) lytCity.findViewById(R.id.spinner_drop_down);
        txtCity = (TextView) lytCity.findViewById(R.id.text_spinner);
        txtCity.setHint("City");
        spinnerCity.setPrompt("Select City");

        adpCountries = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, SELECT_COUNTRY);
        adpStates = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, SELECT_STATE);
        adpCities = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, SELECT_CITY);

        spinnerCountry.setAdapter(adpCountries);
        spinnerState.setAdapter(adpStates);
        spinnerCity.setAdapter(adpCities);

        RelativeLayout lytDob = (RelativeLayout) findViewById(R.id.dob_layout);

        txtCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerCode.performClick();
            }
        });

        textGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinnerGender.performClick();
            }
        });

        txtCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinnerCountry.performClick();
            }
        });

        txtState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinnerState.performClick();
            }
        });

        txtCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinnerCity.performClick();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
           //     showSignUpPage3();
            }
        });

        Intent lastIntent = getIntent();
        deviceId = lastIntent.getStringExtra("deviceId");
        email = lastIntent.getStringExtra("emailId");
        ImageView imageView = (ImageView) findViewById(R.id.calendar_icon);

        txtDob.setEnabled(false);
        imageView.setOnClickListener(new DateTimeClickListener(txtDob, this, new DateTimeClickListener.DateSelectedCallback() {
            @Override
            public void onSet(Date date) {
                dateDOB = date;
            }
        }));

        getCountries();
    }

    private void validateData() {
        try {
            String fName = txtFirstName.getText().toString();
            String lName = txtLastName.getText().toString();
            String gender = spinnerGender.getSelectedItem().toString();
            String country = null;
            String state = null;
            String city = null;

            if(txtCountry.getText() != null){
                country = txtCountry.getText().toString();
            }

            if(txtState.getText() != null){
                 state = txtState.getText().toString();
            }

            if(txtCity.getText() != null) {
                city = txtCity.getText().toString();
            }

            String dob = txtDob.getText().toString();
            String code = txtCode.getText().toString();
            String number = txtNumber.getText().toString();

            if (Utility.isStringNullEmpty(fName)) {
                Utility.makeNewToast(this, "Please enter first name");
                return;
            }

            if (Utility.isStringNullEmpty(lName)) {
                Utility.makeNewToast(this, "Please enter last name");
                return;
            }

            if (Utility.isStringNullEmpty(dob)) {
                Utility.makeNewToast(this, "Please enter date of birth");
                return;
            }

            if (Utility.isStringNullEmpty(gender)) {
                Utility.makeNewToast(this, "Please select gender");
                return;
            }

            if (Utility.isStringNullEmpty(code)) {
                Utility.makeNewToast(this, "Please select code");
                return;
            }

            if (Utility.isStringNullEmpty(number) || !Utility.isValidPhone(number)) {
                Utility.makeNewToast(this, "Please enter valid number");
                return;
            }

            if (Utility.isStringNullEmpty(country)) {
                Utility.makeNewToast(this, "Please select country");
                return;
            }

            if (Utility.isStringNullEmpty(state)) {
                Utility.makeNewToast(this, "Please select state");
                return;
            }

            if (Utility.isStringNullEmpty(city)) {
                Utility.makeNewToast(this, "Please select city");
                return;
            }

            GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.IS_VALID_EMAIL_ID + "/" + email + "/", "VERIFY_EMAIL", "", "", this, true);
            service.execute();

        } catch (Exception ex) {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void getStatesForCountry(String country)
    {
        try {
            clearStates();
            clearCity();

            GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_STATES+"/"+ URLEncoder.encode(country, "UTF-8"), SELECT_STATE, "", "", this, true);
            service.execute();

        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void getCitiesForState(String state)
    {
        try {
            GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_CITIES+"/"+URLEncoder.encode(state, "UTF-8"), SELECT_CITY, "", "", this, true);
            service.execute();
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void getCountries()
    {
        try {
                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.GET_COUNTRIES, SELECT_COUNTRY, "", "", this, true);
                service.execute();

        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }

    private void showSignUpPage3() {
        Intent page3 = new Intent(this, SignupPage3.class);

        String name = txtFirstName.getText().toString() + " " + txtLastName.getText().toString();

        page3.putExtra(Constants.DEVICE_ID, deviceId);
        page3.putExtra(Constants.NAME, name);
        page3.putExtra(Constants.EMAIL_ID, email);
        page3.putExtra(Constants.DOB, txtDob.getText().toString());
        page3.putExtra(Constants.GENDER, spinnerGender.getSelectedItem().toString());
        page3.putExtra(Constants.COUNTRY, txtCountry.getText().toString());
        page3.putExtra(Constants.STATE, txtState.getText().toString());
        page3.putExtra(Constants.CITY, txtCity.getText().toString());
        page3.putExtra(Constants.ADDRESS_LINE, txtAddress.getText().toString());
        page3.putExtra(Constants.CODE, txtCode.getText().toString());
        page3.putExtra(Constants.NUMBER, txtNumber.getText().toString());

        startActivity(page3);
    }

    @Override
    public void finish(String response, String method) throws Exception {
        try {
            if (response != null) {
                Log.d("Device", response);
                this.method = method;
                Gson gson = new Gson();
                serviceResponse = gson.fromJson(response, ServiceResponse.class);
            }

        } catch (Exception e) {
            ExceptionHandler.handlerException(e, this);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        if (serviceResponse != null) {
            if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {

                if(SELECT_COUNTRY.equals(method))
                {
                    List<CountryDataWrapper> countries = serviceResponse.getCountries();
                    adpCountries.clear();

                    List<String> data = new ArrayList<>();

                    if(countries != null) {
                        for (CountryDataWrapper type : countries) {
                            data.add(type.getCountry());
                        }

                        Collections.sort(data, Collator.getInstance());
                    }

                    adpCountries.addAll(data);

                    adpCountries.notifyDataSetChanged();

                }else if(SELECT_STATE.equals(method)) {
                    List<String> states = serviceResponse.getState();
                    adpStates.clear();
                    if (states != null) {
                        Collections.sort(states, Collator.getInstance());
                        adpStates.addAll(states);
                    }
                    adpStates.notifyDataSetChanged();
                }else if(SELECT_CITY.equals(method)) {
                    List<String> cities = serviceResponse.getCity();
                    adpCities.clear();
                    if (cities != null) {
                        Collections.sort(cities, Collator.getInstance());
                        adpCities.addAll(cities);
                    }
                    adpCities.notifyDataSetChanged();
                }
                else if ("VERIFY_EMAIL".equals(method)) {
                    if (serviceResponse.isValid()) {
                        showSignUpPage3();
                    } else {
                        Utility.makeNewToast(this, "This '" + email + "' already exists in system, please enter different email Id");
                    }
                }


            } else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                {
                    Utility.makeNewToast(this, getString(R.string.server_error));
                }
            }
        } else {
            Utility.makeNewToast(this, getString(R.string.server_error));
        }
    }


    private class SpinnerAdapter<T> extends ArrayAdapter<T>
    {
        private Activity activity;
        private String type;

        public SpinnerAdapter(Context context, int resource, String type) {
            super(context, resource);

            this.type = type;
            this.activity = (Activity) context;
        }


        @Override
        public View getDropDownView(final int position, View convertView, final ViewGroup parent) {

            if(convertView == null) {
                convertView = LayoutInflater.from(activity).inflate(R.layout.spinner_row_layout, null);
            }

            T item = getItem(position);
            Log.d("Item", item.toString());
            // convertView.setVisibility(View.GONE);

            final TextView textView = (TextView) convertView.findViewById(R.id.text_content);
            textView.setText(item.toString());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (SELECT_COUNTRY.equals(type)) {
                        hideSpinnerDropDown(spinnerCountry);
                        clearStates();
                        clearCity();
                        txtCountry.setText(textView.getText().toString());
                        getStatesForCountry(textView.getText().toString());
                    }else if(SELECT_STATE.equals(type))
                    {
                        hideSpinnerDropDown(spinnerState);
                        clearCity();
                        txtState.setText(textView.getText().toString());
                        getCitiesForState(textView.getText().toString());
                    }else if(SELECT_CITY.equals(type))
                    {
                        hideSpinnerDropDown(spinnerCity);
                        txtCity.setText(textView.getText().toString());
                    }

                }
            });

            return convertView;
        }
    }

    private void setDefaults()
    {
        clearCountries();
        clearStates();
        clearCity();
    }

    private void clearCountries() {
        List<String> vehicleType = new ArrayList<>();
        adpCountries.clear();
        txtCountry.setText("");
        adpCountries.notifyDataSetChanged();
    }

    private void clearStates() {

        List<String> vehicleType = new ArrayList<>();
        adpStates.clear();
        txtState.setText("");
        adpStates.notifyDataSetChanged();
    }

    private void clearCity()
    {
        txtCity.setText("");
        adpCities.clear();
        adpCities.notifyDataSetChanged();
    }

    public static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
