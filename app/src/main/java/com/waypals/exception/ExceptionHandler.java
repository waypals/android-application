package com.waypals.exception;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.waypals.utils.Utility;

public class ExceptionHandler {

    public static void handleException(Throwable e, Activity context) {
        if(e != null && e.getMessage() != null) {
            Log.e("Error", e.getMessage());
        }
//		Toast toast = Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//		toast.setGravity(Gravity.TOP, 0, 60);
//		toast.show();
    }

    public static void handleException(Throwable e) {
        // TODO: Log.e()  , check type of exception, if tokenexception redirect to login_screen
        if (e != null && e.getMessage() != null) {
            Log.e("Error", e.getMessage());
        }

        if (Looper.myLooper() == Looper.getMainLooper()) {
        }
    }

    public static void handlerException(Exception e, final Activity context) {
        try {
            e.printStackTrace();
            Handler handler = new Handler(Looper.getMainLooper());

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Utility.makeNewToast(context, "Network Error ! Please try again later.");
                    } catch (Exception e) {
                        ExceptionHandler.handleException(e, context);
                    }
                }
            }, 10);

        } catch (Exception e1) {
            Log.e("Error", e1.getMessage());
        }

    }

}
