package com.waypals.exception;

public class ParseException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -9146524957206342844L;

    public ParseException() {
        super();
    }

    public ParseException(String message, Throwable th) {
        super(message, th);
    }

    public ParseException(String message) {
        super(message);
    }
}
