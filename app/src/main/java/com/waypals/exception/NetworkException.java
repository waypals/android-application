package com.waypals.exception;

public class NetworkException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -9146524957206342844L;

    public NetworkException() {
        super();
    }

    public NetworkException(String message, Throwable th) {
        super(message, th);
    }

    public NetworkException(String message) {
        super(message);
    }
}
