package com.waypals;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.waypals.utils.CH_Constant;

/**
 * Created by shantanu on 13/1/15.
 */
public class FollowMe_Screen extends FragmentActivity implements FollowMeFollowingFragment.OnFragmentInteractionListener {

    Button invite, right_button;
    private Activity context;
    private String tag = "Follow me screen";
    private TextView viewButton, inviteButton;
    private ImageView back_btn;
    private LinearLayout layViewInvite;
    private boolean isShowingMap = false;

    private BroadcastReceiver fragmentHelper = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case CH_Constant.broadcast_fragment_view_map:
                    isShowingMap = true;
                    String to = intent.getStringExtra("for");
                    String userid = null;
                    String token = null;
                    String userName = null;
                    if (!to.equals("ALL")) {
                        userid = intent.getStringExtra("userId");
                        token = intent.getStringExtra("token");
                        userName = intent.getStringExtra("userName");
                    }
                    layViewInvite.setVisibility(View.GONE);

                    getSupportFragmentManager().beginTransaction().replace(R.id.followme_fragment_container, FollowMeViewOnMap.getInstance(getSupportFragmentManager(), to, userid, token, userName)).commit();
                    break;
                case "BACK":
                    isShowingMap = false;
                    layViewInvite.setVisibility(View.VISIBLE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.followme_fragment_container, new FollowMeFollowingFragment()).commit();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.followme_screen);
        context = this;

        IntentFilter filter = new IntentFilter();
        filter.addAction(CH_Constant.broadcast_fragment_view_map);
        filter.addAction("BACK");
        LocalBroadcastManager.getInstance(this).registerReceiver(fragmentHelper, filter);

        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBackPress();
            }
        });

        right_button = (Button) findViewById(R.id.right_btn);
        right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.followme_fragment_container, new FollowMeAddRequestFragment()).commit();
            }
        });
        viewButton = (TextView) findViewById(R.id.view_button);
        inviteButton = (TextView) findViewById(R.id.invite_button);


        layViewInvite = (LinearLayout) findViewById(R.id.button_area);
        layViewInvite.setVisibility(View.VISIBLE);

        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewButton.setBackgroundColor(getResources().getColor(R.color.follow_me_deactive));
                viewButton.setTextColor(getResources().getColor(R.color.follow_me_dactive_text_color));
                viewButton.requestLayout();

                inviteButton.setBackgroundColor(getResources().getColor(R.color.follow_me_active));
                inviteButton.setTextColor(getResources().getColor(R.color.white));
                inviteButton.requestLayout();
                getSupportFragmentManager().beginTransaction().replace(R.id.followme_fragment_container, new FollowMeFollowersFragment()).commit();
            }
        });


        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                inviteButton.setBackgroundColor(getResources().getColor(R.color.follow_me_deactive));
                inviteButton.setTextColor(getResources().getColor(R.color.follow_me_dactive_text_color));
                inviteButton.requestLayout();

                viewButton.setBackgroundColor(getResources().getColor(R.color.follow_me_active));
                viewButton.setTextColor(getResources().getColor(R.color.white));
                viewButton.requestLayout();
                getSupportFragmentManager().beginTransaction().replace(R.id.followme_fragment_container, new FollowMeFollowingFragment()).addToBackStack("view").commit();
            }
        });

        viewButton.performClick();
        Intent i = getIntent();
        if(i != null && i.getBooleanExtra("follow_me_view",false))
        {
           // viewButton.performClick();
        }
        else
        {
         //   inviteButton.performClick();
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fragmentHelper);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handleBackPress();
    }

    public void handleBackPress() {

        if (layViewInvite != null && layViewInvite.getVisibility() != View.VISIBLE) {
            Intent i = new Intent();
            i.setAction("BACK");
            LocalBroadcastManager.getInstance(this).sendBroadcast(i);
        } else {
            finish();
        }

    }
}
