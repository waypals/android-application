package com.waypals;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.waypals.feedImpl.AppController;
import com.waypals.feedImpl.FeedImageView;
import com.waypals.feedItems.Images;
import com.waypals.objects.IncidentAlerts;
import com.waypals.objects.MyImageView;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.wareninja.opensource.common.ObjectSerializer;

import java.util.ArrayList;

/**
 * Created by shantanu on 6/2/15.
 */
public class IncidentDetailScreen extends Activity {

    private EditText alert_type_edit, inci_date_edit, reporting_date_edit, tickt_edit, driver_edt, description_edt, damage_edt, location_edit, vehicle_edit;
    private FeedImageView v1, v2, v3, v4;
    private TableRow incidntType_row, ticketNo_row, damage_row;
    private ImageView left_btn;
    private TextView title;
    private Typeface tf;
    private GridLayout grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incident_detail_page_get);
        grid = (GridLayout) findViewById(R.id.grid);
        grid.setColumnCount(2);
        grid.setRowCount(2);

        String input = getIntent().getStringExtra("alert");
        IncidentAlerts alert = (IncidentAlerts) ObjectSerializer.deserialize(input);

        Integer number = getIntent().getIntExtra("number", 1);

        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);

        left_btn = (ImageView) findViewById(R.id.left_btn);
        left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setText("INCIDENT NO. " + (++number));
        title.setTypeface(tf, Typeface.NORMAL);

        alert_type_edit = (EditText) findViewById(R.id.alert_type_edit);
        inci_date_edit = (EditText) findViewById(R.id.inci_date_edit);
        reporting_date_edit = (EditText) findViewById(R.id.reporting_date_edit);
        tickt_edit = (EditText) findViewById(R.id.tickt_edit);
        driver_edt = (EditText) findViewById(R.id.driver_edt);
        description_edt = (EditText) findViewById(R.id.description_edt);
        damage_edt = (EditText) findViewById(R.id.damage_edt);
        location_edit = (EditText) findViewById(R.id.location_edit);
        vehicle_edit = (EditText) findViewById(R.id.vehicle_edit);


        incidntType_row = (TableRow) findViewById(R.id.incidntType_row);
        ticketNo_row = (TableRow) findViewById(R.id.ticketNo_row);
        damage_row = (TableRow) findViewById(R.id.damage_row);


        String EventType = alert.getEvent().getType().getEventType();

        Log.d("Alert Type -", EventType);

        if (EventType.equals("DEVICE_BREAKDOWN")) {
            EventType = "BREAKDOWN";
        }


        alert_type_edit.setText(EventType);
        inci_date_edit.setText(Utility.getUTCtoIST(alert.getEvent().getIncidentTime()));
        reporting_date_edit.setText(Utility.getUTCtoIST(alert.getEvent().getReportingTime()));
        driver_edt.setText(alert.getEvent().getDriver());
        description_edt.setText(alert.getEvent().getDescription());
        //location_edit.setText(alert.getEvent().getIncidentLocation());
        vehicle_edit.setText(alert.getEvent().getVehicleMetadata().getRegistrationNo());


        String alertType = alert.getEvent().getType().getEventType();
        if (alertType.contains("TRAFFIC")) {
            ticketNo_row.setVisibility(View.VISIBLE);
            tickt_edit.setText(alert.getEvent().getReferenceNumber());
        } else {
            ticketNo_row.setVisibility(View.GONE);
        }

        if (alertType.contains("THEFT")) {
            damage_row.setVisibility(View.GONE);
        } else {
            damage_edt.setText(alert.getEvent().getDamages());
        }

        Images[] images = alert.getEvent().getIncidentImages();
        if (images != null && images.length > 0) {
            ImageLoader imageLoader = new AppController(this).getImageLoader();
            ArrayList<String> url = new ArrayList<>();
            for (Images u : images) {
                if (u.getImageLink() != null) {
                    url.add(u.getImageLink());
                }
            }

            for (String u : url) {
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final MyImageView imageView = new MyImageView(this);
                imageView.setLayoutParams(params);
                imageView.setPadding(15, 5, 15, 15);
                imageView.getLayoutParams().height = Utility.getScreenSize(this) / 2 - 20;
                imageView.getLayoutParams().width = Utility.getScreenSize(this) / 2 - 20;
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);

                RequestCreator creator = Picasso.with(this).load(u);
                creator.into(imageView);
                creator.placeholder(R.drawable.no_image);
                creator.fetch(new Callback() {
                    @Override
                    public void onSuccess() {
                        grid.addView(imageView);
                    }

                    @Override
                    public void onError() {
                        grid.removeView(imageView);
                    }
                });
            }

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
