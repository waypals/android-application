package com.waypals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.gson.vo.SosNumberRequest;
import com.waypals.gson.vo.SosNumber;
import com.waypals.objects.EmergencyContact;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.lang.reflect.Method;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class EmergencyNoAddActivity extends Activity implements AsyncFinishInterface {

    private ImageView back_btn;
    private TextView nav_title_txt;
    private Typeface tf;
    private ImageView right_btn;
    private ApplicationPreferences appPref;

    private TextView code;
    private EditText number, name, relation;
    private ServiceResponse serviceResponse;
    private String method;
    private List<EmergencyContact> lstContacts;

    private boolean isEditMode = false;
    private int insertId = -1;
    private RelativeLayout lytCodeSpinner;
    private SpinnerAdapter<String> spinnerCodeAdapter;
    private Spinner spinnerCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_no_add);

        appPref = new ApplicationPreferences(this);

        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        nav_title_txt = (TextView) findViewById(R.id.nav_title_txt);
        nav_title_txt.setTypeface(tf);
        nav_title_txt.setText(getString(R.string.emergency));

        right_btn = (ImageView) findViewById(R.id.right_btn);
        right_btn.setVisibility(View.VISIBLE);
        right_btn.setImageResource(R.drawable.save_btn);


        lytCodeSpinner = (RelativeLayout) findViewById(R.id.custom_spinner);

        spinnerCode = (Spinner) lytCodeSpinner.findViewById(R.id.spinner_drop_down);
        spinnerCode.setPrompt("Select Code");

        String[] gender = {"+91", "+1"};

        spinnerCodeAdapter = new SpinnerAdapter<String>(this, R.layout.spinner_text_layout, gender);

        spinnerCode.setAdapter(spinnerCodeAdapter);

        code = (TextView) lytCodeSpinner.findViewById(R.id.text_spinner);
        code.setHint("Code");

        number = (EditText) findViewById(R.id.number);
        name = (EditText) findViewById(R.id.name);
        relation = (EditText) findViewById(R.id.relation);

        code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinnerCode.performClick();
            }
        });

        right_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String codeText = code.getText().toString();
                String numberText = number.getText().toString();
                String nameText = name.getText().toString();
                String relationText = relation.getText().toString();

                if(Utility.isStringNullEmpty(codeText))
                {
                    Utility.makeNewToast(EmergencyNoAddActivity.this, "Please select country code");
                    return;
                }

                if(Utility.isStringNullEmpty(numberText) || !Utility.isValidPhone(numberText))
                {
                    Utility.makeNewToast(EmergencyNoAddActivity.this, "Please enter valid number");
                    return;
                }

                if(Utility.isStringNullEmpty(nameText))
                {
                    Utility.makeNewToast(EmergencyNoAddActivity.this, "Please enter name");
                    return;
                }

                if(Utility.isStringNullEmpty(relationText))
                {
                    Utility.makeNewToast(EmergencyNoAddActivity.this, "Please enter email");
                    return;
                }

                EmergencyContact contact = new EmergencyContact();
                contact.setCountryCode(codeText);
                contact.setPhoneNumber(Long.parseLong(numberText));
                contact.setName(nameText);
                contact.setRelation(relationText);

                if(isEditMode) {
                    lstContacts.add(insertId, contact);
                }else {
                    lstContacts.add(contact);
                }
                // Call sos add contacts service
                addContacts(lstContacts);
            }
        });

        Intent i = getIntent();
        lstContacts = (List<EmergencyContact>) i.getSerializableExtra("contacts");

        int id = i.getIntExtra("editId", -1);

        if(id != -1)
        {
            if(lstContacts != null && id < lstContacts.size()) {
                EmergencyContact emergencyContact = null;

                for(EmergencyContact contact : lstContacts)
                {
                    if(contact.getId() == id)
                    {
                        emergencyContact = contact;
                        break;
                    }
                }

                if(emergencyContact != null) {
                    if(emergencyContact != null) {

                        isEditMode = true;
                        insertId = id;

                        code.setText(emergencyContact.getCountryCode());
                        number.setText(""+emergencyContact.getPhoneNumber());
                        name.setText(emergencyContact.getName());
                        relation.setText(emergencyContact.getRelation());
                    }

                    lstContacts.remove(emergencyContact);
                }
            }
        }

    }

    private  void addContacts(List<EmergencyContact> lstContacts)
    {
        try {
            if (lstContacts != null && !lstContacts.isEmpty()) {
                List<SosNumber> lstSosContacts = Utility.convertToSosContacts(lstContacts);
                SosNumberRequest sosContactsList = new SosNumberRequest();
                sosContactsList.setSosNumbers(lstSosContacts);
                Gson gson = new Gson();
                String input = gson.toJson(sosContactsList);
                Log.d("SosRequest", input);
                GenericAsyncService service = new GenericAsyncService(appPref, this, CH_Constant.SAVE_UPDATE_SOS, "UpdateSoS", input, "UpdateSoS", this, true);
                service.execute();
            }
        }catch (Exception ex)
        {
            ExceptionHandler.handlerException(ex, this);
        }
    }


    @Override
    public void finish(String response, String method) throws Exception {
        try {
            Gson gson = new Gson();
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
            this.method = method;
        } catch (Throwable e) {
            ExceptionHandler.handleException(e);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        try {

            if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                    serviceResponse.getResponse())) {
                String errorMessage = serviceResponse.getErrorCode();
                if (errorMessage != null
                        && errorMessage.equals("AUTHENTICATION_FAILED")) {
                    Toast toast = Toast.makeText(this,
                            CH_Constant.AUTHENTICATION_FAILED, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 60);
                    toast.show();
                    Intent i = new Intent(this, Login_Screen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                else {
                    Utility.makeNewToast(this, "Server not responding, please try after some time!!");
                    finish();
                }

                return;
            }

            if (serviceResponse != null && "SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {

                if(!isEditMode) {
                    Utility.makeNewToast(this, getString(R.string.sos_num_added));
                }
                else {
                    Utility.makeNewToast(this, getString(R.string.sos_num_updated));
                }
                finish();
            } else {
                Utility.makeNewToast(this, getString(R.string.err_try_again));
                finish();
            }
        } catch (Exception e) {
            ExceptionHandler.handlerException(e, this);
        }
    }

    private class SpinnerAdapter<T> extends ArrayAdapter<T> {
        private Activity activity;

        public SpinnerAdapter(Context context, int resource, T[] data) {
            super(context, resource, data);

            this.activity = (Activity) context;
        }


        @Override
        public View getDropDownView(final int position, View convertView, final ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(activity).inflate(R.layout.spinner_row_layout, null);
            }

            T item = getItem(position);

            final TextView textView = (TextView) convertView.findViewById(R.id.text_content);
            textView.setText(item.toString());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideSpinnerDropDown(spinnerCode);
                    code.setText(textView.getText().toString());
                }
            });

            return convertView;
        }
    }

    public static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
