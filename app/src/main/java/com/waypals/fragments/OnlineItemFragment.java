package com.waypals.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.documentcase.VolleySingleton;
import com.waypals.fragments.dummy.DummyContent;
import com.waypals.fragments.dummy.DummyContent.DummyItem;
import com.waypals.response.Product;
import com.waypals.response.ProductResponse;
import com.waypals.response.ProductWrapper;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.Utility;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class OnlineItemFragment extends Fragment implements AsyncFinishInterface {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private ApplicationPreferences appPref;
    private ProductResponse serviceResponse;
    private RecyclerView recyclerView;
    private ImageLoader mImageLoader;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OnlineItemFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static OnlineItemFragment newInstance(int columnCount) {
        OnlineItemFragment fragment = new OnlineItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImageLoader = VolleySingleton.getInstance(getContext()).getImageLoader();

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onlineitem_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //recyclerView.setAdapter(new OnlineItemRecyclerViewAdapter(DummyContent.ITEMS, mListener));

            getItems();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getItems();
    }

    private void getItems(){
        // String input = gson.toJson(paymentDto);
        GenericAsyncService getTnxIdService = new GenericAsyncService(appPref, getActivity(), CH_Constant.SERVER+CH_Constant.PRODUCTS_IN_STORE, "", "",
                "", this, true);
        getTnxIdService.execute();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void finish(String response, String method) throws Exception {
        if (!Utility.isStringNullEmpty(response)) {
            Gson gson = new Gson();
            Log.d("Raw Response", response);
            serviceResponse = gson.fromJson(response, ProductResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {

        if (serviceResponse != null) {
            if (CH_Constant.SUCCESS.equalsIgnoreCase(serviceResponse.getResponse())) {

                Log.d("Response", ""+serviceResponse.getProductWrappers().size());
                recyclerView.setAdapter(new OnlineItemRecyclerViewAdapter(serviceResponse.getProductWrappers(), mListener, mImageLoader));
            } else {

            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onAddToCartTap(ProductWrapper item);
    }
}
