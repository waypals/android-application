package com.waypals.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.waypals.R;
import com.waypals.fragments.dummy.DummyContent;
import com.waypals.fragments.dummy.DummyContent.DummyItem;
import com.waypals.gson.vo.GeoFenceVO;
import com.waypals.gson.vo.ListGeoFenceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.rest.service.VehicleServices;
import com.waypals.utils.ApplicationPreferences;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class GeofenceFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private List<GeoFenceVO> geoFences;
    private ApplicationPreferences appPref;
    private RecyclerView recyclerView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GeofenceFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static GeofenceFragment newInstance(int columnCount) {
        GeofenceFragment fragment = new GeofenceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_geofence_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
           // recyclerView.setAdapter(new GeofenceRecyclerViewAdapter(DummyContent.ITEMS, mListener));
            getGeofences();

        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getGeofences();

    }

    private void getGeofences(){
        try {
            appPref = new ApplicationPreferences(this.getActivity());
            VehicleServices vs = new VehicleServices();
            vs.getListOfGeoFence(appPref, this.getContext(), new AsyncFinishInterface() {
                @Override
                public void finish(String response, String method) throws Exception {
                    String listOfGeoFences = response;
                    Log.d("Reponse", response);
                    if (listOfGeoFences != null) {
                        Gson gson = new Gson();
                        ListGeoFenceResponse listOfGeofence = gson.fromJson(
                                listOfGeoFences, ListGeoFenceResponse.class);
                        if (listOfGeofence != null
                                && listOfGeofence.getResponse().equals("SUCCESS")) {
                            geoFences = listOfGeofence.getGeoFences();

                            if (geoFences != null && !geoFences.isEmpty()) {
                                recyclerView.setAdapter(new GeofenceRecyclerViewAdapter(geoFences, mListener));
                            }
                        }
                    }
                }

                @Override
                public void onPostExecute(Void Result) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(GeoFenceVO item);
    }
}
