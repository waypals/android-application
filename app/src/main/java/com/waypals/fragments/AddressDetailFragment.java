package com.waypals.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.waypals.R;
import com.waypals.objects.Address;
import com.waypals.objects.Phone;
import com.waypals.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnAddressDetailFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddressDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddressDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private EditText name, email, phone, city, state, pincode, add, country, landmark;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnAddressDetailFragmentInteractionListener mListener;
    private float amount;

    public AddressDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddressDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddressDetailFragment newInstance(String param1, String param2) {
        AddressDetailFragment fragment = new AddressDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        amount = getActivity().getIntent().getFloatExtra("Amount", 0.0f);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_address_detail, container, false);

        name = (EditText) view.findViewById(R.id.name_add);
        email = (EditText) view.findViewById(R.id.email_add);
        state = (EditText) view.findViewById(R.id.state_add);
        country = (EditText) view.findViewById(R.id.country_add);
        phone = (EditText) view.findViewById(R.id.phone_add);
        add = (EditText) view.findViewById(R.id.address_add);
        pincode = (EditText) view.findViewById(R.id.pincode_add);
        city = (EditText) view.findViewById(R.id.city_add);
        landmark = (EditText) view.findViewById(R.id.landmark_add);

        TextView amountPayble = (TextView) view.findViewById(R.id.amount_payable);
        amountPayble.setText("INR "+amount);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onAddressDetailFragmentInteraction(null);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddressDetailFragmentInteractionListener) {
            mListener = (OnAddressDetailFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddressDetailFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public Address getAddress(){

        Address address = new Address();

        address.setCity(city.getText().toString());
        address.setAddressLine(add.getText().toString());
        address.setProvince(state.getText().toString());
        address.setZip(pincode.getText().toString());
        address.setCountry(country.getText().toString());


        return address;
    }

    public String getName(){
        return name.getText().toString();
    }

    public String getEmail(){
        return email.getText().toString();
    }

    public Phone getPhone(){
        Phone ph = new Phone();
        ph.setPhoneNo(phone.getText().toString());
        ph.setCountryCode("91");
        return ph;
    }

    public boolean isValid() {

        if(name.getText() == null || name.getText().toString().isEmpty()){

            Utility.makeNewToast(getActivity(),"Please enter your name");
            return false;
        }

        if(add.getText() == null || add.getText().toString().isEmpty()){

            Utility.makeNewToast(getActivity(),"Please enter your address");
            return false;
        }

        if(city.getText() == null || city.getText().toString().isEmpty()){

            Utility.makeNewToast(getActivity(),"Please enter your city");
            return false;
        }

        if(country.getText() == null || country.getText().toString().isEmpty()){

            Utility.makeNewToast(getActivity(),"Please enter your country");
            return false;
        }

        if(pincode.getText() == null || pincode.getText().toString().isEmpty() || !Utility.isValidPinCode(pincode.getText().toString())){

            Utility.makeNewToast(getActivity(),"Please enter your valid pin code");
            return false;
        }

        if(state.getText() == null || state.getText().toString().isEmpty()){

            Utility.makeNewToast(getActivity(),"Please enter your state");
            return false;
        }

        if(email.getText() == null || email.getText().toString().isEmpty() || !Utility.isValidEmailId(email.getText().toString())){

            Utility.makeNewToast(getActivity(),"Please enter your valid email");
            return false;
        }

        if(phone.getText() == null || phone.getText().toString().isEmpty() || !Utility.isValidPhone(phone.getText().toString())){

            Utility.makeNewToast(getActivity(),"Please enter your phone no");
            return false;
        }

        return true;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAddressDetailFragmentInteractionListener {
        // TODO: Update argument type and name
        void onAddressDetailFragmentInteraction(Address address);
    }
}
