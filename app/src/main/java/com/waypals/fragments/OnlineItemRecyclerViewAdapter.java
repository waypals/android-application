package com.waypals.fragments;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.waypals.R;
import com.waypals.fragments.dummy.DummyContent.DummyItem;
import com.waypals.response.ProductWrapper;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnlineItemFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class OnlineItemRecyclerViewAdapter extends RecyclerView.Adapter<OnlineItemRecyclerViewAdapter.ViewHolder> {

    private final List<ProductWrapper> mValues;
    private final OnlineItemFragment.OnListFragmentInteractionListener mListener;
    private ImageLoader mImageLoader;

    public OnlineItemRecyclerViewAdapter(List<ProductWrapper> items, OnlineItemFragment.OnListFragmentInteractionListener listener, ImageLoader imageLoader) {
        mValues = items;
        mListener = listener;
        mImageLoader = imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_onlineitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getProduct().getProductCode());
        //Log.d("Name", ""+mValues.get(position).getProduct().getProductMetaData().getName());
//        holder.mContentView.setText(mValues.get(position).getProductMetaData().getName());
        if (mValues.get(position).getProduct().getProductMetaData() != null){
            holder.mIdView.setText(mValues.get(position).getProduct().getProductMetaData().getName());
            holder.mContentView.setText("INR "+mValues.get(position).getProduct().getProductMetaData().getPrice());

            if(mValues.get(position).getProduct().getProductMetaData().getImages().size() >0) {
                holder.mImageView.setImageUrl(mValues.get(position).getProduct().getProductMetaData().getImages().get(0).getImageLink(), mImageLoader);
            }
        }
        holder.mAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onAddToCartTap(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final NetworkImageView mImageView;
        public final Button mAddToCart;
        public ProductWrapper mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            mImageView = (NetworkImageView) view.findViewById(R.id.product_image);
            mAddToCart = (Button) view.findViewById(R.id.add_to_cart_button);
            mImageView.setDrawingCacheEnabled(true);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
