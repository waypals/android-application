package com.waypals.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.waypals.R;
import com.waypals.adapters.SpinnerAdapter;
import com.waypals.fragments.dummy.DummyContent.DummyItem;
import com.waypals.response.Product;
import com.waypals.response.ProductWrapper;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link CartItemFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class CartItemRecyclerViewAdapter extends RecyclerView.Adapter<CartItemRecyclerViewAdapter.ViewHolder> {

    private final List<ProductWrapper> mValues;
    private final CartItemFragment.OnListFragmentInteractionListener mListener;
    private final Context mContext;
    private final ImageLoader mImageLoader;

    public CartItemRecyclerViewAdapter(List<ProductWrapper> items, CartItemFragment.OnListFragmentInteractionListener listener, Context context, ImageLoader imageLoader) {
        mValues = items;
        mListener = listener;
        mContext = context;
        mImageLoader = imageLoader;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_cartitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getProduct().getProductMetaData().getName());
        holder.mContentView.setText("INR "+mValues.get(position).getProduct().getProductMetaData().getPrice());

        if(mValues.get(position).getProduct().getProductMetaData().getImages().size() >0) {
            holder.mImageView.setImageUrl(mValues.get(position).getProduct().getProductMetaData().getImages().get(0).getImageLink(), mImageLoader);
        }
//        String[] codes = new String[holder.mItem.getStockQty()];
//
//        for(int i= 1; i<holder.mItem.getStockQty(); i++){
//            codes[i] = "" + (i+1);
//        }

        holder.mNumberPicker.setMaxValue(holder.mItem.getStockQty());
        holder.mNumberPicker.setMinValue(1);

      //  holder.mNumberPicker.setWrapSelectorWheel(true);
        holder.mNumberPicker.setValue(holder.mItem.getItemInCart());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    //mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

        holder.mNumberPicker.setOnValueChangedListener(new com.shawnlin.numberpicker.NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(com.shawnlin.numberpicker.NumberPicker picker, int oldVal, int newVal) {
                holder.mItem.setItemsInCart(newVal);
                mListener.onListFragmentInteraction(mValues);
            }
        });

        holder.mDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mValues.remove(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, android.R.style.Theme_Holo_Light_Dialog));

                builder.setMessage("Do you want to delete this item?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                mValues.remove(position);
                                mListener.onListFragmentInteraction(mValues);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return mValues != null ? mValues.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final NetworkImageView mImageView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final LinearLayout mDeleteItem;
        public final com.shawnlin.numberpicker.NumberPicker mNumberPicker;

        public ProductWrapper mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (NetworkImageView) view.findViewById(R.id.product_image);
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            mNumberPicker = (com.shawnlin.numberpicker.NumberPicker) view.findViewById(R.id.number_picker);
            mDeleteItem = (LinearLayout) view.findViewById(R.id.delete_item);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
