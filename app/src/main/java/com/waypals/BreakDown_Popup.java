package com.waypals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.waypals.utils.CH_Constant;

public class BreakDown_Popup extends Activity {
    static String tag = "Break down call";
    Button yes_btn;
    Button no_btn;
    TextView call_text;
    String dstn_number;
    private Typeface tf;

    public static void SendAlert(Context c, String title, String msg) {
        AlertDialog.Builder ad = new AlertDialog.Builder(c);
        ad.setTitle(title);
        ad.setMessage(msg);
        ad.setCancelable(false);
        ad.setNeutralButton("Ok", null);
        ad.create();
        ad.show();
    }

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.break_down_call);
        tf = Typeface.createFromAsset(getAssets(), CH_Constant.Font_Typface_Path);
        call_text = (TextView) findViewById(R.id.break_down_called_number);
        yes_btn = (Button) findViewById(R.id.break_down_yes_btn);
        no_btn = (Button) findViewById(R.id.break_down_no_btn);

        try {
            Intent i = getIntent();
            String action = i.getExtras().getString("action");
            Log.i(tag, "action: " + action);
            if (action.equals("1")) {
                dstn_number = "+911140014001";
                call_text.setText(getString(R.string.coming_soon));
                yes_btn.setVisibility(View.GONE);
                no_btn.setText("Ok");
            } else {
                dstn_number = "+918888888888";
                yes_btn.setVisibility(View.VISIBLE);
                call_text.setText("Call " + dstn_number);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            dstn_number = "+911140014001";
        }

        yes_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                createCall(dstn_number);
                finish();
            }
        });

        call_text.setTypeface(tf);
        //
        no_btn.setTypeface(tf);
        yes_btn.setTypeface(tf);

        no_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });

//
    }

    public void createCall(String number) {
        try {
            Log.i(tag, "inititate call to " + number);
            String url = "tel:" + number;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // ActivityCompat#requestPermissions
                return;
            }
            startActivity(intent);

        } catch (Exception ex) {
            ex.printStackTrace();
            SendAlert(BreakDown_Popup.this, "Error", "Could not create call !");
            return;
        }
    }

}
