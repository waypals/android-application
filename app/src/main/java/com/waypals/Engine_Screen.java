package com.waypals;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.waypals.exception.ExceptionHandler;
import com.waypals.feedItems.RouteCoordinates;
import com.waypals.objects.VehicleTripOverview;
import com.waypals.request.ImmobiliseVehicleRequest;
import com.waypals.response.ServiceResponse;
import com.waypals.rest.service.AsyncFinishInterface;
import com.waypals.services.GenericAsyncService;
import com.waypals.utils.ApplicationPreferences;
import com.waypals.utils.CH_Constant;
import com.waypals.utils.RouteComparator;
import com.waypals.utils.Utility;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class Engine_Screen extends NavigationDrawer implements AsyncFinishInterface {
    static Typeface tf;
    String tag = "Engine Screen";
    ImageView back_btn;
    TextView engine_Text;
    TextView title, engineOn, engineOff;
    Button btnOn, btnOff;
    ImageView engineImage;
    private ApplicationPreferences appPref;
    private AlertDialog.Builder alert;
    private boolean alertReady;
    private ServiceResponse serviceResponse;
    private String method;
    private boolean isTimerRunning;
    private int elapsedTime = 30;

    private TextView retryText;

    private Timer timer;

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;

        mMyBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (appPref.getEngineState()) {
                            engine_Text.setText("Vehicle On");
                            engineImage.setImageResource(R.drawable.engine_on);
                        } else {
                            engine_Text.setText("Vehicle Off");
                            engineImage.setImageResource(R.drawable.engine_off);
                        }

                        if(timer != null) {
                            timer.cancel();
                            retryText.setText("");
                            isTimerRunning = false;
                            btnOn.setEnabled(true);
                            btnOff.setEnabled(true);
                            elapsedTime = 30;
                        }
                    }


                });
            }
        };
        try {

            LocalBroadcastManager.getInstance(this).registerReceiver(mMyBroadcastReceiver,
                    new IntentFilter("engine_state_changed"));

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVisible = false;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMyBroadcastReceiver);

    }

    public static boolean isVisible = false;

    private BroadcastReceiver mMyBroadcastReceiver;

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            if(msg.what <= 0){
                retryText.setText(""); //
            }else{
                retryText.setText("Retry in " + msg.what + " s"); //
            }
        }
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.engine_screen);

        tf = Typeface.createFromAsset(getAssets(),
                CH_Constant.Font_Typface_Path);

        appPref = new ApplicationPreferences(this);
        title = (TextView) findViewById(R.id.nav_title_txt);
        title.setTypeface(tf);
        title.setText("ENGINE");

        back_btn = (ImageView) findViewById(R.id.left_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnOn = (Button) findViewById(R.id.btn_vehicle_on);
        btnOff = (Button) findViewById(R.id.btn_vehicle_off);

        engine_Text = (TextView) findViewById(R.id.engine_text);

        final String number = appPref.getCurrentDeviceNumber();

        retryText = (TextView) findViewById(R.id.retry_text);

        btnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isTimerRunning) {
                    if (number != null && !number.isEmpty()) {
                        Utility.makeNewToast(Engine_Screen.this, "Turning Vehicle on");
                        // String text = "wpl art setdigout 1";
                        // Utility.SendSMS(number, text, context);
                        // appPref.setEngineState(true);
                        makeImmobilisationRequest(false);
                    } else {
                        Utility.makeNewToast(Engine_Screen.this, "No Info");
                    }
                }
            }
        });

        btnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isTimerRunning) {
                    if (number != null && !number.isEmpty()) {
                        Utility.makeNewToast(Engine_Screen.this, "Turning Vehicle off");
                        String text = "wpl art setdigout 0";
                        // Utility.SendSMS(number, text, context);
                        //   appPref.setEngineState(false);
                        makeImmobilisationRequest(true);
                    } else {
                        Utility.makeNewToast(Engine_Screen.this, "No Info");
                    }
                }
            }
        });

        engineImage = (ImageView) findViewById(R.id.imageView);

        if (appPref.getEngineState()) {
            engine_Text.setText("Vehicle On");
            engineImage.setImageResource(R.drawable.engine_on);
        }else {
            engine_Text.setText("Vehicle Off");
            engineImage.setImageResource(R.drawable.engine_off);
        }

        takePermission();

    }

    protected void startTimer() {
        isTimerRunning = true;
        //btnSendCode.setEnabled(false);
        //TaskScheduler timer = new TaskScheduler();
        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                elapsedTime -= 1; //decreases every sec
                mHandler.obtainMessage(elapsedTime).sendToTarget();
                if (elapsedTime <= 0){
                    timer.cancel();
                    isTimerRunning = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnOn.setEnabled(true);
                            btnOff.setEnabled(true);
                            //btnSendCode.setEnabled(true);
                            //btnSendCode.setText("Send Code");
                        }
                    });
                    elapsedTime = 30;

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnOn.setEnabled(false);
                            btnOff.setEnabled(false);
                        }
                    });

                }
            }
        }, 0, 1000);
    }

    private void makeImmobilisationRequest(boolean isForOn){
        try {

            ImmobiliseVehicleRequest immbReq = new ImmobiliseVehicleRequest();
            immbReq.setVehicleId(String.valueOf(appPref.getVehicle_Selected()));
            immbReq.setImmobilise(isForOn);

            Gson gson = new Gson();

            String str = gson.toJson(immbReq, ImmobiliseVehicleRequest.class);

            String method = "engine_on";
            if(isForOn){
                method = "engine_off";
            }

            GenericAsyncService service = new GenericAsyncService(appPref, context, CH_Constant.IMMOBILIZE_SERVICE, method, str, tag, this, true);
            service.execute();
        } catch (Exception e) {
            ExceptionHandler.handleException(e, context);
        }
    }

    private void takePermission(){
        alert = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(this);
        edittext.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        alert.setMessage("Please enter your password");
        alert.setTitle("Authentication Required");

        alert.setView(edittext);
        alert.setCancelable(false);

        alert.setPositiveButton("Authenticate", null);


        alert.setNegativeButton("Cancel", null);

        alertReady = false;
        final AlertDialog alertDialog = alert.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if(!alertReady) {
                    Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            // TODO Do something

                            Editable textValue = edittext.getText();
                            if (textValue != null) {
                                if (textValue.toString().equals(appPref.getUserPassword())) {
                                    alertDialog.dismiss();
                                } else {
                                    Utility.makeNewToast(Engine_Screen.this, "Empty/Invalid password");
                                }
                            }
                        }



                    });
                    Button c = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    c.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });

                }else{
                    alertReady = true;
                }
            }
        });
        alertDialog.show();
    }

    @Override
    protected void setImmobilizer() {

    }

    @Override
    public void finish(String response, String method) throws Exception {
        if ("engine_on".equals(method) || "engine_off".equals(method)) {
            Log.d("Response", response);
            Gson gson = new Gson();
            this.method = method;
            serviceResponse = gson.fromJson(response, ServiceResponse.class);
        }
    }

    @Override
    public void onPostExecute(Void Result) {
        try {
            if (serviceResponse != null) {
                if ("SUCCESS".equalsIgnoreCase(serviceResponse.getResponse())) {
                    if ("engine_on".equals(this.method)) {
                        Utility.makeNewToast(Engine_Screen.this, "Turning Vehicle on");
                        //  appPref.setEngineState(true);
                    } else {
                        Utility.makeNewToast(Engine_Screen.this, "Turning Vehicle off");
                        //  appPref.setEngineState(false);
                    }

                    startTimer();

                    if (appPref.getEngineState()) {
                        //    engine_Text.setText("Vehicle On");
                        //    engineImage.setImageResource(R.drawable.engine_on);
                    } else {
                        //    engine_Text.setText("Vehicle Off");
                        //    engineImage.setImageResource(R.drawable.engine_off);
                    }
                } else if (serviceResponse != null && "ERROR".equalsIgnoreCase(
                        serviceResponse.getResponse())) {
                    Utility.makeNewToast(this, "Server is'nt responding, please try after some time!!");
                }
            }
        } catch (Exception ex) {
            Crashlytics.log(ex.getMessage());
            // ExceptionHandler.handleException(ex, context);
        }
    }
}
