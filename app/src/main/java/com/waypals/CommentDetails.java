package com.waypals;


import android.os.Parcel;
import android.os.Parcelable;

public class CommentDetails implements Parcelable {

    public static final Parcelable.Creator<CommentDetails> CREATOR
            = new Parcelable.Creator<CommentDetails>() {
        public CommentDetails createFromParcel(Parcel in) {
            return new CommentDetails(in);
        }

        public CommentDetails[] newArray(int size) {
            return new CommentDetails[size];
        }
    };
    private String commentedBy;
    private String comment;
    private String timeStamp;

    private CommentDetails(Parcel in) {
        commentedBy = in.readString();
        comment = in.readString();
        timeStamp = in.readString();
    }

    public CommentDetails() {
        // TODO Auto-generated constructor stub
    }

    public String getCommentedBy() {
        return commentedBy;
    }

    public void setCommentedBy(String commentedBy) {
        this.commentedBy = commentedBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(commentedBy);
        dest.writeString(comment);
        dest.writeString(timeStamp);

    }


}
